export const colors = {
  kpfColor: "#cb0202",
  green: "#2d8f89",
  darkGreen: "#2d7a8c",
  white: "#ffffff",
  black: "#191818",
  gray: "#757575",
  lightGray: "#f8f8f8",
  warmGray: "#d0d0d0",
  mediumGray: "#a3a3a3",
  ironGray: "#6c6c6c",
  coldGray: "#f6f6f6",
  darkGray: "#383838",
  steelGray: "#a4a4a4",
  darkeMagenta: "#c00063",
  magenta: "#e20074",
  lightMagenta: "#f3007d"
};
