import { colors as base } from "./base";

const colors = {
  header: {
    default: {
      background: base.lightGray,
      color: base.black
    }
  }
};

export default colors;
