export const Token = () => {
  if (sessionStorage.length != 0) {
    const tokenkey = JSON.parse(Base64.decode(sessionStorage.getItem("token")));

    return "Bearer" + " " + tokenkey;
  }
};
