// For token retrive
let token;
if (sessionStorage.length == 1) {
  let ddd = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  token = ddd.TOKEN_KEY;
}
export { token };

// Dynamic authrisation
let LoginAuth;
let Auth;

if (process.env.NODE_ENV === "development") {
  LoginAuth = {
    "Content-Type": "Application/json"
    // "Access-Control-Allow-Credentials": true,
    // "Access-Control-Allow-Origin": "*",
    // "Access-Control-Allow-Methods": "POST, GET, OPTIONS, DELETE, PUT",
    // "Access-Control-Allow-Headers": "*"
  };
  Auth = {
    "Content-Type": "Application/json",
    Authorization: `Bearer ${token}`
  };
}

if (process.env.NODE_ENV === "production") {
  // Static Token For login
  LoginAuth = {
    "Content-Type": "Application/json",
    Authorization: `Bearer ae6e983c-f98b-338f-9fbf-6b953d37c419`
  };
  // Recieved Token after Authenticate User
  Auth = {
    "Content-Type": "Application/json",
    Authorization: `Bearer ${token}`
  };
}

export { LoginAuth, Auth };

export const CorsOriginAll = {
  Accept: "Application/json",
  "Content-Type": "Application/json",
  "Access-Control-Allow-Credentials": true,
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "POST, GET, OPTIONS, DELETE, PUT",
  "Access-Control-Allow-Headers": "*"
};

export const basicAuth = {
  "Content-Type": "Application/json"
};

export const CorsOriginBasic = {
  Accept: "Application/json",
  "Content-Type": "Application/json"
};
