import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from "react-router-dom";
import Login from "../Containers/Login";
import Profile from "../Containers/Profile";
import ChangePassword from "../Containers/ChangePassword";
import BlankPage from "../Containers/BlankPage";
import Dashboard from "../Containers/Dashboard";
import Products from "../Containers/Products";
import NewProductList from "../Containers/Products/NewProductList";
import LoyaltySchemesHistory from "./../Containers/LoyaltyPrograms/SchemesHistory";
import LoyalityPlan from "../Containers/LoyaltyPrograms/LoyalityPlan";
import TrackDemand from "./../Containers/Queries/TrackDemand";
import BasAbhiSubscribers from "./../Containers/Queries/BasAbhiSubscribers";
import SaudaSubscribers from "./../Containers/Queries/SaudaSubscribers";
import MarketingSchemesHistory from "./../Containers/MarketingSchemes/SchemesHistory";
import LaunchSchemes from "./../Containers/MarketingSchemes/LaunchSchemes";
import PushSchemes from "./../Containers/MarketingSchemes/PushSchemes";
import ApprovedDealer from "../Containers/AuthDealer/ApprovedDealer";
import ApprovalRequest from "../Containers/AuthDealer/ApprovalRequest";
import CustomerCare from "../Containers/Support/CustomerCare";
import RaiseRequest from "../Containers/Support/RaiseRequest";
import RaisedIssue from "../Containers/Support/RaisedIssue";
import LocationHierarchy from "../Containers/Administration/LocationHierarchy";
import SalesHierarchy from "../Containers/Administration/SalesHierarchy";
import LoyaltyMember from "../Containers/LoyaltyPrograms/LoyaltyMember";
import CreateNewUser from "../Containers/Administration/CreateNewUser";
import ListUsers from "../Containers/Administration/ListUsers";
import LoyaltyPoints from "../Containers/Administration/LoyaltyPoints";
import LoyaltyRewards from "../Containers/Administration/Master/LoyaltyRewards";
import MarketingPrizes from "../Containers/Administration/Master/MarketingPrizes";
import SellerLevels from "../Containers/Administration/Master/SellerLevels";
import ConsumerLevels from "../Containers/Administration/Master/ConsumerLevels";
import ReceivedTask from "../Containers/Task/ReceivedTask";
import SentTask from "../Containers/Task/SentTask";
import Alert from "../Containers/Alert";
import Notification from "../Containers/Notification";
import LoyaltyMemberType from "../Containers/Administration/Master/LoyaltyMemberType";
import MarketingMemberType from "../Containers/Administration/Master/MarketingMemberType";
import BasAbhiUsers from "./../Containers/Administration/Master/BasAbhiUsers";
import SaudaUsers from "./../Containers/Administration/Master/SaudaUsers";

export const LoginRoutes = [
  {
    id: "login1",
    path: "/",
    component: Login
  },
  {
    id: "login2",
    path: "/login",
    component: Login
  }
];

export const LoginScreenRoutes = LoginRoutes.map(({ id, path, component }) => (
  <Route key={id} exact path={path} component={component} />
));

// All components Routes Below
export const RouteComponents = [
  {
    id: 1,
    path: "/blankPage",
    component: BlankPage
  },

  {
    id: 2,
    path: "/dashboard",
    component: Dashboard
  },
  {
    id: 3,
    path: "/profile",
    component: Profile
  },
  {
    id: 4,
    path: "/changePassword",
    component: ChangePassword
  },
  {
    id: 5,
    path: "/products/listed-products",
    component: Products
  },
  {
    id: 6,
    path: "/products/new-product-list",
    component: NewProductList
  },
  {
    id: 7,
    path: "/loyalty-plan/current-plans/",
    component: LoyalityPlan
  },
  {
    id: 8,
    path: "/loyalty-plan/manage-plans/",
    component: LoyaltySchemesHistory
  },
  {
    id: 9,
    path: "/loyalty-plan/loyalty-member/",
    component: LoyaltyMember
  },
  {
    id: 10,
    path: "/queries/track-demand/",
    component: TrackDemand
  },
  {
    id: 11,
    path: "/marketing-schemes/manage-schemes",
    component: MarketingSchemesHistory
  },
  {
    id: 12,
    path: "/marketing-schemes/launch-schemes/",
    component: LaunchSchemes
  },
  {
    id: 13,
    path: "/marketing-schemes/push-schemes/",
    component: PushSchemes
  },
  {
    id: 14,
    path: "/auth-dealer/approved-dealer/",
    component: ApprovedDealer
  },
  {
    id: 15,
    path: "/auth-dealer/approval-request/",
    component: ApprovalRequest
  },
  {
    id: 16,
    path: "/support/customer-care/",
    component: CustomerCare
  },
  {
    id: 17,
    path: "/support/raise-request/",
    component: RaiseRequest
  },
  {
    id: 31,
    path: "/support/raised-issue",
    component: RaisedIssue
  },
  {
    id: 18,
    path: "/administration/location-hierarchy/",
    component: LocationHierarchy
  },
  {
    id: 19,
    path: "/administration/sales-hierarchy/",
    component: SalesHierarchy
  },
  {
    id: 20,
    path: "/administration/create-new-user/",
    component: CreateNewUser
  },
  {
    id: 21,
    path: "/administration/list-users/",
    component: ListUsers
  },
  {
    id: 22,
    path: "/administration/loyalty-points/",
    component: LoyaltyPoints
  },
  {
    id: 23,
    path: "/administration/master/loyalty-rewards",
    component: LoyaltyRewards
  },
  {
    id: 24,
    path: "/administration/master/seller-levels",
    component: SellerLevels
  },
  {
    id: 25,
    path: "/administration/master/loyalty-member-level",
    component: ConsumerLevels
  },
  {
    id: 26,
    path: "/administration/master/marketing-prizes",
    component: MarketingPrizes
  },
  {
    id: 27,
    path: "/received-task",
    component: ReceivedTask
  },
  {
    id: 28,
    path: "/sent-task",
    component: SentTask
  },
  {
    id: 29,
    path: "/user-alert",
    component: Alert
  },
  {
    id: 30,
    path: "/user-notification",
    component: Notification
  },
  {
    id: 32,
    path: "/administration/master/loyalty-member-type",
    component: LoyaltyMemberType
  },
  {
    id: 33,
    path: "/administration/master/marketing-member-type",
    component: MarketingMemberType
  },
  {
    id: 34,
    path: "/administration/master/bas-abhi-users",
    component: BasAbhiUsers
  },
  {
    id: 35,
    path: "/administration/master/sauda-users",
    component: SaudaUsers
  },
  {
    id: 37,
    path: "/queries/basabhi-subscribers",
    component: BasAbhiSubscribers
  },
  {
    id: 38,
    path: "/queries/sauda-subscribers",
    component: SaudaSubscribers
  }
];

export const ScreenRoutes = RouteComponents.map(({ id, path, component }) => (
  <Route key={id} exact path={path} component={component} />
));
