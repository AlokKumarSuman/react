let BASE_URL = "http://localhost:3000"; // default URL

//Check the environment...
if (process.env.NODE_ENV === "development") {
  BASE_URL = "http://dev.project.com";
}

if (process.env.NODE_ENV === "production") {
  BASE_URL = "http://prod.project.com";
}
// set "REACT_APP_ENV=development" && npm start
export { BASE_URL };

// const express = require("express");
// const path = require("path");
// const app = express();

// if (process.env.NODE_ENV === "production") {
//   BASE_URL = "http://dev.project.com";
//   app.use(express.static(path.join(__dirname, "/build")));

//   app.get("*", (req, res) => {
//     res.sendFile(path.join(__dirname + "/build/index.html"));
//   });
// }

// const PORT = process.env.PORT || 3001;

// app.listen(PORT, () => {
//   console.log("port", PORT);
// });
