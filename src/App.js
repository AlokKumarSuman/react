import React, { Component } from "react";
// import { BASE_URL } from "./environment.js";
import { applicationMode } from "./Containers/Config";
import { LoginScreenRoutes, ScreenRoutes } from "./ScreenRoutes";

import { strings } from "./Localization";

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from "react-router-dom";
import Layout from "./Layout";

console.log(applicationMode);

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      language: "en",

      loggedIn: false,
      logoutWarn: ""
    };

    this.events = [
      "load",
      "mousemove",
      "mousedown",
      "click",
      "scroll",
      "keypress",
      "mousewheel"
    ];

    this.warn = this.warn.bind(this);
    this.logout = this.logout.bind(this);
    this.resetTimeout = this.resetTimeout.bind(this);

    for (var i in this.events) {
      window.addEventListener(this.events[i], this.resetTimeout);
    }

    this.setTimeout();
  }

  clearTimeout() {
    if (this.warnTimeout) clearTimeout(this.warnTimeout);
    if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
  }

  setTimeout() {
    this.warnTimeout = setTimeout(this.warn, 300 * 1000);
    this.logoutTimeout = setTimeout(this.logout, 600 * 1000);
  }

  resetTimeout() {
    this.clearTimeout();
    this.setTimeout();
  }

  warn() {
    console.log("You will be logout in 1 minute");
  }

  logout() {
    sessionStorage.clear();
    this.setState({ loggedIn: false });
  }

  destroy() {
    this.clearTimeout();
    for (var i in this.events) {
      window.removeEventListener(this.events[i], this.resetTimeout);
    }
  }

  componentDidMount() {
    if (sessionStorage.length == 0) {
      this.setState({
        loggedIn: false
      });
    } else {
      const alldetail = JSON.parse(
        Base64.decode(sessionStorage.getItem("data"))
      );
      const token = alldetail.TOKEN_KEY;
      if (token != null) {
        this.setState({
          loggedIn: true
        });
      }
    }
  }

  setLanguage = event => {
    this.setState({
      language: event
    });
  };

  render() {
    strings.setLanguage(this.state.language);
    this.state.loggedIn == false ? (
      <Redirect to="/login" />
    ) : (
      <Redirect to="/dashboard" />
    );

    return (
      <div className="App">
        <Router>
          <Switch>
            {LoginScreenRoutes}
            <Layout languageProp={this.setLanguage}>{ScreenRoutes}</Layout>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
