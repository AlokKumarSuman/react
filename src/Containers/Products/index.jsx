import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { strings } from "../../Localization";

import Button from "../../Components/Button";
import Icon from "../../Components/Icons";
import Modal from "../../Components/Modal";
import NoDataFound from "../../Components/NoDataFound";
import Spinner from "../../Components/Spinner";

import AddRecord from "./AddRecord";
// import EditRecord from "./EditRecord";
import ViewRecord from "./ViewRecord";

import { productsSummary, userService } from "../Config";
import { Auth } from "../../Auth";

import {
  ProductCard,
  ProductHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  ProductBody,
  TabsMain,
  TabGroup,
  TabLists,
  TabPanels,
  TabFilter,
  ServerError,
  Sortby,
  Sortoption,
  SpinnerWraper,
  ProductImage
} from "./style";

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      spinner: true,
      openViewHandler: false,
      addNewProductModal: false,
      currentItemsID: "",
      serverError: {},
      filterShow: false,
      searchType: "",
      dropdown: false,
      filterByName: ""
    };
  }

  closeViewHandler = () => {
    /* for popup close */
    this.setState({
      openViewHandler: false,
      addNewProductModal: false
    });
  };

  openViewHandler = (e, currentItemsID) => {
    this.setState({
      openViewHandler: true,
      currentItemsID: currentItemsID
    });
  };
  addNewProductModal = () => {
    this.setState({
      addNewProductModal: true
    });
  };

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(productsSummary, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LANGUAGE_ID: "-1",
        LIMIT: "100",
        OFFSET: "0"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(productList => {
        this.setState({
          productList: productList.RESULT,
          spinner: false
        });
      })
      .catch(err =>
        this.setState({
          serverError: err,
          spinner: false
        })
      );
  }

  filterSearchHandler = () => {
    /* for filter toggle*/
    const crrProductList = this.state.productList;
    const setcrrProductList = crrProductList.map(
      byName => byName.ITEM_NAME.split(" ")[0]
    );
    let unique = [...new Set(setcrrProductList)];
    this.setState(prevState => ({
      filterShow: !prevState.filterShow,
      filterByName: unique
    }));
  };

  searchbytab = e => {
    this.setState({
      searchType: e.currentTarget.textContent
    });
  };

  clearFilter = e => {
    this.setState({
      searchType: ""
    });
  };

  dropdownHandler = () => {
    this.setState(prevState => ({
      dropdown: !prevState.dropdown
    }));
  };

  sortByName = () => {
    function sortOn(arr, prop) {
      arr.sort(function(a, b) {
        if (a[prop] < b[prop]) {
          return -1;
        } else if (a[prop] > b[prop]) {
          return 1;
        } else {
          return 0;
        }
      });
    }
    var obj = [...this.state.productList];
    sortOn(obj, "ITEM_NAME");
    this.setState({
      productList: obj
    });
  };

  render() {
    const { currentItemsID, serverError, filterByName } = this.state;
    let productList = this.state.productList;

    const searchType = this.state.searchType.trim().toLowerCase();

    if (searchType.length > 0) {
      productList = this.state.productList.filter(function(i) {
        return i.ITEM_NAME.toLowerCase().match(searchType);
      });
    }
    return (
      <ProductCard>
        <ProductHeader>
          <WrapperLeft>Product Summary</WrapperLeft>
          <WrapperRight>
            <Sortby
              onMouseEnter={this.dropdownHandler}
              onMouseLeave={this.dropdownHandler}
            >
              <Button classname="blue">
                <Icon name="filter" color="white" /> {strings.Sort_By}
              </Button>
              <Sortoption dropdown={this.state.dropdown}>
                <h5 onClick={this.sortByName}>{strings.Sort_By}</h5>
              </Sortoption>
            </Sortby>
            <ButtonGroup>
              <Button classname="blue" onclick={this.filterSearchHandler}>
                <Icon name="filter" color="white" /> {strings.Filter}
              </Button>
              <Button classname="green" onclick={this.addNewProductModal}>
                <Icon name="addCircle" color="white" />
                {strings.Add_new_product}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </ProductHeader>

        <ProductBody toggleFilter={this.state.filterShow}>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="60px" style={{ textAlign: "center !important" }}>
                  {strings.Image}
                </th>
                <th width="50%" align="left" onClick={this.sortByName}>
                  {strings.Product_Name}
                </th>
                <th width="130px">Authorised Dealers</th>
                <th width="130px">{strings.Total_Dealers}</th>
                <th width="130px">{strings.Orders_Today}</th>
                <th width="230px">Active Loyalty Plans</th>
                <th width="150px">{strings.Details}</th>
              </tr>
            </thead>
            <tbody>
              {this.state.filterShow ? (
                <tr className="dashboard-filter">
                  <td colSpan="6">
                    <TabsMain
                      defaultIndex={1}
                      selectedTabClassName="is-selected"
                      selectedTabPanelClassName="is-selected"
                    >
                      <TabGroup>
                        <TabLists disabled>{strings.Filter} :</TabLists>

                        <TabLists>{strings.Product_Name}</TabLists>
                      </TabGroup>
                      <TabPanels>{strings.Disabled}</TabPanels>

                      <TabPanels>
                        {productList.length == 0 ? (
                          <TabFilter onClick={this.clearFilter}>
                            {strings.All}
                          </TabFilter>
                        ) : (
                          <>
                            <TabFilter onClick={this.clearFilter}>
                              {strings.All}
                            </TabFilter>
                            {filterByName.map((byName, index) => (
                              <TabFilter onClick={this.searchbytab}>
                                {byName}
                              </TabFilter>
                            ))}
                          </>
                        )}
                      </TabPanels>
                    </TabsMain>
                  </td>
                </tr>
              ) : null}

              {this.state.spinner == true ? (
                <tr>
                  <td colSpan="7">
                    <SpinnerWraper>
                      <Spinner />
                    </SpinnerWraper>
                  </td>
                </tr>
              ) : productList.length == 0 ? (
                <tr>
                  <td colSpan="7">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                productList.map((item, index) => (
                  <tr key={index}>
                    <td align="left">
                      <ProductImage>
                        <img
                          src={item.IMAGE}
                          alt="Img"
                          className="product-image"
                        />
                      </ProductImage>
                    </td>
                    <td>{item.ITEM_NAME}</td>
                    <td align="center">{item.AUTHORIZED_USER}</td>
                    <td align="center">{item.ORDERS}</td>
                    <td align="center">{item.ACTIVE_SCHEMES}</td>
                    <td align="center">{item.DEALERS}</td>
                    <td align="center">
                      <Button
                        type="primary"
                        size="fullwidth"
                        classname="forMobileView"
                        onclick={e => this.openViewHandler(e, item.ITEM_ID)}
                      >
                        {strings.View_Details}
                      </Button>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          <ServerError>
            {serverError.status} {serverError.statusText}
          </ServerError>
        </ProductBody>

        {/* Modal Popup for View/Edit Product */}
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Product_Details}
          size="lg"
        >
          <ViewRecord currentItemsID={currentItemsID} />
        </Modal>

        {/* Modal Popup for  Add New Product */}
        <Modal
          modalOpen={this.state.addNewProductModal}
          onclick={this.closeViewHandler}
          title={strings.Request_to_Add_New_Product}
        >
          {/* <p>jksdflk</p> */}
          <AddRecord onclickClose={this.closeViewHandler} />
        </Modal>
      </ProductCard>
    );
  }
}

export default Products;
