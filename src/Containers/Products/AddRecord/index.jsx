import React, { Component } from "react";
import { strings } from "../../../Localization";
import PropTypes from "prop-types";
import Button from "../../../Components/Button";
import Modal from "../../../Components/Modal";
import UploadImage from "./UploadImage";
import Icon from "../../../Components/Icons";
import downloadFile from "../../../Assets/Images/logo.png";
import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  SpecialClass,
  NameButtonWrap,
  DUGroup,
  ErrorMsg
} from "./style";
import { Auth } from "../../../Auth";
import {
  sendAddProductRequest,
  userService,
  productTypeSearchApiUrl,
  productTypeSearchCategoryApiUrl
} from "../../Config";

class AddRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productName: "",
      comment: "",
      reason: "",
      productTypeID: "",
      productCategoryID: "",
      productTypeAPI: [],
      productCategoryAPI: [],
      serverError: {},
      imageName: "",
      imageNameByServer: "",
      isloading: true,
      enabled: false,
      modalOpen: false,
      validateAllFilds: "",
      responseAddLevelStatusAPI: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.getProductCategory = this.getProductCategory.bind(this);
  }

  openHandler = () => {
    /* for popup Open */
    this.setState({
      modalOpen: true
    });
  };

  closeHandler = () => {
    /* for popup Open */
    this.setState({
      modalOpen: false
    });
  };

  displayImageNameHandler = (displayImageName, imageNameByServer) => {
    this.setState({
      imageName: displayImageName,
      imageNameByServer
    });
  };

  handleChange(evt) {
    // debugger;
    const value = evt.target.value;
    evt.target.name === "productType"
      ? this.getProductCategory(value)
      : this.setState({
          [evt.target.name]: evt.target.value,
          validateAllFilds: "",
          responseAddLevelStatusAPI: ""
        });
  }

  onProductCategoryChange = e => {
    // debugger;
    const options = e.target.options;
    const productTypeId = options[options.selectedIndex].id;
    this.setState({
      productTypeID: productTypeId
    });
  };

  getProductCategory(id) {
    // debugger;

    if (id != "default") {
      this.setState({
        productCategoryID: id
      });

      fetch(productTypeSearchCategoryApiUrl + id, {
        method: "GET",
        headers: Auth
      })
        .then(response => response.json())
        .then(productCategoryAPI => {
          this.setState({
            productCategoryAPI: productCategoryAPI.response.docs
          });
        })
        .catch(err => console.log(err));
    }

    if (id === "default") {
      this.setState({
        productCategoryAPI: []
      });
      return;
    }
  }

  componentDidMount() {
    fetch(productTypeSearchApiUrl, {
      method: "GET",
      headers: Auth
    })
      .then(response => response.json())
      .then(productTypeAPI => {
        this.setState({
          productTypeAPI: productTypeAPI.response.docs,
          isloading: false
        });
      })
      .catch(err => console.log(err));
  }

  sendRequest = () => {
    // debugger;

    const {
      productName,
      comment,
      reason,
      productTypeID,
      productCategoryID,
      imageNameByServer,
      imageName
    } = this.state;

    if (
      productName === "" ||
      comment === "" ||
      reason === "" ||
      productTypeID === "" ||
      productCategoryID === "" ||
      imageName === ""
    ) {
      return this.setState({
        validateAllFilds: "All field must be filled out."
      });
    }

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    // debugger;
    fetch(sendAddProductRequest, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        PRODUCT_ID: productTypeID,
        PRODUCT_CAT: productCategoryID,
        PRODUCT_NAME: productName,
        COMMENT: comment,
        REASON: reason,
        REQUEST_BY: "Brand Portal",
        FILE_URL: imageNameByServer
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    // debugger;
    const {
      imageName,
      productTypeAPI,
      productCategoryAPI,
      validateAllFilds,
      responseAddLevelStatusAPI,
      serverError
    } = this.state;

    const productTypeOption = productTypeAPI.map((list, index) => (
      <option key={list.CLASS_CODE} value={list.CLASS_CODE}>
        {list.CLASS_NAME}
      </option>
    ));

    const productCategoryOption = productCategoryAPI.map((list, index) => (
      <option key={list.BRICK_CODE} id={list.BRICK_CODE}>
        {list.BRICK_NAME}
      </option>
    ));

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.modalOpen}
          onclick={this.closeHandler}
          title="Upload File"
        >
          <UploadImage
            onclickClose={this.closeHandler}
            displayImageName={this.displayImageNameHandler}
          />
        </Modal>
        <Form>
          <FormRow>
            <LabelInputGroup>
              <h5>{strings.Product_Type}</h5>
              <select name="productType" onChange={this.handleChange}>
                <option value="default">{strings.Choose_Products}...</option>
                {productTypeOption}
              </select>
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>{strings.Product_Category}</h5>
              <select
                name="productCategory"
                onChange={this.handleChange}
                disabled={productCategoryAPI == ""}
                onChange={e => this.onProductCategoryChange(e)}
              >
                <option>{strings.Choose_one_in_list}...</option>
                {productCategoryOption}
              </select>
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <NameButtonWrap>
              <LabelInputGroup>
                <h5>{strings.Product_Name}</h5>
                <input
                  type="text"
                  name="productName"
                  placeholder={strings.Product_Name}
                  value={this.state.productName.value}
                  onChange={this.handleChange}
                />
              </LabelInputGroup>

              <LabelInputGroup>
                <SpecialClass>
                  <h5>{strings.Product_Comment}</h5>
                  <textarea
                    rows="4"
                    cols="50"
                    name="comment"
                    placeholder={strings.Product_Comment}
                    onChange={this.handleChange}
                  />
                </SpecialClass>
              </LabelInputGroup>
            </NameButtonWrap>

            <NameButtonWrap>
              <LabelInputGroup>
                <h5>{strings.Reason}</h5>
                <textarea
                  rows="4"
                  cols="50"
                  name="reason"
                  placeholder={strings.Reason}
                  onChange={this.handleChange}
                />
              </LabelInputGroup>
              <DUGroup>
                <label>
                  <a href={downloadFile} download>
                    <Icon name="filter" color="white" />
                    <p>{strings.Download_Format}</p>
                  </a>
                </label>
                <label>
                  <input
                    type="text"
                    id="uploadDoc"
                    onClick={this.openHandler}
                  />
                  <span for="uploadDoc">
                    <Icon name="filter" color="white" />
                    <p>{strings.Upload_Format}</p>
                  </span>
                </label>
              </DUGroup>
              <p>{imageName}</p>
            </NameButtonWrap>
          </FormRow>

          <FormRow>
            <LabelButtonDU>
              <Button onclick={this.sendRequest}>
                {/* disabled={!enabled} */}
                {strings.Send_Request}
              </Button>
            </LabelButtonDU>
          </FormRow>
        </Form>

        <ErrorMsg>
          {serverError.status} {serverError.statusText}
        </ErrorMsg>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </h4>
        ) : null}
      </React.Fragment>
    );
  }
}

AddRecord.propTypes = {
  addRec: PropTypes.func,
  onclickClose: PropTypes.func
};

export default AddRecord;
