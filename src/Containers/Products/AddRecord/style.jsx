import styled from "styled-components";

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    color: #333;
    font-size: 1.4rem;
    font-weight: 500;
    font-family: "Gotham"
    letter-spacing: 1px;
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.2rem;
    }
  }
`;

export const SpecialClass = styled.div`
  margin-top: 10px;
`;

export const LabelButtonDU = styled.div`
  width: 100% !important;
  div {
    width: 20rem;
    margin: 0 auto;
  }
`;
export const DUGroup = styled.div`
  padding: 20px 0;
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: row;

  label {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    position: relative;
    width: 50%;
    padding: 6px 10px;
    background: green;
    margin: 0 5px;

    a {
      display: flex;
      flex-direction: row;
      align-items: center;
      text-decoration: none;
      color: #fff;

      p {
        font-size: 13px;
      }
    }

    .icon {
      position: static;
      margin-right: 10px;
    }

    input {
      width: 100%;
      height: initial;
      border: none;
      background: none;
      padding: 0px;
      opacity: 0;
    }

    span {
      position: absolute;
      top: 50%;
      left: 50%;
      width: 100%;
      display: flex;
      transform: translateX(-50%) translateY(-50%);
      flex-direction: row;
      align-items: center;
      color: #fff;
      font-size: 14px;
      padding: 6px 10px;
    }
  }
`;

export const NameButtonWrap = styled.div`
  display: flex;
  flex-direction: column;
  p {
    text-align: right;
  }
`;

export const ImageRatio = styled.div`
  object-fit: cover;
  width: 80px;
`;

export const ErrorMsg = styled.div`
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;

export const AddImage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  input {
    border: none;
    box-shadow: none;
    padding: 0px;
    text-align: center;
    display: flex;
    align-self: center;
    margin: 0 auto;
    width: 30rem;
    margin-bottom: 10rem;
  }
`;

export const Preview = styled.div`
  width: 300px;
  height: 200px;
  border: 1px solid #333;
  margin: 10px auto;
  overflow: hidden;

  .previewText {
    font-size: 13px;
    padding: 10px;
    color: #999;
  }

  img {
    width: 100%;
    display: block;
  }
`;
