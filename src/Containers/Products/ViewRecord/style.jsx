import styled from "styled-components";

export const ProductDetailsWraper = styled.div`
  padding-bottom: 1rem;
  // position: relative;
  display: flex;
  flex-direction: column;
`;
export const ProductBasicInfo = styled.div`
  display: flex;
  flex-direction: row;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  font-size: 1.4rem;
  overflow: hidden;
`;
export const ProductImageSlider = styled.div`
  align-items: left;
  display: flex;
  width: 30%;
  flex-direction: row;
  margin-right: 4rem;
  .Product-image-group {
    width: 100%;
    height: auto;
  }
  .bigImage {
    margin-bottom: 10px;
    vertical-align: top;
    // width: calc(100% - 50px);
    width: 100px;
  }
  .smallImage {
    width: 40px;
    margin-right: 1.6rem;
  }
  .smallImage .slick-slide {
    padding: 0 5px;
    -webkit-filter: grayscale(100%);
    filter: grayscale(100%);
  }

  .smallImage .slick-slide.slick-current {
    -webkit-filter: inherit;
    filter: inherit;
  }
`;

export const ProductInfo = styled.div`
  display: table-cell;
  vertical-align: top;
  align-items: left;
  width: 70%;
  font-size: 1.4rem;
  label {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: left;
    padding: 0.7rem 0;
  }
  .table td {
    padding 0.6rem 0.2rem;
  }
`;

export const ProductDiscription = styled.div`
  clear: both;
  margin-top: 3rem;

  p {
    display: inline-block;
    line-height: 1.6em;
  }
  b {
    width: 24%;
    display: inline-block;
  }
  tr {
    background: none !important;
  }
`;

export const Lang = styled.div`
  display: flex;
  width: 18rem;
  justify-content: flex-end;
  flex-wrap: wrap;
  flex-direction: row;
  margin-left: auto;

  select {
    width: 140px;
    padding: 0px;
    height: 30px;
  }
`;
export const DUGroup = styled.div`
  padding: 20px 0;
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: row;
  border-top: solid 1px #c5c5c5;
  margin-top: 2.6rem;
  label {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    position: relative;
    width: 50%;
    padding: 6px 10px;
    background: green;
    margin: 0 5px;

    a {
      display: flex;
      flex-direction: row;
      align-items: center;
      text-decoration: none;
      color: #fff;

      p {
        font-size: 14px;
      }
    }

    .icon {
      position: static;
      margin-right: 10px;
    }

    input {
      width: 100%;
      height: initial;
      border: none;
      background: none;
      padding: 0px;
      opacity: 0;
    }

    span {
      position: absolute;
      top: 50%;
      left: 50%;
      width: 100%;
      display: flex;
      transform: translateX(-50%) translateY(-50%);
      flex-direction: row;
      align-items: center;
      color: #fff;
      font-size: 14px;
      padding: 6px 10px;
    }
  }
`;
export const LabelButtonDU = styled.div`
  width: 100% !important;
  div {
    width: 20rem;
    margin: 0 auto;
  }
`;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const SpinnerWraper = styled.div`
  position: absolute;
  min-height: 100%;
  align-items: center;
  display: flex;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.4);
  z-index: 9;
  width: 100%;
`;
// display: -webkit-box;
//     display: -webkit-flex;
//     display: -ms-flexbox;

//     min-height: 200px;
//     -webkit-align-items: center;
//     -webkit-box-align: center;
//     -ms-flex-align: center;
//     align-items: center;
//     -webkit-box-pack: center;
//     -webkit-justify-content: center;
//     -ms-flex-pack: center;

export const AttachementDetails = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  padding: 1.6rem 1.6rem 1.2rem;
  margin-bottom: 1.6rem;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.1), 0 1px 10px 0 rgba(0, 0, 0, 0.1);
  color: #333;
  background: #fcf4f4;

  a {
    color: blue;
    text-decoration: underline;
  }
`;
