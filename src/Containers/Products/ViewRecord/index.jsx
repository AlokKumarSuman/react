import React, { Component } from "react";
import { strings } from "../../../Localization";
import PropTypes from "prop-types";
import downloadFile from "../../../Assets/Images/logo.png";
// import Button from "../../Components/Button";
import Icon from "../../../Components/Icons";
import Button from "../../../Components/Button";
import Spinner from "../../../Components/Spinner";

import ProductSlider from "../Slider";
import { Accordion, AccordionItem } from "react-light-accordion";
import { fetchItemDetails } from "../../Config";
import { Auth } from "../../../Auth";

import {
  ProductDetailsWraper,
  ProductBasicInfo,
  ProductImageSlider,
  ProductInfo,
  ProductDiscription,
  Lang,
  DUGroup,
  LabelButtonDU,
  ServerError,
  SpinnerWraper,
  AttachementDetails
} from "./style";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentItemsID: this.props.currentItemsID,
      serverError: {},
      productDetails: {},
      itemAttachements: [],
      itemImages: [],
      shortDescription: "",
      longDescription: "",
      contentEditable: false,
      spinner: true,
      language: "en"
    };
  }

  // langChange = e => {
  //   const currLang = e.target.value;
  //   this.setState({
  //     language: currLang
  //   });
  // };

  handleChange = evt => {
    this.setState({ [evt.target.name]: evt.target.value });
  };

  editableTrueHandler = evt => {
    this.setState({ contentEditable: true });
  };
  editableFalseHandler = evt => {
    this.setState({ contentEditable: false });
  };

  componentDidMount() {
    const { currentItemsID } = this.state;
    fetch(fetchItemDetails, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        ITEM_ID: currentItemsID,
        LANGUAGE_CODE: "-1",
        OFFSET: "1",
        LIMIT: "1"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res.RESULT[0]))
      .then(productDetails => {
        this.setState({
          productDetails: productDetails.RESULT[0],
          itemAttachements: productDetails.RESULT[0].ITEM_ATTACHMENTS,
          itemImages: productDetails.RESULT[0].ITEM_IMAGES,
          spinner: false
        });
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  }

  render() {
    const {
      serverError,
      productDetails,
      itemAttachements,
      itemImages,
      contentEditable
    } = this.state;

    const activeLanguage = this.state.language;

    return (
      <ProductDetailsWraper>
        {/* <Lang>
          <select onChange={this.langChange}>
            <option>Choose Language</option>
            <option value="en" onChange={this.langChange}>
              English
            </option>
            <option value="hi" onChange={this.langChange}>
              Hindi
            </option>
          </select>
        </Lang> */}
        <ServerError>
          {serverError.status} {serverError.statusText}
        </ServerError>
        {this.state.spinner == true ? (
          <SpinnerWraper>
            <Spinner />
          </SpinnerWraper>
        ) : // <span style={{ color: "red" }}>Internal Server Error</span>
        null}
        <ProductBasicInfo>
          <ProductImageSlider>
            <ProductSlider productDetailImages={itemImages} />
          </ProductImageSlider>
          <ProductInfo>
            <table width="100%" className="table">
              <tr>
                <td width="106px">
                  <b> {strings.Product_Name}</b>
                </td>
                <td> : {productDetails.NAME}</td>
              </tr>
              <tr>
                <td>
                  <b>{strings.Product_Type}</b>
                </td>
                <td> : {productDetails.BRAND_NAME}</td>
              </tr>
              <tr>
                <td>
                  <b>{strings.Sort_Description}</b>
                </td>
                <td>
                  <div style={{ display: "flex" }}>
                    :&nbsp;
                    {contentEditable ? (
                      <input
                        type="text"
                        name="shortDescription"
                        onChange={this.handleChange}
                        value={productDetails.SHORT_DESC}
                      />
                    ) : (
                      <span>{productDetails.SHORT_DESC}</span>
                    )}
                    {/* <span className="product-details-edit">
                      {contentEditable ? (
                        <Icon
                          name="clear"
                          onclick={this.editableFalseHandler}
                        />
                      ) : (
                        <Icon name="pen" onclick={this.editableTrueHandler} />
                      )}
                    </span> */}
                  </div>
                </td>
              </tr>
            </table>
          </ProductInfo>
        </ProductBasicInfo>
        <ProductDiscription>
          <Accordion atomic={true}>
            <AccordionItem title={strings.Description}>
              <div>
                <table width="100%">
                  <tr>
                    <td>
                      <h4>
                        {strings.Detailed_Description}:
                        {/* <span className="product-details-edit">
                          {contentEditable ? (
                            <Icon
                              name="clear"
                              onclick={this.editableFalseHandler}
                            />
                          ) : (
                            <Icon
                              name="pen"
                              onclick={this.editableTrueHandler}
                            />
                          )}
                        </span> */}
                      </h4>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      {contentEditable ? (
                        <textarea
                          name="longDescription"
                          onChange={this.handleChange}
                          cols="30"
                          rows="5"
                          value={productDetails.LONG_DESC}
                        />
                      ) : (
                        <p>{productDetails.LONG_DESC}</p>
                      )}
                    </td>
                  </tr>
                </table>
              </div>
            </AccordionItem>

            <AccordionItem title={strings.Specification}>
              <div>
                <table width="100%">
                  <tr>
                    <td>
                      <b>{strings.Product_Name}:</b> <p> Retail </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <b>{strings.Product_Size}:</b> <p> 50KG </p>
                    </td>
                  </tr>
                </table>
              </div>
            </AccordionItem>

            <AccordionItem title={strings.Attachment}>
              {itemAttachements.map((item, index) => (
                <AttachementDetails>
                  <table width="100%" key={index}>
                    <tr>
                      <td>
                        <b>{strings.Name}:</b>
                        {item.NAME}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <b>{strings.URL}:</b>
                        <a target="_blank">{item.URL}</a>
                      </td>
                    </tr>
                  </table>
                </AttachementDetails>
              ))}
            </AccordionItem>
          </Accordion>

          {/* <DUGroup>
            <label>
              <a href={downloadFile} download>
                <Icon name="filter" color="white" />
                <p>Download Format</p>
              </a>
            </label>
            <label>
              <input type="file" id="uploadDoc" />
              <span htmlFor="uploadDoc">
                <Icon name="filter" color="white" />
                <p>Upload Format</p>
              </span>
            </label>
          </DUGroup>
          <LabelButtonDU>
            <Button onclick={this.updateRecord}>Submit</Button>
          </LabelButtonDU> */}
        </ProductDiscription>
      </ProductDetailsWraper>
    );
  }
}

// ViewRecord.propTypes = {
//   NAME: PropTypes.any,
//   BRAND_NAME: PropTypes.any,
//   SHORT_DESC: PropTypes.any,
//   LONG_DESC: PropTypes.any
// };

export default ViewRecord;
