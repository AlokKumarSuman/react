import React, { Component } from "react";
import { strings } from "../../../Localization";
import { Auth } from "../../../Auth";
import { userService, listAddProductRequests } from "../../Config";

import Spinner from "../../../Components/Spinner";
import NoDataFound from "../../../Components/NoDataFound";

import Icon from "../../../Components/Icons";
import Modal from "../../../Components/Modal";
import ViewDetailList from "./ViewDetailList";
import {
  ProductListCard,
  ProductListHeader,
  ButtonGroup,
  WrapperLeft,
  // WrapperRight,
  ProductListBody,
  // ViewParent,
  // ViewData,
  SpinnerConfigData,
  ServerError,
  ErrorMsg
} from "./style.jsx";

class NewProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProductList: [],
      currRowData: [],
      fetchErrorMsg: "",
      serverError: {},

      spinnerConfigData: true
    };
  }

  openHandler = (e, item) => {
    /* for popup Open */
    const currkey = e.target.getAttribute("datakey");
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen,
      currRowData: item
    }));
  };

  closeHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  componentDidMount() {
    // const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(listAddProductRequests, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        APP_CODE: "BRANDPORTAL",
        USER_ID: authUserName,
        LIMIT: "100",
        OFFSET: "0"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            newProductList: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })

      .catch(err =>
        this.setState({
          serverError: err,
          spinnerConfigData: false
        })
      );
  }

  render() {
    const {
      newProductList,
      currRowData,
      spinnerConfigData,
      serverError
    } = this.state;
    return (
      <ProductListCard>
        <ProductListHeader>
          <WrapperLeft>
            {strings.Request_Recieved_To_Add_New_Product}
          </WrapperLeft>
        </ProductListHeader>

        <Modal
          modalOpen={this.state.modalOpen}
          onclick={this.closeHandler}
          title={strings.View_Product_Detail}
        >
          <ViewDetailList currRowData={currRowData} />
        </Modal>
        <ProductListBody>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="16%" align="left">
                  {strings.Product_Name}
                </th>
                <th width="24%" align="left">
                  {strings.Product_Category}
                </th>
                <th width="20%" align="left">
                  {strings.Item_Name}
                </th>
                <th width="10%" align="left">
                  {strings.Brand_Name}
                </th>
                <th width="12%" align="left">
                  {strings.Request_Date}
                </th>
                <th width="9%" align="left">
                  {strings.Status}
                </th>
                <th className="action" width="9%" align="center">
                  {strings.Details}
                </th>
              </tr>
            </thead>
            <tbody>
              {spinnerConfigData == true ? (
                <>
                  <tr />
                  <tr>
                    <td colSpan="7">
                      <SpinnerConfigData>
                        <Spinner />
                      </SpinnerConfigData>
                    </td>
                  </tr>
                </>
              ) : newProductList.length == 0 ? (
                <tr>
                  <td colSpan="7">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                newProductList.map((item, index) => (
                  <tr key={index}>
                    <td>{item.PRODUCT_NAME}</td>
                    <td>{item.PRODUCT_CAT_NAME}</td>
                    <td>{item.ITEM_NAME}</td>
                    <td>{item.BRAND_NAME}</td>
                    <td>{item.REQUEST_DATE}</td>
                    <td>{item.STATUS}</td>
                    <td align="center">
                      <Icon
                        name="eye"
                        datakey={item.REQUEST_ID}
                        onclick={e => this.openHandler(e, item)}
                      />
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
          {this.state.serverError.statusText == "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
        </ProductListBody>
      </ProductListCard>
    );
  }
}

export default NewProductList;
