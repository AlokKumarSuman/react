import styled from "styled-components";

export const ProductListCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: "Roboto", sans-serif;
  font-size: initial;

  .trackDemand-image-view {
    height: 6rem;
    margin-bottom: 1rem;
  }
`;

export const ProductListHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  font-weight: 600;
  height: 40px;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;

export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const ProductListBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;

  table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};
  }
  .product-image {
    height: 40px;
  }
`;

export const ViewParent = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ViewRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const ViewData = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
  margin-bottom: 15px;

  h5 {
    font-size: 14px;
  }

  p {
    margin: 10px 0px;
  }
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;
export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
