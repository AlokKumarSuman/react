import React, { Component } from "react";
import { strings } from "../../../../Localization";
import { ViewParent, ViewRow, ViewData } from "../style.jsx";

class ViewDetailList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { currRowData } = this.props;
    return (
      <ViewParent>
        <ViewRow>
          <ViewData>
            <h5>{strings.Product_Name}</h5>
            <p>{currRowData.PRODUCT_NAME}</p>
          </ViewData>
          <ViewData>
            <h5>{strings.Product_Category_ID}</h5>
            <p>{currRowData.PRODUCT_CAT_ID}</p>
          </ViewData>
        </ViewRow>

        <ViewRow>
          <ViewData>
            <h5>{strings.Product_Category_Name}</h5>
            <p>{currRowData.PRODUCT_CAT_NAME}</p>
          </ViewData>
          <ViewData>
            <h5>{strings.Item_Name}</h5>
            <p>{currRowData.ITEM_NAME}</p>
          </ViewData>
        </ViewRow>

        <ViewRow>
          <ViewData>
            <h5>{strings.Brand_ID}</h5>
            <p>{currRowData.BRAND_ID}</p>
          </ViewData>
          <ViewData>
            <h5>{strings.Brand_Name}</h5>
            <p>{currRowData.BRAND_NAME}</p>
          </ViewData>
        </ViewRow>

        <ViewRow>
          <ViewData>
            <h5>{strings.Request_Date}</h5>
            <p>{currRowData.REQUEST_DATE}</p>
          </ViewData>
          <ViewData>
            <h5>{strings.Request_By}</h5>
            <p>{currRowData.REQUEST_BY}</p>
          </ViewData>
        </ViewRow>

        <ViewRow>
          <ViewData>
            <h5>{strings.Status}</h5>
            <p>{currRowData.STATUS}</p>
          </ViewData>
          <ViewData>
            <h5>{strings.URL}</h5>
            <p>{currRowData.FILE_URL}</p>
          </ViewData>
        </ViewRow>

        <ViewData>
          <h5>{strings.Reason}</h5>
          <p>{currRowData.REASON}</p>
        </ViewData>
      </ViewParent>
    );
  }
}

export default ViewDetailList;
