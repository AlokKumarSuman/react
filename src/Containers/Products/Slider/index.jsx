import React, { Component } from "react";
import Slider from "react-slick";
import "../../../slick.css";

class ProductSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }

  render() {
    const bannersImages = this.props.productDetailImages;
    // const banners = [
    //   "product_img",
    //   "product_img1",
    //   "product_img2",
    //   "product_img3",
    //   "product_img4",
    //   "product_img5",
    //   "product_img6"
    // ];
    // const images = banners.map((name, index) => {
    //   return (
    //     <div className="item">
    //       <img
    //         data-src="http://www.jaipuriaschoolballia.in/wp-content/uploads/2016/11/blank-img.jpg"
    //         src={require(`../../../Assets/Images/productImage/${name}.jpg`)}
    //         alt=""
    //       />
    //     </div>
    //   );
    // });
    const images = bannersImages.map((item, index) => {
      return (
        <div className="item">
          <img
            data-src="http://www.jaipuriaschoolballia.in/wp-content/uploads/2016/11/blank-img.jpg"
            src={item.IMAGE_URL}
            alt=""
          />
        </div>
      );
    });

    return (
      <React.Fragment>
        <Slider
          asNavFor={this.state.nav1}
          ref={slider => (this.slider2 = slider)}
          slidesToShow={3}
          swipeToSlide={true}
          focusOnSelect={true}
          arrows={false}
          vertical={true}
          slidesToScroll={1}
          className="smallImage"
        >
          {images}
        </Slider>
        <Slider
          asNavFor={this.state.nav2}
          ref={slider => (this.slider1 = slider)}
          arrows={false}
          className="bigImage"
        >
          {images}
        </Slider>
      </React.Fragment>
    );
  }
}

export default ProductSlider;
