import styled from "styled-components";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

export const ProductCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: "Gotham";
  font-size: initial;
  h4 .product-details-edit {
    float: right;
  }
  .product-details-edit {
    position: relative;
    top: -3px;
    left: 6px;
    width: 36px;
  }
  .product-details-edit .icon {
    position: relative;
    right: 0;
    top: 0;
    height: 16px;
    width: 16px;
  }
  .dashboard-filter {
    div > ul > li {
      padding: 0.8rem 0.6rem;
    }
    div > div {
      padding: 1.2rem;
    }
  }
`;

export const ProductHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  background-color: #2dc3e8;
  width: 100%;
  font-family: "Gotham";
  font-weight: 500;
  color: #fff;
`;
export const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  div {
    margin-right: 1rem;
  }
  div:last-child {
    margin-right: 0;
  }
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    width:auto;
    padding: 0.6rem 0.8rem;
  }
  div svg.icon {
    margin-right:0.6rem;
    margin-top: -3px;
  }
  div svg.icon-fa {
    margin-right:0.6rem;
  }
  }
  svg.icon:hover {
    fill: #cb0202;
    cursor: pointer;
}

@media (max-width: 768px) {
  .blue {
    display: none;
  }
}
`;
export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  text-transform: uppercase;
  font-weight: 500;
  font-size: 14px;

  @media (max-width: 768px) {
    display: none;
  }
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
`;
export const ProductBody = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;
  font-size: 1.4rem;
  text-transform: capitalize;

  .product-image {
    height: 40px;
  }

  @media (max-width: 768px) {
    td:nth-of-type(1):before {
      content: "Image";
    }
    td:nth-of-type(2):before {
      content: "Product Name";
    }
    td:nth-of-type(3):before {
      content: "Total Dealers";
    }
    td:nth-of-type(4):before {
      content: "Orders Today";
    }
    td:nth-of-type(5):before {
      content: "Active Plans";
    }
    td:nth-of-type(6):before {
      content: "Details";
    }

    /*test*/

    td:before {
      position: absolute;
      top: 50%;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      transform: translateY(-50%);
    }
  }
`;

// Custom Tabs
export const TabsMain = styled(Tabs)`
  width: 100%;
`;

export const TabGroup = styled(TabList)`
  list-style-type: none;
  display: flex;
  margin: 0;
`;
// TabGroup.tabsRole = "TabList";

export const TabLists = styled(Tab)`
  border: 1px solid #cecece;
  padding: 1.4rem 0.6rem;
  user-select: none;
  cursor: pointer;
  flex-grow: 1;
  width: 100%;
  background-color: #979899;
  text-align: center;

  &.is-selected {
    background-color: white;
    border-bottom: none;
  }

  &.react-tabs__tab--disabled {
    background-color: #e5e5e8;
    font-weight: 500;
    flex-basis: 40rem;
  }
`;
// TabLists.tabsRole = "Tab";

export const TabPanels = styled(TabPanel)`
  display: none;
  min-height: 4rem;
  padding: 2rem;
  border-top: none;
  background-color: white;
  flex-direction: row;
  align-items: center;
  &.is-selected {
    display: block;
  }
`;
// TabPanels.tabsRole = "TabPanel";
export const TabFilter = styled.span`
  background-color: #9c9d9e;
  margin-right: 2rem;
  padding: 1rem 4rem;
  border-radius: 2rem;
  display: inline-block;
  &:last-child {
    margin-right: 0;
  }
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const Sortby = styled.div`
  display: flex;
  flex-direction: column;
  width: 11rem;
  margin-right: 10px;
  position: relative;

  > div {
    width: 100%;
    display: flex;
    align-items: center;
    padding: 5px;
    background: #fff;

    h5 {
      font-size: 13px;
      padding: 5px 2px;
      cursor: pointer;
      padding: 10px 5px;
      color: #333;
      :hover {
        background: #686464;
        color: #fff;
        transition: 0.2s all;
      }
    }
  }
  div svg.icon {
    margin-right:0.6rem;
    margin-top: -3px;
  }
  div svg.icon-fa {
    margin-right:0.6rem;
  }
  }
  svg.icon:hover {
    fill: #cb0202;
    cursor: pointer;
}

@media (max-width: 768px) {
  display: none;
}
`;

export const Sortoption = styled.div`
  display: ${props => (props.dropdown === true ? "block" : "none")} !important;
  padding: 0px !important;
  position: absolute;
  top: 32px;
  right: 0px;
  width: 15rem !important;
  box-shadow: -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.45);
  box-shadow: -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.45);
  box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.45)
`;

export const SpinnerWraper = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
export const ProductImage = styled.div`
  object-fit: cover;
  width: 80px;
  text-align: center;

  @media (max-width: 768px) {
    text-align: left;
  }
`;
