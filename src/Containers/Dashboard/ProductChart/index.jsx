import React, { Component } from "react";
import { Chart } from "react-google-charts";
import { strings } from "../../../Localization";

import { Auth } from "../../../Auth";
import { userService, FetchDashboradGraph } from "../../Config";

class ProductChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dashboardGraph: []
      // chartData: [
      //   ["Year", "Current Orders", "Current Demands", "Published Schemes"],
      //   ["2019", 860, 1400, 1100]
      // ]
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantName = userService.authTenantName();

    fetch(FetchDashboradGraph, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_NAME: authTenantName
      })
    })
      .then(res => res.json())
      .then(res => {
        this.setState({
          dashboardGraph: res.RESULT
        });
      })
      .catch(err => console.log(err));
  }

  render() {
    console.log(this.state.dashboardGraph);
    return (
      <Chart
        width={"100%"}
        height={"400px"}
        chartType="AreaChart"
        loader={<div>{strings.Loading_Chart}</div>}
        data={this.state.dashboardGraph}
        options={{
          title: "Current History",
          hAxis: { title: "Date", titleTextStyle: { color: "#333" } },
          vAxis: { minValue: 0 },
          // For the legend to fit, we make the chart area smaller
          legend: { position: "top" },
          chartArea: { width: "80%", height: "65%" }
        }}
      />
    );
  }
}

export default ProductChart;
