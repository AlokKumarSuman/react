import styled from "styled-components";

export const ProductsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  // padding: 2rem 5rem;
`;

export const ProductsContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
`;

export const SideBar = styled.div`
  background: #f8f8f8;
  padding: 2rem 2rem 3rem 5rem;
`;

export const ProductsMainArea = styled.div`
  width: 100%;
`;

export const ProductCard = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 2rem;
  color: white;
  font-family: "Roboto", sans-serif;
  > div {
    min-width: 24%;
    min-height: 12rem;
    @media (max-width: 568px) {
      width: 100%;
    }

    p {
      margin-bottom: 0.6rem;
    }
  }
  > div:nth-child(1) {
    background-color: #41c3e3;
  }
  > div:nth-child(2) {
    background-color: #00a7de;
  }
  > div:nth-child(3) {
    background-color: #ffc400;
  }
  > div:nth-child(4) {
    background-color: #ff5e5f;
  }

  @media (max-width: 568px) {
    display: flex;
    flex-direction: column;
  }
`;
export const ProductChartWraper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  color: white;
  font-family: "Roboto", sans-serif;
  min-height: 400px;
  background: #fff;

    width: 100%;
    overflow-y: hidden;
    overflow-x: auto;
    div > div > div > div[dir="ltr"] {
      width:100% !important;
    }
    svg {
      width: 100%;
    }
  }
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;
export const SpinnerWraper = styled.div`
  display: flex;
  min-height: 80px;
  align-items: center;
  justify-content: center;
  .path {
    stroke: #dcdcdc !important;
  }
`;
