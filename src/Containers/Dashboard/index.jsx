import React, { Component } from "react";
import { Chart } from "react-google-charts";
import { strings } from "../../Localization";
import ProductChart from "./ProductChart";

import { Auth } from "../../Auth";
import { userService, fetchDasboardDetails } from "../Config";
import Spinner from "../../Components/Spinner";

import {
  ProductsWrapper,
  ProductCard,
  ProductChartWraper,
  ServerError,
  SpinnerWraper
} from "./style";

import Card from "../../Components/Card";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serverError: {},
      currectOrderDemandSchemes: [],
      lastLogin: "",
      spinner: true
      // chartData: [
      //   ["Year", "Current Orders", "Current Demands", "Published Schemes"],
      //   ["2019", 860, 1400, 1100]
      // ]
    };
  }

  componentDidMount() {
    const authBrandUserName = userService.authBrandUserName();
    const authBrandId = userService.authBrandId();
    const authLastLogin = userService.authLastLogin();

    fetch(fetchDasboardDetails, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authBrandUserName
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res.RESULT))
      .then(currectOrderDemandSchemes => {
        this.setState({
          currectOrderDemandSchemes: currectOrderDemandSchemes.RESULT,
          lastLogin: authLastLogin,
          spinner: false
        });
      })
      .catch(err =>
        this.setState({
          serverError: err,
          lastLogin: authLastLogin
        })
      );
  }

  render() {
    const { currectOrderDemandSchemes, lastLogin, serverError } = this.state;
    // console.log(currectOrderDemandSchemes);
    // console.log(this.state.dashboardGraph);

    const divStyle = {
      color: "#000"
    };

    return (
      <ProductsWrapper>
        <ServerError>
          {serverError.status} {serverError.statusText}
        </ServerError>
        <ProductCard>
          <Card>
            <p>{strings.Orders_Today}</p>
            {this.state.spinner == true ? (
              <SpinnerWraper>
                <Spinner />
              </SpinnerWraper>
            ) : currectOrderDemandSchemes.length != 0 ? (
              <span>{currectOrderDemandSchemes[0].ORDERS}</span>
            ) : (
              <span style={{ color: "#504f4f", fontSize: "12px" }}>
                {strings.No_Record_Found}
              </span>
            )}
          </Card>
          <Card>
            <p>{strings.Demands_Today}</p>
            {this.state.spinner == true ? (
              <SpinnerWraper>
                <Spinner />
              </SpinnerWraper>
            ) : currectOrderDemandSchemes.length != 0 ? (
              <span>{currectOrderDemandSchemes[0].DEMANDS}</span>
            ) : (
              <span style={{ color: "#504f4f", fontSize: "12px" }}>
                {strings.No_Record_Found}
              </span>
            )}
          </Card>
          <Card>
            <p style={divStyle}>{strings.Published_Schemes}</p>
            {this.state.spinner == true ? (
              <SpinnerWraper>
                <Spinner />
              </SpinnerWraper>
            ) : currectOrderDemandSchemes.length != 0 ? (
              <span style={divStyle}>
                {currectOrderDemandSchemes[0].SCHEMES}
              </span>
            ) : (
              <span style={{ color: "#504f4f", fontSize: "12px" }}>
                {strings.No_Record_Found}
              </span>
            )}
          </Card>
          <Card>
            <p>{strings.Last_Login}</p>
            {this.state.spinner == true ? (
              <SpinnerWraper>
                <Spinner />
              </SpinnerWraper>
            ) : lastLogin.length != "" ? (
              <span>{lastLogin}</span>
            ) : (
              <span style={{ color: "#504f4f", fontSize: "12px" }}>
                {strings.No_Record_Found}
              </span>
            )}
          </Card>
        </ProductCard>

        <ProductChartWraper>
          <ProductChart />
        </ProductChartWraper>
      </ProductsWrapper>
    );
  }
}

export default Dashboard;
