import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../Localization/index";
import schemesImg from "../../../Assets/Images/schemes.png";
import moment from "moment";
import { Redirect } from "react-router-dom";
import Spinner from "./../../../Components/Spinner";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";

import AddLevels from "./AddRecord";

import {
  userService,
  fetchMarketingSchemeHistory,
  deleteMarketingScheme,
  fetchAvailablePrize
} from "../../Config";

import { Auth } from "../../../Auth";
import Modal from "../../../Components/Modal";
import ViewRecord from "./ViewRecord";
import clientImg from "../../../Assets/Images/img_avatar.png";
import NoDataFound from "../../../Components/NoDataFound";

import {
  MarketingSchemesCard,
  MarketingSchemesHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  MarketingSchemesBody,
  ButtonWrapDelete,
  SpinnerConfigData,
  SpinnerApproveReq,
  ErrorMsg,
  ServerError,
  Form,
  FormRow,
  LabelInputGroup,
  FormRowSubmit,
  LabelButtonDU
} from "./style";

class LaunchSchemes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      spinnerApproveReq: false,

      searchByName: "",
      searchByType: "",
      searchBySubsFor: "",
      marketingSchemesDetails: [],
      openViewHandler: false,
      currentItem: [],
      responseStatusAPI: "",
      sortIcon: false,
      fetchErrorMsg: "",
      serverError: {},
      //delete marketing scheme
      openDeleteHandler: false,
      serverErrorDelete: {},
      responseDeleteLevelStatusAPI: "",
      // Add marketing scheme
      addNewMarketingSchemeModal: false,
      //Add milestone
      addMilestoneModal: false,
      fetchAddMarketing: [],
      addMilestoneName: "",
      addMilestoneType: "",
      addMilestoneValue: "",
      addMilestoneTypeId: "",

      mileStoneList: [],
      fetchErrorMsgMilestone: ""
    };
  }

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      fetchErrorMsgMilestone: ""
    });
    if (evt.target.name === "addMilestoneType") {
      const options = evt.target.options;
      const id = options[options.selectedIndex].id;
      // // debugger;
      this.setState({
        addMilestoneValue: evt.target.value,
        addMilestoneTypeId: id
      });
    }
  };

  addMilestoneModal = value => {
    this.setState({
      addMilestoneModal: value
    });
  };
  closeMilestoneModal = e => {
    this.setState({
      addMilestoneModal: false,
      addMilestoneName: "",
      addMilestoneType: "",
      addMilestoneValue: "",
      addMilestoneTypeId: ""
    });
  };

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    // fetch fetchAvailablePrize for milestone
    fetch(fetchAvailablePrize, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res.RESULT))
      .then(addMarketing =>
        this.setState({
          fetchAddMarketing: addMarketing.RESULT
        })
      )
      .catch(err => console.log(err));

    /// fetch view List data
    fetch(fetchMarketingSchemeHistory, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LANGUAGE_ID: "-1"
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            marketingSchemesDetails: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  openViewHandler = (e, currentItems) => {
    // debugger;
    /* for popup Open */
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler,
      currentItem: currentItems
    }));
  };

  closeViewHandler = () => {
    /* for popup close */
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler
    }));
  };

  openDeleteHandler = (e, currentItems) => {
    // debugger;
    this.setState({
      openDeleteHandler: true,
      currentItem: currentItems
    });
  };
  closeDeleteHandler = () => {
    this.setState({
      openDeleteHandler: false,
      responseDeleteLevelStatusAPI: ""
    });
  };

  addNewMarketingSchemeModal = e => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      addNewMarketingSchemeModal: true,
      currentItem: []
    });
  };
  closeMarketingSchemeModal = e => {
    // e.preventDefault();
    // e.stopPropagation();
    this.setState({
      addNewMarketingSchemeModal: false,
      mileStoneList: []
    });
  };

  openEditHandler = (e, currentItems) => {
    this.setState({
      addNewMarketingSchemeModal: true,
      currentItem: currentItems
    });
  };
  closeEditHandler = e => {
    this.setState({
      addNewMarketingSchemeModal: false,
      currentItem: []
    });
  };

  // Delete current Level
  deleteCurrentLevel = () => {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    const deleteLevelKey = this.state.currentItem;
    const aa = this.state.marketingSchemesDetails;

    // debugger;
    fetch(deleteMarketingScheme, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        SCHEME_ID: deleteLevelKey.SCHEME_ID
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          // remove current item from table
          const data = this.state.marketingSchemesDetails.filter(
            i => i.SCHEME_ID !== deleteLevelKey.SCHEME_ID
          );
          this.setState({
            marketingSchemesDetails: data,
            responseDeleteLevelStatusAPI: res,
            currentItem: []
          });

          setTimeout(() => {
            this.setState({
              openDeleteHandler: false
            });
          }, 1500);
        } else {
          this.setState({
            responseDeleteLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorDelete: err
        })
      );
  };

  addMilestone = e => {
    const {
      addMilestoneName,
      addMilestoneType,
      addMilestoneValue,
      addMilestoneTypeId,
      mileStoneList
    } = this.state;
    var order = mileStoneList.length + 1;

    if (addMilestoneName === "" || addMilestoneValue === "") {
      return this.setState({
        fetchErrorMsgMilestone: strings.All_field_must_be_filled_out
      });
    }

    var newData = {
      LEVEL_NAME: addMilestoneName,
      PRIZE_ID: addMilestoneTypeId,
      CONDITION: addMilestoneValue,
      ORDER: order
    };

    this.setState({ mileStoneList: mileStoneList.concat([newData]) });

    this.closeMilestoneModal();
  };

  // add form data in table
  // addNewLevelsHandler = formVal => {
  //   // debugger;
  //   const authBrandId = userService.authBrandId();
  //   const authBrandName = userService.authBrandName();
  //   const authUserName = userService.authUserName();

  //   const { dataItems } = this.state;
  //   var ctr = dataItems.length + 1;

  //   // let addSchemeTypeNameChanged, addStatusChanged;
  //   // if (formVal.addSchemeType === "RECURSIVE") {
  //   //   addSchemeTypeNameChanged = "RE-OCCURED";
  //   // }
  //   // if (formVal.addStatus === "SAVED") {
  //   //   addStatusChanged = "Saved";
  //   // }
  //   ////////////////////////////////////////////////////////
  //   var newData = {
  //     BRAND_ID: authBrandId,
  //     BRAND_NAME: authBrandName,
  //     LANGUAGE_ID: "-1",
  //     SCHEME_DTL_ID: ctr,

  //     CREATED_BY: authUserName,
  //     CREATION_DATE: formVal.addStartDate,
  //     SCHEME_NAME: formVal.addSchemeName,
  //     DESCRIPTION: formVal.addDescription,

  //     SCHEME_ID: ctr,
  //     IMAGE: formVal.imageNameByServer,

  //     UPDATION_DATE: formVal.addStartDate,
  //     STATUS_ID: formVal.addSchemeStatusId,
  //     STATUS_NAME: formVal.addSchemeStatus,

  //     SCHEME_TYPE_ID: formVal.addSchemeTypeId,
  //     SCHEME_TYPE_NAME: formVal.addSchemeType,
  //     START_DATE: formVal.addStartDate,
  //     END_DATE: formVal.addEndDate,

  //     SCHEME_BASE_ID: "VAL_BASED",
  //     SCHEME_BASE_NAME: addSchemeBased,
  //     SCHEME_PERIOD_ID: "MONTHLY",
  //     SCHEME_PERIOD_NAME: addSchemePeriod,
  //     MILESTONES: addMileStoneList,
  //     PRODUCTS: addProductList,
  //     LOCATIONS: addUserlocation,
  //     USERS: addSchemeUser,
  //     USER_LEVELS: addCustomerLevel
  //   };
  //   // debugger;
  //   this.setState({ dataItems: dataItems.concat([newData]) });
  // };

  render() {
    // if (sessionStorage.length == 0) {
    //   return <Redirect to="/login" />;
    // }

    const {
      currentItem,
      spinnerConfigData,
      serverError,
      serverErrorDelete,
      responseDeleteLevelStatusAPI,
      fetchAddMarketing,

      addMilestoneName,
      addMilestoneType,
      addMilestoneValue
    } = this.state;
    // debugger;
    const dataItems = this.state.marketingSchemesDetails;

    return (
      <MarketingSchemesCard>
        <MarketingSchemesHeader>
          <WrapperLeft>{strings.Marketing_Schemes}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              <Button
                classname="green"
                onclick={this.addNewMarketingSchemeModal}
              >
                <Icon name="addCircle" color="white" />
                {strings.Add_Marketing_Scheme}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </MarketingSchemesHeader>

        {/* Modal Popup for View Dealer Details */}
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Marketing_Scheme_Details}
          size="md"
        >
          <ViewRecord currentItem={currentItem} />
        </Modal>

        {/* Modal Popup for Add New Levels */}
        <Modal
          modalOpen={this.state.addNewMarketingSchemeModal}
          onclick={this.closeMarketingSchemeModal}
          title={
            currentItem != ""
              ? strings.Update_Marketing_Schemes
              : strings.Add_New_Marketing_Schemes
          }
        >
          <AddLevels
            // onclickClose={this.closeMarketingSchemeModal}
            // addNewLevels={this.addNewLevelsHandler}
            upd={currentItem}
            addMilestoneModal={this.addMilestoneModal}
            mileStoneList={this.state.mileStoneList}
          />
        </Modal>

        {/* Modal Popup for Delete current Levels */}
        <Modal
          modalOpen={this.state.openDeleteHandler}
          onclick={this.closeDeleteHandler}
          title={strings.Delete_Current_Level}
          size="sm"
        >
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              textTransform: "none"
            }}
          >
            {strings.Are_you_sure_you_want_to_delete_this}
          </h4>

          <ServerError>
            {serverErrorDelete.status} {serverErrorDelete.statusText}
          </ServerError>

          <ButtonWrapDelete>
            <Button classname="red" onclick={this.deleteCurrentLevel}>
              {strings.Yes}
            </Button>
            <Button classname="blue" onclick={this.closeDeleteHandler}>
              {strings.No}
            </Button>
          </ButtonWrapDelete>
          {!(
            responseDeleteLevelStatusAPI == "undefined" ||
            responseDeleteLevelStatusAPI == ""
          ) ? (
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                fontSize: "13px",
                color: "red",
                paddingTop: "10px"
              }}
            >
              {responseDeleteLevelStatusAPI.STATUS +
                ": " +
                responseDeleteLevelStatusAPI.MESSAGE}
            </h4>
          ) : null}
        </Modal>

        {/* Modal Popup for Add Milestone */}
        <Modal
          modalOpen={this.state.addMilestoneModal}
          onclick={this.closeMilestoneModal}
          title="Add Milestone"
          size="md"
        >
          <Form onSubmit={this.addRecord}>
            <FormRow>
              <LabelInputGroup>
                <h5>
                  {strings.Milestone_Level_Name}{" "}
                  <span className="error">*</span>
                </h5>
                <input
                  type="text"
                  name="addMilestoneName"
                  placeholder="Add Milestone Name"
                  value={addMilestoneName}
                  onChange={this.handleChange}
                />
              </LabelInputGroup>

              <LabelInputGroup>
                <h5>
                  {strings.Milestone_Prize_Type}{" "}
                  <span className="error">*</span>
                </h5>

                <select
                  name="addMilestoneType"
                  onChange={this.handleChange}
                  value={addMilestoneType}
                >
                  <option value="default">{strings.Select_prize}</option>
                  {fetchAddMarketing.map((item, index) => (
                    <option
                      key={item.PRIZE_ID}
                      value={item.PRIZE_VALUE}
                      id={item.PRIZE_ID}
                    >
                      {item.NAME}
                    </option>
                  ))}
                </select>
              </LabelInputGroup>
            </FormRow>
            <FormRow>
              <LabelInputGroup>
                <h5>
                  {strings.Milestone_Value} <span className="error">*</span>
                </h5>
                <input
                  type="text"
                  name="addMilestoneValue"
                  placeholder="Add Value"
                  disabled
                  value={addMilestoneValue}
                  onChange={this.handleChange}
                />
              </LabelInputGroup>
            </FormRow>
            <FormRowSubmit>
              <LabelButtonDU>
                <Button onclick={this.addMilestone}>{strings.Submit}</Button>
                <Button onclick={this.closeMilestoneModal}>
                  {strings.Cancel}
                </Button>
              </LabelButtonDU>
            </FormRowSubmit>
            {this.state.fetchErrorMsgMilestone != "" ? (
              <ErrorMsg>{this.state.fetchErrorMsgMilestone}</ErrorMsg>
            ) : null}
          </Form>
        </Modal>

        <MarketingSchemesBody>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="60px" style={{ minWidth: "60px" }}>
                  {strings.Image}
                </th>
                <th width="30%" align="left">
                  {strings.Scheme_Name}
                </th>
                <th width="20%" style={{ minWidth: "120px" }}>
                  {strings.Scheme_Type}
                </th>
                <th width="15%">{strings.Status}</th>

                <th width="15%">{strings.Start_At}</th>
                <th width="15%">{strings.End_Date}</th>
                <th width="80px" style={{ minWidth: "80px" }}>
                  {strings.Action}
                </th>
              </tr>
            </thead>
            <tbody>
              {console.log(dataItems)}
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="7">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : dataItems.length == 0 ? (
                <tr>
                  <td colSpan="7">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                dataItems.map((item, index) => (
                  <tr>
                    <td align="center">
                      {item.IMAGE == "" ? (
                        <img
                          src={schemesImg}
                          alt="Img"
                          className="client-image"
                        />
                      ) : (
                        <img
                          src={item.IMAGE}
                          alt="Img"
                          className="client-image"
                        />
                      )}
                    </td>
                    <td>{item.SCHEME_NAME}</td>

                    <td align="center">{item.SCHEME_TYPE_NAME}</td>

                    <td align="center">{item.STATUS_NAME}</td>
                    <td align="center">
                      {moment(moment(item.START_DATE, "DD-MM-YYYY")).format(
                        "DD/MM/YYYY"
                      )}
                    </td>
                    <td align="center">
                      {moment(moment(item.END_DATE, "DD-MM-YYYY")).format(
                        "DD/MM/YYYY"
                      )}
                    </td>

                    <td align="center">
                      <ButtonGroup>
                        <Icon
                          name="pen"
                          onclick={e => this.openEditHandler(e, item)}
                        />
                        <Icon
                          name="trash"
                          onclick={e => this.openDeleteHandler(e, item)}
                        />
                        <Icon
                          name="eye"
                          onclick={e => this.openViewHandler(e, item)}
                        />
                      </ButtonGroup>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {serverError.status != "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
        </MarketingSchemesBody>
      </MarketingSchemesCard>
    );
  }
}

// LaunchSchemes.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default LaunchSchemes;
