import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { TabsMain, TabGroup, TabLists, TabPanels } from "./style";
// import schemesImg from "../../../../Assets/Images/schemes.png";
import schemesImg from "../../../../Assets/Images/schemes.png";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // debugger;
    const { currentItem } = this.props;
    console.log(currentItem);
    return (
      <TabsMain
        defaultIndex={0}
        selectedTabClassName="is-selected"
        selectedTabPanelClassName="is-selected"
      >
        <TabGroup>
          <TabLists>Scheme Details</TabLists>
          <TabLists>Milestones</TabLists>
          <TabLists>More</TabLists>
        </TabGroup>
        <TabPanels>
          <table className="table record-view-table">
            <tbody>
              <tr>
                <td align="center" colSpan="2">
                  {currentItem.IMAGE == "" ? (
                    <img
                      src={schemesImg}
                      alt="Img"
                      className="client-image-view"
                    />
                  ) : (
                    <img
                      src={currentItem.IMAGE}
                      alt="Img"
                      className="client-image-view"
                    />
                  )}
                </td>
              </tr>
              <tr>
                <td width="30%">
                  <b>Scheme Name </b>
                </td>
                <td>: {currentItem.SCHEME_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>Scheme Type </b>
                </td>
                <td>: {currentItem.SCHEME_TYPE_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>Scheme Status </b>
                </td>
                <td>: {currentItem.STATUS_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>Start Date </b>
                </td>
                <td>
                  :{" "}
                  {moment(new Date(currentItem.START_DATE)).format(
                    "DD-MM-YYYY"
                  )}
                </td>
              </tr>

              <tr>
                <td>
                  <b>End Date </b>
                </td>
                <td>
                  :{" "}
                  {moment(new Date(currentItem.END_DATE)).format("DD-MM-YYYY")}{" "}
                </td>
              </tr>

              <tr>
                <td>
                  <b>Description</b>
                </td>
                <td>: {currentItem.DESCRIPTION}</td>
              </tr>
            </tbody>
          </table>
        </TabPanels>
        <TabPanels>
          <table className="table" width="100%">
            <tr>
              <td align="center" colSpan="2">
                {currentItem.IMAGE == "" ? (
                  <img
                    src={schemesImg}
                    alt="Img"
                    className="client-image-view"
                  />
                ) : (
                  <img
                    src={currentItem.IMAGE}
                    alt="Img"
                    className="client-image-view"
                  />
                )}
              </td>
            </tr>
            <table
              className="table odd-even bordered-table"
              cellPadding="0"
              cellSpacing="0"
            >
              <tr>
                <th align="left">Level Name</th>
                <th align="left">Prize Name</th>
                <th align="left">Prize Value</th>
              </tr>
              {currentItem.MILESTONES.map((item, index) => (
                <tr key={index}>
                  <td>{item.LEVEL_NAME}</td>
                  <td>{item.PRIZE_NAME}</td>
                  <td>{item.PRIZE_VALUE} </td>
                </tr>
              ))}
            </table>
          </table>
        </TabPanels>
        <TabPanels>
          <table className="table record-view-table">
            <tbody>
              <tr>
                <td align="center" colSpan="2">
                  {currentItem.IMAGE == "" ? (
                    <img
                      src={schemesImg}
                      alt="Img"
                      className="client-image-view"
                    />
                  ) : (
                    <img
                      src={currentItem.IMAGE}
                      alt="Img"
                      className="client-image-view"
                    />
                  )}
                </td>
              </tr>

              <tr>
                <td>
                  <b>Scheme Base On </b>
                </td>
                <td>: {currentItem.SCHEME_BASE_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>Scheme Period </b>
                </td>
                <td>: {currentItem.SCHEME_PERIOD_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>Created On </b>
                </td>
                <td>
                  :
                  {moment(new Date(currentItem.CREATION_DATE)).format(
                    "DD-MM-YYYY"
                  )}{" "}
                </td>
              </tr>

              {/* <tr>
                <td>
                  <b>MILESTONES </b>
                </td>
                <td>
                  :
                  <table className="table record-view-table">
                    <tr>
                      <td>LEVEL_NAME</td>
                      <td>PRIZE_NAME</td>
                    </tr>
                  </table>
                </td>
              </tr> */}
            </tbody>
          </table>
        </TabPanels>
      </TabsMain>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
