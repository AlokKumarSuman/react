import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../Localization/index";
import moment from "moment";
import Button from "../../../../Components/Button";
// import Icon from "../../../../Components/Icons";
import Modal from "../../../../Components/Modal";
import { Auth } from "../../../../Auth";
import UploadImage from "./UploadImage";
import { MultiSearchSelect } from "../../../../Components/MultiSearchSelect";

import {
  userService,
  schemeStatus,
  schemeType,
  itemsByBrandId,
  fetchLocationsForSchemes,
  searchCustomerLevel,
  addNewMarketingScheme,
  marketingSchemeBase,
  marketingSchemePeriods,
  marketingSchemeType,
  updateMarketingScheme,
  fetchMarketingMemberType
} from "../../../Config";

import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ErrorMsg,
  ServerError,
  SucessMsg,
  FormRowFullLevels,
  FormRowFullMileStone,
  MilestoneHeader,
  WrapperLeftMilestone,
  WrapperRightMilestone,
  ButtonGroupMilestone,
  FormRowSubmit,
  FormRowDivider,
  RadioGroup
} from "./style";

class AddRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updMultiSearchHide: false,
      //
      searchProduct: "",
      searchMemberTypes: "",
      consumerLevelDD: false,
      productsDD: false,
      MemberTypesDD: false,
      // fetch default values for dropdown
      // fetchSchemeStatus: [],
      fetchSchemeType: [],
      // fetchUserNamesForScheme: [],
      fetchCustomerLevelScheme: [],
      fetchSchemeBased: [],
      fetchSchemePeriod: [],
      fetchAllProduct: [],
      fetchAvailableReward: [],
      fetchMemberTypes: [],
      // setState editable content
      addSchemeName: "",
      addSchemeStatus: "SAVED",
      addSchemeType: "",
      addSchemeBased: "",
      addSchemePeriod: "",
      addSchemeImage: "",

      addSchemeUser: [],
      addUserlocation: [],
      addProductList: [],
      addCategoryTypesList: [],
      addSellerLevel: [],
      addSellerLevelId: [],
      addMileStoneListUpdate: [],

      addStartDate: "",
      addEndDate: "",
      addDescription: "",
      // Assign and setState editable content ID

      addSchemeTypeId: "",
      addSchemeBasedId: "",
      // Server responses
      responseAddSucess: "",
      responseError: "",
      serverErrorStatus: {},
      validateFilds: "",
      // Upload Images
      uploadImageModel: false,
      imageNameByServer: "",
      // On update values
      upd: [],

      disabledEdit: false
    };
  }

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      responseAddSucess: "",
      responseError: "",
      validateFilds: "",
      serverErrorStatus: {}
    });

    if (evt.target.name === "addSchemeType") {
      let options = evt.target.options;
      const id = options[options.selectedIndex].id;
      this.setState({
        addSchemeTypeId: id
      });
    }

    if (evt.target.name === "addSchemeBased") {
      let options = evt.target.options;
      const id = options[options.selectedIndex].id;
      this.setState({
        addSchemeBasedId: id
      });
    }
  };

  addMilestoneModal = e => {
    this.props.addMilestoneModal(true);
  };

  // Component Did Mount
  componentDidMount() {
    const upd = this.props.upd;

    console.log(upd);
    ////
    if (upd != "") {
      // disabledEdit
      if (
        upd.STATUS_ID == "SUBMITTED" ||
        upd.STATUS_ID == "APPROVED" ||
        upd.STATUS_ID == "LIVE" ||
        upd.STATUS_ID == "SUSPENDED" ||
        upd.STATUS_ID == "TERMINATE" ||
        upd.STATUS_ID == "REJECTED" ||
        upd.STATUS_ID == "EXPIRED"
      ) {
        this.setState({
          disabledEdit: true
        });
      } else {
        this.setState({
          disabledEdit: false
        });
      }

      const addStartDateFormated = moment(
        moment(upd.START_DATE, "DD-MM_YYYY")
      ).format("YYYY-MM-DD");

      const addEndDateFormated = moment(
        moment(upd.END_DATE, "DD-MM_YYYY")
      ).format("YYYY-MM-DD");

      // product default selected
      let productDefaultSelected = [];
      for (var i = 0; i < upd.PRODUCTS.length; i++) {
        productDefaultSelected.push(upd.PRODUCTS[i].ITEM_ID.toString());
      }

      // Member Type default selected
      let MemberDefaultSelected = [];
      for (var i = 0; i < upd.MEMBER_TYPES.length; i++) {
        MemberDefaultSelected.push(
          upd.MEMBER_TYPES[i].MEMBER_TYPE_ID.toString()
        );
      }

      // Level Type default selected
      let LevelsDefaultSelected = [];
      for (var i = 0; i < upd.USER_LEVELS.length; i++) {
        LevelsDefaultSelected.push(upd.USER_LEVELS[i].LEVEL_NAME.toString());
      }
      let LevelsDefaultSelectedId = [];
      for (var i = 0; i < upd.USER_LEVELS.length; i++) {
        LevelsDefaultSelectedId.push(upd.USER_LEVELS[i].LEVEL_ID.toString());
      }

      // Location Type default selected
      let LocationDefaultSelected = [];
      for (var i = 0; i < upd.LOCATIONS.length; i++) {
        LocationDefaultSelected.push(upd.LOCATIONS[i].LOCATION_ID.toString());
      }

      // Location Type default selected
      let MilestoneDefaultSelected = [];
      for (var i = 0; i < upd.MILESTONES.length; i++) {
        MilestoneDefaultSelected.push({
          LEVEL_NAME: upd.MILESTONES[i].PRIZE_NAME,
          CONDITION: upd.MILESTONES[i].CONDITION,
          PRIZE_ID: upd.MILESTONES[i].PRIZE_ID,
          ORDER: upd.MILESTONES[i].ORDER
        });
      }

      // debugger;

      this.setState({
        upd: upd,
        updMultiSearchHide: true,
        //
        addSchemeName: upd.SCHEME_NAME,
        addSchemeBased: upd.SCHEME_BASE_NAME,
        addSchemeBasedId: upd.SCHEME_BASE_ID,

        addSchemeImage: upd.IMAGE,
        addSchemeStatus: upd.STATUS_ID,
        addDescription: upd.DESCRIPTION,
        addStartDate: addStartDateFormated,
        addEndDate: addEndDateFormated,

        addSchemePeriod: upd.SCHEME_PERIOD_NAME,

        //
        // SCHEME_BASE_ID: "VAL_BASED"
        // SCHEME_BASE_NAME: "VALUE BASED"
        addSchemeType: upd.SCHEME_TYPE_NAME,
        addSchemeTypeId: upd.SCHEME_TYPE_ID,
        imageNameByServer: upd.IMAGE,
        //
        addProductList: productDefaultSelected,
        addCategoryTypesList: MemberDefaultSelected,
        addSellerLevel: LevelsDefaultSelected,
        addSellerLevelId: LevelsDefaultSelectedId,
        addUserlocation: LocationDefaultSelected,
        addMileStoneListUpdate: MilestoneDefaultSelected
        // addMemberTypesList: MemberDefaultSelected,
        //
        // addLocationList: LocationDefaultSelected,
        //
        // fetchConsumerLevelScheme: upd.USER_LEVELS,
        // selectLevelName: LevelsDefaultSelected,
        // addConsumerLevels: upd.USER_LEVELS

        // addSchemeStatusId: upd.STATUS_ID,
      });
    }
    //////////
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    const languageCode = -1;
    const productInputs = `LANGUAGE_CODE=${languageCode}&BRAND_ID=${authBrandId}`;

    //fetch schemeType //fetch searchCustomerLevel //fetch fetchLocationsForSchemes
    //fetch itemsByBrandId //fetch marketingSchemeBase //fetch marketingSchemePeriods //fetch itemsByBrandId
    Promise.all([
      fetch(marketingSchemeType, {
        method: "GET",
        headers: Auth
      }),
      fetch(searchCustomerLevel, {
        method: "POST",
        headers: Auth,
        body: JSON.stringify({
          BRAND_ID: authBrandId,
          USER_ID: authUserName
        })
      }),
      fetch(itemsByBrandId + productInputs, {
        method: "POST",
        headers: Auth
      }),
      fetch(marketingSchemeBase, {
        method: "GET",
        headers: Auth
      }),
      fetch(marketingSchemePeriods, {
        method: "GET",
        headers: Auth
      }),
      fetch(fetchMarketingMemberType, {
        method: "POST",
        headers: Auth,
        body: JSON.stringify({
          USER_ID: authUserName,
          BRAND_ID: authBrandId,
          LANGUAGE_ID: "-1"
        })
      })
    ])

      .then(([res1, res2, res3, res4, res5, res6]) =>
        Promise.all([
          res1.json(),
          res2.json(),
          res3.json(),
          res4.json(),
          res5.json(),
          res6.json()
        ])
      )
      // .then(([res1, res2, res3, res4, res5, res6, res7]) =>
      //   console.log(res1, res2, res3, res4, res5, res6, res7)
      // )
      .then(
        ([
          addSchemeType,
          addSellerLevel,
          addProductList,
          addSchemeBased,
          addSchemePeriod,
          addCategoryTypesList
        ]) => {
          // product default selected
          let productDefaultSelected = [];
          let productResult = addProductList.RESULT;
          for (var i = 0; i < productResult.length; i++) {
            productDefaultSelected.push(productResult[i].ITEM_ID.toString());
          }
          // member type default selected
          let memberTypeDefaultSelected = addCategoryTypesList.RESULT[0].TYPE_ID.toString();
          //

          let schemePeriodDefaultSelected = addSchemePeriod.response.docs[0].NAME.toString();

          let customerLevelDefaultSelected = addSellerLevel.RESULT[0].NAME.toString();
          let customerLevelDefaultSelectedId = addSellerLevel.RESULT[0].ID.toString();

          let schemeTypeDefaultSelected = addSchemeType.response.docs[0].TYPE_NAME.toString();
          let schemeTypeDefaultSelectedId = addSchemeType.response.docs[0].TYPE_ID.toString();

          let schemeBasedDefaultSelected = addSchemeBased.response.docs[0].NAME.toString();
          let schemeBasedDefaultSelectedId = addSchemeBased.response.docs[0].KEY_ID.toString();

          // debugger;

          if (
            addSellerLevel.STATUS == "SUCCESS" &&
            addProductList.STATUS == "SUCCESS" &&
            addCategoryTypesList.STATUS == "SUCCESS"
          ) {
            if (upd != "") {
              this.setState({
                fetchSchemeType: addSchemeType.response.docs,
                fetchCustomerLevelScheme: addSellerLevel.RESULT,
                fetchAllProduct: addProductList.RESULT,
                fetchSchemeBased: addSchemeBased.response.docs,
                fetchSchemePeriod: addSchemePeriod.response.docs,
                fetchMemberTypes: addCategoryTypesList.RESULT
              });
            } else {
              this.setState({
                fetchSchemeType: addSchemeType.response.docs,
                fetchCustomerLevelScheme: addSellerLevel.RESULT,
                fetchAllProduct: addProductList.RESULT,
                fetchSchemeBased: addSchemeBased.response.docs,
                fetchSchemePeriod: addSchemePeriod.response.docs,
                fetchMemberTypes: addCategoryTypesList.RESULT,
                /////default selected
                addSchemeStatus: "SAVED",
                addSchemeBased: schemeBasedDefaultSelected,
                addSchemeType: schemeTypeDefaultSelected,
                addSchemePeriod: schemePeriodDefaultSelected,
                addProductList: productDefaultSelected,
                addCategoryTypesList: [memberTypeDefaultSelected],
                addSellerLevel: [customerLevelDefaultSelected],

                addSchemeTypeId: schemeTypeDefaultSelectedId,
                addSchemeBasedId: schemeBasedDefaultSelectedId,
                addSellerLevelId: [customerLevelDefaultSelectedId]
              });
            }
          } else {
            this.setState({
              responseError: "Failed to fetch."
            });
          }
        }
      )
      .catch(err => {
        console.log(err);
      });
  }

  // Upload image Modal
  uploadImageModelHandler = e => {
    this.setState({
      uploadImageModel: true,
      inputError: ""
    });
  };
  uploadImageModelHandlerClose = e => {
    this.setState({
      uploadImageModel: false
    });
  };

  displayImageNameHandler = (displayImageName, imageNameByServers) => {
    this.setState({
      addSchemeImage: displayImageName,
      imageNameByServer: imageNameByServers
    });
  };

  // Add New record
  addNewRecord = e => {
    // // debugger;
    e.preventDefault();
    const addMileStoneList = this.props.mileStoneList;

    const {
      addSchemeName,
      addSchemeStatus,
      addSchemeType,
      addSchemeBased,
      addSchemePeriod,
      addSchemeImage,
      addSellerLevel,
      addSellerLevelId,
      addCategoryTypesList,

      addSchemeUser,
      addUserlocation,
      addProductList,

      addStartDate,
      addEndDate,
      addDescription,

      //
      imageNameByServer,
      // addSchemeStatusId,
      addSchemeTypeId,
      addSchemeBasedId
    } = this.state;

    // // debugger;
    if (
      addSchemeName === "" ||
      addSchemeStatus === "" ||
      addSchemeType === "" ||
      addSchemeBased === "" ||
      addSchemePeriod === "" ||
      // addSchemeImage === "" ||
      addStartDate === "" ||
      addEndDate === "" ||
      addDescription === ""
    ) {
      return this.setState({
        validateFilds: strings.All_field_must_be_filled_out
      });
    }

    if (addProductList.length === 0) {
      return this.setState({
        validateFilds: strings.Plaese_add_products
      });
    }

    if (addSellerLevel.length === 0) {
      return this.setState({
        validateFilds: strings.Plaese_add_seller_levels
      });
    }

    if (addUserlocation.length === 0) {
      return this.setState({
        validateFilds: strings.Plaese_add_Locations
      });
    }

    if (addCategoryTypesList.length === 0) {
      return this.setState({
        validateFilds: strings.Plaese_add_Member_Type
      });
    }

    if (addMileStoneList.length == "0") {
      return this.setState({
        validateFilds: strings.Add_Milestone_Values
      });
    }

    const addStartDateFormated = moment(new Date(addStartDate)).format(
      "DD-MM-YYYY"
    );
    const addEndDateFormated = moment(new Date(addEndDate)).format(
      "DD-MM-YYYY"
    );

    // addSellerLevel
    let addConsumerLevelsAdd = [];
    for (var i = 0; i < addSellerLevelId.length; i++) {
      addConsumerLevelsAdd.push(addSellerLevelId[i]);
    }

    const addConsumerLevelsAddString = addConsumerLevelsAdd.toString();
    const addSchemeUserString = addSchemeUser.toString();
    const addUserlocationString = addUserlocation.toString();
    const addProductListString = addProductList.toString();
    const addCategoryTypesListString = addCategoryTypesList.toString();

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    let updateSchemePeriod = "";
    if (addSchemeTypeId == "OT") {
      updateSchemePeriod = "";
    } else {
      updateSchemePeriod = addSchemePeriod;
    }

    // debugger;
    fetch(addNewMarketingScheme, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        LANGUAGE_ID: "-1",

        START_DATE: addStartDateFormated,
        END_DATE: addEndDateFormated,
        STATUS: addSchemeStatus,
        SCHEME_TYPE: addSchemeTypeId,
        SCHEME_BASE: addSchemeBasedId,
        SCHEME_PERIOD: updateSchemePeriod,

        SCHEME_NAME: addSchemeName,
        DESCRIPTION: addDescription,
        IMAGE_URL: imageNameByServer,
        PRODUCTS: addProductListString,
        MEMBER_TYPES: addCategoryTypesListString,
        USERS: "",
        LOCATIONS: addUserlocationString,
        USER_LEVELS: addConsumerLevelsAddString,
        MILESTONES: addMileStoneList
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddSucess: res.MESSAGE
          });

          setTimeout(() => {
            location.reload();
          }, 2000);
        } else {
          this.setState({
            responseError: res.MESSAGE
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorStatus: err
        })
      );
  };
  /////////////////
  // Update Record
  UpdateRecord = e => {
    e.preventDefault();
    // // debugger;
    const {
      addSchemeName,
      addSchemeStatus,
      addSchemeType,
      addSchemeBased,
      addSchemePeriod,
      addSchemeImage,
      addSellerLevel,
      addSellerLevelId,
      addCategoryTypesList,

      addSchemeUser,
      addUserlocation,
      addProductList,

      addStartDate,
      addEndDate,
      addDescription,
      //
      imageNameByServer,
      //
      addMileStoneListUpdate,
      // addSchemeStatusId,
      addSchemeTypeId,
      addSchemeBasedId,
      upd
    } = this.state;
    // debugger;
    if (
      addSchemeName === "" ||
      addSchemeStatus === "" ||
      addSchemeType === "" ||
      addSchemeBased === "" ||
      addSchemePeriod === "" ||
      addStartDate === "" ||
      addEndDate === "" ||
      addDescription === ""
    ) {
      return this.setState({
        validateFilds: strings.All_field_must_be_filled_out
      });
    }
    ////////////////////////////

    if (addProductList.length === 0) {
      return this.setState({
        validateFilds: strings.Plaese_add_products
      });
    }

    if (addSellerLevel.length === 0) {
      return this.setState({
        validateFilds: strings.Plaese_add_seller_levels
      });
    }

    if (addUserlocation.length === 0) {
      return this.setState({
        validateFilds: strings.Plaese_add_Locations
      });
    }

    if (addCategoryTypesList.length === 0) {
      return this.setState({
        validateFilds: strings.Plaese_add_Member_Type
      });
    }

    // const addMileStoneList = this.props.mileStoneList;
    // if (addMileStoneList.length == "0") {
    //   return this.setState({
    //     validateFilds: strings.Add_Milestone_Values
    //   });
    // }

    /////////////////////////////////
    const addStartDateFormated = moment(new Date(addStartDate)).format(
      "DD-MM-YYYY"
    );
    const addEndDateFormated = moment(new Date(addEndDate)).format(
      "DD-MM-YYYY"
    );

    // addSellerLevel
    let addSellerLevelAdd = [];
    for (var i = 0; i < addSellerLevelId.length; i++) {
      addSellerLevelAdd.push(addSellerLevelId[i]);
    }

    const addConsumerLevelsAddString = addSellerLevelAdd.toString();
    const addSchemeUserString = addSchemeUser.toString();
    const addUserlocationString = addUserlocation.toString();
    const addProductListString = addProductList.toString();
    const addCategoryTypesListString = addCategoryTypesList.toString();

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    //
    const schemeID = upd.SCHEME_ID.toString();

    // const schemeBasedUPD = upd.SCHEME_BASE_ID;
    // const schemePeriodUPD = upd.SCHEME_PERIOD_NAME;

    const addMileStoneListString = addMileStoneListUpdate;

    let updateSchemePeriod = "";
    if (addSchemeTypeId == "OT") {
      updateSchemePeriod = "";
    } else {
      updateSchemePeriod = addSchemePeriod;
    }

    // debugger;

    fetch(updateMarketingScheme, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        LANGUAGE_ID: "-1",

        START_DATE: addStartDateFormated,
        END_DATE: addEndDateFormated,
        STATUS: addSchemeStatus,
        SCHEME_TYPE: addSchemeTypeId,

        SCHEME_BASE: addSchemeBasedId,
        SCHEME_PERIOD: updateSchemePeriod,

        SCHEME_NAME: addSchemeName,
        DESCRIPTION: addDescription,
        IMAGE_URL: imageNameByServer,
        PRODUCTS: addProductListString,
        //////
        MEMBER_TYPES: addCategoryTypesListString,
        USERS: "",
        LOCATIONS: addUserlocationString,
        USER_LEVELS: addConsumerLevelsAddString,
        MILESTONES: addMileStoneListString,

        SCHEME_ID: schemeID
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddSucess: res.MESSAGE
          });

          setTimeout(() => {
            location.reload();
          }, 2000);
        } else {
          this.setState({
            responseError: res.MESSAGE
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorStatus: err
        })
      );
  };

  // Product Type Search
  productDDHandler = () => {
    this.setState(prevState => ({
      productsDD: !prevState.productsDD,
      MemberTypesDD: false,
      consumerLevelDD: false
    }));
  };
  searchProductsHandler = e => {
    this.setState({
      searchProduct: e.target.value
    });
  };
  selectProducts = e => {
    if (e.currentTarget.checked) {
      // // debugger;
      const addProductName = this.state.addProductList.slice();
      addProductName.push(e.currentTarget.id);
      this.setState({
        addProductList: addProductName
      });
    } else {
      if (this.state.fetchAllProduct.length > "0") {
        const data = this.state.addProductList.filter(
          i => i !== e.currentTarget.id
        );
        this.setState({
          addProductList: data
        });
      }
    }
  };

  // Member Type Search
  MemberTypesDDHandler = () => {
    this.setState(prevState => ({
      MemberTypesDD: !prevState.MemberTypesDD,
      consumerLevelDD: false,

      productsDD: false
    }));
  };
  searchMemberTypesHandler = e => {
    this.setState({
      searchMemberTypes: e.target.value
    });
  };
  selectMember = e => {
    if (e.currentTarget.checked) {
      // debugger;
      const addMemberTypes = this.state.addCategoryTypesList.slice();
      addMemberTypes.push(e.currentTarget.id);
      this.setState({
        addCategoryTypesList: addMemberTypes
      });
    } else {
      if (this.state.fetchMemberTypes.length > "0") {
        // debugger;
        const data = this.state.addCategoryTypesList.filter(
          i => i !== e.currentTarget.id
        );
        // debugger;
        this.setState({
          addCategoryTypesList: data
        });
      }
    }
  };

  /////// Cunsumer Levels
  consumerLevelDDHandler = () => {
    this.setState(prevState => ({
      consumerLevelDD: !prevState.consumerLevelDD,
      MemberTypesDD: false,
      productsDD: false
    }));
  };

  selectSellerLevels = e => {
    if (e.currentTarget.checked) {
      const addName = this.state.addSellerLevel.slice();
      const addId = this.state.addSellerLevelId.slice();
      // debugger;
      addName.push(e.currentTarget.name);
      addId.push(e.currentTarget.id);

      this.setState({
        addSellerLevel: addName,
        addSellerLevelId: addId
      });
    } else {
      // debugger;
      if (this.state.addSellerLevel.length > "0") {
        const data = this.state.addSellerLevel.filter(
          i => i !== e.currentTarget.name
        );
        const dataId = this.state.addSellerLevelId.filter(
          i => i !== e.currentTarget.id
        );

        this.setState({
          addSellerLevel: data,
          addSellerLevelId: dataId
        });
      }
    }
  };

  //User Location Search Area and dropdown Finish //
  selectLocations = value => {
    this.setState({
      addUserlocation: value
    });
  };

  render() {
    // // debugger;
    const {
      MemberTypesDD,
      fetchSchemeType,
      // fetchUserNamesForScheme,
      fetchCustomerLevelScheme,
      fetchSchemeBased,
      fetchSchemePeriod,
      fetchAllProduct,
      fetchMemberTypes,
      //
      addSchemeName,
      addSchemeStatus,
      addSchemeType,
      addSchemeBased,
      addSchemePeriod,
      addSchemeImage,
      addSchemeUser,
      addSellerLevel,
      addUserlocation,
      addProductList,
      addStartDate,
      addEndDate,
      addDescription,
      addCategoryTypesList,
      ///
      responseAddSucess,
      responseError,
      validateFilds,

      upd,

      serverErrorStatus,
      //
      consumerLevelDD,
      productsDD,
      //
      disabledEdit
    } = this.state;

    // debugger;

    let dataItems = this.state.fetchAllProduct;
    const searchProduct = this.state.searchProduct.trim().toLowerCase();
    if (searchProduct.length > 0) {
      dataItems = this.state.fetchAllProduct.filter(function(i) {
        return i.NAME.toLowerCase().match(searchProduct);
      });
    }

    let memberTypesDataItems = this.state.fetchMemberTypes;
    const searchMemberTypes = this.state.searchMemberTypes.trim().toLowerCase();
    if (searchMemberTypes.length > 0) {
      memberTypesDataItems = this.state.fetchMemberTypes.filter(function(i) {
        return i.TYPE_NAME.toLowerCase().match(searchMemberTypes);
      });
    }

    // Radio group renders based on plan status
    let renderRadioGroup;
    if (
      this.state.addSchemeStatus === "SAVED" ||
      this.state.addSchemeStatus === "SUBMITTED"
    ) {
      renderRadioGroup = (
        <RadioGroup>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="SAVED"
              id="idSaved"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "SAVED" ? true : false}
              disabled={this.state.disabledEdit}
            />
            <label htmlFor="idSaved">Saved</label>
          </div>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="SUBMITTED"
              id="idSubmitted"
              onClick={this.handleChange}
              checked={
                this.state.addSchemeStatus === "SUBMITTED" ? true : false
              }
              // disabled={this.state.disabledEdit}
            />
            <label htmlFor="idSubmitted">Submitted</label>
          </div>
        </RadioGroup>
      );
    } else if (
      this.state.addSchemeStatus === "APPROVED" ||
      this.state.addSchemeStatus === "REJECTED"
    ) {
      renderRadioGroup = (
        <RadioGroup>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="APPROVED"
              id="idApproved"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "APPROVED" ? true : false}
              disabled={
                this.state.addSchemeStatus === "APPROVED"
                  ? false
                  : this.state.disabledEdit
              }
            />
            <label htmlFor="idApproved">Approved</label>
          </div>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="REJECTED"
              id="idRejected"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "REJECTED" ? true : false}
              disabled={
                this.state.addSchemeStatus === "REJECTED"
                  ? false
                  : this.state.disabledEdit
              }
              // disabled={this.state.disabledEdit}
            />
            <label htmlFor="idRejected">Rejected</label>
          </div>
        </RadioGroup>
      );
    } else if (
      this.state.addSchemeStatus === "LIVE" ||
      this.state.addSchemeStatus === "SUSPENDED" ||
      this.state.addSchemeStatus === "TERMINATE"
    ) {
      renderRadioGroup = (
        <RadioGroup>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="SUSPENDED"
              id="idSuspended"
              onClick={this.handleChange}
              checked={
                this.state.addSchemeStatus === "SUSPENDED" ? true : false
              }
            />
            <label htmlFor="idSuspended">Suspended</label>
          </div>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="TERMINATE"
              id="idTerminated"
              onClick={this.handleChange}
              checked={
                this.state.addSchemeStatus === "TERMINATE" ? true : false
              }
            />
            <label htmlFor="idTerminated">Terminate</label>
          </div>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="LIVE"
              id="idLive"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "LIVE" ? true : false}
            />
            <label htmlFor="idLive">Live</label>
          </div>
        </RadioGroup>
      );
    } else if (this.state.addSchemeStatus === "EXPIRED") {
      renderRadioGroup = (
        <RadioGroup>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="EXPIRED"
              id="idExpired"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "EXPIRED" ? true : false}
            />
            <label htmlFor="idExpired">Expired</label>
          </div>
        </RadioGroup>
      );
    }

    let submitButtons;
    /* eslint-disable */
    submitButtons =
      this.state.addSchemeStatus === "SAVED" ||
      this.state.addSchemeStatus === "SUBMITTED" ||
      this.state.addSchemeStatus === "LIVE" ||
      this.state.addSchemeStatus === "SUSPENDED" ||
      this.state.addSchemeStatus === "TERMINATE" ? (
        upd != "" ? (
          (upd.STATUS_ID === "SAVED" &&
            this.state.addSchemeStatus === "SAVED") ||
          (upd.STATUS_ID === "SUBMITTED" &&
            this.state.addSchemeStatus === "SUBMITTED") ||
          (upd.STATUS_ID === "LIVE" &&
            this.state.addSchemeStatus === "LIVE") ? null : (
            <>
              <FormRow>
                <LabelButtonDU>
                  {upd != "" ? (
                    <Button onclick={this.updateRecord}>
                      {strings.Submit}
                    </Button>
                  ) : (
                    <Button onclick={this.addNewRecord}>
                      {strings.Submit}
                    </Button>
                  )}
                  <Button onclick={this.props.onclickClose}>
                    {strings.Cancel}
                  </Button>
                </LabelButtonDU>
              </FormRow>
            </>
          )
        ) : (
          <FormRow>
            <LabelButtonDU>
              {upd != "" ? (
                <Button onclick={this.updateRecord}>{strings.Submit}</Button>
              ) : (
                <Button onclick={this.addNewRecord}>{strings.Submit}</Button>
              )}

              <Button onclick={this.props.onclickClose}>
                {strings.Cancel}
              </Button>
            </LabelButtonDU>
          </FormRow>
        )
      ) : null;
    /* eslint-enable */

    return (
      <>
        <Form onSubmit={this.addRecord}>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Marketing_Scheme_Name} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="addSchemeName"
                placeholder="Add scheme name"
                value={addSchemeName}
                onChange={this.handleChange}
                disabled={disabledEdit}
              />
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.Marketing_Scheme_Based}{" "}
                <span className="error">*</span>
              </h5>

              <select
                name="addSchemeBased"
                onChange={this.handleChange}
                value={addSchemeBased}
                disabled={disabledEdit}
              >
                {fetchSchemeBased.map((item, index) => (
                  <option key={index} value={item.NAME} id={item.KEY_ID}>
                    {item.NAME}
                  </option>
                ))}
              </select>
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Marketing_Start_Date} <span className="error">*</span>
              </h5>
              <input
                type="date"
                name="addStartDate"
                placeholder="Start at"
                value={addStartDate}
                onChange={this.handleChange}
                disabled={disabledEdit}
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Marketing_End_Date} <span className="error">*</span>
              </h5>
              <input
                type="date"
                name="addEndDate"
                placeholder="End at"
                value={addEndDate}
                onChange={this.handleChange}
                disabled={
                  this.state.addSchemeStatus != "SUSPENDED"
                    ? disabledEdit
                    : null
                }
              />
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup>
              <div style={{ marginBottom: "1.2rem" }}>
                <h5>
                  {strings.Marketing_Scheme_Image}{" "}
                  <span className="error">*</span>
                </h5>
                <div className="input-group-addon-left">
                  <span
                    class="input-group blue-button"
                    onClick={this.uploadImageModelHandler}
                    disabled={disabledEdit}
                  >
                    {strings.Browse}
                  </span>
                  <input
                    type="text"
                    name="addSchemeImage"
                    placeholder="upload image"
                    value={addSchemeImage}
                    onChange={this.onChange}
                    autoComplete="off"
                    disabled
                  />
                </div>
              </div>

              <div>
                <h5>
                  {strings.Marketing_Scheme_Type}{" "}
                  <span className="error">*</span>
                </h5>

                <select
                  name="addSchemeType"
                  onChange={this.handleChange}
                  value={addSchemeType}
                  disabled={disabledEdit}
                >
                  {fetchSchemeType.map((item, index) => (
                    <option
                      key={index}
                      value={item.TYPE_NAME}
                      id={item.TYPE_ID}
                    >
                      {item.TYPE_NAME}
                    </option>
                  ))}
                </select>
              </div>
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Marketing_Description} <span className="error">*</span>
              </h5>

              <textarea
                rows="4"
                cols="50"
                name="addDescription"
                placeholder={strings.Description}
                value={addDescription}
                onChange={this.handleChange}
                disabled={disabledEdit}
              />
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Marketing_Scheme_Status}{" "}
                <span className="error">*</span>
              </h5>
              {renderRadioGroup}
              {/* <LabelInputGroup>
             
              <select
                name="addSchemeStatus"
                onChange={this.handleChange}
                value={addSchemeStatus}
              >
                <option value="SAVED">SAVED</option>
                <option value="SUBMITTED">SUBMITTED</option>
              </select>
            </LabelInputGroup> */}
            </LabelInputGroup>
            {this.state.addSchemeTypeId != "OT" ? (
              <LabelInputGroup>
                <h5>
                  {strings.Marketing_Scheme_Period}{" "}
                  <span className="error">*</span>
                </h5>
                <select
                  name="addSchemePeriod"
                  onChange={this.handleChange}
                  value={addSchemePeriod}
                  disabled={disabledEdit}
                >
                  {fetchSchemePeriod.map((item, index) => (
                    <option
                      key={index}
                      value={item.KEY_ID}
                      // id={item.SCHEME_TYPE_ID}
                    >
                      {item.NAME}
                    </option>
                  ))}
                </select>
              </LabelInputGroup>
            ) : null}
          </FormRow>
          {/* /////////////// */}
          <FormRowDivider>
            <WrapperLeftMilestone>Select User locations</WrapperLeftMilestone>
          </FormRowDivider>
          {upd != "" ? (
            <FormRowFullLevels>
              <div className="selected-locations">
                <h5>Current selected locations</h5>
                {disabledEdit ? null : (
                  <span
                    onClick={e => this.setState({ updMultiSearchHide: false })}
                    disabled={disabledEdit}
                  >
                    Change location
                  </span>
                )}

                <select>
                  {upd.LOCATIONS.map(item => (
                    <option>{item.LOCATION_NAME}</option>
                  ))}
                </select>
              </div>
            </FormRowFullLevels>
          ) : null}
          {this.state.updMultiSearchHide ? null : (
            <FormRowFullLevels>
              <MultiSearchSelect recievedLocationsId={this.selectLocations} />
            </FormRowFullLevels>
          )}{" "}
          <FormRowDivider>
            <WrapperLeftMilestone>
              {strings.Scope_Constrants}
            </WrapperLeftMilestone>
          </FormRowDivider>
          <FormRow>
            <LabelInputGroup productsDD={productsDD}>
              <h5>
                {strings.Marketing_Products} <span className="error">*</span>
              </h5>
              <div style={{ position: "relative" }}>
                <h4 onClick={this.productDDHandler} disabled={disabledEdit}>
                  {addProductList.length > 0
                    ? `${addProductList.length} Items Selected`
                    : "Select"}
                </h4>
                <ul className="productsDefine">
                  <li>
                    <input
                      type="text"
                      value={this.state.searchProduct}
                      onChange={this.searchProductsHandler}
                      placeholder="Search here..."
                    />
                  </li>
                  {dataItems.map((item, index) => (
                    <li key={item.ITEM_ID}>
                      <label>
                        <input
                          type="checkbox"
                          name={item.NAME}
                          onClick={this.selectProducts}
                          id={item.ITEM_ID}
                          checked={addProductList.includes(
                            item.ITEM_ID.toString()
                          )}
                        />

                        {item.NAME}
                      </label>
                    </li>
                  ))}
                </ul>
              </div>
            </LabelInputGroup>
            <LabelInputGroup MemberTypesDD={MemberTypesDD}>
              <h5>
                {strings.Category_Type} <span className="error">*</span>
              </h5>
              <div style={{ position: "relative" }}>
                <h4 onClick={this.MemberTypesDDHandler} disabled={disabledEdit}>
                  {addCategoryTypesList.length > 0
                    ? `${addCategoryTypesList.length} Items Selected`
                    : "Select"}
                </h4>
                <ul className="memberTypes">
                  <li>
                    <input
                      type="text"
                      value={this.state.memberTypesDataItems}
                      onChange={this.searchMemberTypesHandler}
                      placeholder="Search here..."
                    />
                  </li>

                  {memberTypesDataItems.map((item, index) => (
                    <li key={item.TYPE_ID}>
                      <label>
                        <input
                          type="checkbox"
                          name={item.TYPE_NAME}
                          onClick={this.selectMember}
                          id={item.TYPE_ID}
                          checked={addCategoryTypesList.includes(
                            item.TYPE_ID.toString()
                          )}
                        />
                        {item.TYPE_NAME}
                      </label>
                    </li>
                  ))}
                </ul>
              </div>
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup consumerLevelDD={consumerLevelDD}>
              <h5>
                {strings.Seller_Levels} <span className="error">*</span>
              </h5>

              <div style={{ position: "relative" }}>
                <h4
                  onClick={this.consumerLevelDDHandler}
                  disabled={disabledEdit}
                >
                  {addSellerLevel.length > 0
                    ? `${addSellerLevel.length} Items Selected`
                    : "Select"}
                </h4>
                <ul className="consumerLevel">
                  {/* <li>
                        <input
                          type="text"
                          value={this.state.memberTypesDataItems}
                          onChange={this.searchMemberTypesHandler}
                          placeholder="Search here..."
                        />
                      </li> */}

                  {fetchCustomerLevelScheme.map((item, index) => (
                    <li key={item.ID}>
                      <label>
                        <input
                          type="checkbox"
                          name={item.NAME}
                          onClick={this.selectSellerLevels}
                          id={item.ID}
                          checked={addSellerLevel.includes(
                            item.NAME.toString()
                          )}
                        />
                        {item.NAME}
                      </label>
                    </li>
                  ))}
                </ul>
              </div>
            </LabelInputGroup>
          </FormRow>
          <FormRowFullMileStone>
            <MilestoneHeader>
              <WrapperLeftMilestone>
                {strings.Milestones} <span className="error">*</span>
              </WrapperLeftMilestone>
              <WrapperRightMilestone>
                <ButtonGroupMilestone>
                  {upd == "" ? (
                    <Button classname="green" onclick={this.addMilestoneModal}>
                      {strings.Add_Milestones}
                    </Button>
                  ) : null}
                </ButtonGroupMilestone>
              </WrapperRightMilestone>
            </MilestoneHeader>
            {this.props.mileStoneList.length != "0" ? (
              <table className="table milestone-table">
                <tr>
                  <th align="left">Order</th>
                  <th align="left">Level Name</th>
                  <th align="left">Prize Id</th>
                  <th align="left">Consition</th>
                </tr>
                {this.props.mileStoneList.map((item, index) => (
                  <tr>
                    <td>{item.ORDER}</td>
                    <td>{item.LEVEL_NAME}</td>
                    <td>{item.PRIZE_ID}</td>
                    <td>{item.CONDITION}</td>
                  </tr>
                ))}
              </table>
            ) : null}

            {upd != "" ? (
              <table className="table milestone-table">
                <tr>
                  <th align="left">Order</th>
                  <th align="left">Level Name</th>
                  <th align="left">Prize Name</th>
                  <th align="left">Prize Value</th>
                  <th align="left">Consition</th>
                </tr>
                {upd.MILESTONES.map((item, index) => (
                  <tr key={index}>
                    <td>{item.ORDER}</td>
                    <td>{item.LEVEL_NAME}</td>
                    <td>{item.PRIZE_NAME}</td>

                    <td>{item.PRIZE_VALUE}</td>

                    <td>{item.CONDITION}</td>
                  </tr>
                ))}
              </table>
            ) : null}
          </FormRowFullMileStone>
          {/* end with limit */}
          {/* Submit button */}
          {submitButtons}
          {/* ///// */}
          {responseAddSucess != "" ? (
            <SucessMsg>{responseAddSucess}</SucessMsg>
          ) : null}
          {responseError != "" ? <ErrorMsg>{responseError}</ErrorMsg> : null}
          {serverErrorStatus.status == "" ? (
            <ServerError>
              {serverErrorStatus.status} {serverErrorStatus.statusText}
            </ServerError>
          ) : null}
          {!(validateFilds === "") ? (
            <ErrorMsg>{validateFilds}</ErrorMsg>
          ) : null}
          {/* ////////////// */}
          {this.state.uploadImageModel == true ? (
            <Modal
              modalOpen={this.state.uploadImageModel}
              onclick={this.uploadImageModelHandlerClose}
              title="Upload Scheme Image"
              size="sm"
            >
              <UploadImage
                onclickClose={this.uploadImageModelHandlerClose}
                displayImageName={this.displayImageNameHandler}
              />
            </Modal>
          ) : null}
        </Form>
      </>
    );
  }
}

// AddRecord.propTypes = {
//   addRec: PropTypes.func,
//   onclickClose: PropTypes.func
// };

export default AddRecord;
