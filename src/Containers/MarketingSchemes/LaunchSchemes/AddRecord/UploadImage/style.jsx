import styled from "styled-components";

export const AddImage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  input {
    border: none;
    box-shadow: none;
    padding: 0px;
    text-align: center;
    display: flex;
    align-self: center;
    margin: 0 auto;
    width: 30rem;
  }
`;
export const Preview = styled.div`
  width: 300px;
  height: 200px;
  border: 1px solid #333;
  margin: 10px auto;
  overflow: hidden;

  .previewText {
    font-size: 13px;
    padding: 10px;
    color: #999;
  }

  img {
    width: 100%;
    display: block;
  }
`;
export const ErrorMsg = styled.div`
  color: red;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 0 auto;
  text-align: center;
`;