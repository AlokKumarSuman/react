import styled from "styled-components";

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  span[disabled] {
    pointer-events: none;
  }
  h4[disabled] {
    pointer-events: none;
    background: #f9f0f0;
  }

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;

export const FormRowSubmit = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;
  margin-top: 2.2rem;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  h4 {
    border: 1px solid #ccc;
    padding: 7px 10px;
    border-radius: 4px;
    width: 364px;
    color: #939393;
  }

  ul.consumerLevel {
    width: 100%;
    li {
      display: inline-block;
      width: 40%;
    }
  }

  ul.productsDefine {
    display: ${props => (props.productsDD === false ? "none" : "block")};
    max-height: 20rem;
    overflow-y: auto;
    position: absolute;
    background: #eaeaea;
    width: 100%;
    border: solid 1px #c5c5c5;
    padding: 4px;
    border-top-right-radius: 2px;
    border-top-left-radius: 2px;
    z-index: 9;

    li {
      display: block;
      padding: 5px 0px;

      input[type="text"] {
        width: 100%;
        height: 30px;
      }

      input[type="checkbox"] {
        margin-right: 5px;
        width: 14px;
        height: 14px;
      }

      p {
        display: inline-block;
      }
    }
  }

  ul.consumerLevel {
    display: ${props => (props.consumerLevelDD === false ? "none" : "block")};

    max-height: 20rem;
    overflow-y: auto;
    position: absolute;
    background: #eaeaea;
    width: 100%;
    border: solid 1px #c5c5c5;
    padding: 4px;
    border-top-right-radius: 2px;
    border-top-left-radius: 2px;
    z-index: 9;

    li {
      display: block;
      padding: 5px 0px;

      input[type="text"] {
        width: 100%;
        height: 30px;
      }

      input[type="checkbox"] {
        margin-right: 5px;
        width: 14px;
        height: 14px;
      }

      p {
        display: inline-block;
      }
    }
  }

  ul.memberTypes {
    display: ${props => (props.MemberTypesDD === false ? "none" : "block")};
    //
    max-height: 20rem;
    overflow-y: auto;
    position: absolute;
    background: #eaeaea;
    width: 100%;
    border: solid 1px #c5c5c5;
    padding: 4px;
    border-top-right-radius: 2px;
    border-top-left-radius: 2px;
    z-index: 9;

    li {
      display: block;
      padding: 5px 0px;

      input[type="text"] {
        width: 100%;
        height: 30px;
      }

      input[type="checkbox"] {
        margin-right: 5px;
        width: 14px;
        height: 14px;
      }

      p {
        display: inline-block;
      }
    }
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.2rem;
    }
  }
`;

export const FormRowFullLevels = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;
  position: relative;

  & > div {
    width: 100%;
  }
  & > div > div:first-child {
    width: 49%;
  }
  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;
  }
  .milestone-table th,
  .milestone-table td {
    height: auto;
  }
  .selected-locations {
    width: 49%;
    h5 {
      display: inline-block;
      max-width: 100%;
      margin-bottom: 5px;
      font-weight: 600;
      color: #333;
      font-size: 1.4rem;
      font-family: "Gotham";
      font-weight: 500;
      letter-spacing: 1px;
    }
    span {
      color: #2dc3e8;
      font-size: 11px;
      float: right;
      margin-top: 6px;
      cursor: pointer;
    }
  }
`;
export const FormRowFullMileStone = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;
  & > div {
    width: 100%;
    background: #c5c5c5;
    height: 32px;
    margin-bottom: 1.2rem;
  }
  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
  }
  .milestone-table th,
  .milestone-table td {
    height: auto;
  }
`;

export const LabelButtonDU = styled.div`
  width: auto !important;
  display: flex;
  margin: 0 auto;
  float: none !important;
  div {
    width: auto;
    margin: 0 auto;
    display: inline-block;
    min-width: 151px;
  }
  div:first-child {
    margin-right: 20px;
  }
`;

export const ImageRatio = styled.div`
  object-fit: cover;
  width: 80px;
`;
export const ErrorMsg = styled.div`
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
export const SucessMsg = styled.div`
  display: flex;
  justify-content: center;
  color: green;
  font-size: 13px;
`;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const MilestoneHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  background-color: #f0f3f5;
  width: 100%;
  height: 40px;
`;
export const WrapperLeftMilestone = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const WrapperRightMilestone = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const ButtonGroupMilestone = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  div {
    margin-right: 1rem;
  }
  div:last-child {
    margin-right: 0;
  }
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: auto;
    padding: 0.6rem 0.8rem;
  }
  div svg.icon {
    margin-right: 0.6rem;
    margin-top: 0px;
    width: 14px;
    height: 14px;
    transform: scale(1.5);
  }
  div svg.icon-fa {
    margin-right: 0.6rem;
  }
`;
export const FormRowDivider = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background: #c5c5c5;
  height: 32px;
  margin-bottom: 1.2rem;
  justify-content: center;
  padding: 0.6rem;

  .milestone-table th,
  .milestone-table td {
    height: auto;
  }
`;
export const RadioGroup = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-around;
  flex-direction: row;
  padding-top: 6px;
`;
