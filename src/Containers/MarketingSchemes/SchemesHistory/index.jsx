import React, { Component } from "react";
import PropTypes from "prop-types";

import { strings } from "./../../../Localization/index";
import { Redirect } from "react-router-dom";
import Spinner from "../../../Components/Spinner";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";
import Modal from "../../../Components/Modal";
import NoDataFound from "../../../Components/NoDataFound";
import { userService, fetchMarketingSchemeHistory } from "../../Config";
import { Auth } from "../../../Auth";

import {
  AuthLevelsCard,
  AuthLevelsHeader,
  ButtonGroup,
  WrapperLeft,
  AuthLevelsBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError
} from "./style";
import ViewRecord from "./ViewRecord";

class SchemesHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      allMarketingData: [],
      responseStatusAPI: "",
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      responseDeleteLevelStatusAPI: "",
      openViewHandler: false,
      currentRowData: []
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(fetchMarketingSchemeHistory, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LANGUAGE_ID: "-1"
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      // .then(res => console.log(res))

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            allMarketingData: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  openViewHandler = (e, currentItems) => {
    // debugger;
    this.setState({
      openViewHandler: true,
      currentRowData: currentItems
    });
  };

  closeViewHandler = () => {
    this.setState({
      openViewHandler: false
    });
  };

  render() {
    // if (sessionStorage.length == 0) {
    //   return <Redirect to="/login" />;
    // }

    const {
      allMarketingData,
      currentRowData,
      spinnerConfigData,
      serverError
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Marketing_Scheme_Details}
        >
          <ViewRecord currentItem={currentRowData} />
        </Modal>

        <AuthLevelsCard>
          <AuthLevelsHeader>
            <WrapperLeft>{strings.Marketing_Scheme_History}</WrapperLeft>
          </AuthLevelsHeader>

          <AuthLevelsBody toggleFilter={this.state.filterShow}>
            <table className="table bordered odd-even">
              <thead>
                <tr>
                  <th width="10%" align="left">
                    {strings.Image}
                  </th>
                  <th width="30%" align="left">
                    {strings.Scheme_Name}
                  </th>
                  <th width="15%" align="left">
                    {strings.Start_Date}
                  </th>
                  <th width="15%" align="left">
                    {strings.End_Date}
                  </th>
                  <th width="20%" align="center">
                    {strings.Scheme_Subscribers}
                  </th>
                  <th width="80px" style={{ minWidth: "80px" }}>
                    {strings.Action}
                  </th>
                </tr>
              </thead>
              <tbody>
                {spinnerConfigData == true ? (
                  <tr>
                    <td colSpan="6">
                      <SpinnerConfigData>
                        <Spinner />
                      </SpinnerConfigData>
                    </td>
                  </tr>
                ) : allMarketingData.length == 0 ? (
                  <tr>
                    <td colSpan="6">
                      <NoDataFound />
                    </td>
                  </tr>
                ) : (
                  allMarketingData.map((item, index) => (
                    <tr>
                      <td>
                        <img src={item.IMAGE} alt="image" />
                      </td>
                      <td>{item.SCHEME_NAME}</td>
                      <td>{item.START_DATE}</td>
                      <td>{item.END_DATE}</td>
                      <td align="center">{item.SCHEME_SUBSCRIBERS}</td>
                      <td align="center" width="80px">
                        <ButtonGroup>
                          <Icon
                            name="eye"
                            onclick={e => this.openViewHandler(e, item)}
                          />
                        </ButtonGroup>
                      </td>
                    </tr>
                  ))
                )}
              </tbody>
            </table>
            {this.state.fetchErrorMsg != "" ? (
              <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
            ) : null}
            {this.state.serverError.statusText == "" ? (
              <ServerError>
                {serverError.status} {serverError.statusText}
              </ServerError>
            ) : null}
          </AuthLevelsBody>
        </AuthLevelsCard>
      </React.Fragment>
    );
  }
}

// ViewDealer.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default SchemesHistory;
