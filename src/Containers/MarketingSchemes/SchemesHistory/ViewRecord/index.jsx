import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../Localization/index";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { currentItem } = this.props;
    return (
      <table className="table record-view-table">
        <tbody>
          <tr>
            <td>
              <b>{strings.Scheme_Name}</b>
            </td>
            <td>: {currentItem.SCHEME_NAME}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Creation_Date}</b>
            </td>
            <td>: {currentItem.CREATION_DATE}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Updation_Date} </b>
            </td>
            <td>: {currentItem.UPDATION_DATE}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Start_Date}</b>
            </td>
            <td>: {currentItem.START_DATE}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.End_Date}</b>
            </td>
            <td>: {currentItem.END_DATE}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Scheme_Subscribers}</b>
            </td>
            <td>:{currentItem.SCHEME_SUBSCRIBERS}</td>
          </tr>
          <tr>
            <td>
              <b>{strings.Description}</b>
            </td>
            <td>: {currentItem.DESCRIPTION}</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
