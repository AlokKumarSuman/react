import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../Localization/index";
import moment from "moment";
import { Redirect } from "react-router-dom";
import Spinner from "./../../../Components/Spinner";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";

import PushNewMarketingScheme from "./AddRecord";

import { userService, fetchPushedMarketingSchemes } from "../../Config";

import { Auth } from "../../../Auth";
import Modal from "../../../Components/Modal";
import NoDataFound from "../../../Components/NoDataFound";

import {
  MarketingSchemesCard,
  MarketingSchemesHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  MarketingSchemesBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError
} from "./style";

class PushSchemes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pushedMarketingSchemes: [],
      // push marketing scheme
      pushNewMarketingSchemeModal: false,
      // error
      spinnerConfigData: true,
      fetchErrorMsg: "",
      serverError: {}
    };
  }

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value
    });
  };

  pushNewMarketingSchemeModal = e => {
    this.setState({
      pushNewMarketingSchemeModal: true
    });
  };
  closeMarketingSchemeModal = e => {
    this.setState({
      pushNewMarketingSchemeModal: false,

      mileStoneList: []
    });
  };

  componentDidMount() {
    // debugger;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch(fetchPushedMarketingSchemes, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            pushedMarketingSchemes: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  // add form data in table
  addNewLevelsHandler = formVal => {
    const { pushedMarketingSchemes } = this.state;
    var currDate = moment(new Date()).format("DD-MM-YYYY");
    var newData = {
      SUBJECT: formVal.addSchemeSubject,
      MESSAGE: formVal.addDescription,
      IMAGE_URL: formVal.imageNameByServer,
      SENT_DATE: currDate,
      STATUS: "SENT"
    };
    // debugger;
    this.setState({
      pushedMarketingSchemes: pushedMarketingSchemes.concat([newData])
    });
  };

  render() {
    // if (sessionStorage.length == 0) {
    //   return <Redirect to="/login" />;
    // }

    const {
      pushedMarketingSchemes,
      spinnerConfigData,
      serverError,
      fetchErrorMsg
    } = this.state;
    // debugger;

    return (
      <MarketingSchemesCard>
        <MarketingSchemesHeader>
          <WrapperLeft>{strings.Push_Marketing_Schemes}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              <Button
                classname="green"
                // onclick={this.pushNewMarketingSchemeModal}
                onclick={this.pushNewMarketingSchemeModal}
              >
                <Icon name="addCircle" color="white" />{" "}
                {strings.Push_New_Marketing_Scheme}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </MarketingSchemesHeader>
        {/* Modal Popup for Add New Levels */}
        <Modal
          modalOpen={this.state.pushNewMarketingSchemeModal}
          onclick={this.closeMarketingSchemeModal}
          title={strings.Push_New_Marketing_Scheme}
        >
          <PushNewMarketingScheme
            onclickClose={this.closeMarketingSchemeModal}
            addNewLevels={this.addNewLevelsHandler}
          />
        </Modal>

        <MarketingSchemesBody>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="60px" style={{ minWidth: "60px" }}>
                  {strings.Image}
                </th>
                <th width="30%" align="left">
                  {strings.Subject}
                </th>
                <th width="40%" align="left">
                  {strings.Message}
                </th>
                <th width="15%">{strings.Status} </th>

                <th width="15%">{strings.Sent_Date}</th>
              </tr>
            </thead>
            <tbody>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="5">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : pushedMarketingSchemes.length == 0 ? (
                <tr>
                  <td colSpan="5">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                pushedMarketingSchemes.map((item, index) => (
                  <tr>
                    <td align="center">
                      <img
                        src={item.IMAGE_URL}
                        alt="Img"
                        className="client-image"
                      />
                    </td>
                    <td>{item.SUBJECT}</td>

                    <td>{item.MESSAGE}</td>

                    <td align="center">
                      <Button type="primary" size="fullwidth" classname="green">
                        {item.STATUS}
                      </Button>
                    </td>
                    <td align="center">
                      {/* {moment(new Date(item.SENT_DATE)).format("DD-MM-YYYY")} */}
                      {item.SENT_DATE}
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {serverError.status != "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
          {fetchErrorMsg != "" ? <ErrorMsg>{fetchErrorMsg}</ErrorMsg> : null}
        </MarketingSchemesBody>
      </MarketingSchemesCard>
    );
  }
}

export default PushSchemes;
