import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../Localization/index";
import moment from "moment";

import Button from "../../../../Components/Button";
// import Icon from "../../../../Components/Icons";
import Modal from "../../../../Components/Modal";

import { Auth } from "../../../../Auth";

import UploadImage from "./UploadImage";

import { userService, pushMarketingNotification } from "../../../Config";

import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ErrorMsg,
  ServerError,
  SucessMsg,
  FormRowFull,
  FormRowSubmit
} from "./style";

class AddRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addSchemeSubject: "",
      addSchemeImage: "",
      addDescription: "",
      // upload image
      imageNameByServer: "",
      uploadImageModel: false,
      // Validate fields
      validateFilds: "",
      // Server responses
      responseAddSucess: "",
      responseAddError: "",
      serverErrorStatus: {}
      //
    };
  }

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value
    });
  };

  addNewRecord = e => {
    e.preventDefault();
    // debugger;

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    const {
      addSchemeSubject,
      addSchemeImage,
      addDescription,
      // upload image
      imageNameByServer
    } = this.state;

    if (
      addSchemeSubject === "" ||
      addSchemeImage === "" ||
      addDescription === ""
    ) {
      return this.setState({
        validateFilds: strings.All_field_must_be_filled_out
      });
    }

    var formValue = {
      addSchemeSubject,
      addSchemeImage,
      addDescription,
      imageNameByServer
    };
    // debugger;

    fetch(pushMarketingNotification, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        'BRAND_ID': "authBrandId",
        "USER_ID": "authUserName",
        "TENANT_ID": "authTenantId",
        "SUBJECT": "addSchemeSubject",
        "MESSAGE": "addDescription",
        "DATA": "imageNameByServer"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddSucess: res.MESSAGE,

            addSchemeSubject: "",
            addSchemeImage: "",
            addDescription: "",
            imageNameByServer: ""
          });

          setTimeout(() => {
            this.props.addNewLevels(formValue);
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddError: res.MESSAGE
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorStatus: err
        })
      );
  };

  // Upload image Modal
  uploadImageModelHandler = e => {
    // debugger;
    this.setState({
      uploadImageModel: true
    });
  };
  uploadImageModelHandlerClose = e => {
    this.setState({
      uploadImageModel: false
    });
  };

  displayImageNameHandler = (displayImageName, imageNameByServers) => {
    // debugger;
    this.setState({
      addSchemeImage: displayImageName,
      imageNameByServer: imageNameByServers
    });
  };

  render() {
    // debugger;
    const {
      addSchemeName,
      addSchemeImage,
      addDescription,
      // image by server
      imageNameByServer,
      // validate fields
      validateFilds,
      //add response
      responseAddSucess,
      responseAddError,
      serverErrorStatus
    } = this.state;

    return (
      <>
        <Form onSubmit={this.addRecord}>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Push_Scheme_Subject} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="addSchemeSubject"
                placeholder="Add scheme name"
                value={addSchemeName}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Push_Scheme_Image} <span className="error">*</span>
              </h5>
              <div className="input-group-addon-left">
                <span
                  className="input-group blue-button"
                  onClick={this.uploadImageModelHandler}
                >
                  {strings.Browse}
                </span>
                <input
                  type="text"
                  name="addSchemeImage"
                  placeholder="upload image"
                  value={addSchemeImage}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </div>
            </LabelInputGroup>
          </FormRow>
          <FormRowFull>
            <LabelInputGroup>
              <h5>
                {strings.Push_Scheme_Message} <span className="error">*</span>
              </h5>
              <textarea
                name="addDescription"
                placeholder="Add Description"
                value={addDescription}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
          </FormRowFull>

          <FormRowSubmit>
            <LabelButtonDU>
              <Button onclick={this.addNewRecord}>
                {strings.Push_Schemes}
              </Button>

              <Button onclick={this.props.onclickClose}>
                {strings.Cancel}
              </Button>
            </LabelButtonDU>
          </FormRowSubmit>
          {/* -- Responses and Errors -- */}
          {validateFilds != "" ? <ErrorMsg>{validateFilds}</ErrorMsg> : null}

          {responseAddError != "" ? (
            <ErrorMsg>{responseAddError}</ErrorMsg>
          ) : null}

          {responseAddSucess != "" ? (
            <SucessMsg>{responseAddSucess}</SucessMsg>
          ) : null}
          {serverErrorStatus.status == "" ? (
            <ServerError>
              {serverErrorStatus.status} {serverErrorStatus.statusText}
            </ServerError>
          ) : null}

          {/* -- Upload images -- */}
          {this.state.uploadImageModel == true ? (
            <Modal
              modalOpen={this.state.uploadImageModel}
              onclick={this.uploadImageModelHandlerClose}
              title={strings.Upload_Scheme_Image}
              size="sm"
            >
              <UploadImage
                onclickClose={this.uploadImageModelHandlerClose}
                displayImageName={this.displayImageNameHandler}
              />
            </Modal>
          ) : null}
        </Form>
      </>
    );
  }
}

// AddRecord.propTypes = {
//   addRec: PropTypes.func,
//   onclickClose: PropTypes.func
// };

export default AddRecord;
