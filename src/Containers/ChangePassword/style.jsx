import styled from "styled-components";

export const ProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
`;

export const ProfileHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1.5rem 2rem;
  background-color: #f0f3f5;
  width: 100%;
`;

export const ProfileBody = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 3rem 4rem;
  background-color: white;
`;

export const Form = styled.div`
  width: 100%;
`;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.8rem;
    }
  }
`;
export const LabelButton = styled.div`
  padding-top: 2rem;
  width: 100% !important;
  float: none !important;
  div {
    margin: 0 auto;
    width: 180px;
  }
`;
export const ErrorMsg = styled.div`
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
export const ErrorRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;
`;
