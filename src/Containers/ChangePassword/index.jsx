import React, { Component } from "react";
import { strings } from "./../../Localization/index";

import { userService, changePassword } from "../Config";
import { Auth } from "../../Auth";

import {
  ProfileWrapper,
  ProfileHeader,
  ProfileBody,
  Form,
  FormRow,
  LabelInputGroup,
  LabelButton,
  ErrorMsg,
  ErrorRow
} from "./style.jsx";
import Button from "../../Components/Button";

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      addNewPassword: "",
      confirmNewPassword: "",
      responseStatusAPI: "",
      StatusErrorMsg: "",
      passwordConfirmation: ""
    };
  }

  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      passwordConfirmation: "",
      responseStatusAPI: ""
    });
  };

  // Update Paassword
  updatePassword = e => {
    const oldPassword = this.state.oldPassword;
    const addNewPassword = this.state.addNewPassword;
    const confirmNewPassword = this.state.confirmNewPassword;

    // To check a password between 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character
    var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

    if (addNewPassword === "" && confirmNewPassword === "") {
      return this.setState({
        passwordConfirmation: strings.New_Password_field_must_be_filled_out
      });
    }

    if (!addNewPassword.match(decimal)) {
      return this.setState({
        passwordConfirmation: strings.Validate_password
      });
    }
    if (!(addNewPassword == confirmNewPassword)) {
      return this.setState({
        passwordConfirmation: strings.New_password_must_be_same
      });
    }

    const authBrandUserName = userService.authBrandUserName();

    fetch(changePassword, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authBrandUserName,
        OLD_PASSWORD: oldPassword,
        PASSWORD: confirmNewPassword
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseStatusAPI: res,
            oldPassword: ""
          });
          setTimeout(() => {
            this.setState({
              responseStatusAPI: ""
            });
          }, 4000);
        } else {
          this.setState({
            responseStatusAPI: res
          });
        }
      })
      .catch(error => console.log("Request failed:", error));
  };

  render() {
    const { responseStatusAPI, passwordConfirmation, oldPassword } = this.state;
    return (
      <ProfileWrapper>
        <ProfileHeader>{strings.Change_Password}</ProfileHeader>
        <ProfileBody>
          <Form>
            <FormRow>
              <LabelInputGroup>
                <h5>{strings.Confirm_old_password}</h5>
                <input
                  type="text"
                  // type="password"
                  name="oldPassword"
                  onChange={this.handleInputChange}
                />
              </LabelInputGroup>
            </FormRow>
            <FormRow>
              <LabelInputGroup>
                <h5>{strings.Add_new_password}</h5>
                <input
                  type="text"
                  // type="password"
                  name="addNewPassword"
                  onChange={this.handleInputChange}
                  disabled={oldPassword === ""}
                />
              </LabelInputGroup>
              <LabelInputGroup>
                <h5>{strings.Confirm_new_password}</h5>
                <input
                  type="password"
                  name="confirmNewPassword"
                  onChange={this.handleInputChange}
                  disabled={oldPassword === ""}
                />
              </LabelInputGroup>
            </FormRow>

            <FormRow>
              <LabelButton>
                <Button
                  classname="green"
                  onclick={this.updatePassword}
                  disabled={oldPassword === ""}
                >
                  {strings.Update_password}
                </Button>
              </LabelButton>
            </FormRow>
            <ErrorRow>
              {/* <ErrorMsg>{this.state.StatusErrorMsg}</ErrorMsg> */}

              {!(passwordConfirmation === "") ? (
                <ErrorMsg>{passwordConfirmation}</ErrorMsg>
              ) : null}
              {!(
                responseStatusAPI == "undefined" || responseStatusAPI == ""
              ) ? (
                <h4
                  style={{
                    fontWeight: "500",
                    textAlign: "center",
                    fontSize: "13px",
                    color: "red",
                    paddingTop: "10px"
                  }}
                >
                  {responseStatusAPI.STATUS + ":" + responseStatusAPI.MESSAGE}
                </h4>
              ) : null}
            </ErrorRow>
          </Form>
        </ProfileBody>
      </ProfileWrapper>
    );
  }
}

export default ChangePassword;
