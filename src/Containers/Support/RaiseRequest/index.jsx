import React, { Component } from "react";
import Button from "../../../Components/Button";
import { userService, sendSupportRequest } from "../../Config";
import { strings } from "./../../../Localization/index";
import { Auth } from "../../../Auth";

import {
  ProfileWrapper,
  ProfileHeader,
  ProfileBody,
  Form,
  FormRow,
  LabelInputGroup,
  LabelButton,
  Success_Msg,
  Error_Msg
} from "./style.jsx";

class RaiseRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subject: "",
      description: "",
      successMsg: "",
      errorMsg: "",
      inputError: ""
    };
  }

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      inputError: ""
    });
  };

  successmsg = () => {
    setTimeout(() => {}, 4000);
  };

  raiseRequest = () => {
    // debugger;
    const { subject, description } = this.state;
    const userID = userService.authUserName();

    if (subject == "" || description == "") {
      return this.setState({
        inputError: strings.All_field_must_be_filled_out
      });
    }

    fetch(sendSupportRequest, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        SUBJECT: subject,
        REQUEST_FOR: "SUPPORT",
        DESCRIPTION: description,
        USER_ID: userID,
        APP_CODE: "BRANDPORTAL"
      })
    })
      .then(res => res.json())
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            successMsg: strings.Your_request_raised_successfully,
            subject: "",
            description: ""
          });
        } else {
          this.setState({
            errorMsg: res.MESSAGE
          });
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    const {
      subject,
      description,
      successMsg,
      errorMsg,
      inputError
    } = this.state;

    return (
      <ProfileWrapper>
        <ProfileHeader>{strings.Raise_Request}</ProfileHeader>
        <ProfileBody>
          <Form>
            <FormRow>
              <LabelInputGroup>
                <h5>{strings.Subject}</h5>
                <input
                  type="text"
                  name="subject"
                  placeholder={strings.Issue}
                  value={subject}
                  onChange={this.onChange}
                />
              </LabelInputGroup>
              <LabelInputGroup>
                <h5>{strings.Description}</h5>
                <textarea
                  name="description"
                  rows="4"
                  cols="500"
                  placeholder={strings.Describe_your_issue}
                  value={description}
                  onChange={this.onChange}
                />
              </LabelInputGroup>
            </FormRow>

            <FormRow>
              <LabelButton>
                <Button classname="green" onclick={this.raiseRequest}>
                  {strings.Submit}
                </Button>
              </LabelButton>
            </FormRow>
            <Success_Msg>{successMsg}</Success_Msg>
            <Error_Msg>
              {subject == "" || description == "" ? inputError : null}
              {errorMsg}
            </Error_Msg>
          </Form>
        </ProfileBody>
      </ProfileWrapper>
    );
  }
}

export default RaiseRequest;
