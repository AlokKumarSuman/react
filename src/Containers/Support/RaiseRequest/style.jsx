import styled from "styled-components";

export const ProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
`;

export const ProfileHeader = styled.div`
  padding: 1.5rem 2rem;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;

export const ProfileBody = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 3rem 4rem;
  background-color: white;

  @media (max-width: 480px) {
    padding: 4rem 1rem 10rem 1rem;
  }
`;

export const Form = styled.div`
  width: 100%;
`;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;
  width: 50%;
  margin: 0 auto;

  @media (max-width: 768px) {
    margin-bottom: 1.2rem;
    width: 90%;
  }
`;

export const LabelInputGroup = styled.div`
  margin-bottom: 1.2rem;

  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }
  input {
    font-size: 14px;
    color: #333;
    font-family: "Roboto", sans-serif;
  }
  textarea {
    height: auto;
    font-size: 14px;
    color: #333;
    font-family: "Roboto", sans-serif;
    resize: vertical;
  }
  @media (max-width: 768px) {
    input {
      margin-bottom: 1.8rem;
    }
  }
`;
export const LabelButton = styled.div`
  padding-top: 2rem;
  width: 100% !important;
  float: none !important;
  div {
    margin: 0 auto;
    width: 180px;
    cursor: pointer;
  }
`;

export const Success_Msg = styled.div`
  color: green;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 10px auto;
  text-align: center;
`;

export const Error_Msg = styled.div`
  color: red;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 10px auto;
  text-align: center;
`;
