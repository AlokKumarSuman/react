import styled from "styled-components";

export const ProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
`;

export const ProfileHeader = styled.div`
  padding: 1.5rem 2rem;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;

export const ProfileBody = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 4rem 4rem 10rem 4rem;
  background-color: white;
  align-items: center;

  h4 {
    margin-bottom: 10rem;
    font-size: 18px;
    letter-spacing: 1px;
    color: #e3bd54;

    &:after {
      content: "";
      display: block;
      height: 3px;
      background: #e3bd54;
      width: 7rem;
      margin: 10px auto 0px;
    }
  }

  @media (max-width: 480px) {
    padding: 4rem 1rem 10rem 1rem;
  }
`;
export const CustomerCareSupport = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 0 auto;

  @media (max-width: 768px) {
    margin-bottom: 1.2rem;
    width: 90%;
    display: flex;
    flex-direction: column;
  }
`;
export const CCareGroup = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  background: #e3bd54;
  margin: 0px 30px;
  width: 300px;
  height: 140px;
  position: relative;
  span {
    margin-top: 30px;
    color: #fff;

    a {
      color: #fff;
      text-decoration: none;
      cursor: pointer;
      letter-spacing: 1px;
    }

    p {
      text-align: center;
      line-height: 20px;
      letter-spacing: 1px;
    }
  }
  }

  @media (max-width: 768px) {
    margin: 0 auto 5rem;
  }

  @media (max-width: 480px) {
    width: 100%;
    height: auto;
  }
`;

export const ImageWrap = styled.div`
  position: absolute;
  top: -37px;
  left: 50%;
  transform: translateX(-50%);

  img {
    border: 3px solid #fff;
    border-radius: 50%;
    max-width: 65px;
  }
`;
