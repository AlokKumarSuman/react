import React, { Component } from "react";
import Icon from "../../../Components/Icons";
import { strings } from "./../../../Localization/index";

import {
  ProfileWrapper,
  ProfileHeader,
  ProfileBody,
  CustomerCareSupport,
  CCareGroup,
  ImageWrap
} from "./style.jsx";

class CustomerCare extends Component {
  render() {
    return (
      <ProfileWrapper>
        <ProfileHeader>{strings.Support_team_eager_to_hear_you}</ProfileHeader>
        <ProfileBody>
          <h4>{strings.Get_in_touch}</h4>
          <CustomerCareSupport>
            <CCareGroup>
              <ImageWrap>
                <img
                  key="1"
                  src={require(`../../../Assets/Images/phone-icon.png`)}
                  alt=""
                />
              </ImageWrap>

              <span>
                +91 - <a href="tel:9625923606">9625923606</a>
              </span>
            </CCareGroup>
            <CCareGroup>
              <ImageWrap>
                <img
                  key="2"
                  src={require(`../../../Assets/Images/mail-icon.png`)}
                  alt=""
                />
              </ImageWrap>

              <span>
                <a href="mailto:support@kpfactors.com" target="_top">
                  support@kpfactors.com
                </a>
              </span>
            </CCareGroup>
            <CCareGroup>
              <ImageWrap>
                <img
                  key="3"
                  src={require(`../../../Assets/Images/map-icon.png`)}
                  alt=""
                />
              </ImageWrap>

              <span>
                <p>Unitect Business Zone</p>
                <p>C - 1001 Nirvana Country</p>
                <p> Sec - 50 Gurgaon</p>
                <p> Haryana - 122018</p>
              </span>
            </CCareGroup>
          </CustomerCareSupport>
        </ProfileBody>
      </ProfileWrapper>
    );
  }
}

export default CustomerCare;
