import styled from "styled-components";

export const RaisedWrapper = styled.div``;

export const IssueHeader = styled.div`
  display: flex;
  width: 100%;
  padding: 10px 10px;
  background: #2dc3e8;
`;

export const HeaderLeft = styled.div`
  font-family: "Gotham";
  font-size: 16px;
  color: #fff;
  letter-spacing: 1px;
  font-weight: 500;
`;

export const IssueBody = styled.div`
  @media (max-width: 768px) {
    td:nth-of-type(1):before {
      content: "Issue ID";
    }
    td:nth-of-type(2):before {
      content: "Issue Type";
    }
    td:nth-of-type(3):before {
      content: "Issue Details";
    }
    td:nth-of-type(4):before {
      content: "Raised By";
    }
    td:nth-of-type(5):before {
      content: "User ID";
    }
    td:nth-of-type(6):before {
      content: "Status";
    }
    td:nth-of-type(7):before {
      content: "Comment";
    }

    td:before {
      position: absolute;
      top: 50%;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      transform: translateY(-50%);
    }
  }
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;

export const ModalContent = styled.div`
  ul li {
    display: block;
    margin: 18px 0px;
  }

  h4 {
    font-family: "Gotham";
    font-size: 16px;
    font-color: #333;
    font-weight: 500;
    margin-bottom: 5px;
  }

  h5 {
    font-weight: 500;
    text-align: center;
    font-size: 13px;
    color: green;
    padding-top: 10px;
  }
`;
