import React, { Component } from "react";
import { Auth } from "../../../Auth";
import { strings } from "./../../../Localization/index";
import { raisedIssue, userService, issueUpdate } from "../../Config";
import NoDataFound from "../../../Components/NoDataFound";
import Spinner from "../../../Components/Spinner";
import Modal from "../../../Components/Modal";
import Button from "../../../Components/Button";
import moment from "moment";
import {
  RaisedWrapper,
  IssueHeader,
  IssueBody,
  HeaderLeft,
  ErrorMsg,
  SpinnerConfigData,
  ModalContent
} from "./style.jsx";

class RaisedIssue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      issueList: [],
      fetchErrorMsg: "",
      openEditHandler: false,
      currentItem: [],
      comment: "",
      statusName: "NEW",
      responseAddLevelStatusAPI: ""
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantName = userService.authTenantName();

    // debugger;
    fetch(raisedIssue, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantName
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      // .then(res => console.log(res))

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            issueList: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err,
          spinnerConfigData: false
        })
      );
  }

  openEditHandler = (e, currentItems) => {
    // debugger;
    this.setState({
      openEditHandler: true,
      currentItem: currentItems
    });
  };

  closeViewHandler = () => {
    this.setState({
      openEditHandler: false
    });
  };

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      responseAddLevelStatusAPI: ""
    });
  };

  statusChange = e => {
    // let options = e.target.options;
    // const id = options[options.selectedIndex].id;
    this.setState({
      // currencyId: id,
      statusName: e.target.value
    });
  };

  updateDataPush = () => {
    const { comment, statusName, currentItem } = this.state;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantName = userService.authTenantName();
    const authBrandName = userService.authBrandName();

    const currentDate = new Date();
    const formatCurrentDate = moment(moment(currentDate)).format(
      "DD-MM-YYYY hh:mm:ss"
    );

    var currPushArray = {
      BRAND_ID: authBrandId,
      BRAND_NAME: authBrandName,
      COMMENT: comment,
      ID: currentItem.ID,
      ISSUE_DETAILS: currentItem.ISSUE_DETAILS,
      ISSUE_TYPE: currentItem.ISSUE_TYPE,
      ISSUE_TYPE_ID: currentItem.ISSUE_TYPE_ID,
      RAISED_BY: currentItem.RAISED_BY,
      RAISED_BY_USER_ID: currentItem.RAISED_BY_USER_ID,
      RAISED_DATE: currentItem.RAISED_DATE,
      STATUS: statusName,
      TENANT_NAME: authTenantName,
      UPDATED_BY: authUserName,
      UPDATED_DATE: "11-10-2019 08:33:39"
    };

    var obj = this.state.issueList;

    var index = obj.findIndex(e => e.ID == currentItem.ID);
    obj[index] = currPushArray;
    this.setState({ issueList: obj, UPD: {} });
  };

  updateHandler = e => {
    // debugger;

    e.preventDefault();
    const { comment, statusName, currentItem } = this.state;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantName = userService.authTenantName();

    if (comment === "" || statusName === "") {
      return this.setState({
        validateAllFilds: "All field must be filled out."
      });
    }

    fetch(issueUpdate, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        ISSUE_ID: currentItem.ID,
        COMMENT: comment,
        USER_ID: authUserName,
        TENANT_NAME: authTenantName,
        STATUS: statusName
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          // setTimeout(() => {
          //   this.closeViewHandler();
          // }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
    this.updateDataPush();
  };

  render() {
    // debugger;
    const {
      issueList,
      spinnerConfigData,
      responseAddLevelStatusAPI
    } = this.state;
    return (
      <RaisedWrapper>
        <Modal
          modalOpen={this.state.openEditHandler}
          onclick={this.closeViewHandler}
          title={strings.Edit_status}
          size="sm"
        >
          <ModalContent>
            <ul>
              <li>
                <h4>{strings.Status}</h4>
                <select name="statusName" onChange={e => this.statusChange(e)}>
                  <option>{strings.Choose_status}</option>
                  <option>NEW</option>
                  <option>CLOSED</option>
                </select>
              </li>
              <li>
                <h4>{strings.Comment}</h4>
                <textarea
                  rows="4"
                  cols="50"
                  name="comment"
                  onChange={this.handleChange}
                ></textarea>
              </li>
            </ul>
            <Button size="fullwidth" onclick={this.updateHandler}>
              {strings.Update}
            </Button>
            {!(
              responseAddLevelStatusAPI == "undefined" ||
              responseAddLevelStatusAPI == ""
            ) ? (
              <h5>
                {responseAddLevelStatusAPI.STATUS +
                  ": " +
                  responseAddLevelStatusAPI.MESSAGE}
              </h5>
            ) : null}
          </ModalContent>
        </Modal>
        <IssueHeader>
          <HeaderLeft>{strings.Raised_issue_list}</HeaderLeft>
        </IssueHeader>
        <IssueBody>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th align="left" width="5%">
                  {strings.Issue_id}
                </th>
                <th align="left" width="15%">
                  {strings.Issue_type}
                </th>
                <th align="left" width="15%">
                  {strings.Issue_details}
                </th>
                <th align="left" width="10%">
                  {strings.Raised_by}
                </th>
                <th align="left" width="10%">
                  {strings.User_id}
                </th>
                <th align="left" width="10%">
                  {strings.Status}
                </th>
                <th align="left" width="10%">
                  {strings.Comment}
                </th>
              </tr>
            </thead>
            <tbody>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="7">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : issueList.length == 0 ? (
                <tr>
                  <td colSpan="7">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                issueList.map((item, index) => (
                  <tr key={index}>
                    <td>{item.ISSUE_TYPE_ID}</td>
                    <td>{item.ISSUE_TYPE}</td>
                    <td>{item.ISSUE_DETAILS}</td>
                    <td>{item.RAISED_BY}</td>
                    <td>{item.RAISED_BY_USER_ID}</td>
                    <td onClick={e => this.openEditHandler(e, item)}>
                      <Button>{item.STATUS}</Button>
                    </td>
                    <td>{item.COMMENT}</td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
        </IssueBody>
      </RaisedWrapper>
    );
  }
}

export default RaisedIssue;
