import React, { Component } from "react";
import styled from "styled-components";

const FooterWrap = styled.div`
  font-family: "Roboto", sans-serif;
  font-size: 1.4rem;
  padding: 2.4rem 0;
  text-align: center;
  background: #191818;
  color: #fff;
  position:fixed;
  bottom:0;
  left:0;
  right:0;
`;

const Footer = () => {
  return (
    <FooterWrap>
      <h5>
        KP Factors Business Solution Pvt. Ltd. © 2018. All Rights Reserved
      </h5>
    </FooterWrap>
  );
};

export default Footer;
