import React, { Component } from "react";
import Button from "../../../Components/Button";
import { Auth } from "../../../Auth";
import { uploadfile } from "../../Config";
import { AddImage, Preview, ErrorMsg } from "./style";

class UploadImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFileName: "",
      selectedFile: "",
      brandName: "",
      forPreview: "",
      inputError: ""
    };
  }

  fileChangedHandler = event => {
    const alldata = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
    const brandName = alldata.BRAND_NAME;
    const imageName = event.target.files[0].name;
    var file = event.target.files[0];
    let reader = new FileReader();
    // debugger;

    if (file.size > 5242880) {
      // debugger;
      return this.setState({
        inputError: "Your image size should not be greater then 5MB"
      });
    } else {
      reader.onloadend = () => {
        // debugger;
        let forPreview = reader.result;
        let result = reader.result.split(",");
        let finalResult = result[1];

        this.setState({
          selectedFileName: imageName,
          selectedFile: finalResult,
          forPreview: forPreview,
          brandName: brandName,
          inputError: ""
        });
      };
      reader.readAsDataURL(file);
    }
  };

  uploadHandler = () => {
    // debugger;
    fetch(uploadfile, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        fileName: this.state.selectedFileName,
        fileContent: this.state.selectedFile,
        brandName: this.state.brandName
      })
    })
      .then(res => res.json())
      .then(res =>
        this.props.displayImageName(this.state.selectedFileName, res.FileName)
      )
      .then(this.props.onclickClose)
      .catch(error => console.log(error));
  };

  render() {
    let { selectedFile, forPreview, inputError } = this.state;
    let $imagePreview = null;
    if (selectedFile) {
      $imagePreview = <img src={forPreview} alt="image" />;
    } else {
      $imagePreview = (
        <div className="previewText">Please select an Image for Preview</div>
      );
    }

    return (
      <AddImage>
        <input type="file" onChange={this.fileChangedHandler} accept="image/*" />
        <ErrorMsg>{inputError != "" ? inputError : null}</ErrorMsg>
        <Preview>{$imagePreview}</Preview>
        <Button size="fullwidth" onclick={this.uploadHandler}>
          Upload Image
        </Button>
      </AddImage>
    );
  }
}

export default UploadImage;
