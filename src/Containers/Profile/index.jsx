import React, { Component } from "react";
import { strings } from "./../../Localization/index";
import Modal from "../../Components/Modal";
import UploadImage from "./UploadImage";
// import logo from "../../Assets/Images/img_avatar.png";
import bgImage from "../../Assets/Images/img_avatar.png";

import { userService, updateProfile } from "../Config";

import { basicAuth, LoginAuth } from "../../Auth";
import {
  ProfileWrapper,
  ProfileHeader,
  ProfileBody,
  Form,
  FormRow,
  LabelInputGroup,
  LabelButton,
  ProfileImage,
  SuccessMsg,
  ErrorMsg
} from "./style.jsx";
import Button from "../../Components/Button";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      successMessage: "",
      errorMessage: "",
      userid: "",
      tenantname: "",
      confirmpassword: "",
      firstname: "",
      lastname: "",
      mobile: "",
      email: "",
      address: "",
      country: "India",
      uploadImageModel: false,
      imageName: "",
      imageNameByServer: ""
    };
  }

  componentDidMount() {
    const alldata = userService.authUserDataAll();
    this.setState({
      tenantname: alldata.TENANT_NAME,
      userid: alldata.username,
      firstname: alldata.givenname,
      lastname: alldata.lastname,
      mobile: alldata.mobile,
      email: alldata.emailaddress,
      address: alldata.address,
      country: alldata.country,
      photo: alldata.photo
    });
  }

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      successMessage: "",
      errorMessage: ""
    });
  };

  updateProfile = () => {
    const {
      confirmpassword,
      firstname,
      lastname,
      mobile,
      email,
      address,
      country,
      userid,
      tenantname,
      imageNameByServer
    } = this.state;

    // debugger;
    fetch(updateProfile, {
      method: "POST",
      headers: LoginAuth,
      body: JSON.stringify({
        USER_ID: userid,
        PASSWORD: confirmpassword,
        EMAIL: email,
        PHOTO: imageNameByServer,
        MOBILE: mobile,
        FIRST_NAME: firstname,
        LAST_NAME: lastname,
        COUNTRY: country,
        ADDRESS: address,
        TENANT_NAME: tenantname
      })
    })
      .then(res => res.json())
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            successMessage: res.MESSAGE,
            confirmpassword: ""
          });
          const alldata = userService.authUserDataAll();
          const RESULT = {
            username: alldata.username,
            organization: alldata.organization,
            role: alldata.role,
            Country: alldata.Country,
            address: address,
            photo: imageNameByServer,
            mobile: mobile,
            emailaddress: email,
            lastname: lastname,
            givenname: firstname,
            COMPANY_ID: alldata.COMPANY_ID,
            COMPANY_NAME: alldata.COMPANY_NAME,
            BRAND_ID: alldata.BRAND_ID,
            BRAND_NAME: alldata.BRAND_NAME,
            LOGO: alldata.LOGO,
            TOKEN_KEY: alldata.TOKEN_KEY,
            BASIC_FLAG: alldata.BASIC_FLAG,
            LANGUAGE_FLAG: alldata.LANGUAGE_FLAG
          };
          sessionStorage.setItem("data", Base64.encode(JSON.stringify(RESULT)));
        } else {
          this.setState({
            errorMessage: res.MESSAGE
          });
        }
      })
      .catch(err => console.log(err));
  };

  uploadImageModelHandler = e => {
    /* for popup Open */
    this.setState({
      uploadImageModel: true,
      inputError: ""
    });
  };
  uploadImageModelHandlerClose = e => {
    /* for popup Open */
    this.setState({
      uploadImageModel: false
    });
  };
  displayImageNameHandler = (displayImageName, imageNameByServers) => {
    // debugger;
    this.setState({
      imageName: displayImageName,
      imageNameByServer: imageNameByServers
    });
  };

  render() {
    const {
      userid,
      confirmpassword,
      firstname,
      lastname,
      mobile,
      email,
      address,
      country,
      photo
    } = this.state;

    return (
      <ProfileWrapper>
        <ProfileHeader>{strings.Edit_Update_profile}</ProfileHeader>
        <ProfileBody>
          <Form>
            <FormRow>
              <ProfileImage>
                <img
                  src={photo}
                  alt="img"
                  onClick={this.uploadImageModelHandler}
                />
              </ProfileImage>

              <LabelInputGroup>
                <h5>{strings.User_id}</h5>
                <input type="text" name="userId" value={userid} disabled />
              </LabelInputGroup>
              <LabelInputGroup>
                <h5>{strings.Confirm_password}</h5>
                <input
                  type="text"
                  name="confirmpassword"
                  value={confirmpassword}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </LabelInputGroup>
              <LabelInputGroup>
                <h5>{strings.First_Name}</h5>
                <input
                  type="text"
                  name="firstname"
                  value={firstname}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </LabelInputGroup>
              <LabelInputGroup>
                <h5>{strings.Last_Name}</h5>
                <input
                  type="text"
                  name="lastname"
                  value={lastname}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </LabelInputGroup>

              <LabelInputGroup>
                <h5>{strings.Mobile}</h5>
                <input
                  type="tel"
                  name="mobile"
                  pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                  value={mobile}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </LabelInputGroup>
              <LabelInputGroup>
                <h5>{strings.Email}</h5>
                <input
                  type="text"
                  name="email"
                  value={email}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </LabelInputGroup>
              <LabelInputGroup>
                <h5>{strings.Country}</h5>
                <input type="text" name="country" value={country} disabled />
              </LabelInputGroup>

              <LabelInputGroup>
                <h5>{strings.Full_Address}</h5>
                <textarea
                  type="text"
                  rows="4"
                  cols="50"
                  name="address"
                  value={address}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </LabelInputGroup>
              <ErrorMsg>{this.state.errorMessage}</ErrorMsg>
              <SuccessMsg>{this.state.successMessage}</SuccessMsg>
              <LabelButton>
                <Button size="fullwidth" onclick={this.updateProfile}>
                  {strings.Update_profile}
                </Button>
              </LabelButton>
            </FormRow>
          </Form>
        </ProfileBody>

        {this.state.uploadImageModel == true ? (
          <Modal
            modalOpen={this.state.uploadImageModel}
            onclick={this.uploadImageModelHandlerClose}
            title={strings.Upload_Image}
            size="sm"
          >
            <UploadImage
              onclickClose={this.uploadImageModelHandlerClose}
              displayImageName={this.displayImageNameHandler}
            />
          </Modal>
        ) : null}
      </ProfileWrapper>
    );
  }
}

export default Profile;
