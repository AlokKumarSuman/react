import styled from "styled-components";

export const ProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
`;

export const ProfileHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1.5rem 2rem;
  background-color: #f0f3f5;
  width: 100%;
`;

export const ProfileBody = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 3rem 4rem;
  background-color: white;
`;

export const Form = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  align-self: center;
  margin: 0 auto;
  padding: 30px 20px 10px 20px;
  background: #ccc;
  margin-top:45px;
}
`;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;
  position: relative;

  @media (min-width: 769px) {
    flex-direction: column;

    & > div {
      width: 100%;
      float: left;
      margin-bottom: 20px;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.8rem;
    }
  }
`;
export const LabelButton = styled.div`
  padding-top: 2rem;
  .button {
    color: #fff;
  }
`;

export const ProfileImage = styled.div`
  width: 70px !important;
  position: absolute !important;
  height: 70px;
  cursor: pointer;
  float: none !important;
  left: 0;
  right: 0;
  margin: 0 auto !important;
  top: -70px;
  border-radius: 50%;
  border: solid 1px #8e85856e;
  background: #c5c5c5;
  overflow: hidden;

  img {
    width: 100%;
    display: block;
  }
`;

export const SuccessMsg = styled.div`
  color: green;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 0 auto;
  text-align: center;
`;

export const ErrorMsg = styled.div`
  color: red;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 0 auto;
  text-align: center;
`;
