import React, { Component } from "react";
import { strings } from "./../../../Localization/index";
import Button from "../../../Components/Button";
import NoDataFound from "../../../Components/NoDataFound";
import Icon from "../../../Components/Icons";
import { fetchDemandsList, userService, trackDemands } from "../../Config";
import { Auth } from "../../../Auth";
import Spinner from "../../../Components/Spinner";
import Modal from "../../../Components/Modal";
import ViewRecord from "./ViewRecord";

import {
  DemandsCard,
  DemandsHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  DemandsBody,
  SpinnerConfigData,
  ProductImage,
  Sortby,
  Sortoption,
  ServerError,
  ErrorMsg
} from "./style";

class TrackDemand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterShow: false,
      searchProductItemName: "",
      searchProductName: "",
      searchSentBy: "",
      totalQuantity: "",
      createdOn: "",
      demandPosts: [],
      // modalOpen: false,
      spinnerConfigData: true,
      openViewHandler: false,
      currentItem: [],

      dropdown: false,
      serverError: {},

      fetchErrorMsg: ""
    };
  }

  openViewHandler = (e, currentItems) => {
    /* for popup Open */
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler,
      currentItem: currentItems
    }));
  };
  closeViewHandler = () => {
    /* for popup close */
    this.setState(prevState => ({
      openViewHandler: false
    }));
  };

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(trackDemands, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      // .then(res => console.log(res))

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            demandPosts: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })

      .catch(err =>
        this.setState({
          serverError: err,
          spinnerConfigData: false
        })
      );
  }

  filterHandler = () => {
    /* for filter toggle*/
    this.setState(prevState => ({
      filterShow: !prevState.filterShow
    }));
  };

  searchProductItemNameHandler = e => {
    /*  for Action Type Search */
    this.setState({
      searchProductItemName: e.target.value
    });
  };

  searchProductNameHandler = e => {
    /* for name search */
    this.setState({
      searchProductName: e.target.value
    });
  };

  seasearchSentByHandler = e => {
    /*  for Action Type Search */
    this.setState({
      searchSentBy: e.target.value
    });
  };

  totalQuantityHandler = e => {
    /*  for Action Type Search */
    this.setState({
      totalQuantity: e.target.value
    });
  };

  createdOnHandler = e => {
    /*  for Action Type Search */
    this.setState({
      createdOn: e.target.value
    });
  };

  dropdownHandler = () => {
    this.setState(prevState => ({
      dropdown: !prevState.dropdown
    }));
  };

  sortByName = () => {
    // debugger;
    function sortOn(arr, prop) {
      arr.sort(function(a, b) {
        if (a[prop] < b[prop]) {
          return -1;
        } else if (a[prop] > b[prop]) {
          return 1;
        } else {
          return 0;
        }
      });
    }
    var obj = [...this.state.demandPosts];
    sortOn(obj, "ITEM_NAME");
    this.setState({
      demandPosts: obj
    });
  };

  sortByQTY = () => {
    var obj = [...this.state.demandPosts];
    obj.reverse((a, b) => a.QTY - b.QTY);
    this.setState(prevState => ({
      demandPosts: obj
    }));
  };

  render() {
    // debugger;
    const { currentItem, serverError, spinnerConfigData } = this.state;
    let dataItems = this.state.demandPosts;
    // const searchProductItemName = this.state.searchProductItemName
    //   .trim()
    //   .toLowerCase();
    // const searchProductName = this.state.searchProductName.trim().toLowerCase();

    // const searchSentBy = this.state.searchSentBy.trim().toLowerCase();
    // const totalQuantity = this.state.totalQuantity.trim().toLowerCase();
    // const createdOn = this.state.createdOn.trim().toLowerCase();

    // if (searchProductItemName.length > 0) {
    //   dataItems = this.state.demandPosts.filter(function(i) {
    //     return i.ITEM_NAME.toLowerCase().match(searchProductItemName);
    //   });
    // }

    // if (searchProductName.length > 0) {
    //   dataItems = this.state.demandPosts.filter(function(i) {
    //     return i.PRODUCT_NAME.toLowerCase().match(searchProductName);
    //   });
    // }

    // if (searchSentBy.length > 0) {
    //   dataItems = this.state.demandPosts.filter(function(i) {
    //     return i.sentBy.toLowerCase().match(searchSentBy);
    //   });
    // }

    // if (totalQuantity.length > 0) {
    //   dataItems = this.state.demandPosts.filter(function(i) {
    //     return i.totalQuantity.toLowerCase().match(totalQuantity);
    //   });
    // }

    // if (createdOn.length > 0) {
    //   dataItems = this.state.demandPosts.filter(function(i) {
    //     return i.createdOn.toLowerCase().match(createdOn);
    //   });
    // }

    return (
      <DemandsCard>
        <DemandsHeader>
          <WrapperLeft>{strings.Track_Today_Demand}</WrapperLeft>
          <WrapperRight>
            <Sortby
              onMouseEnter={this.dropdownHandler}
              onMouseLeave={this.dropdownHandler}
            >
              <Button classname="blue">
                <Icon name="filter" color="white" /> {strings.Sort_By}
              </Button>
              <Sortoption dropdown={this.state.dropdown}>
                <h5 onClick={this.sortByName}>{strings.Name}</h5>

                <h5 onClick={this.sortByQTY}>{strings.Quantity}</h5>
              </Sortoption>
            </Sortby>
            <ButtonGroup>
              <Button classname="blue" onclick={this.filterHandler}>
                <Icon name="filter" color="white" /> {strings.Filter}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </DemandsHeader>

        <DemandsBody toggleFilter={this.state.filterShow}>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="70px">{strings.Image}</th>
                <th width="25%" align="left">
                  {strings.Product_Name}
                </th>
                <th width="20%">{strings.Product_Type}</th>
                <th width="18%">{strings.Sent_by}</th>
                <th width="10%" style={{ minWidth: "120px" }}>
                  {strings.Total_Quantity}
                </th>
                <th width="10%" style={{ minWidth: "120px" }}>
                  {strings.Created_On}
                </th>
                <th width="110px" style={{ minWidth: "60px" }}>
                  {strings.Action}
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td />

                <td>
                  <input
                    type="text"
                    value={this.state.searchProductItemName}
                    onChange={this.searchProductItemNameHandler}
                    placeholder="By Category..."
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchProductName}
                    onChange={this.searchProductNameHandler}
                    placeholder="By Name..."
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchSentBy}
                    onChange={this.seasearchSentByHandler}
                    placeholder="Sent By..."
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.totalQuantity}
                    onChange={this.totalQuantityHandler}
                    placeholder="By Quantity..."
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.createdOn}
                    onChange={this.createdOnHandler}
                    placeholder="By Created..."
                    className="textbox"
                  />
                </td>
                <td />
              </tr>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="7">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : dataItems.length == 0 ? (
                <tr>
                  <td colSpan="7">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                dataItems.map((item, index) => (
                  <tr key={index}>
                    <td>
                      <ProductImage>
                        <img
                          src={item.ITEM_IMAGE}
                          alt="Img"
                          className="product-image"
                        />
                      </ProductImage>
                    </td>
                    <td align="left">{item.ITEM_NAME}</td>
                    <td align="center">{item.PRODUCT_NAME}</td>
                    <td align="center">{item.SENDER_NAME}</td>
                    <td align="center">{item.QTY}</td>
                    <td align="center">{item.DATE}</td>
                    <td align="center">
                      {/* <Icon name="eye" onclick={this.openHandler} /> */}
                      <Button
                        type="primary"
                        size="fullwidth"
                        onclick={e => this.openViewHandler(e, item)}
                      >
                        {strings.Action}
                      </Button>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>

          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
          {this.state.serverError.statusText == "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
        </DemandsBody>
        {/* Modal Popup for View Dealer Details */}
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Dealer_Details}
          size="md"
        >
          <ViewRecord currentItem={currentItem} />
        </Modal>
      </DemandsCard>
    );
  }
}

export default TrackDemand;
