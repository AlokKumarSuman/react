import React, { Component } from "react";
import PropTypes from "prop-types";
import { TabsMain, TabGroup, TabLists, TabPanels } from "./style";
import clientImg from "../../../../Assets/Images/img_avatar.png";
import { strings } from "./../../../../Localization/index";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // debugger;
    const { currentItem } = this.props;
    return (
      <TabsMain
        defaultIndex={0}
        selectedTabClassName="is-selected"
        selectedTabPanelClassName="is-selected"
      >
        <TabGroup>
          <TabLists>{strings.Demand_Details}</TabLists>
          <TabLists>{strings.Sender_Details}</TabLists>
        </TabGroup>
        <TabPanels>
          <table className="table record-view-table">
            <tbody>
              <tr>
                <td align="center" colSpan="2">
                  <img
                    src={currentItem.ITEM_IMAGE}
                    alt="Image"
                    className="trackDemand-image-view"
                  />
                </td>
              </tr>
              <tr>
                <td width="30%">
                  <b>{strings.Demand_Id} </b>
                </td>
                <td>: {currentItem.DEAMND_ID}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Product_Name}</b>
                </td>
                <td>: {currentItem.ITEM_NAME}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Product_Type}</b>
                </td>
                <td>: {currentItem.PRODUCT_NAME}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Total_Order}</b>
                </td>
                <td>: {currentItem.QTY}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Order_Date}</b>
                </td>
                <td>: {currentItem.DATE}</td>
              </tr>
            </tbody>
          </table>
        </TabPanels>
        <TabPanels>
          <table className="table record-view-table">
            <tbody>
              {/* <tr>
                <td align="center" colSpan="2">
                  <img
                    src={currentItem.SENDER_PHOTO}
                    alt="Img"
                    className="trackDemand-image-view"
                  />
                </td>
              </tr> */}

              <tr>
                <td width="30%">
                  <b>{strings.Sender_Name}</b>
                </td>
                <td>: {currentItem.SENDER_NAME}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Sender_Email}</b>
                </td>
                <td>: {currentItem.SENDER_EMAIL}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Mobile}</b>
                </td>
                <td>: {currentItem.SENDER_MOBILE}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Order_Date}</b>
                </td>
                <td>: {currentItem.DATE}</td>
              </tr>
            </tbody>
          </table>
        </TabPanels>
      </TabsMain>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
