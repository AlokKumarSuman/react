import styled from "styled-components";

export const DemandsCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: "Roboto", sans-serif;
  font-size: initial;

  .trackDemand-image-view {
    height: 6rem;
    margin-bottom: 1rem;
  }
`;

export const DemandsHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;
export const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  div {
    margin-right: 1rem;
  }
  div:last-child {
    margin-right: 0;
  }
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: auto;
    padding: 0.6rem 0.8rem;
  }
  div svg.icon {
    margin-right: 0.6rem;
    margin-top: -3px;
  }
  div svg.icon-fa {
    margin-right: 0.6rem;
  }
`;
export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const DemandsBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;

  table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};
  }
  .product-image {
    height: 40px;
  }
`;
export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
export const ProductImage = styled.div`
  object-fit: cover;
  width: 80px;
  text-align: center;
`;
export const Sortby = styled.div`
  display: flex;
  flex-direction: column;
  width: 11rem;
  margin-right: 10px;
  position: relative;

  > div {
    width: 100%;
    display: flex;
    align-items: center;
    padding: 5px;
    background: #fff;

    h5 {
      font-size: 13px;
      padding: 5px 2px;
      cursor: pointer;
      padding: 10px 5px;
      :hover {
        background: #686464;
        color: #fff;
        transition: 0.2s all;
      }
    }
  }
  div svg.icon {
    margin-right:0.6rem;
    margin-top: -3px;
  }
  div svg.icon-fa {
    margin-right:0.6rem;
  }
  }
  svg.icon:hover {
    fill: #cb0202;
    cursor: pointer;
}
`;
export const Sortoption = styled.div`
  display: ${props => (props.dropdown === true ? "block" : "none")} !important;
  padding: 0px !important;
  position: absolute;
  top: 32px;
  right: 0px;
  width: 15rem !important;
  box-shadow: -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.45);
  box-shadow: -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.45);
  box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.45)
`;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;
export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
