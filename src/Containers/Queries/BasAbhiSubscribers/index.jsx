import React, { Component } from "react";
import BasAbhiList from "./BasAbhiList";
import {
  MainWrapper,
  SubscriberHeader,
  WrapperLeft,
  SubscriberBody,
  SubscriberFilter,
  ChooseFilter,
  FilterArea,
  FilterLevel,
  FilterLocation,
  FilterUser
} from "./style";

class BasAbhiSubscribers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionName: "",
      userSearch: "",
      levelValues: []
    };
  }

  filterChange = e => {
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      optionName: id
    });
  };

  onChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch("https://192.168.0.10:8244/fetchLevelsForDropDown/1.0.0/getList", {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        ROLE: "BRAND-ADMIN",
        LOC_TYPE: "CUSTOM"
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res))
      .then(res =>
        this.setState({
          levelValues: res.RESULT
        })
      )
      .catch(err => console.log(err));
  }

  render() {
    const { userSearch } = this.state;
    return (
      <React.Fragment>
        <MainWrapper>
          <SubscriberHeader>
            <WrapperLeft>Bas Abhi Subscribers</WrapperLeft>
          </SubscriberHeader>

          <SubscriberFilter>
            <h4>Filter By:</h4>
            <ChooseFilter>
              <select onChange={e => this.filterChange(e)}>
                <option id="default">Choose Option</option>
                <option id="location">Location</option>
                <option id="user">User</option>
              </select>
            </ChooseFilter>

            {this.state.optionName === "location" ? (
              <FilterArea>
                <FilterLevel>
                  <select>
                    <option>Country</option>
                    <option>Country</option>
                    <option>Country</option>
                  </select>
                </FilterLevel>
                <FilterLocation>
                  <select>
                    <option>Country</option>
                    <option>Country</option>
                    <option>Country</option>
                  </select>
                </FilterLocation>
              </FilterArea>
            ) : this.state.optionName === "user" ? (
              <FilterUser>
                <input
                  type="text"
                  placeholder="Search Here..."
                  name="userSearch"
                  onChange={this.onChangeHandler}
                />
              </FilterUser>
            ) : null}
          </SubscriberFilter>

          <SubscriberBody>
            <BasAbhiList userSearchProp={userSearch} />
          </SubscriberBody>
        </MainWrapper>
      </React.Fragment>
    );
  }
}

export default BasAbhiSubscribers;
