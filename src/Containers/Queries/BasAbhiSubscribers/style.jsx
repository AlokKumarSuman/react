import styled from "styled-components";

export const MainWrapper = styled.div``;

export const SubscriberHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
  height: 40px;
`;

export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 768px) {
    display: none;
  }
`;

export const SubscriberBody = styled.div`
  background-color: #fff;
`;

export const SubscriberFilter = styled.div`
  display: flex;
  flex-direction: row;
  height: 40px;
  align-items: center;
  background: #1c87a1;
  padding: 0px 8px;

  h4 {
    color: #fff;
    font-size: 14px;
    font-weight: 400;
    letter-spacing: 1px;
    font-family: "Gotham";
  }
`;

export const ChooseFilter = styled.div`
  width: 220px;
  margin-left: 20px;
`;

export const FilterArea = styled.div`
  display: flex;
`;

export const FilterLevel = styled.div`
  width: 180px;
  margin: 0px 10px;
`;

export const FilterLocation = styled.div`
  width: 180px;
`;

export const FilterUser = styled.div`
  margin-left: 10px;
`;

export const SpinnerDiv = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
