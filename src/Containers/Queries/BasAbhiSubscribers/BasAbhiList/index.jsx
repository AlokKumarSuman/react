import React, { Component } from "react";
import { userService } from "../../../Config";
import { Auth } from "../../../../Auth";
import Spinner from "../../../../Components/Spinner";
import NoDataFound from "./../../../../Components/NoDataFound";
import { SpinnerDiv } from "../style.jsx";

class BasAbhiList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allSubscriberList: [],
      spinnerlogo: true,
      fetchErrorMsg: "",
      serverError: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ userSearch: nextProps.userSearchProp }, () => {
      this.getListHandler();
    });
  }

  componentDidMount() {
    this.getListHandler();
  }

  getListHandler = () => {
    const authBrandId = userService.authBrandId();
    const authTenantId = userService.authTenantId();

    fetch("https://192.168.0.10:8244/fetchBrandSubscriberList/1.0.0/getList", {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        LEVEL_ID: "",
        LOC_VAL: "",
        BRAND_ID: authBrandId,
        TENANT_ID: authTenantId,
        APP_CODE: "BASABHI",
        USERNAME: this.state.userSearch,
        OFFSET: "0",
        LIMIT: "200"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerlogo: false
          });
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            allSubscriberList: res.RESULT,
            spinnerlogo: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerlogo: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerlogo: false
        })
      );
  };

  render() {
    const { allSubscriberList, spinnerlogo } = this.state;
    console.log("alok" + this.state.userSearch);
    return (
      <React.Fragment>
        <table className="table bordered odd-even">
          <thead>
            <tr>
              <th width="6%" align="left">
                User Id
              </th>
              <th width="20%" align="center">
                Name
              </th>
              <th width="12%" align="center">
                Country
              </th>
              <th width="12%" align="center">
                State
              </th>
              <th width="10%" align="center">
                Pin Code
              </th>
              <th width="30%" align="center">
                Address
              </th>
            </tr>
          </thead>
          <tbody>
            {spinnerlogo == true ? (
              <tr>
                <td colSpan="6">
                  <SpinnerDiv>
                    <Spinner />
                  </SpinnerDiv>
                </td>
              </tr>
            ) : allSubscriberList.length == 0 ? (
              <tr>
                <td colSpan="6">
                  <NoDataFound />
                </td>
              </tr>
            ) : (
              allSubscriberList.map((item, index) => (
                <tr>
                  <td align="left">{item.USER_ID}</td>
                  <td align="center">{item.NAME}</td>
                  <td align="center">{item.COUNTRY_NM}</td>
                  <td align="center">{item.STATE_NM}</td>
                  <td align="center">{item.PINCODE}</td>
                  <td align="center">{item.ADDRESS}</td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default BasAbhiList;
