import styled from "styled-components";

export const AuthDealerCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .product-image {
    height: 4rem;
    width: 4rem;
  }

  .client-image {
    height: 4rem;
    width: 4rem;
    border-radius: 50%;
  }
  .client-image-view {
    height: 6rem;
    width: 6rem;
    border-radius: 50%;
  }
`;

export const AuthDealerHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;
export const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  div {
    margin-right: 1rem;
  }
  div:last-child {
    margin-right: 0;
  }
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: auto;
    padding: 0.6rem 0.8rem;
  }
  div svg.icon {
    margin-right: 0.6rem;
    margin-top: -3px;
  }
  div svg.icon-fa {
    margin-right: 0.6rem;
  }

  .blue:last-child {
    display: none;
  }
`;
export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 768px) {
    display: none;
  }
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const AuthDealerBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;

  table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};
  }

  @media (max-width: 768px) {
    table.table tbody tr:first-child {
      display: none;
    }
    td:nth-of-type(1):before {
      content: "Image";
    }
    td:nth-of-type(2):before {
      content: "Name";
    }
    td:nth-of-type(3):before {
      content: "User Type";
    }
    td:nth-of-type(4):before {
      content: "Action";
    }
    td:nth-of-type(5):before {
      content: "Request Date";
    }
    td:nth-of-type(6):before {
      content: "Details";
    }

    td:before {
      position: absolute;
      top: 50%;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      transform: translateY(-50%);
      font-weight: 600;
      text-transform: capitalize;
    }

    .green,
    .blue {
      width: 12rem;
    }
  }
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;
export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
