import React, { Component } from "react";
import moment from "moment";
import PropTypes from "prop-types";
import clientImg from "../../../../Assets/Images/img_avatar.png";
import { strings } from "./../../../../Localization/index";
// import { Button } from "../../../../Components/Button";
import Button from "../../../../Components/Button";
import Modal from "../../../../Components/Modal";

import { approveRetailerRequest, userService } from "../../../Config";
import { Auth } from "../../../../Auth";

import {
  TabsMain,
  TabGroup,
  TabLists,
  TabPanels,
  ButtonWrapDelete,
  ErrorMsg,
  ButtonWrapConfirm,
  RejectButton
} from "./style";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmationModal: false,
      crrStatus: "",
      responseStatusAPI: "",
      StatusErrorMsg: "",
      //
      rejectStatusMessage: "",
      validateRejectFilds: ""
    };
  }

  changeStatusModal = (e, item) => {
    // debugger;
    this.setState(prevState => ({
      confirmationModal: !prevState.confirmationModal,
      crrStatus: item
    }));
  };
  confirmationModalClose = () => {
    // debugger;
    this.setState({
      confirmationModal: false,
      validateRejectFilds: ""
    });
  };
  // Approve Request
  changeStatusHandler = e => {
    // debugger;
    const { crrStatus, rejectStatusMessage } = this.state;
    const { currentItem } = this.props;

    let addReason;

    if (crrStatus == "REJECTED") {
      if (rejectStatusMessage == "") {
        return this.setState({
          validateRejectFilds: "Rejection comment must be filled out."
        });
      }
      addReason = rejectStatusMessage;
    } else {
      addReason = "";
    }

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    // debugger;

    fetch(approveRetailerRequest, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        BRAND_ID: authBrandId,
        REQUEST_NO: currentItem.REQUEST_ID,
        REASON: addReason,
        STATUS: crrStatus
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseStatusAPI: res.MESSAGE,

            confirmationModal: false
          });
          setTimeout(() => {
            location.reload();
          }, 1000);
        } else {
          this.setState({
            StatusErrorMsg: res.MESSAGE
          });
        }
      })
      .catch(error => console.log("Request failed:", error));
  };

  onChangeHandler = e => {
    this.setState({
      rejectStatusMessage: e.target.value,
      validateRejectFilds: ""
    });
  };

  render() {
    // debugger;
    const { currentItem } = this.props;
    const {
      crrStatus,
      responseStatusAPI,
      StatusErrorMsg,
      validateRejectFilds
    } = this.state;

    let modelTitle = crrStatus + " Dealer";

    let confirmBody;

    if (crrStatus == "REJECTED") {
      confirmBody = (
        <>
          <h5
            style={{
              marginBottom: "8px"
            }}
          >
            Reasion for Reject
          </h5>
          <textarea onChange={this.onChangeHandler} />
          <RejectButton>
            <Button classname="blue" onclick={this.changeStatusHandler}>
              {strings.Submit}
            </Button>
          </RejectButton>
          {validateRejectFilds != "" ? (
            <ErrorMsg>{validateRejectFilds}</ErrorMsg>
          ) : (
            " "
          )}
        </>
      );
    } else {
      confirmBody = (
        <>
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              textTransform: "none",
              lineHeight: "22px"
            }}
          >
            Are you sure? <br />
            You want change the status to {crrStatus}.
          </h4>
          <ButtonWrapConfirm>
            <Button classname="green" onclick={this.changeStatusHandler}>
              Yes
            </Button>
            <Button classname="red" onclick={this.confirmationModalClose}>
              Cancel
            </Button>
          </ButtonWrapConfirm>
        </>
      );
    }

    return (
      <>
        <TabsMain
          defaultIndex={0}
          selectedTabClassName="is-selected"
          selectedTabPanelClassName="is-selected"
        >
          <TabGroup>
            <TabLists>{strings.Basic_Details}</TabLists>
            <TabLists>{strings.Business_Details}</TabLists>
          </TabGroup>
          <TabPanels>
            <table className="table record-view-table">
              <tbody>
                <tr>
                  <td align="center" colSpan="2">
                    <img
                      src={clientImg}
                      alt="Img"
                      className="client-image-view"
                    />
                  </td>
                </tr>
                {currentItem.USER_ID ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.User_id} </b>
                    </td>
                    <td>: {currentItem.USER_ID}</td>
                  </tr>
                ) : null}
                {currentItem.CUSTOMER_NAME ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.Customer_Name} </b>
                    </td>
                    <td>: {currentItem.CUSTOMER_NAME}</td>
                  </tr>
                ) : null}

                {currentItem.STATUS ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.Current_status} </b>
                    </td>
                    <td>: {currentItem.STATUS}</td>
                  </tr>
                ) : null}

                {currentItem.REQUEST_DATE ? (
                  <tr>
                    <td>
                      <b>{strings.Request_Date} </b>
                    </td>
                    <td>
                      :&nbsp;
                      {moment(new Date(currentItem.REQUEST_DATE)).format(
                        "DD-MM-YYYY"
                      )}
                    </td>
                  </tr>
                ) : null}

                {currentItem.LAST_UPDATE ? (
                  <tr>
                    <td>
                      <b>{strings.Last_Update} </b>
                    </td>
                    <td>
                      :&nbsp;
                      {moment(new Date(currentItem.LAST_UPDATE)).format(
                        "DD-MM-YYYY"
                      )}
                    </td>
                  </tr>
                ) : null}

                {currentItem.MOBILE ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.Mobile} </b>
                    </td>
                    <td>: {currentItem.MOBILE}</td>
                  </tr>
                ) : null}
                {currentItem.EMAIL ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.Email} </b>
                    </td>
                    <td>: {currentItem.EMAIL}</td>
                  </tr>
                ) : null}

                {currentItem.STATUS == "REJECT" ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.Reasion_to_reject} </b>
                    </td>
                    <td>: {currentItem.STATUS}</td>
                  </tr>
                ) : null}

                {/* {currentItem.COMMENTS ? (
                <tr>
                  <td>
                    <b>{strings.Comment}</b>
                  </td>
                  <td>: {currentItem.COMMENTS}</td>
                </tr>
              ) : (
                ""
              )} */}
              </tbody>
            </table>
          </TabPanels>
          <TabPanels>
            <table className="table record-view-table">
              <tbody>
                <tr>
                  <td align="center" colSpan="2">
                    <img
                      src={clientImg}
                      alt="Img"
                      className="client-image-view"
                    />
                  </td>
                </tr>
                {currentItem.BUSINESS_TYPE ? (
                  <tr>
                    <td>
                      <b>{strings.Business_Type} </b>
                    </td>
                    <td>: {currentItem.BUSINESS_TYPE}</td>
                  </tr>
                ) : null}
                {currentItem.BUSINESS_NAME ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.Business_Name} </b>
                    </td>
                    <td>: {currentItem.BUSINESS_NAME}</td>
                  </tr>
                ) : null}
                {currentItem.BRAND_NAME ? (
                  <tr>
                    <td>
                      <b>{strings.Brand_Name} </b>
                    </td>
                    <td>: {currentItem.BRAND_NAME}</td>
                  </tr>
                ) : null}
                {currentItem.GSTIN ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.GSTIN} </b>
                    </td>
                    <td>: {currentItem.GSTIN}</td>
                  </tr>
                ) : (
                  ""
                )}
                {currentItem.PAN ? (
                  <tr>
                    <td width="30%">
                      <b>{strings.PAN} </b>
                    </td>
                    <td>: {currentItem.PAN}</td>
                  </tr>
                ) : null}

                {currentItem.REASON ? (
                  <tr>
                    <td width="30%">
                      <b>Reasion </b>
                    </td>
                    <td>: {currentItem.REASON}</td>
                  </tr>
                ) : null}

                {currentItem.COMMENTS ? (
                  <tr>
                    <td>
                      <b>{strings.Comment}</b>
                    </td>
                    <td>: {currentItem.COMMENTS}</td>
                  </tr>
                ) : null}
              </tbody>
            </table>
          </TabPanels>

          <ButtonWrapDelete>
            <Button
              classname="green"
              value="APPROVED"
              onclick={e => this.changeStatusModal(e, "APPROVED")}
            >
              {strings.APPROVE}
            </Button>
            <Button
              classname="blue"
              value="INPROGRESS"
              onclick={e => this.changeStatusModal(e, "INPROGRESS")}
            >
              {strings.IN_PROGRESS}
            </Button>
            <Button
              classname="red"
              onclick={e => this.changeStatusModal(e, "REJECTED")}
            >
              {strings.REJECT}
            </Button>
          </ButtonWrapDelete>

          {responseStatusAPI != "" ? (
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                fontSize: "13px",
                color: "green",
                paddingTop: "10px"
              }}
            >
              {responseStatusAPI}
            </h4>
          ) : (
            ""
          )}

          {StatusErrorMsg != "" ? <ErrorMsg>{StatusErrorMsg}</ErrorMsg> : ""}
        </TabsMain>

        {/* Modal Popup for Approval Request */}
        <Modal
          modalOpen={this.state.confirmationModal}
          modelClose={this.confirmationModalClose}
          title={modelTitle}
          size="sm"
        >
          {confirmBody}
        </Modal>
      </>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
