import styled from "styled-components";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

// Custom Tabs
export const TabsMain = styled(Tabs)`
  width: 100%;
`;

export const TabGroup = styled(TabList)`
  list-style-type: none;
  display: flex;
  margin: 0;
`;

// TabGroup.tabsRole = "TabList";
export const TabLists = styled(Tab)`
  border: 1px solid #6f94ac;
  padding: 1.2rem 0.6rem;
  user-select: none;
  cursor: pointer;
  flex-grow: 1;
  width: 100%;
  background-color: #6f94ac;
  text-align: center;
  color: white;

  &.is-selected {
    background-color: #025063;
    border-bottom: none;
  }
`;
// TabLists.tabsRole = "Tab";
export const TabPanels = styled(TabPanel)`
  display: none;
  min-height: 4rem;
  padding: 1rem 0;
  border-top: none;
  background-color: white;
  flex-direction: row;
  align-items: center;
  // min-height: 24rem;
  &.is-selected {
    display: block;
  }
`;

export const ButtonWrapDelete = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 0 auto;
  width: 340px;
  div:nth-child(2) {
    width: 140px;
  }
`;

export const SpinnerApproveReq = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;

export const ButtonWrapConfirm = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 18px auto 0;
  width: 270px;
  div {
    min-width: 120px;
  }
`;

export const RejectButton = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
`;
