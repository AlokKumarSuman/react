import React, { Component } from "react";
import moment from "moment";
import PropTypes from "prop-types";
import { TabsMain, TabGroup, TabLists, TabPanels } from "./style";
import clientImg from "../../../../Assets/Images/img_avatar.png";
import { strings } from "./../../../../Localization/index";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // debugger;
    const { currentItem } = this.props;
    return (
      <TabsMain
        defaultIndex={0}
        selectedTabClassName="is-selected"
        selectedTabPanelClassName="is-selected"
      >
        <TabGroup>
          <TabLists>{strings.Basic_Details}</TabLists>
          <TabLists>{strings.Business_Details}</TabLists>
        </TabGroup>
        <TabPanels>
          <table className="table record-view-table">
            <tbody>
              <tr>
                <td align="center" colSpan="2">
                  <img
                    src={clientImg}
                    alt="Img"
                    className="client-image-view"
                  />
                </td>
              </tr>
              <tr>
                <td width="30%">
                  <b>{strings.User_id} </b>
                </td>
                <td>: {currentItem.USER_ID}</td>
              </tr>
              <tr>
                <td width="30%">
                  <b>{strings.Customer_Name} </b>
                </td>
                <td>: {currentItem.BUSINESS_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>{strings.Requested_Date} </b>
                </td>
                <td>
                  :&nbsp;
                  {moment(new Date(currentItem.REQUEST_DATE)).format(
                    "DD-MM-YYYY"
                  )}
                </td>
              </tr>

              <tr>
                <td>
                  <b>{strings.Last_Update} </b>
                </td>
                <td>
                  :&nbsp;
                  {moment(new Date(currentItem.LAST_UPDATE)).format(
                    "DD-MM-YYYY"
                  )}
                </td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Status} </b>
                </td>
                <td>: {currentItem.STATUS}</td>
              </tr>

              {currentItem.REASON ? (
                <tr>
                  <td width="30%">
                    <b>Reason </b>
                  </td>
                  <td>: {currentItem.REASON}</td>
                </tr>
              ) : null}

              {currentItem.COMMENTS ? (
                <tr>
                  <td width="30%">
                    <b>{strings.Comment} </b>
                  </td>
                  <td>: {currentItem.COMMENTS}</td>
                </tr>
              ) : null}
            </tbody>
          </table>
        </TabPanels>
        <TabPanels>
          <table className="table record-view-table">
            <tbody>
              <tr>
                <td align="center" colSpan="2">
                  <img
                    src={clientImg}
                    alt="Img"
                    className="client-image-view"
                  />
                </td>
              </tr>

              <tr>
                <td>
                  <b>{strings.Business_Type} </b>
                </td>
                <td>: {currentItem.BUSINESS_TYPE}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Business_Name} </b>
                </td>
                <td>: {currentItem.BUSINESS_NAME}</td>
              </tr>
              <tr>
                <td>
                  <b>{strings.Brand_Name} </b>
                </td>
                <td>: {currentItem.BRAND_NAME}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.GSTIN} </b>
                </td>
                <td>: {currentItem.GSTIN}</td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.PAN} </b>
                </td>
                <td>: {currentItem.PAN}</td>
              </tr>
            </tbody>
          </table>
        </TabPanels>
      </TabsMain>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
