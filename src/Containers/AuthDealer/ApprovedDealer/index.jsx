import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../Localization/index";
import moment from "moment";
import { Redirect } from "react-router-dom";
import Spinner from "./../../../Components/Spinner";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";
import { listApprovalRequests, userService } from "../../Config";
import { Auth } from "../../../Auth";
import Modal from "../../../Components/Modal";
import ViewRecord from "./ViewRecord";
import clientImg from "../../../Assets/Images/img_avatar.png";
import NoDataFound from "../../../Components/NoDataFound";

import {
  AuthDealerCard,
  AuthDealerHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  AuthDealerBody,
  ButtonWrapDelete,
  SpinnerConfigData,
  SpinnerApproveReq,
  ErrorMsg,
  ServerError
} from "./style";

class ApprovedDealer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      spinnerApproveReq: false,
      filterShow: false,
      searchByName: "",
      searchByType: "",
      searchBySubsFor: "",
      totalReqForApproval: [],
      openViewHandler: false,
      currentItem: [],
      responseStatusAPI: "",
      sortIcon: false,
      fetchErrorMsg: "",
      serverError: {}
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(listApprovalRequests, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LIMIT: "100",
        OFFSET: "0",
        STATUS: "APPROVED"
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            totalReqForApproval: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  filterHandler = () => {
    /* for filter toggle*/
    this.setState(prevState => ({
      filterShow: !prevState.filterShow
    }));
  };

  searchByNameHandler = e => {
    /* for name search */
    this.setState({
      searchByName: e.target.value
    });
  };

  searchByTypeHandler = e => {
    /*  for Action Type Search */
    this.setState({
      searchByType: e.target.value
    });
  };

  searchBySubsForHandler = e => {
    /*  for Action Type Search */
    this.setState({
      searchBySubsFor: e.target.value
    });
  };

  openViewHandler = (e, currentItems) => {
    /* for popup Open */
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler,
      currentItem: currentItems
    }));
  };

  closeViewHandler = () => {
    /* for popup close */
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler
    }));
  };

  sortBy = () => {
    var obj = [...this.state.totalReqForApproval];
    obj.reverse((a, b) => a.USER_ID - b.USER_ID);
    this.setState(prevState => ({
      totalReqForApproval: obj,
      sortIcon: !prevState.sortIcon
    }));
  };

  render() {
    // if (sessionStorage.length == 0) {
    //   return <Redirect to="/login" />;
    // }

    const { currentItem, spinnerConfigData, serverError } = this.state;

    let dataItems = this.state.totalReqForApproval;

    const searchByName = this.state.searchByName.trim().toLowerCase();
    const searchByType = this.state.searchByType.trim().toLowerCase();
    const searchBySubsFor = this.state.searchBySubsFor.trim().toLowerCase();

    if (searchByName.length > 0) {
      dataItems = this.state.totalReqForApproval.filter(function(i) {
        return i.CUSTOMER_NAME.toLowerCase().match(searchByName);
      });
    }

    if (searchByType.length > 0) {
      dataItems = this.state.totalReqForApproval.filter(function(i) {
        return i.BUSINESS_TYPE.toLowerCase().match(searchByType);
      });
    }

    if (searchBySubsFor.length > 0) {
      dataItems = this.state.totalReqForApproval.filter(function(i) {
        return i.BRAND_NAME.toLowerCase().match(searchBySubsFor);
      });
    }

    return (
      <AuthDealerCard>
        <AuthDealerHeader>
          <WrapperLeft>{strings.Authorised_Dealers}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              {/* <Button classname="blue" onclick={this.sortBy}>
                {this.state.sortIcon ? (
                  <Icon name="arrowUp" color="white" />
                ) : (
                  <Icon name="arrowDown" color="white" />
                )}
                Sort
              </Button> */}
              <Button classname="blue" onclick={this.filterHandler}>
                <Icon name="filter" color="white" /> {strings.Filter}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </AuthDealerHeader>

        {/* Modal Popup for View Dealer Details */}
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Dealer_Details}
          size="md"
        >
          <ViewRecord currentItem={currentItem} />
        </Modal>

        <AuthDealerBody toggleFilter={this.state.filterShow}>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="60px" style={{ minWidth: "60px" }}>
                  {strings.Image}
                </th>
                {/* <th width="110px">Subscription Id</th> */}
                <th width="40%" align="left">
                  {strings.Name}
                </th>
                <th width="20%" style={{ minWidth: "120px" }}>
                  {strings.Subscriber_Type}
                </th>
                <th width="20%">{strings.Subscribe_For}</th>

                <th width="15%">{strings.Subscribe_On}</th>
                <th width="80px" style={{ minWidth: "80px" }}>
                  {strings.Details}
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td />
                {/* <td /> */}
                <td>
                  <input
                    type="text"
                    value={this.state.searchByName}
                    onChange={this.searchByNameHandler}
                    placeholder="Search By Name..."
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchByType}
                    onChange={this.searchByTypeHandler}
                    placeholder="Search By Type..."
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchBySubsFor}
                    onChange={this.searchBySubsForHandler}
                    placeholder="Search By Type..."
                    className="textbox"
                  />
                </td>
                <td />
                <td />
              </tr>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="5">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : dataItems.length == 0 ? (
                <tr>
                  <td colSpan="6">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                dataItems.map((item, index) => (
                  <tr>
                    <td align="center">
                      <img src={clientImg} alt="Img" className="client-image" />
                    </td>
                    {/* <td>{item.REQUEST_ID}</td> */}
                    <td>{item.BUSINESS_NAME}</td>

                    <td align="center">{item.BUSINESS_TYPE}</td>

                    <td align="center">{item.BRAND_NAME}</td>

                    <td align="center">
                      {moment(new Date(item.REQUEST_DATE)).format("DD-MM-YYYY")}
                      {/* {new Date(item.REQUEST_DATE).getDate() +
                        "/" +
                        new Date(item.REQUEST_DATE).getMonth() +
                        "/" +
                        new Date(item.REQUEST_DATE).getFullYear()} */}
                    </td>

                    <td align="center">
                      <Button
                        type="primary"
                        size="fullwidth"
                        onclick={e => this.openViewHandler(e, item)}
                      >
                        View
                      </Button>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {serverError.status != "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
        </AuthDealerBody>
      </AuthDealerCard>
    );
  }
}

ApprovedDealer.propTypes = {
  currentItem: PropTypes.array,
  CUSTOMER_NAME: PropTypes.string,
  BUSINESS_NAME: PropTypes.string,
  BUSINESS_TYPE: PropTypes.string,
  BRAND_NAME: PropTypes.string,
  REQUEST_DATE: PropTypes.string,
  COMMENTS: PropTypes.string
};

export default ApprovedDealer;
