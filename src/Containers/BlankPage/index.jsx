import React, { Component } from "react";
import { Redirect } from "react-router-dom";

class BlankPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: ""
    };
  }

  componentDidMount() {
    location.reload();
  }

  render() {
    return <Redirect to="/dashboard" />;
  }
}

export default BlankPage;
