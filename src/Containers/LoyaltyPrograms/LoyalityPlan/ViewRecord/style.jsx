import styled from "styled-components";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

// Custom Tabs
export const TabsMain = styled(Tabs)`
  width: 100%;

  .test_truncate {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
  .inner_table {
    border: solid 1px #c5c5c5 !important;
    width: 100%;
    height: 18px;
  }
  .inner_table td {
    padding: 0 0.6rem;
    height: 18px;
  }
`;

export const TabGroup = styled(TabList)`
  list-style-type: none;
  display: flex;
  margin: 0;
`;

// TabGroup.tabsRole = "TabList";
export const TabLists = styled(Tab)`
  border: 1px solid #6f94ac;
  padding: 1.2rem 0.6rem;
  user-select: none;
  cursor: pointer;
  flex-grow: 1;
  width: 100%;
  background-color: #6f94ac;
  text-align: center;
  color: white;

  &.is-selected {
    background-color: #025063;
    border-bottom: none;
  }
`;
// TabLists.tabsRole = "Tab";
export const TabPanels = styled(TabPanel)`
  display: none;
  min-height: 4rem;
  padding: 1rem 0;
  border-top: none;
  background-color: white;
  flex-direction: row;
  align-items: center;
  min-height: 24rem;
  &.is-selected {
    display: block;
  }
`;
