import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import schemesImg from "../../../../Assets/Images/schemes.png";
import { strings } from "./../../../../Localization/index";

import { TabsMain, TabGroup, TabLists, TabPanels } from "./style";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { currentItem } = this.props;

    return (
      <TabsMain
        defaultIndex={0}
        selectedTabClassName="is-selected"
        selectedTabPanelClassName="is-selected"
      >
        <TabGroup>
          <TabLists>{strings.Basic_Scheme_Details}</TabLists>
          <TabLists>{strings.More_Details}</TabLists>
        </TabGroup>
        <TabPanels>
          <table className="table record-view-table">
            <tbody>
              <tr>
                <td align="center" colSpan="2">
                  {currentItem.IMAGE == "" ? (
                    <img
                      src={schemesImg}
                      alt="Img"
                      className="client-image-view"
                    />
                  ) : (
                    <img
                      src={currentItem.IMAGE}
                      alt="Img"
                      className="client-image-view"
                    />
                  )}
                </td>
              </tr>

              <tr>
                <td width="30%">
                  <b>{strings.Scheme_Name}</b>
                </td>
                <td>: {currentItem.SCHEME_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>{strings.Scheme_Type}</b>
                </td>
                <td>: {currentItem.SCHEME_TYPE_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>{strings.Scheme_Status}</b>
                </td>
                <td>: {currentItem.STATUS_NAME}</td>
              </tr>

              <tr>
                <td>
                  <b>{strings.Start_Date}</b>
                </td>
                <td>
                  :
                  {moment(new Date(currentItem.START_DATE)).format(
                    "DD-MM-YYYY"
                  )}
                </td>
              </tr>

              <tr>
                <td>
                  <b>{strings.End_Date}</b>
                </td>
                <td>
                  :{moment(new Date(currentItem.END_DATE)).format("DD-MM-YYYY")}
                </td>
              </tr>
              <tr>
                <td>
                  <b>{strings.Scheme_Description}</b>
                </td>
                <td>: {currentItem.DESCRIPTION}</td>
              </tr>
            </tbody>
          </table>
        </TabPanels>
        <TabPanels>
          <table className="table record-view-table">
            <tbody>
              <tr>
                <td align="center" colSpan="2">
                  {currentItem.IMAGE == "" ? (
                    <img
                      src={schemesImg}
                      alt="Img"
                      className="client-image-view"
                    />
                  ) : (
                    <img
                      src={currentItem.IMAGE}
                      alt="Img"
                      className="client-image-view"
                    />
                  )}
                </td>
              </tr>
              <tr>
                <td width="30%">
                  <b>{strings.Brand_Name}</b>
                </td>
                <td>
                  : <small>{currentItem.BRAND_NAME}</small>{" "}
                </td>
              </tr>
              <tr>
                <td width="30%">
                  <b>{strings.Created_By}</b>
                </td>
                <td>
                  : <small>{currentItem.CREATED_BY}</small>{" "}
                </td>
              </tr>
              <tr>
                <td width="30%">
                  <b>{strings.Created_Date}</b>
                </td>
                <td>
                  :
                  <small>
                    {moment(new Date(currentItem.CREATION_DATE)).format(
                      "DD-MM-YYYY"
                    )}
                  </small>
                </td>
              </tr>
              {currentItem.UPDATION_DATE ? (
                <tr>
                  <td width="30%">
                    <b>{strings.Created_By}</b>
                  </td>
                  <td>
                    :{" "}
                    <small>
                      {moment(new Date(currentItem.UPDATION_DATE)).format(
                        "DD-MM-YYYY"
                      )}
                    </small>
                  </td>
                </tr>
              ) : null}
              {/* /////////////////////// */}
              <tr>
                <td width="30%">
                  <b>Scheme Products</b>
                </td>
                <td style={{ display: "flex", alignItems: "center" }}>
                  :{" "}
                  <small className="test_truncate">
                    {currentItem.PRODUCTS.map(item => item.ITEM_NAME + ", ")}
                  </small>
                </td>
              </tr>

              <tr>
                <td width="30%">
                  <b>Member Type</b>
                </td>
                <td style={{ display: "flex", alignItems: "center" }}>
                  :{" "}
                  <small className="test_truncate">
                    {currentItem.MEMBER_TYPES.map(
                      item => item.MEMBER_TYPE_NAME + ", "
                    )}
                  </small>
                </td>
              </tr>

              <tr>
                <td width="30%">
                  <b>User Locations</b>
                </td>
                <td style={{ display: "flex", alignItems: "center" }}>
                  :{" "}
                  <small className="test_truncate">
                    {currentItem.LOCATIONS.map(
                      item => item.LOCATION_NAME + ", "
                    )}
                  </small>
                </td>
              </tr>

              {currentItem.USER_LEVELS.length > 0 ? (
                <tr>
                  <td width="30%" colSpan="2">
                    <level>
                      <b>User Levels</b>
                    </level>

                    <table className="inner_table">
                      <tr>
                        <td>
                          <small>
                            <i>Consumer Levels</i>
                          </small>
                        </td>
                        <td>
                          <small>
                            <i>Purchase Required (Rs)</i>
                          </small>
                        </td>
                        <td>
                          <small>
                            <i>Points Awarded</i>
                          </small>
                        </td>
                      </tr>
                      {currentItem.USER_LEVELS.map(item => (
                        <tr>
                          <td>
                            <small>{item.LEVEL_NAME} </small>
                          </td>
                          <td>
                            <small>{item.PURCHASE_REQUIRED} </small>
                          </td>
                          <td>
                            <small>{item.POINTS_EARN} </small>
                          </td>
                        </tr>
                      ))}
                    </table>
                  </td>
                </tr>
              ) : null}
            </tbody>
          </table>
        </TabPanels>
      </TabsMain>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
