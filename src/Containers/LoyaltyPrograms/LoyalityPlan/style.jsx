import styled from "styled-components";

export const SchemesCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .product-image {
    height: 4rem;
    width: 4rem;
  }

  .client-image {
    height: 4rem;
    width: 4rem;
    border-radius: 50%;
  }
  .client-image-view {
    height: 6rem;
    width: 6rem;
    border-radius: 50%;
  }
`;

export const SchemesHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;

  @media (max-width: 320px) {
    font-size: 12px;

    .green {
      font-size: 12px;
    }
  }
`;

export const SchemesBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;

  .table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};

    @media (max-width: 768px) {
      display: none !important;
    }
  }

  .table tbody tr td:first-child {
    text-transform: lowercase;
  }
  .table tbody tr td:first-child:first-letter {
    text-transform: capitalize;
  }

  @media (max-width: 768px) {
    td:nth-of-type(1):before {
      content: "Image";
    }
    td:nth-of-type(2):before {
      content: "Scheme Name";
    }
    td:nth-of-type(3):before {
      content: "Scheme Type";
    }
    td:nth-of-type(4):before {
      content: "Status";
    }
    td:nth-of-type(5):before {
      content: "Start At";
    }
    td:nth-of-type(6):before {
      content: "End At";
    }
    td:nth-of-type(7):before {
      content: "Action";
    }

    td:before {
      position: absolute;
      top: 50%;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      transform: translateY(-50%);
      text-transform: capitalize;
      font-weight: 600;
    }
  }

  table.table tbody tr td .green {
    @media (max-width: 768px) {
      width: 10rem;
    }
  }
`;
export const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  div {
    margin-right: 1rem;
  }
  div:last-child {
    margin-right: 0;
  }
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: auto;
    padding: 0.6rem 0.8rem;
  }
  div svg.icon {
    margin-right: 0.6rem;
    margin-top: 0px;
    width: 14px;
    height: 14px;
    transform: scale(1.5);
  }
  div svg.icon-fa {
    margin-right: 0.6rem;
  }

  @media (max-width: 768px) {
    .blue {
      display: none;
    }
  }
`;
export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const ButtonWrapDelete = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px auto 0 auto;
  width: 60%;
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
export const SpinnerApproveReq = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
