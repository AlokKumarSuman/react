import React, { Component } from "react";
import moment from "moment";
import { strings } from "../../../Localization";
import { Redirect } from "react-router-dom";
import Spinner from "../../../Components/Spinner";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";
import Modal from "../../../Components/Modal";
import NoDataFound from "../../../Components/NoDataFound";
import schemesImg from "../../../Assets/Images/schemes.png";
import AddLevels from "./AddRecord";

import {
  listAllLoyaltySchemes,
  deleteLoyaltyScheme,
  userService
} from "../../Config";
import { Auth } from "../../../Auth";

import ViewRecord from "./ViewRecord";

import {
  SchemesCard,
  SchemesHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  SchemesBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError,
  ButtonWrapDelete
} from "./style";

class LoyalityPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      filterShow: false,
      searchBySchemeName: "",
      searchBySchemeType: "",
      crrLevelLists: [],
      openViewHandler: false,
      currentItem: [],
      responseStatusAPI: "",
      sortIcon: false,
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      addNewLevelsModal: false,
      openDeleteHandler: false,
      responseDeleteLevelStatusAPI: ""
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(listAllLoyaltySchemes, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LANGUAGE_ID: "-1"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res.RESULT))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            crrLevelLists: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  filterHandler = () => {
    /* for filter toggle*/
    this.setState(prevState => ({
      filterShow: !prevState.filterShow
    }));
  };

  searchBySchemeNameHandler = e => {
    this.setState({
      searchBySchemeName: e.target.value
    });
  };

  searchBySchemeTypeHandler = e => {
    this.setState({
      searchBySchemeType: e.target.value
    });
  };

  addNewLevelsModal = () => {
    this.setState({
      addNewLevelsModal: true,
      currentItem: []
    });
  };
  openEditHandler = (e, currentItems) => {
    this.setState({
      addNewLevelsModal: true,
      currentItem: currentItems
    });
  };

  openViewHandler = (e, currentItems) => {
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler,
      currentItem: currentItems
    }));
  };

  openDeleteHandler = (e, currentItems) => {
    this.setState(prevState => ({
      openDeleteHandler: !prevState.openDeleteHandler,
      currentItem: currentItems,
      responseDeleteLevelStatusAPI: ""
    }));
  };

  closeViewHandler = () => {
    this.setState({
      openViewHandler: false,
      addNewLevelsModal: false,
      openDeleteHandler: false,
      currentItem: []
      // openEditHandler: false
    });
  };

  // Delete current Level
  deleteCurrentLevel = () => {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    const deleteLevelKey = this.state.currentItem;

    fetch(deleteLoyaltyScheme, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        SCHEME_ID: deleteLevelKey.SCHEME_ID
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        // console.log(res.response);
        if (res.STATUS == "SUCCESS") {
          // remove current item from table
          const data = this.state.crrLevelLists.filter(
            i => i.SCHEME_ID !== deleteLevelKey.SCHEME_ID
          );
          this.setState({
            crrLevelLists: data,
            responseDeleteLevelStatusAPI: res,
            currentItem: []
          });

          setTimeout(() => {
            this.setState({
              openDeleteHandler: false
            });
          }, 1500);
        } else {
          this.setState({
            responseDeleteLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorDelete: err
        })
      );
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }

    const {
      currentItem,
      spinnerConfigData,
      serverError,
      responseDeleteLevelStatusAPI,
      serverErrorDelete
    } = this.state;

    let dataItems = this.state.crrLevelLists;

    const searchBySchemeName = this.state.searchBySchemeName
      .trim()
      .toLowerCase();
    const searchBySchemeType = this.state.searchBySchemeType
      .trim()
      .toLowerCase();

    if (searchBySchemeName.length > 0) {
      dataItems = this.state.crrLevelLists.filter(function(i) {
        return i.SCHEME_NAME.toLowerCase().match(searchBySchemeName);
      });
    }

    if (searchBySchemeType.length > 0) {
      dataItems = this.state.crrLevelLists.filter(function(i) {
        return i.SCHEME_TYPE_NAME.toLowerCase().match(searchBySchemeType);
      });
    }

    return (
      <SchemesCard>
        <SchemesHeader>
          <WrapperLeft>{strings.Current_Loyality_plans}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              <Button classname="green" onclick={this.addNewLevelsModal}>
                <Icon name="addCircle" color="white" /> {strings.Add_New_Scheme}
              </Button>
              <Button classname="blue" onclick={this.filterHandler}>
                <Icon name="filter" color="white" /> {strings.Filter}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </SchemesHeader>

        {/* Modal Popup for View Levels */}
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Scheme_Details}
          size="md"
        >
          <ViewRecord currentItem={currentItem} />
        </Modal>

        {/* Modal Popup for Add New Levels */}
        <Modal
          modalOpen={this.state.addNewLevelsModal}
          onclick={this.closeViewHandler}
          title={
            currentItem != "" ? strings.Update_Schemes : strings.Add_New_Scheme
          }
        >
          <AddLevels
            onclickClose={this.closeViewHandler}
            // addNewLevels={this.addNewLevelsHandler}
            upd={currentItem}
            // updateSchemes={this.updateSchemes}
          />
        </Modal>

        {/* Modal Popup for Delete current Levels */}
        <Modal
          modalOpen={this.state.openDeleteHandler}
          onclick={this.closeViewHandler}
          title={strings.Delete_Current_Level}
          size="sm"
        >
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              textTransform: "none",
              color: "#000",
              fontFamily: "Gotham"
            }}
          >
            {strings.Are_you_sure_you_want_to_delete_this}
          </h4>

          <ServerError>
            {serverErrorDelete.status} {serverErrorDelete.statusText}
          </ServerError>

          <ButtonWrapDelete>
            <Button classname="red" onclick={this.deleteCurrentLevel}>
              {strings.Yes}
            </Button>
            <Button classname="blue" onclick={this.closeViewHandler}>
              {strings.No}
            </Button>
          </ButtonWrapDelete>
          {!(
            responseDeleteLevelStatusAPI == "undefined" ||
            responseDeleteLevelStatusAPI == ""
          ) ? (
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                fontSize: "13px",
                color: "red",
                paddingTop: "10px"
              }}
            >
              {responseDeleteLevelStatusAPI.STATUS +
                ": " +
                responseDeleteLevelStatusAPI.MESSAGE}
            </h4>
          ) : null}
        </Modal>

        <SchemesBody toggleFilter={this.state.filterShow}>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="10%">{strings.Image}</th>
                <th width="30%" align="left">
                  Plan Name
                </th>
                <th width="20%" align="left">
                  Plan Type
                </th>
                <th width="12%">Plan Status</th>

                <th width="10%" style={{ minWidth: "120px" }}>
                  {strings.Start_At}
                </th>
                <th width="10%" style={{ minWidth: "120px" }}>
                  {strings.End_At}
                </th>
                <th width="80px" style={{ minWidth: "80px" }}>
                  {strings.Action}
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td />
                <td>
                  <input
                    type="text"
                    value={this.state.searchBySchemeName}
                    onChange={this.searchBySchemeNameHandler}
                    placeholder="Search By Name..."
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchBySchemeType}
                    onChange={this.searchBySchemeTypeHandler}
                    placeholder="Search By Type..."
                    className="textbox"
                  />
                </td>
                <td />
                <td />
                <td />
                <td />
              </tr>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="6">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : dataItems.length == 0 ? (
                <tr>
                  <td colSpan="6">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                dataItems.map((item, index) => (
                  <tr key={index}>
                    <td align="center">
                      {item.IMAGE == "" ? (
                        <img
                          src={schemesImg}
                          alt="Img"
                          className="client-image"
                        />
                      ) : (
                        <img
                          src={item.IMAGE}
                          alt="Img"
                          className="client-image"
                        />
                      )}
                    </td>
                    <td>{item.SCHEME_NAME}</td>
                    <td>{item.SCHEME_TYPE_NAME}</td>
                    <td align="center">{item.STATUS_NAME}</td>

                    <td align="center">
                      {moment(new Date(item.START_DATE)).format("DD-MM-YYYY")}
                    </td>
                    <td align="center">
                      {moment(new Date(item.END_DATE)).format("DD-MM-YYYY")}
                    </td>
                    <td align="center">
                      <ButtonGroup>
                        <Icon
                          name="pen"
                          onclick={e => this.openEditHandler(e, item)}
                        />
                        <Icon
                          name="trash"
                          onclick={e => this.openDeleteHandler(e, item)}
                        />
                        <Icon
                          name="eye"
                          onclick={e => this.openViewHandler(e, item)}
                        />
                      </ButtonGroup>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
          {this.state.serverError.statusText == "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
        </SchemesBody>
      </SchemesCard>
    );
  }
}

// Schemes.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default LoyalityPlan;
