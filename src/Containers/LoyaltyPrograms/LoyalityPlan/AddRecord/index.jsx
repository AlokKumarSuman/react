import React, { Component } from "react";
import { strings } from "./../../../../Localization/index";
import PropTypes from "prop-types";
import moment from "moment";
import Button from "../../../../Components/Button";
// import Icon from "../../../../Components/Icons";
import Modal from "../../../../Components/Modal";
import { Auth } from "../../../../Auth";
import UploadImage from "./UploadImage";
import Spinner from "../../../../Components/Spinner";

import {
  userService,
  addNewLoyaltyScheme,
  schemeStatus,
  schemeType,
  itemsByBrandId,
  fetchLocationsForSchemes,
  fetchAvailableConsumerLevels,
  updateLoyaltyScheme,
  fetchMemberTypesList
} from "../../../Config";

import {
  Form,
  FormRow,
  // FormRowFull,
  LabelInputGroup,
  LabelButtonDU,
  ErrorMsg,
  ServerError,
  SucessMsg,
  FormRowDivider,
  MilestoneHeader,
  WrapperLeftMilestone,
  WrapperRightMilestone,
  ButtonGroupMilestone,
  SpinnerWrap,
  RadioGroup
} from "./style";

class AddRecord extends Component {
  constructor(props) {
    super(props);
    // this.inputRef = React.createRef();
    this.state = {
      searchProduct: "",
      searchMemberTypes: "",
      searchConsumerLevels: "",
      searchLoaction: "",
      productsDD: false,
      MemberTypesDD: false,
      consumerLevelDD: false,
      consumerLocationDD: false,
      consumerLocationSearch: "",
      selectLevelName: [],
      //default fetch
      fetchSchemeType: [],
      fetchConsumerLevelScheme: [],
      fetchUserlocation: [],
      fetchAllProduct: [],
      fetchMemberTypes: [],
      fetchAvailableReward: [],
      // setState editable content
      addSchemeName: "",

      addSchemeType: "",
      addSchemeImage: "",

      addSchemeStatus: "",
      addDescription: "",
      addStartDate: "",
      addEndDate: "",
      addProductList: [],
      addMemberTypesList: [],
      addLocationList: [],
      addConsumerLevels: [],
      // server image
      imageNameByServer: "",
      addSchemeTypeId: "",
      //
      addRewardId: "",
      addUsersList: "",
      // Server responses
      responseAddSucess: "",
      responseError: "",
      serverErrorStatus: {},
      validateFilds: "",
      // Upload Images
      uploadImageModel: false,
      // On update values
      upd: [],
      disabledEdit: false
      // searchDisable: false
    };
    // this.textInput = React.createRef();
  }

  handleChange = evt => {
    // debugger;
    this.setState({
      [evt.target.name]: evt.target.value,
      responseAddSucess: "",
      responseError: "",
      validateFilds: "",
      serverErrorStatus: {}
    });

    if (evt.target.name === "addSchemeType") {
      let options = evt.target.options;
      const id = options[options.selectedIndex].id;
      this.setState({
        addSchemeTypeId: id
      });
    }
  };

  // Component Did Mount
  componentDidMount() {
    const upd = this.props.upd;
    // debugger;
    console.log(upd);

    if (upd != "") {
      // disabledEdit
      if (
        upd.STATUS_ID == "SUBMITTED" ||
        upd.STATUS_ID == "APPROVED" ||
        upd.STATUS_ID == "LIVE" ||
        upd.STATUS_ID == "SUSPENDED" ||
        upd.STATUS_ID == "TERMINATE" ||
        upd.STATUS_ID == "REJECTED" ||
        upd.STATUS_ID == "EXPIRED"
      ) {
        this.setState({
          disabledEdit: true
        });
      } else {
        this.setState({
          disabledEdit: false
        });
      }

      const addStartDateFormated = moment(
        moment(upd.START_DATE, "YYYY-MM-DD")
      ).format("YYYY-MM-DD");

      const addEndDateFormated = moment(
        moment(upd.END_DATE, "YYYY-MM-DD")
      ).format("YYYY-MM-DD");

      // product default selected
      let productDefaultSelected = [];
      for (var i = 0; i < upd.PRODUCTS.length; i++) {
        productDefaultSelected.push(upd.PRODUCTS[i].ITEM_ID.toString());
      }

      // Member Type default selected
      let MemberDefaultSelected = [];
      for (var i = 0; i < upd.MEMBER_TYPES.length; i++) {
        MemberDefaultSelected.push(
          upd.MEMBER_TYPES[i].MEMBER_TYPE_ID.toString()
        );
      }

      // Location Type default selected
      let LocationDefaultSelected = [];
      for (var i = 0; i < upd.LOCATIONS.length; i++) {
        LocationDefaultSelected.push(upd.LOCATIONS[i].LOCATION_ID.toString());
      }

      // Level Type default selected
      let LevelsDefaultSelected = [];
      for (var i = 0; i < upd.USER_LEVELS.length; i++) {
        LevelsDefaultSelected.push(upd.USER_LEVELS[i].LEVEL_NAME.toString());
      }

      // debugger;
      this.setState({
        upd: upd,
        //
        addSchemeName: upd.SCHEME_NAME,
        addSchemeType: upd.SCHEME_TYPE_NAME,
        addSchemeImage: upd.IMAGE,
        addSchemeStatus: upd.STATUS_ID,
        addDescription: upd.DESCRIPTION,
        addStartDate: addStartDateFormated,
        addEndDate: addEndDateFormated,
        ///////
        imageNameByServer: upd.IMAGE,
        addSchemeTypeId: upd.SCHEME_TYPE_ID,
        //
        addProductList: productDefaultSelected,
        addMemberTypesList: MemberDefaultSelected,
        //
        addLocationList: LocationDefaultSelected,
        //
        fetchConsumerLevelScheme: upd.USER_LEVELS,
        selectLevelName: LevelsDefaultSelected,
        addConsumerLevels: upd.USER_LEVELS
      });
      // return;
    }
    //////////
    // debugger;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    const languageCode = -1;
    const productInputs = `LANGUAGE_CODE=${languageCode}&BRAND_ID=${authBrandId}`;

    Promise.all([
      fetch(schemeType, {
        method: "GET",
        headers: Auth
      }),
      fetch(fetchAvailableConsumerLevels, {
        method: "POST",
        headers: Auth,
        body: JSON.stringify({
          BRAND_ID: authBrandId,
          USER_ID: authUserName
        })
      }),
      fetch(itemsByBrandId + productInputs, {
        method: "POST",
        headers: Auth
      }),
      fetch(fetchMemberTypesList, {
        method: "POST",
        headers: Auth,
        body: JSON.stringify({
          USER_ID: authUserName,
          BRAND_ID: authBrandId,
          LANGUAGE_ID: "-1"
        })
      }),
      fetch(fetchLocationsForSchemes, {
        method: "POST",
        headers: Auth,
        body: JSON.stringify({
          BRAND_ID: authBrandId,
          LOCATION_NAME: "",
          OPTION: "BUSABHI"
        })
      })
    ])
      .then(([res1, res2, res3, res4, res5]) =>
        Promise.all([
          res1.json(),
          res2.json(),
          res3.json(),
          res4.json(),
          res5.json()
        ])
      )
      // .then(([res1, res2, res3, res4, res5]) => console.log(res5))
      .then(
        ([
          addSchemeType,
          addConsumerLevel,
          addProductList,
          addMemberTypesList,
          addLocationList
        ]) => {
          // product default selected
          let productDefaultSelected = [];
          let productResult = addProductList.RESULT;
          for (var i = 0; i < productResult.length; i++) {
            productDefaultSelected.push(productResult[i].ITEM_ID.toString());
          }
          // member type default selected
          let memberTypeDefaultSelected = addMemberTypesList.RESULT[0].TYPE_ID.toString();
          //
          let schemeTypeDefaultSelected = addSchemeType.response.docs[0].SCHEME_TYPE_NAME.toString();
          let schemeTypeIdDefaultSelected = addSchemeType.response.docs[0].SCHEME_TYPE_ID.toString();

          if (
            addConsumerLevel.STATUS == "SUCCESS" &&
            addProductList.STATUS == "SUCCESS" &&
            addMemberTypesList.STATUS == "SUCCESS" &&
            addLocationList.STATUS == "SUCCESS"
          ) {
            if (upd != "") {
              this.setState({
                fetchSchemeType: addSchemeType.response.docs,
                fetchConsumerLevelScheme: addConsumerLevel.RESULT,
                fetchAllProduct: addProductList.RESULT,
                fetchMemberTypes: addMemberTypesList.RESULT,
                fetchUserlocation: addLocationList.RESULT
              });
            } else {
              this.setState({
                fetchSchemeType: addSchemeType.response.docs,
                fetchConsumerLevelScheme: addConsumerLevel.RESULT,
                fetchAllProduct: addProductList.RESULT,
                fetchMemberTypes: addMemberTypesList.RESULT,
                fetchUserlocation: addLocationList.RESULT,
                /////default selected
                addSchemeStatus: "SAVED",
                addSchemeType: schemeTypeDefaultSelected,
                addSchemeTypeId: schemeTypeIdDefaultSelected,
                addProductList: productDefaultSelected,
                addMemberTypesList: [memberTypeDefaultSelected]
              });
            }
          } else {
            this.setState({
              responseError: "Failed to fetch."
            });
          }
        }
      )
      .catch(err => {
        console.log(err);
      });
  }

  // Upload image Modal
  uploadImageModelHandler = e => {
    this.setState({
      uploadImageModel: true,
      inputError: ""
    });
  };
  uploadImageModelHandlerClose = e => {
    this.setState({
      uploadImageModel: false
    });
  };

  displayImageNameHandler = (displayImageName, imageNameByServers) => {
    this.setState({
      addSchemeImage: displayImageName,
      imageNameByServer: imageNameByServers
    });
  };

  // Add New record
  addNewRecord = e => {
    e.preventDefault();
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    const {
      addSchemeName,
      addSchemeType,
      addSchemeImage,
      addSchemeStatus,
      addDescription,
      addStartDate,
      addEndDate,
      addProductList,
      addMemberTypesList,
      addLocationList,
      addConsumerLevels,
      // server image
      imageNameByServer,
      addSchemeTypeId,
      addRewardId,
      addUsersList
    } = this.state;
    // debugger;

    //  start Validate Fields
    if (addSchemeTypeId === "WLMT") {
      if (
        addSchemeName === "" ||
        addSchemeType === "" ||
        addSchemeImage === "" ||
        addSchemeStatus === "" ||
        addDescription === "" ||
        addStartDate === "" ||
        addEndDate === ""
      ) {
        return this.setState({
          validateFilds: strings.All_field_must_be_filled_out
        });
      }
    } else {
      if (
        addSchemeName === "" ||
        addSchemeType === "" ||
        addSchemeImage === "" ||
        addSchemeStatus === "" ||
        addDescription === "" ||
        addStartDate === "" ||
        addEndDate === ""
      ) {
        return this.setState({
          validateFilds: strings.All_field_must_be_filled_out
        });
      }
      if (addProductList.length === 0) {
        return this.setState({
          validateFilds: strings.Plaese_add_products
        });
      } else if (addConsumerLevels.length === 0) {
        return this.setState({
          validateFilds: strings.Plaese_add_consumer_levels
        });
      } else if (addLocationList.length === 0) {
        return this.setState({
          validateFilds: strings.Plaese_add_Locations
        });
      } else if (addMemberTypesList.length === 0) {
        return this.setState({
          validateFilds: strings.Plaese_add_Member_Type
        });
      }
    }
    // End Validate Fields

    const addStartDateFormated = moment(new Date(addStartDate)).format(
      "DD-MM-YYYY"
    );
    const addEndDateFormated = moment(new Date(addEndDate)).format(
      "DD-MM-YYYY"
    );

    let addConsumerLevelsAdd = [];
    let addUserlocationString = "";
    let addProductListString = "";
    let addMemberTypesListString = "";

    if (addSchemeTypeId === "WLMT") {
      addConsumerLevelsAdd;
    } else {
      for (var i = 0; i < addConsumerLevels.length; i++) {
        addConsumerLevelsAdd.push({
          LEVEL_ID: addConsumerLevels[i].LEVEL_ID,
          PURCHASE_REQUIRED: addConsumerLevels[i].PURCHASE_REQUIRED,
          POINTS_EARN: addConsumerLevels[i].POINTS_EARN
        });
      }
      addUserlocationString = addLocationList.toString();
      addProductListString = addProductList.toString();
      addMemberTypesListString = addMemberTypesList.toString();
    }

    // debugger;

    fetch(addNewLoyaltyScheme, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        LANGUAGE_ID: "-1",

        START_DATE: addStartDateFormated,
        END_DATE: addEndDateFormated,
        REWARD_ID: addRewardId,
        STATUS: addSchemeStatus,
        SCHEME_TYPE: addSchemeTypeId,

        SCHEME_NAME: addSchemeName,
        DESCRIPTION: addDescription,
        IMAGE_URL: imageNameByServer,
        PRODUCTS: addProductListString,
        MEMBER_TYPES: addMemberTypesListString,
        USERS: addUsersList,
        LOCATIONS: addUserlocationString,
        USER_LEVELS: addConsumerLevelsAdd
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddSucess: res.MESSAGE
          });

          setTimeout(() => {
            location.reload();
          }, 2000);
        } else {
          this.setState({
            responseError: res.MESSAGE
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorStatus: err
        })
      );
  };

  // Update Record
  updateRecord = e => {
    e.preventDefault();
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    const {
      addSchemeName,
      addSchemeType,
      addSchemeImage,
      addSchemeStatus,
      addDescription,
      addStartDate,
      addEndDate,
      addProductList,
      addMemberTypesList,
      addLocationList,
      addConsumerLevels,
      // server image
      imageNameByServer,
      addSchemeTypeId,
      addRewardId,
      addUsersList,
      //
      upd
    } = this.state;

    // debugger;

    // Start Validate Fields
    if (addSchemeTypeId === "WLMT") {
      if (
        addSchemeName === "" ||
        addSchemeType === "" ||
        addSchemeImage === "" ||
        addSchemeStatus === "" ||
        addDescription === "" ||
        addStartDate === "" ||
        addEndDate === ""
      ) {
        return this.setState({
          validateFilds: strings.All_field_must_be_filled_out
        });
      }
    } else {
      if (
        addSchemeName === "" ||
        addSchemeType === "" ||
        addSchemeImage === "" ||
        addSchemeStatus === "" ||
        addDescription === "" ||
        addStartDate === "" ||
        addEndDate === ""
      ) {
        return this.setState({
          validateFilds: strings.All_field_must_be_filled_out
        });
      }
      if (addProductList.length === 0) {
        return this.setState({
          validateFilds: strings.Plaese_add_products
        });
      } else if (addConsumerLevels.length === 0) {
        return this.setState({
          validateFilds: strings.Plaese_add_consumer_levels
        });
      } else if (addLocationList.length === 0) {
        return this.setState({
          validateFilds: strings.Plaese_add_Locations
        });
      } else if (addMemberTypesList.length === 0) {
        return this.setState({
          validateFilds: strings.Plaese_add_Member_Type
        });
      }
    }
    // End Validate Fields

    const addStartDateFormated = moment(new Date(addStartDate)).format(
      "DD-MM-YYYY"
    );
    const addEndDateFormated = moment(new Date(addEndDate)).format(
      "DD-MM-YYYY"
    );

    let addConsumerLevelsAdd = [];
    let addUserlocationString = "";
    let addProductListString = "";
    let addMemberTypesListString = "";
    // debugger;
    if (addSchemeTypeId === "WLMT") {
      addConsumerLevelsAdd;
    } else {
      for (var i = 0; i < addConsumerLevels.length; i++) {
        addConsumerLevelsAdd.push({
          LEVEL_ID: addConsumerLevels[i].LEVEL_ID,
          LEVEL_NAME: addConsumerLevels[i].LEVEL_NAME,
          POINTS_EARN: addConsumerLevels[i].POINTS_EARN,
          PURCHASE_REQUIRED: addConsumerLevels[i].PURCHASE_REQUIRED
        });
      }
      addUserlocationString = addLocationList.toString();
      addProductListString = addProductList.toString();
      addMemberTypesListString = addMemberTypesList.toString();
    }

    //////
    let updatedSchemeId = upd.SCHEME_ID;
    // debugger;
    fetch(updateLoyaltyScheme, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        LANGUAGE_ID: "-1",

        START_DATE: addStartDateFormated,
        END_DATE: addEndDateFormated,
        REWARD_ID: addRewardId,
        STATUS: addSchemeStatus,
        SCHEME_TYPE: addSchemeTypeId,

        SCHEME_NAME: addSchemeName,
        DESCRIPTION: addDescription,
        IMAGE_URL: imageNameByServer,
        PRODUCTS: addProductListString,
        MEMBER_TYPES: addMemberTypesListString,
        USERS: addUsersList,
        LOCATIONS: addUserlocationString,
        USER_LEVELS: addConsumerLevelsAdd,

        SCHEME_ID: updatedSchemeId
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddSucess: res.MESSAGE
          });

          setTimeout(() => {
            // this.props.updateSchemes(formValue);
            location.reload();
          }, 2000);
        } else {
          this.setState({
            responseError: res.MESSAGE
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorStatus: err
        })
      );
  };

  // Product Type Search
  productDDHandler = () => {
    this.setState(prevState => ({
      productsDD: !prevState.productsDD,
      consumerLevelDD: false,
      consumerLocationDD: false,
      MemberTypesDD: false
    }));
  };
  searchProductsHandler = e => {
    this.setState({
      searchProduct: e.target.value
    });
  };
  selectProducts = e => {
    // debugger;
    if (e.currentTarget.checked) {
      let addProductName = this.state.addProductList.slice();
      addProductName.push(e.currentTarget.id);
      this.setState({
        addProductList: addProductName
      });
    } else {
      if (this.state.fetchAllProduct.length > "0") {
        let data = this.state.addProductList.filter(
          i => i !== e.currentTarget.id
        );
        // console.log(data);
        this.setState({
          addProductList: data
        });
      }
    }
  };
  // clear all MemberType
  clearSelectProducts = e => {
    let addProductName = [];
    this.setState({
      addProductList: addProductName
    });
  };

  /// member type search
  MemberTypesDDHandler = () => {
    this.setState(prevState => ({
      MemberTypesDD: !prevState.MemberTypesDD,
      consumerLevelDD: false,
      productsDD: false,
      consumerLocationDD: false
    }));
  };
  searchMemberTypesHandler = e => {
    this.setState({
      searchMemberTypes: e.target.value
    });
  };
  selectMember = e => {
    // debugger;
    if (e.currentTarget.checked) {
      let addMemberTypes = [];
      addMemberTypes = this.state.addMemberTypesList.slice();
      addMemberTypes.push(e.currentTarget.id);
      this.setState({
        addMemberTypesList: addMemberTypes
      });
    } else {
      if (this.state.fetchMemberTypes.length > "0") {
        let data = this.state.addMemberTypesList.filter(
          i => i !== e.currentTarget.id
        );
        // console.log(data);
        this.setState({
          addMemberTypesList: data
        });
      }
    }
  };
  // clear all MemberType
  clearSelectMember = e => {
    let addMemberTypes = [];
    this.setState({
      addMemberTypesList: addMemberTypes
    });
  };

  //User Location Search Area and dropdown Start //
  userLocationDDHandler = () => {
    this.setState(prevState => ({
      consumerLocationDD: !prevState.consumerLocationDD,
      consumerLevelDD: false,
      productsDD: false,
      MemberTypesDD: false
    }));
  };

  selectLocations = e => {
    // debugger;
    if (e.currentTarget.checked) {
      let addLocationName = this.state.addLocationList.slice();
      addLocationName.push(e.currentTarget.id);
      this.setState({
        addLocationList: addLocationName
      });
    } else {
      if (this.state.fetchUserlocation.length > "0") {
        let data = this.state.addLocationList.filter(
          i => i !== e.currentTarget.id
        );
        // console.log(data);
        this.setState({
          addLocationList: data
        });
      }
    }
  };
  // clear all selected locations
  clearSelectLocations = e => {
    let addLocationName = [];
    this.setState({
      addLocationList: addLocationName
    });
  };

  // search location by pin
  searchLoactionHandler = e => {
    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === "" || re.test(e.target.value)) {
      let currValue = e.target.value;
      this.setState(
        {
          consumerLocationSearch: currValue
        },
        () => {
          this.autocompleteSearch(currValue);
        }
      );
    }
  };

  autocompleteSearch = () => {
    const authBrandId = userService.authBrandId();
    const { consumerLocationSearch } = this.state;
    fetch(fetchLocationsForSchemes, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        LOCATION_NAME: consumerLocationSearch,
        OPTION: "BASABHI"
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res.RESULT));
      .then(addLocationList =>
        this.setState({
          fetchUserlocation: addLocationList.RESULT
        })
      )
      .catch(err => console.log(err));
  };

  // consumer levels levels
  ConsumerLevelsDDHandler = () => {
    this.setState(prevState => ({
      consumerLevelDD: !prevState.consumerLevelDD,
      productsDD: false,
      consumerLocationDD: false,
      MemberTypesDD: false
    }));
  };

  searchConsumerLevelsHandler = e => {
    this.setState({
      searchConsumerLevels: e.target.value
    });
  };

  selectLevels = e => {
    // debugger;
    if (e.currentTarget.checked) {
      // debugger;
      let addLevelName = this.state.selectLevelName.slice();
      let addLevelData = this.state.addConsumerLevels;
      addLevelName.push(e.currentTarget.name);
      addLevelData.push({
        LEVEL_ID: e.currentTarget.id,
        LEVEL_NAME: e.currentTarget.name,
        PURCHASE_REQUIRED: "",
        POINTS_EARN: ""
      });
      this.setState({
        selectLevelName: addLevelName,
        addConsumerLevels: addLevelData
      });
    } else {
      if (this.state.addConsumerLevels.length > "0") {
        const filterData = this.state.selectLevelName.filter(
          i => i !== e.currentTarget.name
        );

        const filterLevelData = this.state.addConsumerLevels.filter(
          i => i.LEVEL_ID !== e.currentTarget.id
        );

        // debugger;
        this.setState({
          selectLevelName: filterData,
          addConsumerLevels: filterLevelData
        });
      }
    }
  };

  // Consumer Level derails
  dynamicHandler = (evt, index, key) => {
    const { addConsumerLevels } = this.state;
    // checked user can add only numbers
    const re = /^[0-9\b]+$/;
    if (evt.target.value === "" || re.test(evt.target.value)) {
      if (key === "value")
        addConsumerLevels[index].PURCHASE_REQUIRED = evt.target.value;
      else addConsumerLevels[index].POINTS_EARN = evt.target.value;
      this.setState({
        addConsumerLevels
      });
    }
  };

  //User Location Search Area and dropdown Finish //
  // openCalanderHandler = e => {
  //   this.textInput.current.focus();
  // };

  render() {
    const {
      addPurchasedValue,
      addPurchasedPoint,
      productsDD,
      MemberTypesDD,
      consumerLocationDD,
      consumerLevelDD,
      fetchSchemeType,
      // fetchUserNamesForScheme,
      fetchConsumerLevelScheme,
      fetchUserlocation,
      fetchAllProduct,
      fetchMemberTypes,
      // fetchRewardType,
      //
      addSchemeName,
      addSchemeStatus,
      addSchemeType,
      addSchemeImage,
      // addSchemeUser,
      addConsumerLevels,
      addLocationList,
      addProductList,
      addMemberTypesList,
      addStartDate,
      addEndDate,
      // addRewardName,
      addDescription,
      ///
      responseAddSucess,
      responseError,
      validateFilds,
      //
      selectLevelName,

      upd,
      serverErrorStatus,
      //
      disabledEdit
    } = this.state;
    // debugger;

    let dataItems = this.state.fetchAllProduct;
    const searchProduct = this.state.searchProduct.trim().toLowerCase();
    if (searchProduct.length > 0) {
      dataItems = this.state.fetchAllProduct.filter(function(i) {
        return i.NAME.toLowerCase().match(searchProduct);
      });
    }

    let memberTypesDataItems = this.state.fetchMemberTypes;
    const searchMemberTypes = this.state.searchMemberTypes.trim().toLowerCase();
    if (searchMemberTypes.length > 0) {
      memberTypesDataItems = this.state.fetchMemberTypes.filter(function(i) {
        return i.TYPE_NAME.toLowerCase().match(searchMemberTypes);
      });
    }

    let consumerLevelDataItems = this.state.fetchConsumerLevelScheme;
    const searchConsumerLevels = this.state.searchConsumerLevels
      .trim()
      .toLowerCase();
    if (searchConsumerLevels.length > 0) {
      consumerLevelDataItems = this.state.fetchConsumerLevelScheme.filter(
        function(i) {
          return i.NAME.toLowerCase().match(searchConsumerLevels);
        }
      );
    }

    let locationLevelDataItems = this.state.fetchUserlocation;
    const searchLoaction = this.state.searchLoaction.trim().toLowerCase();
    if (searchLoaction.length > 0) {
      locationLevelDataItems = this.state.fetchUserlocation.filter(function(i) {
        return i.NAME.toLowerCase().match(searchLoaction);
      });
    }

    // Radio group renders based on plan status
    let renderRadioGroup;
    if (
      this.state.addSchemeStatus === "SAVED" ||
      this.state.addSchemeStatus === "SUBMITTED"
    ) {
      renderRadioGroup = (
        <RadioGroup>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="SAVED"
              id="idSaved"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "SAVED" ? true : false}
              disabled={this.state.disabledEdit}
            />
            <label htmlFor="idSaved">Saved</label>
          </div>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="SUBMITTED"
              id="idSubmitted"
              onClick={this.handleChange}
              checked={
                this.state.addSchemeStatus === "SUBMITTED" ? true : false
              }
              // disabled={this.state.disabledEdit}
            />
            <label htmlFor="idSubmitted">Submitted</label>
          </div>
        </RadioGroup>
      );
    } else if (
      this.state.addSchemeStatus === "APPROVED" ||
      this.state.addSchemeStatus === "REJECTED"
    ) {
      renderRadioGroup = (
        <RadioGroup>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="APPROVED"
              id="idApproved"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "APPROVED" ? true : false}
              disabled={
                this.state.addSchemeStatus === "APPROVED"
                  ? false
                  : this.state.disabledEdit
              }
            />
            <label htmlFor="idApproved">Approved</label>
          </div>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="REJECTED"
              id="idRejected"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "REJECTED" ? true : false}
              disabled={
                this.state.addSchemeStatus === "REJECTED"
                  ? false
                  : this.state.disabledEdit
              }
              // disabled={this.state.disabledEdit}
            />
            <label htmlFor="idRejected">Rejected</label>
          </div>
        </RadioGroup>
      );
    } else if (
      this.state.addSchemeStatus === "LIVE" ||
      this.state.addSchemeStatus === "SUSPENDED" ||
      this.state.addSchemeStatus === "TERMINATE"
    ) {
      renderRadioGroup = (
        <RadioGroup>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="SUSPENDED"
              id="idSuspended"
              onClick={this.handleChange}
              checked={
                this.state.addSchemeStatus === "SUSPENDED" ? true : false
              }
            />
            <label htmlFor="idSuspended">Suspended</label>
          </div>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="TERMINATE"
              id="idTerminated"
              onClick={this.handleChange}
              checked={
                this.state.addSchemeStatus === "TERMINATE" ? true : false
              }
            />
            <label htmlFor="idTerminated">Terminate</label>
          </div>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="LIVE"
              id="idLive"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "LIVE" ? true : false}
            />
            <label htmlFor="idLive">Live</label>
          </div>
        </RadioGroup>
      );
    } else if (this.state.addSchemeStatus === "EXPIRED") {
      renderRadioGroup = (
        <RadioGroup>
          <div>
            <input
              type="radio"
              name="addSchemeStatus"
              value="EXPIRED"
              id="idExpired"
              onClick={this.handleChange}
              checked={this.state.addSchemeStatus === "EXPIRED" ? true : false}
            />
            <label htmlFor="idExpired">Expired</label>
          </div>
        </RadioGroup>
      );
    }

    let submitButtons;
    /* eslint-disable */
    submitButtons =
      this.state.addSchemeStatus === "SAVED" ||
      this.state.addSchemeStatus === "SUBMITTED" ||
      this.state.addSchemeStatus === "LIVE" ||
      this.state.addSchemeStatus === "SUSPENDED" ||
      this.state.addSchemeStatus === "TERMINATE" ? (
        upd != "" ? (
          (upd.STATUS_ID === "SAVED" &&
            this.state.addSchemeStatus === "SAVED") ||
          (upd.STATUS_ID === "SUBMITTED" &&
            this.state.addSchemeStatus === "SUBMITTED") ||
          (upd.STATUS_ID === "LIVE" &&
            this.state.addSchemeStatus === "LIVE") ? null : (
            <>
              <FormRow>
                <LabelButtonDU>
                  {upd != "" ? (
                    <Button onclick={this.updateRecord}>
                      {strings.Submit}
                    </Button>
                  ) : (
                    <Button onclick={this.addNewRecord}>
                      {strings.Submit}
                    </Button>
                  )}
                  <Button onclick={this.props.onclickClose}>
                    {strings.Cancel}
                  </Button>
                </LabelButtonDU>
              </FormRow>
            </>
          )
        ) : (
          <FormRow>
            <LabelButtonDU>
              {upd != "" ? (
                <Button onclick={this.updateRecord}>{strings.Submit}</Button>
              ) : (
                <Button onclick={this.addNewRecord}>{strings.Submit}</Button>
              )}

              <Button onclick={this.props.onclickClose}>
                {strings.Cancel}
              </Button>
            </LabelButtonDU>
          </FormRow>
        )
      ) : null;
    /* eslint-enable */
    return (
      <Form onSubmit={this.addRecord}>
        <FormRow>
          <LabelInputGroup>
            <h5>
              {strings.Plan_Name} <span className="error">*</span>
            </h5>
            <input
              type="text"
              name="addSchemeName"
              placeholder={strings.Scheme_Name}
              value={addSchemeName}
              onChange={this.handleChange}
              disabled={disabledEdit}
            />
          </LabelInputGroup>

          <LabelInputGroup>
            <h5>
              {strings.Plan_Scope} <span className="error">*</span>
            </h5>

            <select
              name="addSchemeType"
              onChange={this.handleChange}
              value={addSchemeType}
              disabled={disabledEdit}
            >
              {fetchSchemeType.map((item, index) => (
                <option
                  key={index}
                  value={item.SCHEME_TYPE_NAME}
                  id={item.SCHEME_TYPE_ID}
                >
                  {item.SCHEME_TYPE_NAME}
                </option>
              ))}
            </select>
          </LabelInputGroup>
        </FormRow>
        <FormRow>
          <LabelInputGroup>
            <div style={{ marginBottom: "1.2rem" }}>
              <h5>{strings.Image}</h5>
              <div className="input-group-addon-left">
                <span
                  class="input-group blue-button"
                  onClick={this.uploadImageModelHandler}
                  disabled={disabledEdit}
                >
                  {strings.Browse}
                </span>
                <input
                  type="text"
                  name="addSchemeImage"
                  placeholder={strings.Browse}
                  value={addSchemeImage}
                  onChange={this.onChange}
                  autoComplete="off"
                  disabled
                />
              </div>
            </div>
            <div>
              <h5>
                {strings.Start_Date}
                <span className="error">*</span>
              </h5>
              <input
                type="date"
                name="addStartDate"
                placeholder={strings.Start_Date}
                value={addStartDate}
                onChange={this.handleChange}
                disabled={disabledEdit}
                // onClick={this.openCalanderHandler}
                // ref={this.textInput}
              />
            </div>
          </LabelInputGroup>
          <LabelInputGroup>
            <h5>
              {strings.Description}
              <span className="error">*</span>
            </h5>
            <textarea
              rows="4"
              cols="50"
              name="addDescription"
              placeholder={strings.Description}
              value={addDescription}
              onChange={this.handleChange}
              disabled={disabledEdit}
            />
          </LabelInputGroup>
        </FormRow>
        <FormRow>
          <LabelInputGroup>
            <h5>
              {strings.End_Date}
              <span className="error">*</span>
            </h5>
            <input
              type="date"
              name="addEndDate"
              placeholder={strings.End_Date}
              value={addEndDate}
              onChange={this.handleChange}
              disabled={
                this.state.addSchemeStatus != "SUSPENDED" ? disabledEdit : null
              }
              // disabled={disabledEdit}
            />
          </LabelInputGroup>
          <LabelInputGroup>
            <h5>
              {strings.Plan_Status} <span className="error">*</span>
            </h5>

            {renderRadioGroup}
          </LabelInputGroup>
        </FormRow>
        {/* ------ Start with limit ------- */}
        {this.state.addSchemeTypeId != "WLMT" ? (
          <>
            <FormRowDivider>
              <WrapperLeftMilestone>
                {strings.Scope_Constrants}
              </WrapperLeftMilestone>
            </FormRowDivider>
            <FormRow>
              {/* Product Search */}
              <LabelInputGroup productsDD={productsDD}>
                <h5>
                  {strings.Products} <span className="error">*</span>
                </h5>
                <div style={{ position: "relative" }}>
                  <h4 onClick={this.productDDHandler} disabled={disabledEdit}>
                    {addProductList.length > 0
                      ? `${addProductList.length} Items Selected`
                      : "Select"}
                  </h4>
                  <ul className="productsDefine">
                    <li>
                      <input
                        type="text"
                        value={this.state.searchProduct}
                        onChange={this.searchProductsHandler}
                        placeholder="Search here..."
                      />
                      <span onClick={this.clearSelectProducts}>Clear</span>
                    </li>
                    {dataItems.map((item, index) => (
                      <li key={item.ITEM_ID}>
                        <label>
                          <input
                            type="checkbox"
                            name={item.NAME}
                            onClick={this.selectProducts}
                            id={item.ITEM_ID}
                            checked={addProductList.includes(
                              item.ITEM_ID.toString()
                            )}
                          />
                          {item.NAME}
                        </label>
                      </li>
                    ))}
                  </ul>
                </div>
              </LabelInputGroup>
              {/* Location search */}
              <LabelInputGroup consumerLocationDD={consumerLocationDD}>
                <h5>
                  {strings.User_Locations} <span className="error">*</span>
                </h5>
                <div style={{ position: "relative" }}>
                  <h4
                    onClick={this.userLocationDDHandler}
                    disabled={disabledEdit}
                  >
                    {addLocationList.length > 0
                      ? `${addLocationList.length} Items Selected`
                      : "Select"}
                  </h4>
                  <ul className="userLocationDefine">
                    <li>
                      <input
                        type="text"
                        placeholder="Search by Pin Code"
                        name="consumerLocationSearch"
                        autoComplete="off"
                        value={this.state.consumerLocationSearch}
                        onChange={this.searchLoactionHandler}
                      />
                      <span onClick={this.clearSelectLocations}>Clear</span>
                    </li>
                    {locationLevelDataItems != 0
                      ? // eslint-disable-line no-console
                        locationLevelDataItems.map((item, index) => (
                          // eslint-disable-line no-console
                          <li key={item.LOCATION_ID}>
                            <label>
                              <input
                                type="checkbox"
                                name={item.LOCATION_NAME}
                                onClick={this.selectLocations}
                                id={item.LOCATION_ID}
                                checked={addLocationList.includes(
                                  item.LOCATION_ID.toString()
                                )}
                              />
                              {item.LOCATION_NAME}
                            </label>
                          </li>
                        ))
                      : null}
                  </ul>
                </div>
              </LabelInputGroup>
            </FormRow>

            <FormRow>
              <LabelInputGroup MemberTypesDD={MemberTypesDD}>
                <h5>
                  {strings.Member_Types} <span className="error">*</span>
                </h5>
                <div style={{ position: "relative" }}>
                  <h4
                    onClick={this.MemberTypesDDHandler}
                    disabled={disabledEdit}
                  >
                    {addMemberTypesList.length > 0
                      ? `${addMemberTypesList.length} Items Selected`
                      : "Select"}
                  </h4>
                  <ul className="memberTypes">
                    <li>
                      <input
                        type="text"
                        value={this.state.memberTypesDataItems}
                        onChange={this.searchMemberTypesHandler}
                        placeholder="Search here..."
                      />
                      <span onClick={this.clearSelectMember}>Clear</span>
                    </li>

                    {memberTypesDataItems.map((item, index) => (
                      <li key={index}>
                        <label>
                          <input
                            type="checkbox"
                            name={item.TYPE_NAME}
                            onClick={this.selectMember}
                            id={item.TYPE_ID}
                            checked={addMemberTypesList.includes(
                              item.TYPE_ID.toString()
                            )}
                          />
                          {item.TYPE_NAME}
                        </label>
                      </li>
                    ))}
                  </ul>
                </div>
              </LabelInputGroup>
              <LabelInputGroup consumerLevelDD={consumerLevelDD}>
                <h5>
                  {strings.Consumer_Levels} <span className="error">*</span>
                </h5>
                <div style={{ position: "relative" }}>
                  <h4
                    onClick={this.ConsumerLevelsDDHandler}
                    disabled={disabledEdit}
                  >
                    {this.state.selectLevelName.length > 0
                      ? `${this.state.selectLevelName.length} Items Selected`
                      : "Select"}
                  </h4>
                  <ul className="levelDefine">
                    <li>
                      <input
                        type="text"
                        value={this.state.consumerLevelDataItems}
                        onChange={this.searchConsumerLevelsHandler}
                        placeholder="Search here..."
                      />
                    </li>
                    {consumerLevelDataItems.map((item, index) => (
                      <li>
                        <label>
                          <input
                            type="checkbox"
                            name={item.NAME}
                            onClick={this.selectLevels}
                            id={item.ID}
                            checked={this.state.selectLevelName.includes(
                              item.NAME
                            )}
                          />
                          {item.NAME}
                        </label>
                      </li>
                    ))}
                  </ul>
                </div>
              </LabelInputGroup>
            </FormRow>

            {this.state.selectLevelName.length != "0" ? (
              <table className="table">
                <tr>
                  <th width="20%" align="left">
                    {strings.Consumer_Levels}
                  </th>
                  <th width="30%" align="left">
                    {strings.Purchase_Required_Rs}
                  </th>
                  <th width="30%" align="left">
                    {strings.Points_Awarded}
                  </th>
                </tr>
                {this.state.selectLevelName.map((item, index) => (
                  <tr key={item.LEVEL_ID}>
                    <td width="10%">{item}</td>
                    <td>
                      <input
                        type="text"
                        placeholder="Purchase Value"
                        pattern="[0-9\.]+"
                        value={
                          this.state.addConsumerLevels[index].PURCHASE_REQUIRED
                        }
                        onChange={e => this.dynamicHandler(e, index, "value")}
                        disabled={disabledEdit}
                      />
                    </td>
                    <td>
                      <input
                        type="text"
                        placeholder="Purchase Point"
                        pattern="[0-9\.]+"
                        value={this.state.addConsumerLevels[index].POINTS_EARN}
                        onChange={e => this.dynamicHandler(e, index, "point")}
                        disabled={disabledEdit}
                      />
                    </td>
                  </tr>
                ))}
              </table>
            ) : null}
          </>
        ) : null}
        {/* end with limit */}

        {submitButtons}

        {/* ///// */}
        {responseAddSucess != "" ? (
          <SucessMsg>{responseAddSucess}</SucessMsg>
        ) : null}
        {responseError != "" ? <ErrorMsg>{responseError}</ErrorMsg> : null}
        {serverErrorStatus.status == "" ? (
          <ServerError>
            {serverErrorStatus.status} {serverErrorStatus.statusText}
          </ServerError>
        ) : null}
        {!(validateFilds === "") ? <ErrorMsg>{validateFilds}</ErrorMsg> : null}
        {/* ////////////// */}
        {this.state.uploadImageModel == true ? (
          <Modal
            modalOpen={this.state.uploadImageModel}
            onclick={this.uploadImageModelHandlerClose}
            title={strings.Upload_Scheme_Image}
            size="sm"
          >
            <UploadImage
              onclickClose={this.uploadImageModelHandlerClose}
              displayImageName={this.displayImageNameHandler}
            />
          </Modal>
        ) : null}
      </Form>
    );
  }
}

// AddRecord.propTypes = {
//   addRec: PropTypes.func,
//   onclickClose: PropTypes.func
// };

export default AddRecord;
