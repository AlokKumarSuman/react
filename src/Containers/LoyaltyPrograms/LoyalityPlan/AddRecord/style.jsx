import styled from "styled-components";

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  span[disabled] {
    pointer-events: none;
  }
  h4[disabled] {
    pointer-events: none;
    background: #f9f0f0;
  }

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;
    & > div {
      width: 49%;
      float: left;
    }
  }
`;
// export const FormRowFull = styled.div`
//   display: flex;
//   margin-bottom: 0;
//   justify-content: space-between;
//   flex-direction: column;

//   @media (min-width: 769px) {
//     margin-bottom: 1.2rem;
//     flex-direction: row;
//   }
//   & > div {
//     width: 100%;
//   }
// `;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
    font-family: "Gotham";
    font-weight: 500;
    letter-spacing: 1px;
  }

  h4 {
    border: 1px solid #ccc;
    padding: 7px 10px;
    border-radius: 4px;
    width: 364px;
    color: #939393;
  }

  ul.productsDefine {
    display: ${props => (props.productsDD === false ? "none" : "block")};
    max-height: 20rem;
    overflow-y: auto;
    position: absolute;
    background: #eaeaea;
    width: 100%;
    border: solid 1px #c5c5c5;
    padding: 4px;
    border-top-right-radius: 2px;
    border-top-left-radius: 2px;
    z-index: 9;

    li {
      display: block;
      padding: 5px 0px;

      :first-child {
        display: flex;
        align-items: center;
        justify-content: space-between;

        span {
          color: red;
          margin-left: 6px;
        }
      }

      input[type="text"] {
        width: 100%;
        height: 30px;
      }

      input[type="checkbox"] {
        margin-right: 5px;
        width: 14px;
        height: 14px;
      }

      p {
        display: inline-block;
      }
    }
  }

  ul.userLocationDefine {
    display: ${props =>
      props.consumerLocationDD === false ? "none" : "block"};
    max-height: 20rem;
    overflow-y: auto;
    position: absolute;
    background: #eaeaea;
    width: 100%;
    border: solid 1px #c5c5c5;
    padding: 4px;
    border-top-right-radius: 2px;
    border-top-left-radius: 2px;
    z-index: 9;

    li {
      display: block;
      padding: 5px 0px;

      :first-child {
        display: flex;
        align-items: center;
        justify-content: space-between;

        span {
          color: red;
          margin-left: 6px;
        }
      }
      p {
        position: relative;
      }

      p:before {
        content: "";
        display: inline-block;
        width: 13px;
        height: 13px;
        border: 1px solid #333;
        margin-right: 10px;
      }
    }
  }

  li.checked p:after {
    content: "";
    color: #000;
    position: absolute;
    right: auto;
    left: 0px;
    width: 13px;
    height: 13px;
    background: #2dc3e8;
    border: none;
  }

  ul.levelDefine {
    display: ${props => (props.consumerLevelDD === false ? "none" : "block")};
    max-height: 20rem;
    overflow-y: auto;
    position: absolute;
    background: #eaeaea;
    width: 100%;
    border: solid 1px #c5c5c5;
    padding: 4px;
    border-top-right-radius: 2px;
    border-top-left-radius: 2px;
    z-index: 9;

    li {
      display: block;
      padding: 5px 0px;

      input[type="text"] {
        width: 100%;
        height: 30px;
      }

      input[type="checkbox"] {
        margin-right: 5px;
        width: 14px;
        height: 14px;
      }

      p {
        display: inline-block;
      }
    }
  }

  ul.memberTypes {
    display: ${props => (props.MemberTypesDD === false ? "none" : "block")};
    //
    max-height: 20rem;
    overflow-y: auto;
    position: absolute;
    background: #eaeaea;
    width: 100%;
    border: solid 1px #c5c5c5;
    padding: 4px;
    border-top-right-radius: 2px;
    border-top-left-radius: 2px;
    z-index: 9;

    li {
      display: block;
      padding: 5px 0px;

      :first-child {
        display: flex;
        align-items: center;
        justify-content: space-between;

        span {
          color: red;
          margin-left: 6px;
        }
      }

      input[type="text"] {
        width: 100%;
        height: 30px;
      }

      input[type="checkbox"] {
        margin-right: 5px;
        width: 14px;
        height: 14px;
      }

      p {
        display: inline-block;
      }
    }
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.2rem;
    }
  }
`;

export const LabelButtonDU = styled.div`
  width: auto !important;
  display: flex;
  margin: 0 auto;
  float: none !important;
  div {
    width: auto;
    margin: 0 auto;
    display: inline-block;
    min-width: 151px;
  }
  div:first-child {
    margin-right: 20px;
  }
`;

export const ImageRatio = styled.div`
  object-fit: cover;
  width: 80px;
`;
export const ErrorMsg = styled.div`
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
export const SucessMsg = styled.div`
  display: flex;
  justify-content: center;
  color: green;
  font-size: 13px;
`;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const FormRowDivider = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background: #c5c5c5;
  height: 32px;
  margin-bottom: 1.2rem;
  justify-content: center;
  padding: 0.6rem;
`;

export const WrapperLeftMilestone = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const SpinnerWrap = styled.div`
  background: red;
  width: 100%;
  height: 30px;
  position: absolute;
  top: 8px;
  opacity: 0.3;
`;

export const RadioGroup = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-around;
  flex-direction: row;
  padding-top: 6px;
`;
