import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import Spinner from "../../../Components/Spinner";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";
import Modal from "../../../Components/Modal";
import NoDataFound from "../../../Components/NoDataFound";
import { userService, fetchLoyaltySchemeHistory } from "../../Config";
import { Auth } from "../../../Auth";

import {
  AuthLevelsCard,
  AuthLevelsHeader,
  ButtonGroup,
  WrapperLeft,
  AuthLevelsBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError
} from "./style";
import ViewRecord from "./ViewRecord";
import { strings } from "./../../../Localization/index";

class SchemesHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      allMarketingData: [],
      responseStatusAPI: "",
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      responseDeleteLevelStatusAPI: "",
      openViewHandler: false,
      currentRowData: []
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(fetchLoyaltySchemeHistory, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LANGUAGE_ID: "-1"
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      // .then(res => console.log(res))

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            allMarketingData: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  openViewHandler = (e, currentItems) => {
    // debugger;
    this.setState({
      openViewHandler: true,
      currentRowData: currentItems
    });
  };

  closeViewHandler = () => {
    this.setState({
      openViewHandler: false
    });
  };

  render() {
    // if (sessionStorage.length == 0) {
    //   return <Redirect to="/login" />;
    // }

    const {
      allMarketingData,
      currentRowData,
      spinnerConfigData,
      serverError
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Loyalty_Scheme_Details}
          size="md"
        >
          <ViewRecord currentItem={currentRowData} />
        </Modal>

        <AuthLevelsCard>
          <AuthLevelsHeader>
            <WrapperLeft>{strings.Manage_Plans}</WrapperLeft>
          </AuthLevelsHeader>

          <AuthLevelsBody toggleFilter={this.state.filterShow}>
            <table className="table bordered odd-even">
              <thead>
                <tr>
                  <th width="30%" align="left">
                    Plan Name
                  </th>
                  <th width="15%" align="left">
                    {strings.Created_By}
                  </th>
                  {/* <th width="15%" align="left">
                    {strings.Updated_By}
                  </th> */}
                  <th width="15%" align="left">
                    Outstanding Liability
                  </th>
                  <th width="15%" align="left">
                    Plan Status
                  </th>
                  <th width="20%" align="left">
                    Loyalty Member
                  </th>
                  <th width="80px" style={{ maxWidth: "80px" }}>
                    {strings.Action}
                  </th>
                </tr>
              </thead>
              <tbody>
                {spinnerConfigData == true ? (
                  <tr>
                    <td colSpan="6">
                      <SpinnerConfigData>
                        <Spinner />
                      </SpinnerConfigData>
                    </td>
                  </tr>
                ) : allMarketingData.length == 0 ? (
                  <tr>
                    <td colSpan="6">
                      <NoDataFound />
                    </td>
                  </tr>
                ) : (
                  allMarketingData.map((item, index) => (
                    <tr>
                      <td>{item.SCHEME_NAME}</td>
                      <td>{item.CREATED_BY}</td>
                      {/* <td>{item.UPDATED_BY}</td> */}
                      <td align="center">
                        {Math.round(item.CURRENT_LIABILITY * 100) / 100}{" "}
                      </td>
                      <td>{item.STATUS_NAME}</td>
                      <td>
                        <Link to="/loyalty-plan/loyalty-member/">
                          <Button>{item.SCHEME_SUBSCRIBERS}</Button>
                        </Link>
                      </td>
                      <td align="center">
                        <ButtonGroup>
                          <Icon
                            name="eye"
                            onclick={e => this.openViewHandler(e, item)}
                          />
                        </ButtonGroup>
                      </td>
                    </tr>
                  ))
                )}
              </tbody>
            </table>
            {this.state.fetchErrorMsg != "" ? (
              <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
            ) : null}
            {this.state.serverError.statusText == "" ? (
              <ServerError>
                {serverError.status} {serverError.statusText}
              </ServerError>
            ) : null}
          </AuthLevelsBody>
        </AuthLevelsCard>
      </React.Fragment>
    );
  }
}

// ViewDealer.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default SchemesHistory;
