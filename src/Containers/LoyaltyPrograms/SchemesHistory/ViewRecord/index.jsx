import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { ViewWrap } from "./style";
import { strings } from "./../../../../Localization/index";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { currentItem } = this.props;
    return (
      <ViewWrap>
        <table className="table record-view-table">
          <tbody>
            <tr>
              <td width="35%">
                <b>{strings.Scheme_Name} </b>
              </td>
              <td>
                : <small>{currentItem.SCHEME_NAME}</small>
              </td>
            </tr>
            <tr>
              <td>
                <b>{strings.Start_Date}</b>
              </td>
              <td>
                :{" "}
                <small>
                  {moment(new Date(currentItem.START_DATE)).format(
                    "DD-MM-YYYY"
                  )}
                </small>
              </td>
            </tr>

            <tr>
              <td>
                <b>{strings.End_Date}</b>
              </td>
              <td>
                :{" "}
                <small>
                  {moment(new Date(currentItem.END_DATE)).format("DD-MM-YYYY")}
                </small>
              </td>
            </tr>
            <tr>
              <td>
                <b>{strings.Scheme_Subscribers}</b>
              </td>
              <td>
                : <small>{currentItem.SCHEME_SUBSCRIBERS}</small>
              </td>
            </tr>

            <tr>
              <td>
                <b>{strings.Created_Date}</b>
              </td>
              <td>
                :{" "}
                <small>
                  {moment(new Date(currentItem.CREATION_DATE)).format(
                    "DD-MM-YYYY"
                  )}
                </small>
              </td>
            </tr>
            {currentItem.UPDATION_DATE ? (
              <tr>
                <td>
                  <b>{strings.Updation_Date} </b>
                </td>
                <td>
                  :{" "}
                  <small>
                    {moment(new Date(currentItem.UPDATION_DATE)).format(
                      "DD-MM-YYYY"
                    )}
                  </small>
                </td>
              </tr>
            ) : null}
            <tr>
              <td>
                <b>{strings.Description}</b>
              </td>
              <td>
                : <small>{currentItem.DESCRIPTION}</small>
              </td>
            </tr>
            {/* // */}
            <tr>
              <td width="30%">
                <b>Scheme Products</b>
              </td>
              <td style={{ display: "flex", alignItems: "center" }}>
                :{" "}
                <small className="test_truncate">
                  {currentItem.PRODUCTS.map(item => item.ITEM_NAME + ", ")}
                </small>
              </td>
            </tr>

            <tr>
              <td width="30%">
                <b>Member Type</b>
              </td>
              <td style={{ display: "flex", alignItems: "center" }}>
                :{" "}
                <small className="test_truncate">
                  {currentItem.MEMBER_TYPES.map(
                    item => item.MEMBER_TYPE_NAME + ", "
                  )}
                </small>
              </td>
            </tr>

            <tr>
              <td width="30%">
                <b>User Locations</b>
              </td>
              <td style={{ display: "flex", alignItems: "center" }}>
                :{" "}
                <small className="test_truncate">
                  {currentItem.LOCATIONS.map(item => item.LOCATION_NAME + ", ")}
                </small>
              </td>
            </tr>

            {currentItem.USER_LEVELS.length > 0 ? (
              <tr>
                <td width="30%" colSpan="2">
                  <level>
                    <b>User Levels</b>
                  </level>

                  <table className="inner_table">
                    <tr>
                      <td>
                        <small>
                          <i>Consumer Levels</i>
                        </small>
                      </td>
                      <td>
                        <small>
                          <i>Purchase Required (Rs)</i>
                        </small>
                      </td>
                      <td>
                        <small>
                          <i>Points Awarded</i>
                        </small>
                      </td>
                    </tr>
                    {currentItem.USER_LEVELS.map(item => (
                      <tr>
                        <td>
                          <small>{item.LEVEL_NAME} </small>
                        </td>
                        <td>
                          <small>{item.PURCHASE_REQUIRED} </small>
                        </td>
                        <td>
                          <small>{item.POINTS_EARN} </small>
                        </td>
                      </tr>
                    ))}
                  </table>
                </td>
              </tr>
            ) : null}
          </tbody>
        </table>
      </ViewWrap>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
