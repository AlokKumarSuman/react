import styled from "styled-components";

export const ViewWrap = styled.div`
  width: 100%;

  .test_truncate {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
  .inner_table {
    border: solid 1px #c5c5c5 !important;
    width: 100%;
    height: 18px;
  }
  .inner_table td {
    padding: 0 0.6rem;
    height: 18px;
  }
`;
