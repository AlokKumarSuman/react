import React, { Component } from "react";
import Spinner from "../../../Components/Spinner";
import NoDataFound from "../../../Components/NoDataFound";
import { userService, fetchLoyaltySchemeMembers } from "../../Config";
import { Auth } from "../../../Auth";
import { strings } from "./../../../Localization/index";

import {
  AuthLevelsCard,
  AuthLevelsHeader,
  WrapperLeft,
  AuthLevelsBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError
} from "./style";

class LoyaltyMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      allMarketingData: [],
      responseStatusAPI: "",
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      responseDeleteLevelStatusAPI: "",
      openViewHandler: false,
      currentRowData: []
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(fetchLoyaltySchemeMembers, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        SCHEME_ID: ""
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            allMarketingData: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  openViewHandler = (e, currentItems) => {
    // debugger;
    this.setState({
      openViewHandler: true,
      currentRowData: currentItems
    });
  };

  closeViewHandler = () => {
    this.setState({
      openViewHandler: false
    });
  };

  render() {
    // if (sessionStorage.length == 0) {
    //   return <Redirect to="/login" />;
    // }

    const { allMarketingData, spinnerConfigData, serverError } = this.state;

    return (
      <React.Fragment>
        <AuthLevelsCard>
          <AuthLevelsHeader>
            <WrapperLeft>{strings.Loyalty_Member}</WrapperLeft>
          </AuthLevelsHeader>

          <AuthLevelsBody toggleFilter={this.state.filterShow}>
            <table className="table bordered odd-even">
              <thead>
                <tr>
                  <th width="20%" align="left">
                    Plan Id
                  </th>
                  <th width="20%" align="left">
                    {strings.User_Name}
                  </th>
                  <th width="20%" align="left">
                    {strings.Subscription_Date}
                  </th>
                  <th width="20%" align="left">
                    {strings.Points_Earned}
                  </th>
                </tr>
              </thead>
              <tbody>
                {spinnerConfigData == true ? (
                  <tr>
                    <td colSpan="6">
                      <SpinnerConfigData>
                        <Spinner />
                      </SpinnerConfigData>
                    </td>
                  </tr>
                ) : allMarketingData.length == 0 ? (
                  <tr>
                    <td colSpan="6">
                      <NoDataFound />
                    </td>
                  </tr>
                ) : (
                  allMarketingData.map((item, index) => (
                    <tr>
                      <td>{item.SCHEME_ID}</td>
                      <td>{item.USERNAME}</td>
                      <td>{item.SUBSCRIPTION_DATE}</td>
                      <td>{item.POINTS_EARNED}</td>
                    </tr>
                  ))
                )}
              </tbody>
            </table>
            {this.state.fetchErrorMsg != "" ? (
              <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
            ) : null}
            {this.state.serverError.statusText == "" ? (
              <ServerError>
                {serverError.status} {serverError.statusText}
              </ServerError>
            ) : null}
          </AuthLevelsBody>
        </AuthLevelsCard>
      </React.Fragment>
    );
  }
}

// ViewDealer.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default LoyaltyMember;
