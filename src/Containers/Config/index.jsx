let apiVersion;
let apiStoreVersion;
let apiStoreVersionNew;
let baseUrlApi;
let baseUrlApiPort;
// let authenticateUserLogin;
let loginAuthApi;
let registerUser;
let updateProfile;

let listDomainUsers;
let updatePassword;
let deleteUser;

let applicationMode;

if (process.env.NODE_ENV === "development") {
  applicationMode = "Development";
  apiVersion = "v1.0";
  apiStoreVersion = "1.0.0";
  apiStoreVersionNew = "v1.0.0";
  baseUrlApi = "https://192.168.0.10:8244/";
  baseUrlApiPort = "http://192.168.0.10:8282/";
  // authenticateUserLogin = "http://192.168.0.10:8282/";
  //
  loginAuthApi = "http://192.168.0.10:8282/authenticateUser";
  // registerUser = "http://192.168.0.10:8282/registerUser";
  // updateProfile = "http://192.168.0.10:8282/updateProfile";
  registerUser = "http://192.168.0.10:8282/registerUser";
  updateProfile = "http://192.168.0.10:8282/updateUserProfile/1.0.0/update";

  /* Users List */
  listDomainUsers = "https://192.168.0.10:8244/listDomainUsers/1.0.0/getList"; // Store
  updatePassword = "https://192.168.0.10:8244/updatePassword/1.0.0/update"; // Store
  deleteUser = "https://192.168.0.10:8244/deleteUser/1.0.0/delete"; // Store
}

if (process.env.NODE_ENV === "production") {
  applicationMode = "Production";
  apiVersion = "1.0.0";
  apiStoreVersion = "1.0.0";
  apiStoreVersionNew = "1.0.0";
  baseUrlApi = "https://api.dealjustnow.net/";
  baseUrlApiPort = "https://services.dealjustnow.net/";
  // authenticateUserLogin = "https://services.dealjustnow.net/"; // servcice for HTTP// and service-s for HTTPS//
  //
  loginAuthApi = "https://api.dealjustnow.net/authenticateUser/1.0.0/outh";
  // registerUser = "https://services.dealjustnow.net/registerUser";
  // updateProfile = "https://services.dealjustnow.net/updateProfile";
  registerUser = "https://api.dealjustnow.net/registerUser/1.0.0/addUser";
  updateProfile = "https://api.dealjustnow.net/updateUserProfile/1.0.0/update";

  /* Users List */
  listDomainUsers = "https://api.dealjustnow.net/listDomainUsers/1.0.0/getList"; // Store
  updatePassword = "https://api.dealjustnow.net/updatePassword/1.0.0/update"; // Store
  deleteUser = "https://api.dealjustnow.net/AdminDeleteUser/1.0.0/delete"; // Store
}

export {
  apiVersion,
  apiStoreVersion,
  apiStoreVersionNew,
  baseUrlApi,
  baseUrlApiPort,
  // authenticateUserLogin,
  loginAuthApi,
  registerUser,
  updateProfile,
  listDomainUsers,
  updatePassword,
  deleteUser,
  applicationMode
};

/* DASHBOARD SERVICES */
export const fetchDasboardDetails =
  baseUrlApi + "fetchDashboardDetails/" + apiStoreVersion + "/fetchDetails"; // store

export const FetchDashboradGraph =
  baseUrlApi + "FetchDashboradGraph/" + apiStoreVersion + "/getList"; // store

/* DASHBOARD SERVICES */

/* PRODUCTS SERVICES */
export const productsSummary =
  baseUrlApi + "productSummary/" + apiStoreVersion + "/fetchSummary"; //store

export const fetchItemDetails =
  baseUrlApi + "fetchItemDetails/" + apiStoreVersion + "/getList"; //store

export const productTypeSearchCategoryApiUrl =
  baseUrlApi +
  "cache/" +
  apiVersion +
  "/solr/productcategory/select?q=CLASS_CODE%3A"; //Solr
export const productTypeSearchApiUrl =
  baseUrlApi +
  "cache/" +
  apiVersion +
  "/solr/productclasses/select?q=*%3A*&rows=500"; //Solr

export const sendAddProductRequest =
  baseUrlApi + "sendAddProductRequest/" + apiStoreVersion + "/sendRequest"; //store
/* PRODUCTS SERVICES */

/* LOYALTY PROGRAM > SCHEMES > Loylity Plan*/
export const listAllLoyaltySchemes =
  baseUrlApi + "listAllLoyaltySchemes/" + apiStoreVersion + "/listAll"; // store

export const deleteLoyaltyScheme =
  baseUrlApi + "deleteLoyaltyScheme/" + apiStoreVersion + "/deleteScheme"; // store

export const addNewLoyaltyScheme =
  baseUrlApi + "addLoyaltyScheme/" + apiStoreVersion + "/addNew";
export const updateLoyaltyScheme =
  baseUrlApi + "updateLoyaltyScheme/" + apiStoreVersion + "/updateScheme"; // store

/* LOYALTY PROGRAM > SCHEMES > Loylity Member*/
export const fetchLoyaltySchemeMembers =
  baseUrlApi + "fetchLoyaltySchemeMembers/" + apiStoreVersion + "/getList";
/* LOYALTY PROGRAM > SCHEMES > Scheme History*/
export const fetchLoyaltySchemeHistory =
  baseUrlApi + "fetchLoyaltySchemeHistory/" + apiStoreVersion + "/getList";

/* Global */
export const currency =
  baseUrlApi + "cache/" + apiVersion + "/solr/currency/select?q=*%3A*"; // Solr// Solr pending

export const schemeStatus =
  baseUrlApi + "cache/" + apiVersion + "/solr/schemeStatus/select?q=*%3A*"; //Solr// Solr pending
export const schemeType =
  baseUrlApi + "cache/" + apiVersion + "/solr/schemeType/select?q=*%3A*"; //Solr// Solr pending

export const itemsByBrandId =
  baseUrlApi + "itemsByBrandId/" + apiVersion + "/itemsByBrandId?";

export const fetchLocationsForSchemes =
  baseUrlApi + "fetchLocationsForSchemes/" + apiStoreVersion + "/fetchList"; // store

export const fetchLevelsForDropDown =
  baseUrlApi + "fetchLevelsForDropDown/" + apiStoreVersion + "/getList"; // store

export const fetchLocationsForDropDown =
  baseUrlApi + "fetchLocationsForDropDown/" + apiStoreVersion + "/getList"; // store

export const listUsersLoyaltySchemes =
  baseUrlApi + "listUsersLoyaltySchemes/" + apiStoreVersion + "/listSchemes"; // store

/* MARKETING SCHEMES */
export const fetchMarketingSchemeHistory =
  baseUrlApi + "fetchMarketingSchemeHistory/" + apiStoreVersion + "/getList"; // store

export const deleteMarketingScheme = // store
  baseUrlApi + "deleteMarketingScheme/" + apiStoreVersion + "/delete";

export const addNewMarketingScheme =
  baseUrlApi + "addNewMarketingScheme/" + apiStoreVersion + "/add"; // Store

export const updateMarketingScheme =
  baseUrlApi + "updateMarketingScheme/" + apiStoreVersion + "/update";

export const fetchMarketingMemberType =
  baseUrlApi + "fetchMarketingMemberType/" + apiStoreVersion + "/getList";

// export const marketingSchemeBase =
//   solrBaseApi + "solr/marketingSchemeBase/select?q=*%3A*"; // Solr// Solr pending
// export const marketingSchemePeriods =
//   solrBaseApi + "solr/marketingSchemePeriods/select?q=*%3A*"; // Solr// Solr pending
// export const marketingSchemeType =
//   solrBaseApi + "solr/marketingSchemeType/select?q=*%3A*"; // Solr pending// Solr pending

export const marketingSchemeBase =
  baseUrlApi +
  "cache/" +
  apiVersion +
  "/solr/marketingSchemeBase/select?q=*%3A*"; // Solr// Solr pending
export const marketingSchemePeriods =
  baseUrlApi +
  "cache/" +
  apiVersion +
  "/solr/marketingSchemePeriods/select?q=*%3A*"; // Solr// Solr pending
export const marketingSchemeType =
  baseUrlApi +
  "cache/" +
  apiVersion +
  "/solr/marketingSchemeType/select?q=*%3A*"; // Solr pending// Solr pending

/* Push Marketing Scheme */
export const fetchPushedMarketingSchemes =
  baseUrlApi + "fetchPushedMarketingSchemes/" + apiStoreVersion + "/getList"; // store
export const pushMarketingNotification =
  baseUrlApi + "pushMarketingNotification/" + apiStoreVersion + "/send"; // store

/* QUIRIES > TRACK DEMANDS */
export const trackDemands =
  baseUrlApi + "trackdemands/" + apiStoreVersion + "/fetchDetails"; // store
/* QUIRIES > TRACK DEMANDS */

export const countryMaster =
  baseUrlApi + "CountryMaster/" + apiVersion + "/_getfetchcountries"; // Solr

export const productCategoryApiUrl =
  "Products/" + apiVersion + "/_getlistbricks?LANGUAGE_CODE=-1"; // Solr

// based on product name // *%3A*
// based on product category //BRICK_ID%3A%20 + id //eg: id = 10002526
// export const productNameApiUrl =
// baseUrlApi + "cache/" + apiVersion + "//solr/items/select?q="; // Solr

// export const rewardCampaignTypeApiUrl =
//   "compaignTypeMaster/" + apiVersion + "/listCampaignTypes"; // not in use

//based on status // status + Type (Eg: campaign)
export const statusMaster =
  "statusmaster/" + apiVersion + "/statusList?fetchforform=";
//////////////////////////////////////////////////
// export const userTypeApiUrl =
//   "userTypesMaster/" + apiVersion + "/listUserTypes"; // not in use

/* Auth Dealer */
export const listApprovalRequests =
  baseUrlApi + "listApprovalRequests/" + apiVersion + "/listApprovalRequests"; // store

export const approveRetailerRequest =
  baseUrlApi + "approveRetailer/" + apiStoreVersion + "/approveRetailer"; // store

/* ADMINISTRATION > MASTER > REWARDS */
export const addRewardChooseType =
  baseUrlApi + "cache/" + apiVersion + "/solr/reward/select?q=*%3A*"; // Cache

export const fetchRewradsList =
  baseUrlApi + "FetchRewards/" + apiStoreVersion + "/fetch"; //Store
export const addRewards = baseUrlApi + "addReward/" + apiStoreVersion + "/add"; //Store
export const updateReward =
  baseUrlApi + "updateReward/" + apiStoreVersion + "/update"; //Store
/* ADMINISTRATION > MASTER > REWARDS */

/* ADMINISTRATION > Loyalty Points */
export const listLoyaltyPointsValuesByBrand =
  baseUrlApi + "listLoyaltyPointsValuesByBrand/" + apiStoreVersion + "/getList"; // store
export const saveLoyaltyPointValue =
  baseUrlApi + "saveLoyaltyPointValue/" + apiStoreVersion + "/addUpdate"; // store
/* ADMINISTRATION > Loyalty Points */

/* SELLER LEVEL */
export const searchCustomerLevel =
  baseUrlApi +
  "searchCustomerLevels/" +
  apiStoreVersionNew +
  "/searchCustomerLevels"; // Store

export const deleteCustomerLevel =
  baseUrlApi +
  "deleteCustomerLevel/" +
  apiStoreVersionNew +
  "/deleteCustomerLevel"; // store

export const addCustomerLevel =
  baseUrlApi + "addCustomerLevel/" + apiStoreVersionNew + "/addCustomerLevel"; // Store

export const updateCustomerLevel =
  baseUrlApi +
  "updateCustomerLevel/" +
  apiStoreVersionNew +
  "/updateCustomerLevel"; //Store

/* SELLER LEVEL */
/* Levels */
export const addLevel =
  baseUrlApi + "addLevels/" + apiStoreVersion + "/addLevels"; //Store

export const listLevels =
  baseUrlApi + "listLevels/" + apiVersion + "/listLevels"; //Store

/* CONSUMER LEVEL */
export const fetchAvailableConsumerLevels =
  baseUrlApi + "fetchConsumerLevels/" + apiStoreVersion + "/listLevels"; //Store

export const fetchMemberTypesList =
  baseUrlApi + "fetchMemberTypesList/" + apiStoreVersion + "/getList"; //Store

export const addNewConsumerLevel =
  baseUrlApi + "addNewConsumerLevel/" + apiStoreVersion + "/addLevel"; //Store
export const updateConsumerLevels =
  baseUrlApi + "updateConsumerLevel/" + apiStoreVersion + "/updateLevel"; //Store

/* CONSUMER LEVEL */

/* MARKETING PRIZE */
export const fetchAvailablePrize =
  baseUrlApi + "FetchBrandPrizeList/" + apiStoreVersion + "/getList"; //Store
export const addNewPrize =
  baseUrlApi + "addPrizeForBrand/" + apiStoreVersion + "/addNew"; //Store
export const updateMarketingPrize =
  baseUrlApi + "updateMarketingPrize/" + apiStoreVersion + "/updatePrize"; //Store
/* MARKETING PRIZE */

/* Location Hierarchy */
export const fetchDefaultHierarchy =
  baseUrlApi + "fetchDefaultHierarchy/" + apiVersion + "/fetchDefaultHierarchy";

export const updateLevel =
  baseUrlApi + "updateLevel/" + apiVersion + "/updateLevel"; // store

/* Custom Sales Hierarchy*/
export const assignUserLocation =
  baseUrlApi + "assignUserLocation/" + apiStoreVersion + "/assign"; // Store
export const getBrandUsers =
  baseUrlApi + "getBrandUsersList/" + apiStoreVersion + "/getList"; // Store

export const fetchAssignedUsersForLocation =
  baseUrlApi + "fetchAssignedUserForLocation/" + apiStoreVersion + "/getList"; // store
export const unAssignUserFromLocation =
  baseUrlApi + "unassignUserFromLocation/" + apiStoreVersion + "/unassign"; // store

export const fetchSiblings =
  baseUrlApi + "fetchSiblings/" + apiVersion + "/fetchSiblings"; // store

export const updateLocation =
  baseUrlApi + "updateLocation/" + apiVersion + "/updateLocation"; // store

export const addLocation =
  baseUrlApi + "addLocation/" + apiVersion + "/addLocation"; // store

export const deleteLocation =
  baseUrlApi + "deleteLocation/" + apiVersion + "/deleteLocation"; // store

export const customHierarchy =
  baseUrlApi + "customHierarchy/" + apiVersion + "/customHierarchy"; // store

/* Custom Location Hierarchy */
export const fetchLocationUsers =
  baseUrlApi + "fetchLocationUsers/" + apiVersion + "/fetchLocationUsers"; // store
export const fetchunassignedlocations =
  baseUrlApi +
  "fetchunassignedlocations/" +
  apiVersion +
  "/fetchunassignedlocations"; // store

/* Support */
export const sendSupportRequest =
  baseUrlApi + "sendSupportRequest/" + apiStoreVersion + "/send"; //Store

export const raisedIssue =
  baseUrlApi + "fetchRaisedIssueForBrands/" + apiStoreVersion + "/getList"; //Store

export const issueUpdate =
  baseUrlApi + "updateIssueStatus/" + apiStoreVersion + "/update"; //Store

/* Product */
export const listAddProductRequests =
  baseUrlApi + "listAddProductRequests/" + apiStoreVersion + "/listAll"; //Store

/* Change Password */
export const changePassword =
  baseUrlApi + "changePassword/" + apiStoreVersion + "/update";

/* Upload File */
export const uploadfile =
  baseUrlApi + "uploadfile/" + apiStoreVersion + "/upload"; // store

/* Register User */
export const fetchBrandRoles =
  baseUrlApi + "fetchBrandRoles/" + apiStoreVersion + "/getList";

export const fetchBrandUserGroups =
  baseUrlApi + "fetchBrandUserGroups/" + apiStoreVersion + "/getList";

/*Bas Abhi User */
export const fetchLoyaltyMemberUserList =
  baseUrlApi + "fetchLoyaltyMemberUserList/" + apiStoreVersion + "/getList";

export const updateLoyaltyUserMemberType =
  baseUrlApi + "updateLoyaltyUserMemberType/" + apiStoreVersion + "/update";

/* Sauda Users */
export const FetchSaudaUserForBrands =
  baseUrlApi + "FetchSaudaUserForBrands/" + apiStoreVersion + "/getList";

export const UpdateMarketingUserMemberType =
  baseUrlApi + "UpdateMarketingUserMemberType/" + apiStoreVersion + "/update";

/* Bas Abhi/Loyalty Member Type */
export const addLoyaltyMemberType =
  baseUrlApi + "addLoyaltyMemberType/" + apiStoreVersion + "/save";

export const deleteLoyaltyMemberType =
  baseUrlApi + "deleteLoyaltyMemberType/" + apiStoreVersion + "/delete";

/* Sauda/Marketing Member Type */
export const addSaudaMemberType =
  baseUrlApi + "addSaudaMemberType/" + apiStoreVersion + "/save";

export const deleteSaudaMemberType =
  baseUrlApi + "deleteSaudaMemberType/" + apiStoreVersion + "/delete";

/* Alert */
export const fetchUserAlertsList =
  baseUrlApi + "fetchUserAlertsList/" + apiStoreVersion + "/getList"; // Store

/* Notification */
export const fetchUsersNotifications =
  baseUrlApi + "fetchUsersNotifications/" + apiStoreVersion + "/getList"; // Store

// Tasks
export const fetchTasksListRaisedByUser =
  baseUrlApi + "fetchTasksListRaisedByUser/" + apiStoreVersion + "/getList";

export const fetchUsersTaskList =
  baseUrlApi + "fetchUsersTaskList/" + apiStoreVersion + "/getList"; // Store

// Task Loyality Scheme

export const approveLoyaltySchemeTask =
  baseUrlApi + "approveLoyaltySchemeTask/" + apiStoreVersion + "/approve"; // store

export const approveMarketingScheme =
  baseUrlApi + "approveMarketingScheme/" + apiStoreVersion + "/approve"; // store

export const updateLoyaltySchemeTask =
  baseUrlApi + "updateLoyaltySchemeTask/" + apiStoreVersion + "/update"; // Store not in use

export const updateMarketingSchemeTask =
  baseUrlApi + "updateMarketingSchemeTask/" + apiStoreVersion + "/update"; // Store

export const approveMarketingRedeemTask =
  baseUrlApi + "approveMarketingRedeemTask/" + apiStoreVersion + "/updateClaim";

export const approveLoyaltyRedeemTask =
  baseUrlApi + "approveLoyaltyRedeemTask/" + apiStoreVersion + "/updateTask"; // Store

export const approvePurchaseTask =
  baseUrlApi + "approvePurchaseTask/" + apiStoreVersion + "/update"; // Store

// Task Marketing Scheme

export const approveNewMarketingScheme =
  baseUrlApi + "approveNewMarketingScheme/" + apiStoreVersion + "/approve"; // Store

export const brandID = "2";

export const userService = {
  authUserDataAll,
  authUserName,
  authLastLogin,
  authOrganization,
  authEmailaddress,
  authFullName,
  authPhoto,
  authBrandUserName,
  authBrandId,
  authCompanyId,
  authBrandName,

  authTenantName,
  authTenantId,
  authRoles,
  authGroups
};

function authUserDataAll() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return authUser;
}
function authUserName() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.username}`;
}
function authLastLogin() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.lastLogin}`;
}
function authOrganization() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.organization}`;
}
function authEmailaddress() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.emailaddress}`;
}
function authFullName() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.fullname}`;
}
function authPhoto() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.photo}`;
}
function authBrandUserName() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.BRAND_NAME}/${authUser.username}`;
}
function authBrandId() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.BRAND_ID}`;
}
function authCompanyId() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.COMPANY_ID}`;
}
function authBrandName() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.BRAND_NAME}`;
}

function authTenantName() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.TENANT_NAME}`;
}

function authTenantId() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.TENANT_ID}`;
}

function authRoles() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.ROLES}`;
}

function authGroups() {
  const authUser = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
  return `${authUser.GROUPS}`;
}

// product page brand ID static
