import React, { Component } from "react";
import { Base64 } from "js-base64";
import { loginAuthApi } from "../Config";
import { LoginAuth } from "../../Auth";
import { BlankPage } from "../BlankPage";
// Localization
import Hi from "../../Assets/Images/Country/Icons/hi.png";
import En from "../../Assets/Images/Country/Icons/en.png";
import { strings } from "../../Localization";

// import { MultiSearchSelect } from "../../Components/MultiSearchSelect";

import {
  DashboardWrapper,
  ImageBg,
  LoginDetaillWrap,
  LoginIconWrap,
  Form,
  InputWrap,
  RememberPassword,
  InputPasswordWrap,
  IconWrap,
  ErrorMsg,
  HeaderStyled,
  InnerHeaderStyled,
  RightAlign,
  Localization
} from "./style.jsx";
import ClientLogo from "./ClientLogo";
import Icon from "../../Components/Icons";
import ForgotPassword from "./ForgotPassword";
import { Redirect } from "react-router-dom";
import Button from "../../Components/Button";
import Checkbox from "../../Components/Checkbox";
import Footer from "../Footer/index.jsx";
import Logo from "../../Components/Logo";
import SupportEmail from "./../../Components/SupportEmail/index";
import { ServerError } from "./../Dashboard/style";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: "en",
      // Localization
      IsLoggedIn: false,
      username: "",
      password: "",
      type: "password",
      checked: false,
      redirect: false,
      displayname: "",
      inputError: "",
      message: "",
      serverError: {}
    };
  }

  toggleHandler = () => {
    if (this.state.type === "password") {
      this.setState({
        type: "text"
      });
    } else {
      this.setState({
        type: "password"
      });
    }
  };

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleCheckboxChange = event => {
    this.setState({ checked: event.target.checked });
  };

  login = () => {
    this.state.username != "" && this.state.password != ""
      ? fetch(loginAuthApi, {
          method: "POST",
          headers: LoginAuth,
          body: JSON.stringify({
            username: this.state.username,
            passkey: this.state.password,
            app_code: "PORTAL",
            device_id: "",
            tenantName: "kjscement.com"
          })
        })
          .then(res => {
            if (res.ok) {
              if (this.state.checked) {
                document.cookie = Base64.encode(
                  "username=" + this.state.username
                );

                document.cookie = Base64.encode(
                  "password=" + this.state.password
                );
              }
              return res.json();
            } else {
              return Promise.reject(
                Object.assign({}, res, {
                  status: res.status,
                  statusText: res.statusText
                })
              );
            }
          })
          .then(res => {
            if (res.STATUS == "SUCCESS") {
              sessionStorage.setItem(
                "data",
                Base64.encode(JSON.stringify(res.RESULT))
              );

              this.setState({
                redirect: true
              });
            } else {
              this.setState({
                message: res.MESSAGE
              });
            }
          })
          .catch(err =>
            this.setState({
              serverError: err
            })
          )
      : this.setState({
          inputError: "Username & Password must be filled out"
        });
  };

  onKeyPress = e => {
    if (e.which === 13) {
      this.login();
    }
  };

  changeLanguage = event => {
    // debugger;
    this.setState({
      language: event.target.getAttribute("value")
    });
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/blankPage" />;
    }
    strings.setLanguage(this.state.language);
    return (
      <DashboardWrapper>
        <HeaderStyled>
          <InnerHeaderStyled>
            <Logo />
            <RightAlign>
              {/* <Localization>
                <img src={Hi} onClick={this.changeLanguage} value="hi" />
                <img src={En} onClick={this.changeLanguage} value="en" />
              </Localization> */}
              <SupportEmail />
            </RightAlign>
          </InnerHeaderStyled>
        </HeaderStyled>
        <ImageBg>
          <ClientLogo />
          <LoginDetaillWrap>
            <LoginIconWrap>
              <Icon name="systemUpdate" />
              <p>{strings.Log_In}</p>
            </LoginIconWrap>
            <Form onKeyPress={this.onKeyPress}>
              <InputWrap>
                <label>{strings.User_Name}</label>
                <input
                  type="text"
                  placeholder="User Name"
                  name="username"
                  autoComplete="on"
                  value={this.state.username}
                  onChange={this.onChange}
                />
              </InputWrap>
              <InputPasswordWrap>
                <label>{strings.Password}</label>
                <input
                  type={this.state.type}
                  placeholder="Password"
                  name="password"
                  autoComplete="on"
                  value={this.state.password}
                  onChange={this.onChange}
                />
                <IconWrap onClick={this.toggleHandler}>
                  {this.state.type == "password" ? (
                    <Icon name="disabledEye" />
                  ) : (
                    <Icon name="eye" />
                  )}
                </IconWrap>
              </InputPasswordWrap>
              {/* ////////////////////////////////////////////// */}
              {/* <MultiSearchSelect /> */}
              {/* ////////////////////////////////////////////// */}
              <RememberPassword>
                <label>
                  <Checkbox
                    checked={this.state.checked}
                    onChange={this.handleCheckboxChange}
                  />
                  <span>{strings.Remember_Password}</span>
                </label>
              </RememberPassword>
              {/* <ErrorMsg>{loginError}</ErrorMsg> */}
              <ErrorMsg>
                {this.state.username == "" || this.state.password == ""
                  ? this.state.inputError
                  : this.state.message}
              </ErrorMsg>
              <ErrorMsg>
                {this.state.serverError.status}
                {this.state.serverError.statusText}
              </ErrorMsg>

              <Button type="primary" classname="submit" onclick={this.login}>
                {strings.Login}
              </Button>
            </Form>
            <ForgotPassword />
          </LoginDetaillWrap>
        </ImageBg>
        <Footer />
      </DashboardWrapper>
    );
  }
}

export default Login;
