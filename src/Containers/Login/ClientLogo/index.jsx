import React from "react";
import styled from "styled-components";

const LogoWrap = styled.div`
  max-width: 14rem;
  margin: 0 5.5rem;
  display: inline-block;

  @media (max-width: 768px) {
    max-width: calc(50% - 32px);
    margin: 0rem 16px;
  }

  img {
    width: 100%;
    display: block;

    @media (max-width: 768px) {
      max-width: 110px;
      width: 100%;
    }
  }
`;

const LogoListWrap = styled.div`
  width: 70%;
  margin-right: 3rem;

  @media (max-width: 1023px) {
    width: 52%;
  }

  @media (max-width: 480px) {
    width: 100%;
    margin-right: 0rem;
    display: none;
  }
`;

const ClientLogo = () => {
  const logos = [
    "clientLogo1",
    "clientLogo2",
    "clientLogo3",
    "clientLogo4",
    "clientLogo5",
    "clientLogo6"
  ];
  const logoList = logos.map((logo, index) => (
    <LogoWrap>
      <img
        key={logo[index]}
        src={require(`../../../Assets/Images/${logo}.png`)}
        alt=""
      />
    </LogoWrap>
  ));
  return <LogoListWrap>{logoList}</LogoListWrap>;
};

export default ClientLogo;
