import styled from "styled-components";
import img from "../../Assets/Images/login-bg.png";

export const DashboardWrapper = styled.div``;

export const ImageBg = styled.div`
  max-width: 120rem;
  margin: 0 auto;
  // background-image: url(${img});
  background-position: center;
  background-size: cover;
  width: 100%;
  height: calc(100vh - 84px);
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 2rem;

  @media (max-width: 480px) {
    flex-direction: column;
    height: calc(100vh + 84px);
    overflow-x: scroll;
  }
`;

export const LoginDetaillWrap = styled.div`
  width: 30%;
  background: rgba(204, 204, 204, 0.85);
  border-radius: 1rem;
  align-items: center;
  padding: 3rem;
  position: relative;

  @media (max-width: 1023px) {
    width: 46%;
  }

  @media (max-width: 480px) {
    width: 100%;
    margin-top: 1rem;
  }
`;

export const FormWrap = styled.div`
  padding: 0 2rem;
  margin: 5rem 0;
`;

export const LoginIconWrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background: #e5e5e8;
  padding: 1.5rem 2rem;
  width: 12rem;
  position: absolute;
  top: -20px;
  left: 50%;
  transform: translateX(-50%);

  p {
    font-size: 1.6rem;
    color: #333;
    font-family: "Gotham";
  }
`;

export const Form = styled.div`
  margin-top: 8rem;
  margin-top: 5rem;
  display: flex;
  flex-direction: column;
  align-items: center;

  a {
    margin: 4rem 0;
    width: 100%;
    text-decoration: none;
  }

  .submit {
    border-radius: 2rem;
    width: 100%;
    background: #9b4955;
    margin: 4rem 0;
  }
`;

export const InputWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  font-family: "Roboto", sans-serif;
  font-size: 1.4rem;
  margin-bottom: 3rem;

  label {
    margin-bottom: 1rem;
  }

  input {
    background: none;
    border: none;
    -webkit-appearance: none;
    border: 1px solid #a8a8a8;
    height: 3rem;
    outline: none;
    font-family: "Roboto", sans-serif;
    color: #333;
    font-size: 1.4rem;
  }

  p {
    color: red;
    font-size: 12px;
  }
`;

export const InputPasswordWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  font-family: "Roboto", sans-serif;
  font-size: 1.4rem;
  position: relative;
  margin-bottom: 3rem;

  label {
    margin-bottom: 1rem;
  }

  input {
    background: none;
    border: none;
    -webkit-appearance: none;
    border: 1px solid #a8a8a8;
    height: 3rem;
    outline: none;
    font-family: "Roboto", sans-serif;
    color: #333;
    font-size: 1.4rem;
    padding-right: 3rem;
  }

  p {
    color: red;
    font-size: 12px;
  }
`;

export const IconWrap = styled.div`
  position: absolute;
  right: 0;
  bottom: 4px;
  z-index: 9;

  @media (max-width: 1023px) {
    right: 5px;
    bottom: 2px;
  }
  @media (max-width: 1023px) {
    bottom: 14px;
  }
`;

export const RememberPassword = styled.div`
  display: flex;
  flex-direction: row;
  margin-right: auto;
  align-items: center;
  font-size: 1.4rem;
  font-family: "Roboto", sans-serif;
  label span {
    margin-left: 1rem;
  }
`;

export const ErrorMsg = styled.div`
  color: red;
  margin-top: 10px;
  display: flex;
  width: 100%;
  font-size: 12px;
`;

export const HeaderStyled = styled.div`
  background: #e5e5e5;
  padding: 1rem 0;

  @media (max-width: 480px) {
    padding: 1rem;
  }
`;

export const InnerHeaderStyled = styled.div`
  max-width: 120rem;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const RightAlign = styled.div`
  display: flex;
`;

export const Localization = styled.div`
  display: flex;
  padding: 10px;
  img {
    height: 18px;
    margin-right: 10px;
  }
`;
