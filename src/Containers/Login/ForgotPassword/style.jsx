import styled from "styled-components";

export const ForgotPasswordWrap = styled.div`
  font-size: 1.4rem;
  color: #333;
  font-family: "Roboto", sans-serif;
`;
