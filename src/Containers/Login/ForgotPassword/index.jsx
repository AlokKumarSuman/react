import React, { Component } from "react";
import { strings } from "../../../Localization";
import { ForgotPasswordWrap } from "./style.jsx";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ForgotPasswordWrap>
        <p>{strings.Forgot_Password}</p>
      </ForgotPasswordWrap>
    );
  }
}

export default ForgotPassword;
