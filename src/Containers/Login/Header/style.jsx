import styled from "styled-components";

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 1rem 5rem;
  background: #2dc3e8;
  position: fixed;
  width: 100%;
  z-index: 999;

  @media (max-width: 1023px) {
    padding: 1rem 2rem;
  }

  @media (max-width: 480px) {
    flex-direction: column;
    padding: 0rem;
  }

  h5 {
    font-size: 1.6rem;
    font-family: "Gotham";
    margin: 15px 0;
    font-weight: 400;
    a {
      text-decoration: none;
      color: #333;
      cursor: pointer;
    }
    a.active {
      color: #ff5e5f;
    }
  }
`;
export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 30rem;

  @media (max-width: 568px) {
    width: 17rem;
  }

  h5 {
    a {
      color: #fff;
    }

    @media (max-width: 480px) {
      display: none;
    }
  }
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  a.active .icon {
    fill: #cb0202;
  }
`;

export const IconList = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 6rem;
  margin-right: 3rem;
  position: relative;
  z-index: 1;

  @media (max-width: 480px) {
    display: none;

    .mobile {
      display: block;
    }
  }
`;

export const AvatarDropDownWrap = styled.div`
  position: relative;
`;
export const UserDetails = styled.div`
  font-size: 1.2rem;
  margin-right: 1.2rem;
  color: #fff;
`;

export const DropDownWrap = styled.div`
  position: absolute;
  top: 60px;
  width: 20rem;
  right: 0;
  text-align: left;
  padding: 0;
  background: #fff;
  border-radius: 5px;
  display: ${props => (props.dropdown === true ? "block" : "none")};
  transition: 0.5s all;
  box-shadow: 0 0 6px 1px rgba(0, 0, 0, 0.16);

  &:before {
    content: "";
    display: block;
    width: 0px;
    height: 0px;
    border-left: 12px solid transparent;
    border-right: 14px solid transparent;
    border-bottom: 14px solid #fff;
    position: absolute;
    top: -14px;
    left: 66%;
  }

  a {
    padding: 10px;
    margin: 0;
    display: block;
    text-decoration: none;
    color: black;
    font-size: 13px;
  }
  a:hover {
    background: #e4e5e6;
  }
`;

export const ProfilePhoto = styled.div`
  width: 50px;
  height: 50px;
  cursor: pointer;
  position: relative;
  z-index: 1;

  img {
    width: 100%;
    display: block;
    border-radius: 50%;
  }
`;

export const BrandLogo = styled.div`
  width: 200px;
  height: 50px;
  cursor: pointer;
  position: relative;
  z-index: 1;
  overflow: hidden;

  @media (max-width: 1023px) {
    width: 60px;
  }

  img {
    height: 100%;
    display: block;
    position: relative;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
  }
`;

export const TaskPage = styled.div`
  font-size: 14px;
  margin: 0px 20px;
  display: flex;
  flex-direction: column;
  position: relative;
  width: 120px;
  text-align: right;

  a {
    color: #000;
    text-decoration: none;
    cursor: pointer;
    font-size: 13px;
  }

  p {
    cursor: pointer;
    color: #fff;
  }

  ul {
    display: ${props => (props.showTask === true ? "block" : "none")};
    position: absolute;
    top: 40px;
    right: 0;
    background: #fff;
    width: 160px;
    border-radius: 5px;
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.35);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.35);
    box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.35);

    &:before {
      content: "";
      display: block;
      width: 0px;
      height: 0px;
      border-left: 12px solid transparent;
      border-right: 14px solid transparent;
      border-bottom: 14px solid #fff;
      position: absolute;
      top: -14px;
      left: 66%;
    }

    li {
      padding: 10px 5px;
      border-bottom: 1px solid #333;

      h5 {
        margin: 2px 0px;
      }
    }
  }

  @media (max-width: 480px) {
    display: none;
  }
`;

export const Localization = styled.div`
  display: flex;
  padding: 10px;
  img {
    height: 18px;
    margin-right: 10px;
  }
`;

export const MobileHeader = styled.div`
  display: none;
  @media (max-width: 480px) {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 1rem;
    background: #ccc;

    h5 {
      margin: 4px 0px;
    }

    .mobile {
      display: block;
      margin: 0px;
    }
  }
`;

export const InnerWrapper = styled.div`
  display: flex;
  width: 100%;
  padding: 0rem;
  justify-content: space-between;
  @media (max-width: 480px) {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 1rem;
  }
`;
