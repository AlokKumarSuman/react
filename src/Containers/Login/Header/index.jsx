import React, { Component } from "react";
import { Base64 } from "js-base64";
import Logo from "../../../Components/Logo";
import { BrowserRouter as Router, NavLink, Redirect } from "react-router-dom";
import Avatar from "../../../Components/Avatar";
import Icon from "../../../Components/Icons";
// import { logo } from "../../Config";
// Localization
import Hi from "../../../Assets/Images/Country/Icons/hi.png";
import En from "../../../Assets/Images/Country/Icons/en.png";
import { strings } from "../../../Localization";

import {
  HeaderWrapper,
  WrapperLeft,
  WrapperRight,
  IconList,
  DropDownWrap,
  AvatarDropDownWrap,
  UserDetails,
  ProfilePhoto,
  BrandLogo,
  TaskPage,
  Localization,
  InnerWrapper,
  MobileHeader
} from "./style";

export class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdown: false,
      displayname: "",
      photo: "",
      logo: "",
      showTask: false
    };
  }

  dropdownHandler = () => {
    this.setState(prevState => ({
      dropdown: !prevState.dropdown
    }));
  };

  logOut = () => {
    sessionStorage.clear();
  };

  componentWillReceiveProps() {
    if (sessionStorage.length == 0) {
      this.setState({
        displayname: ""
      });
    } else {
      const alldata = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
      const name = alldata.givenname;
      const profilephoto = alldata.photo;
      const companylogo = alldata.LOGO;

      if (name != null) {
        this.setState({
          displayname: name,
          photo: profilephoto == undefined ? "" : profilephoto,
          logo: companylogo
        });
      }
    }
  }

  showHandler = () => {
    this.setState(prevState => ({
      showTask: !prevState.showTask
    }));
  };

  changeLanguage = event => {
    const crrLang = event.target.getAttribute("value");
    this.props.languageProp(crrLang);
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }

    return (
      <React.Fragment>
        <HeaderWrapper>
          <InnerWrapper>
            <WrapperLeft>
              <BrandLogo>
                <img src={this.state.logo} alt="logo" />
              </BrandLogo>

              <h5>
                <NavLink to="/dashboard">{strings.Dashboard}</NavLink>
              </h5>
            </WrapperLeft>
            <WrapperRight>
              {/* <Localization>
              <img src={Hi} onClick={this.changeLanguage} value="hi" />
              <img src={En} onClick={this.changeLanguage} value="en" />
            </Localization> */}
              <TaskPage showTask={this.state.showTask} className="desktop">
                <p onClick={this.showHandler}>{strings.Task_Type}</p>
                <ul>
                  <li>
                    <h5>
                      <NavLink to="/received-task">
                        {strings.Received_Task}
                      </NavLink>
                    </h5>
                  </li>
                  <li>
                    <h5>
                      <NavLink to="/sent-task">{strings.Sent_Task}</NavLink>
                    </h5>
                  </li>
                </ul>
              </TaskPage>
              <IconList>
                {/* <Icon name="notificationEmail" /> */}

                <NavLink to="/user-notification">
                  <Icon name="notificationBell" color="#fff" />
                </NavLink>
                <NavLink to="/user-alert">
                  <Icon name="notificatonWarning" color="#fff" />
                </NavLink>
              </IconList>
              <UserDetails>
                {strings.Hi} {this.state.displayname}
              </UserDetails>

              <AvatarDropDownWrap
                onmouseenter={this.dropdownHandler}
                onMouseLeave={this.dropdownHandler}
              >
                {this.state.photo == "" ? (
                  <Avatar />
                ) : (
                  <ProfilePhoto>
                    <img src={this.state.photo} alt="Profile Picture" />
                  </ProfilePhoto>
                )}

                <DropDownWrap dropdown={this.state.dropdown}>
                  <NavLink to="/profile">{strings.Profile}</NavLink>
                  <NavLink to="/changePassword">
                    {strings.Change_Password}
                  </NavLink>
                  <NavLink to="/login" onClick={this.logOut}>
                    {strings.Log_Out}
                  </NavLink>
                </DropDownWrap>
              </AvatarDropDownWrap>
            </WrapperRight>
          </InnerWrapper>

          <MobileHeader>
            <h5>
              <NavLink to="/dashboard">{strings.Dashboard}</NavLink>
            </h5>
            <TaskPage showTask={this.state.showTask} className="mobile">
              <p onClick={this.showHandler}>{strings.Task_Type}</p>
              <ul>
                <li>
                  <h5>
                    <NavLink to="/received-task">
                      {strings.Received_Task}
                    </NavLink>
                  </h5>
                </li>
                <li>
                  <h5>
                    <NavLink to="/sent-task">{strings.Sent_Task}</NavLink>
                  </h5>
                </li>
              </ul>
            </TaskPage>
            <IconList className="mobile">
              <NavLink to="/user-notification">
                <Icon name="notificationBell" color="#fff" />
              </NavLink>
              <NavLink to="/user-alert">
                <Icon name="notificatonWarning" color="#fff" />
              </NavLink>
            </IconList>
          </MobileHeader>
        </HeaderWrapper>
      </React.Fragment>
    );
  }
}
