import styled from "styled-components";

export const SidebarComp = styled.div`
  display: block;
  justify-content: start;
  font-size: 1.4rem;
  transition: 0.5s all;
  background: #fff;
  padding: 0rem 0rem 3rem 0rem;
  overflow-x: hidden;
  overflow-y: auto;
  font-size: 13px;
  font-family: "Gotham";
  font-weight: 500;
  letter-spacing: 1px;

  ::-webkit-scrollbar {
    width: 5px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #888;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555;
  }

  ul.main {
    margin-top: 6px;
    transition: all 0.3s ease-in-out;

    @media (max-width: 1023px) {
      margin-top: 0px;
    }

    i.fa {
      width: 22px;
    }
  }

  li.sidenav {
    display: flex;
    flex-direction: column;
    border-bottom: 1px solid #e5e6e4;
  }

  .wrapper {
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: ${props =>
      props.navShow ? "10px 2rem 10px 2rem" : "10px 0px 10px 2rem"};

      @media (max-width: 1023px) {
        padding: ${props =>
          props.navShow == false
            ? "10px 2rem 10px 2rem"
            : "10px 0px 10px 2rem"};
      }

    .icon {
      width: 24px;
    }

    &:hover {
      background: #e6e6e6;
      color: #cb0202;

      .icon {
        fill: #cb0202;
      }
    }
  }

  .showHide {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    width: ${props => (props.navShow ? "230px" : "0px")};
    // display: ${props => (props.navShow ? "block" : "none")};
    transition: all 0.3s ease-in-out;
    margin-left: 10px;

    span {
      white-space: nowrap;
    }

    @media (max-width: 1023px) {
      width: ${props => (props.navShow == false ? "250px" : "0px")};
    }
    @media (max-width: 768px) {
      width: ${props => (props.navShow == false ? "280px" : "0px")};
    }
    @media (max-width: 568px) {
      width: ${props => (props.navShow == false ? "340px" : "0px")};
    }
    @media (max-width: 480px) {
      width: ${props => (props.navShow == false ? "410px" : "0px")};
    }
    @media (max-width: 320px) {
      width: ${props => (props.navShow == false ? "90rem" : "0px")};
    }
  }

  ul.subNav {
    display: none;
    background: #e6e6e6;
    transition: all 0.3s ease-in-out;

    li.sidenavchild {
      padding: ${props =>
        props.navShow ? "10px 2rem 10px 2rem" : "10px 0px 10px 2rem"};

        @media (max-width: 1023px) {
          padding: 10px 0px 10px 2rem;
        }

      .wrapper {
        padding: 0px;
      }

      &:hover > a {
        color: #cb0202;

        .icon {
          fill: #cb0202;
        }
      }

      a {
        color: #000;
        display: flex;
        align-items: center;
        text-decoration: none;

        span {
          margin-left: 10px;
          transition: all 0.3s ease-in-out;
          width: ${props => (props.navShow ? "230px" : "0px")};
          white-space: nowrap;

          @media (max-width: 1023px) {
            width: ${props => (props.navShow == false ? "250px" : "0px")};
          }
          @media (max-width: 768px) {
            width: ${props => (props.navShow == false ? "280px" : "0px")};
          }
          @media (max-width: 568px) {
            width: ${props => (props.navShow == false ? "340px" : "0px")};
          }
          @media (max-width: 480px) {
            width: ${props => (props.navShow == false ? "410px" : "0px")};
          }
          @media (max-width: 320px) {
            width: ${props => (props.navShow == false ? "90rem" : "0px")};
          }

        }
      }
    }
  }

  li.sidenav.active {
    background: #e6e6e6;
    color: #cb0202;

    .wrapper {
      .showHide {
        .icon {
          transform: rotate(180deg);
          transition: all 0.3s ease-in-out;
        }
      }

      .icon {
        fill: #cb0202;
      }
    }

    ul.subNav {
      display: block;
      box-shadow: inset 0 4px 4px -2px rgba(0, 0, 0, 0.15),
        inset 0 -4px 4px -2px rgba(0, 0, 0, 0.15);

      li.sidenavchild a.active {
        color: #cb0202;

        .icon {
          fill: #cb0202;
        }
      }
    }
  }

  ul.subNavChild {
    display: none;
  }

  li.sidenavchild.active {
    // box-shadow: inset 0 4px 4px -2px rgba(0, 0, 0, 0.15)
    .wrapper .showHide .icon {
      transform: rotate(180deg);
      
    }

    .subNavChild {
      display: block;
      // background: #f6f6f6;      
      background: #d1d2d2;
      margin: 0px -22px;
      padding: 0px 22px;
      // box-shadow: inset 0 4px 4px -2px rgba(0, 0, 0, 0.15);

      li {
        padding: 10px 0px;

        &:hover a {
          color: red;
        }
      }
    }
  }

  ul.subNav li.sidenavchild.active .wrapper {
    padding-bottom: 10px;
  }

  li.sidenav ul.subNav li.sidenavchild .wrapper .showHide .icon {
    transform: rotate(0deg);
  }

  li.sidenav.active ul.subNav li.sidenavchild.active .wrapper .showHide .icon {
    transform: rotate(180deg);
  }

  ul.subNav li.sidenavchild .wrapper {
    .showHide span {
      color: #000;
      white-space: nowrap;
    }

    .icon {
      fill: black;
    }
  }

  ul.subNav li.sidenavchild {
    &:hover {
      .wrapper .showHide span {
        color: red;
      }

      .wrapper .icon {
        fill: red;
      }
    }
  }

  ul.subNav li.sidenavchild.active {
    .wrapper .showHide span {
      color: #cb0202;
    }

    .wrapper .icon {
      fill: red;
    }
  }
`;
