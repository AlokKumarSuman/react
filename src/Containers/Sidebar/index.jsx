import React, { Component } from "react";
import "font-awesome/css/font-awesome.min.css";
import Icon from "../../Components/Icons/index.jsx";
import HamBurger from "../../Components/HamBurger";
import { SidebarComp } from "./style.jsx";
import { BrowserRouter as Router, NavLink } from "react-router-dom";

import { strings } from "./../../Localization/index";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navShow: true,
      navChildToggle: false,
      admin: true
    };
  }

  clickHandler = () => {
    this.setState(prevState => ({
      navShow: !prevState.navShow
    }));
  };

  clickToggle = e => {
    e.stopPropagation();
    const accFirst = document.getElementsByClassName("sidenav");
    const sidenav = e.currentTarget.getAttribute("class");

    for (var i = 0; i < accFirst.length; i++) {
      accFirst[i].setAttribute("class", "sidenav");
    }
    if (sidenav === "sidenav") {
      e.currentTarget.setAttribute("class", `${sidenav} active`);
    }
  };

  clickChildToggle = e => {
    e.stopPropagation();

    if (e.currentTarget.children.length > 1) {
      const text = e.target.innerText;
      const sidenavchild = e.currentTarget.getAttribute("class");
      if (sidenavchild === "sidenavchild") {
        e.currentTarget.setAttribute("class", `${sidenavchild} active`);
      } else if (text != "Master") {
        e.currentTarget.setAttribute("class", `${sidenavchild}`);
      } else {
        e.currentTarget.setAttribute("class", "sidenavchild");
      }
    }
  };

  componentDidMount() {
    const alldata = JSON.parse(Base64.decode(sessionStorage.getItem("data")));
    const role = alldata.role;
    const roleArray = role.split(",");
    const yes = roleArray.includes("admin");
    this.setState({
      admin: yes
    });
  }

  render() {
    if (this.state.admin === false) {
      for (const list of document.querySelectorAll(
        "ul.main li.sidenav .wrapper .showHide span"
      )) {
        if (list.textContent.includes("Administration")) {
          list.parentElement.parentElement.parentElement.style.display = "none";
        }
      }
    }
    const navigations = [
      {
        name: strings.Products,
        icon: "fa fa-product-hunt",
        path: "products",
        subNav: [
          {
            name: strings.Current_Products,
            icon: "fa fa-tasks",
            path: "products/listed-products"
          },
          {
            name: strings.Addition_Request,
            icon: "fa fa-tasks",
            path: "products/new-product-list"
          }
        ]
      },
      {
        name: strings.Loyalty_Plans,
        icon: "fa fa-btc",
        path: "loyalty-plan",
        subNav: [
          {
            name: strings.Current_Plans,
            icon: "fa fa-btc",
            path: "loyalty-plan/current-plans"
          },
          {
            name: strings.Manage_Plans,
            icon: "fa fa-history",
            path: "loyalty-plan/manage-plans"
          },
          {
            name: strings.Loyalty_Member,
            icon: "fa fa-users",
            path: "loyalty-plan/loyalty-member"
          }
        ]
      },
      {
        name: strings.Marketing_Schemes,
        icon: "fa fa-suitcase",
        path: "marketing-schemes/launch-schemes",
        subNav: [
          {
            name: strings.Launch_Schemes,
            icon: "fa fa-globe",
            path: "marketing-schemes/launch-schemes"
          },
          {
            name: strings.Push_Schemes,
            icon: "fa fa-globe",
            path: "marketing-schemes/push-schemes"
          },
          {
            name: strings.Manage_Schemes,
            icon: "fa fa-history",
            path: "marketing-schemes/manage-schemes"
          }
        ]
      },
      {
        name: strings.Queries,
        icon: "fa fa-quora",
        path: "queries/track-demand",
        subNav: [
          {
            name: strings.Track_Demand,
            icon: "fa fa-cart-plus",
            path: "queries/track-demand"
          },
          {
            name: "Bas Abhi Subscriber",
            icon: "fa fa-users",
            path: "queries/basabhi-subscribers"
          },
          {
            name: "Sauda Subscribers",
            icon: "fa fa-users",
            path: "queries/sauda-subscribers"
          }
        ]
      },
      {
        name: strings.Auth_Dealer,
        icon: "fa fa-users",
        path: "auth-dealer/approved-dealer",
        subNav: [
          {
            name: strings.Approved_Dealer,
            icon: "fa fa-users",
            path: "auth-dealer/approved-dealer"
          },
          {
            name: strings.Approval_Request,
            icon: "fa fa-users",
            path: "auth-dealer/approval-request"
          }
        ]
      },

      {
        name: strings.Administration,
        icon: "fa fa-user",
        path: "administration/location-hierarchy",
        subNav: [
          {
            name: strings.Location_Hierarchy,
            icon: "fa fa-map-marker",
            path: "administration/location-hierarchy"
          },
          {
            name: strings.Sales_Hierarchy,
            icon: "fa fa-map-marker",
            path: "administration/sales-hierarchy"
          },
          {
            name: strings.Create_New_User,
            icon: "fa fa-user-plus",
            path: "administration/create-new-user"
          },
          {
            name: strings.List_Users,
            icon: "fa fa-user-plus",
            path: "administration/list-users"
          },
          {
            name: strings.Loyalty_Points,
            icon: "fa fa-btc",
            path: "administration/loyalty-points"
          },
          {
            name: strings.Master,
            icon: "fa fa-user-plus",
            path: "administration/master/loyalty-rewards",
            subNavChild: [
              {
                name: strings.Loyalty_Rewards,
                icon: "fa fa-btc",
                path: "administration/master/loyalty-rewards"
              },
              {
                name: strings.Marketing_Prizes,
                icon: "fa fa-btc",
                path: "administration/master/marketing-prizes"
              },
              {
                name: strings.Seller_Levels,
                icon: "fa fa-users",
                path: "administration/master/seller-levels"
              },
              {
                name: "Loyalty Member Level",
                icon: "fa fa-users",
                path: "administration/master/loyalty-member-level"
              },
              {
                name: "BasAbhi Consumers",
                icon: "fa fa-users",
                path: "administration/master/bas-abhi-users"
              },
              {
                name: "Sauda Consumers",
                icon: "fa fa-users",
                path: "administration/master/sauda-users"
              }
            ]
          }
        ]
      },

      {
        name: strings.Support,
        icon: "fa fa-phone-square",
        path: "support/customer-care",
        subNav: [
          {
            name: strings.Customer_Care,
            icon: "fa fa-phone-square",
            path: "support/customer-care"
          },
          {
            name: strings.Raise_Request,
            icon: "fa fa-question-circle",
            path: "support/raise-request"
          },
          {
            name: strings.Issue_List,
            icon: "fa fa-question-circle",
            path: "support/raised-issue"
          }
        ]
      }
    ];

    const navList = navigations.map(navItem => (
      <li className="sidenav" onClick={this.clickToggle} key={navItem.name}>
        {"subNav" in navItem ? (
          <div className="wrapper">
            <div className="sideIcon">
              <i class={navItem.icon} aria-hidden="true"></i>
            </div>
            <div className="showHide">
              <span>{navItem.name}</span>
              <Icon name="arrowDown" />
            </div>
          </div>
        ) : null}

        {"subNav" in navItem ? (
          <ul className="subNav">
            {navItem.subNav.map(subChild => (
              /*eslint-disable */
              <li
                className="sidenavchild"
                key={subChild.name}
                onClick={this.clickChildToggle}
              >
                {"subNavChild" in subChild ? (
                  <div className="wrapper">
                    <div className="sideIcon">
                      <i class={subChild.icon} aria-hidden="true"></i>
                    </div>
                    <div className="showHide">
                      <span>{subChild.name}</span>
                      <Icon name="arrowDown" />
                    </div>
                  </div>
                ) : (
                  <NavLink to={`/${subChild.path}`}>
                    <div className="childIcon">
                      <i class={subChild.icon} aria-hidden="true"></i>
                    </div>
                    <span>{subChild.name}</span>
                  </NavLink>
                )}

                {"subNavChild" in subChild ? (
                  <ul className="subNavChild">
                    {subChild.subNavChild.map(subnavchild => (
                      <li>
                        <NavLink to={`/${subnavchild.path}`}>
                          <div className="subNavChildIcon">
                            <i class={subnavchild.icon} aria-hidden="true"></i>
                          </div>
                          <span>{subnavchild.name}</span>
                        </NavLink>
                      </li>
                    ))}
                  </ul>
                ) : null}
              </li>
            ))}
          </ul>
        ) : null}
      </li>
    ));

    return (
      <SidebarComp
        style={{ height: `${this.props.sidebarHeight}px` }}
        navShow={this.state.navShow}
      >
        <HamBurger change={this.state.navShow} onclick={this.clickHandler} />
        <ul className="main">{navList}</ul>
      </SidebarComp>
    );
  }
}

export default Sidebar;
