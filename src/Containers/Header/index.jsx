import React, { Component } from "react";
import { HeaderWrapper, WrapperLeft, WrapperRight } from "./style.jsx";
import logo from "../../Assets/Images/logo.png";
import HamBurger from "../../Components/HamBurger";

class Header extends Component {
  render() {
    return (
      <HeaderWrapper>
        <WrapperLeft>
          <img src={logo} alt="logo" />
          <HamBurger onclick={this.clickHandler} />
        </WrapperLeft>
        <WrapperRight>
          <HamBurger onclick={this.clickHandler} />
        </WrapperRight>
      </HeaderWrapper>
    );
  }
}

export default Header;
