import styled from "styled-components";

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;

export const AlertCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .product-image {
    height: 4rem;
    width: 4rem;
  }

  .client-image {
    height: 4rem;
    width: 4rem;
    border-radius: 50%;
  }
  .client-image-view {
    height: 6rem;
    width: 6rem;
    border-radius: 50%;
  }
`;

export const AlertHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  background-color: #f0f3f5;
  width: 100%;
  font-weight: 600;
  height: 45px;
`;

export const AlertBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 2rem 4rem;
  background-color: white;
  width: 100%;
`;

export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const AlertList = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  padding: 1.6rem 1.6rem 1.2rem;
  margin-bottom: 1.6rem;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.1), 0 1px 10px 0 rgba(0, 0, 0, 0.1);
  color: #333;
  background: ${props => (props.STATUS === "UNREAD" ? "white" : "#fcf4f4")};
  div {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    h5 {
      font-size: 14px;
      margin-bottom: 7px;
    }
    p {
      font-size: 14px;
    }
  }
`;
