import React, { Component } from "react";
import moment from "moment";
import Spinner from "../../Components/Spinner";
import NoDataFound from "../../Components/NoDataFound";

import { userService, fetchUserAlertsList } from "../Config";
import { Auth } from "../../Auth";

import {
  SpinnerConfigData,
  AlertCard,
  AlertHeader,
  WrapperLeft,
  AlertBody,
  ServerError,
  AlertList
} from "./style.jsx";

// icon commented

class Alert extends Component {
  constructor(props) {
    super(props);
    this.state = {
      crrAlertLists: [],

      spinnerConfigData: true,
      fetchErrorMsg: "",
      serverError: {}
    };
  }

  componentDidMount() {
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch(fetchUserAlertsList, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        APP_CODE: "BASABHI",
        LANGUAGE_ID: "-1"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res.RESULT))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            crrAlertLists: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );

    setTimeout(() => {
      this.setState({
        fetchErrorMsg: "",
        serverError: {}
      });
    }, 10000);
  }
  render() {
    const {
      crrAlertLists,
      spinnerConfigData,
      fetchErrorMsg,
      serverError
    } = this.state;

    return (
      <AlertCard>
        <AlertHeader>
          <WrapperLeft>Alerts</WrapperLeft>
        </AlertHeader>
        <AlertBody>
          {spinnerConfigData == true ? (
            <SpinnerConfigData>
              <Spinner />
            </SpinnerConfigData>
          ) : crrAlertLists.length == 0 ? (
            <NoDataFound />
          ) : (
            crrAlertLists.map((item, index) => (
              <AlertList>
                <div>
                  <h5>{item.SUBJECT}</h5>
                  <small>{moment(new Date(item.DATE)).format("Do MMM")}</small>
                </div>
                <p>{item.MESSAGE}</p>
              </AlertList>
            ))
          )}
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
          {this.state.serverError.statusText == "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
        </AlertBody>
      </AlertCard>
    );
  }
}

export default Alert;
