import React, { Component } from "react";
import styled from "styled-components";
import { strings } from "./../../../../../Localization/index";

const FormRowDivider = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background: #c5c5c5;
  height: 32px;
  margin-bottom: 1.2rem;
  justify-content: center;
  padding: 0.6rem;
  font-size: 1.5rem;
`;
const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const DetailView = styled.div`
  ul {
    display: flex;
    flex-flow: row wrap;
    align-items: center;
  }
  li {
    font-size: 1.5rem;
    font-family: "Roboto", sans-serif;

    box-sizing: border-box;
    flex-basis: 50%;
    list-style: none;
    padding: 0.6rem 1.6rem 0.6rem 0rem;
  }
  b {
    font-weight: 500;
  }
`;
const FullRowTable = styled.div`
  font-size: 1.5rem;
  font-family: "Roboto", sans-serif;
  padding: 0.6rem 1.6rem 0.6rem 0rem;
  table {
    width: 100%;
  }
  label {
    // font-size: 1.5rem;
    // font-family: "Roboto", sans-serif;
  }
  .table > tr > th {
    font-weight: 500;
    border-bottom: 3px solid #2dc3e8;
    text-align: left;
  }
  .table > tr > th,
  .table > tr > td {
    height: 32px;
  }
  .table.odd-even tr:nth-child(odd) {
    background: #e5e6e4 !important;
  }
`;

class ViewRecord extends Component {
  render() {
    const { currentRowData } = this.props;
    const currentRowDataTaskData = JSON.parse(currentRowData.TASK_DATA);
    console.log(currentRowDataTaskData);
    return (
      <React.Fragment>
        <DetailView>
          {/* If Task type id != 8 */}
          {currentRowData.TASK_TYPE_ID != "8" ? (
            <ul>
              <li>
                <label>
                  <b>{strings.Scheme_Name} </b>
                </label>{" "}
                : {currentRowDataTaskData.SCHEME_NAME}
              </li>
              <li>
                <label>
                  <b>{strings.Scheme_Type}</b>
                </label>{" "}
                : {currentRowDataTaskData.SCHEME_TYPE_ID}
              </li>
              <li>
                <label>
                  <b>{strings.Start_Date}</b>
                </label>{" "}
                : {currentRowDataTaskData.START_DATE}
              </li>
              <li>
                <label>
                  <b>{strings.End_Date}</b>
                </label>{" "}
                : {currentRowDataTaskData.END_DATE}
              </li>
              <li>
                <label>
                  <b>{strings.Product_Name}</b>
                </label>{" "}
                : {currentRowDataTaskData.PRODUCTS}
              </li>

              <li>
                <label>
                  <b>{strings.Location_name}</b>
                </label>{" "}
                : {currentRowDataTaskData.LOCATIONS}
              </li>
              <li>
                <label>
                  <b>{strings.Description}</b>
                </label>{" "}
                : {currentRowDataTaskData.DESCRIPTION}
              </li>
            </ul>
          ) : null}

          {/* If Task type id === 8 */}
          {currentRowData.TASK_TYPE_ID === "8" ? (
            <>
              <ul>
                <li>
                  <label>
                    <b>Seller name</b>
                  </label>{" "}
                  : {currentRowDataTaskData.SELLER_NAME}
                </li>

                <li>
                  <label>
                    <b>Seller id</b>
                  </label>{" "}
                  : {currentRowDataTaskData.SELLER_ID}
                </li>

                <li>
                  <label>
                    <b>Total sale</b>
                  </label>{" "}
                  : {currentRowDataTaskData.TOTAL_SALE}
                </li>
                <li>
                  <label>
                    <b>Order date</b>
                  </label>{" "}
                  : {currentRowDataTaskData.ORDER_DATE}
                </li>

                <li>
                  <label>
                    <b>Buyer name</b>
                  </label>{" "}
                  : {currentRowDataTaskData.BUYER_NAME}
                </li>

                <li>
                  <label>
                    <b>Buyer id</b>
                  </label>{" "}
                  : {currentRowDataTaskData.BUYER_ID}
                </li>

                <li>
                  <label>
                    <b>Payment Date</b>
                  </label>{" "}
                  : {currentRowDataTaskData.PAYMENT_DATE}
                </li>
              </ul>
              <FullRowTable>
                <label>
                  <b>Items</b>
                </label>
                <table className="table bordered odd-even">
                  <tr>
                    {/* <td>
                      <b>ORDER_ID</b>
                    </td> */}
                    <th>
                      <b>ITEM_NAME</b>
                    </th>
                    <th>
                      <b>QUANTITY</b>
                    </th>
                    <th>
                      <b>Mrp.(Rs)</b>
                    </th>
                    <th>
                      <b>TAX_AMT</b>
                    </th>
                    <th>
                      <b>DISCOUNT_PRICE</b>
                    </th>
                    {/* <td>
                      <b>TAX_INCLUDED</b>
                    </td> */}
                  </tr>

                  {currentRowDataTaskData.ITEMS.map((item, index) => (
                    <tr>
                      <td>{item.ITEM_NAME}</td>
                      <td>{item.QUANTITY}</td>
                      <td>{item.MRP}</td>
                      <td>{item.TAX_AMT}</td>
                      <td>{item.DISCOUNT_PRICE}</td>
                    </tr>
                  ))}
                </table>
              </FullRowTable>
            </>
          ) : null}
        </DetailView>
      </React.Fragment>
    );
  }
}

export default ViewRecord;
