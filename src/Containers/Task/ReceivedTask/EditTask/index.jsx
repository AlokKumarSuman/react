import React, { Component } from "react";
import Button from "../../../../Components/Button";
import { strings } from "./../../../../Localization/index";

import ViewRecord from "./ViewRecord";
import axios from "axios";
import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ErrorMsg,
  SucessMsg,
  FormRowDivider,
  WrapperLeft,
  DetailView
} from "./style";
import { Auth } from "../../../../Auth";
import {
  userService,
  approveLoyaltySchemeTask,
  approveMarketingScheme,
  updateLoyaltySchemeTask,
  updateMarketingSchemeTask,
  approveMarketingRedeemTask,
  approveLoyaltyRedeemTask,
  approvePurchaseTask
} from "../../../Config";

class EditTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currData: {},
      taskTypeId: "",
      taskId: "",
      //
      taskStatus: "",
      addComment: "",
      ttttt: "",
      //
      validateAllFilds: "",
      responseAddLevelStatusAPI: "",
      serverError: {}
    };
  }

  changeHandler = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      validateAllFilds: ""
    });
  };
  componentDidMount() {
    const { currentRowData } = this.props;

    // debugger;
    let commentUpdated = "";
    if (currentRowData.CURRENT_STATUS === "NEW") {
      commentUpdated = "";
    } else {
      commentUpdated = currentRowData.COMMENTS;
    }
    // debugger;
    this.setState({
      currData: currentRowData,
      taskStatus: currentRowData.CURRENT_STATUS,
      addComment: commentUpdated,
      taskTypeId: currentRowData.TASK_TYPE_ID,
      taskId: currentRowData.TASK_ID
    });
  }

  taskTypeApi = taskTypeId => {
    if (taskTypeId === "1") {
      return approveLoyaltySchemeTask;
    } else if (taskTypeId === "2") {
      return approveMarketingScheme;
    } else if (taskTypeId === "3") {
      return;
    } else if (taskTypeId === "4") {
      return updateLoyaltySchemeTask;
    } else if (taskTypeId === "5") {
      return updateMarketingSchemeTask;
    } else if (taskTypeId === "6") {
      return approveMarketingRedeemTask;
    } else if (taskTypeId === "7") {
      return approveLoyaltyRedeemTask;
    } else if (taskTypeId === "8") {
      return approvePurchaseTask;
    }
  };

  submitStatus = () => {
    const {
      currData,
      addComment,
      ttttt,
      taskStatus,
      taskTypeId,
      taskId
    } = this.state;

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    let submitApi = this.taskTypeApi(taskTypeId);

    if (taskTypeId === "3") {
      return;
    }

    if (currData.CURRENT_STATUS === "New" || taskStatus === "NEW") {
      return this.setState({
        validateAllFilds: "You can not submit with status NEW "
      });
    }

    if (currData.CURRENT_STATUS === "UNDER-REVIEW") {
      if (taskStatus === "NEW") {
        return this.setState({
          validateAllFilds: "You can not change task status UNDER-REVIEW to NEW"
        });
      }
    }

    if (taskStatus === "" || addComment === "") {
      return this.setState({
        validateAllFilds: strings.All_field_must_be_filled_out
      });
    }

    // debugger;

    fetch(submitApi, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        TASK_ID: taskId,
        STATUS: taskStatus,
        COMMENT: addComment
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        console.log(res);
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            location.reload();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    const { currentRowData } = this.props;
    console.log(currentRowData);
    const {
      addComment,
      taskStatus,

      validateAllFilds,
      responseAddLevelStatusAPI,
      serverError
    } = this.state;

    // debugger;
    return (
      <React.Fragment>
        <ViewRecord currentRowData={currentRowData}></ViewRecord>
        <FormRowDivider>
          <WrapperLeft>{strings.More_Details}</WrapperLeft>
        </FormRowDivider>

        <Form onSubmit={this.addRecord}>
          <FormRow>
            <LabelInputGroup>
              <h5>{strings.Task_name}</h5>
              <input type="text" value={currentRowData.TASK_NAME} disabled />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>{strings.Current_status}</h5>
              <select
                name="taskStatus"
                onChange={this.changeHandler}
                disabled={
                  currentRowData.CURRENT_STATUS == "APPROVED" ||
                  currentRowData.CURRENT_STATUS == "REJECTED"
                    ? true
                    : false
                }
                value={taskStatus}
              >
                <option value="NEW">NEW</option>
                <option value="APPROVED">APPROVED</option>
                <option value="REJECTED">REJECTED</option>
                <option value="UNDER-REVIEW">UNDER-REVIEW</option>
              </select>
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <LabelInputGroup>
              <h5>{strings.Submitter_id}</h5>
              <input type="text" value={currentRowData.SUBMITTER_ID} disabled />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>{strings.Submission_date}</h5>
              <input
                type="text"
                value={currentRowData.SUBMISSION_DATE}
                disabled
              />
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <textarea
              name="addComment"
              row="4"
              col="20"
              value={addComment}
              onChange={this.changeHandler}
              disabled={
                currentRowData.CURRENT_STATUS == "APPROVED" ||
                currentRowData.CURRENT_STATUS == "REJECTED"
                  ? true
                  : false
              }
            ></textarea>
          </FormRow>

          <FormRow>
            <LabelButtonDU>
              {currentRowData.CURRENT_STATUS === "APPROVED" ||
              currentRowData.CURRENT_STATUS === "REJECTED" ? null : (
                <Button onclick={this.submitStatus}>{strings.Submit}</Button>
              )}
            </LabelButtonDU>
          </FormRow>
        </Form>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        <ErrorMsg>
          {serverError.STATUS} {serverError.MESSAGE}
        </ErrorMsg>

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <ErrorMsg>
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </ErrorMsg>
        ) : null}
        {/* SucessMsg */}
      </React.Fragment>
    );
  }
}

export default EditTask;
