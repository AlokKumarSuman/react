import styled from "styled-components";

export const AuthLevelsCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .product-image {
    height: 4rem;
    width: 4rem;
  }

  .client-image {
    height: 4rem;
    width: 4rem;
    border-radius: 50%;
  }
  .client-image-view {
    height: 6rem;
    width: 6rem;
    border-radius: 50%;
  }
`;

export const AuthLevelsHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  background-color: #f0f3f5;
  width: 100%;
  font-weight: 600;
  height: 45px;
`;

export const AuthLevelsBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;

  .table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};
  }
  .table tbody tr td {
    padding: 0.8rem 0.2rem;
  }
`;
export const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  div {
    margin-right: 1rem;
  }
  div:last-child {
    margin-right: 0;
  }
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    width:auto
    padding: 0.6rem 0.8rem;
  }
  div svg.icon {
    margin-right:0.6rem;
    margin-top: 0px;
    width: 14px;
    height: 14px;
    transform: scale(1.5);
  }
  div svg.icon-fa {
    margin-right:0.6rem;
  }
`;
export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const ButtonWrapDelete = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px auto 0 auto;
  width: 60%;
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
export const SpinnerApproveReq = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
