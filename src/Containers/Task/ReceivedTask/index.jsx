import React, { Component } from "react";
import moment from "moment";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import Spinner from "../../../Components/Spinner";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";
import Modal from "../../../Components/Modal";
import NoDataFound from "../../../Components/NoDataFound";
import { strings } from "./../../../Localization/index";
import { userService, fetchUsersTaskList } from "../../Config";
import { Auth } from "../../../Auth";

import LoyaltySchemeData from "./../tasksView/LoyaltySchemeData";
import MarketingSchemeData from "./../tasksView/MarketingSchemeData";

import ApprovePurchaseTask from "./../tasksView/ApprovePurchaseTask";
import ApproveLoyaltyClaimTask from "./../tasksView/ApproveLoyaltyClaimTask";
import ApproveMarketingRedeemTask from "./../tasksView/ApproveMarketingRedeemTask";

import EditTask from "./EditTask";

import {
  AuthLevelsCard,
  AuthLevelsHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  AuthLevelsBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError,
  ButtonWrapDelete
} from "./style";

class ReceivedTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      allTaskData: [],
      responseStatusAPI: "",
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      responseDeleteLevelStatusAPI: "",
      openEditHandler: false,
      currentRowData: [],
      taskTypeId: "",
      currentStatus: ""
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch(fetchUsersTaskList, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res.RESULT))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            allTaskData: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })

      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  openEditHandler = (e, currentItems) => {
    // debugger;
    this.setState({
      taskTypeId: currentItems.TASK_TYPE_ID,
      currentStatus: currentItems.CURRENT_STATUS,
      currentRowData: currentItems,
      openEditHandler: true
    });
  };

  closeEditHandler = () => {
    this.setState({
      openEditHandler: false
    });
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }

    const {
      allTaskData,
      currentRowData,
      taskTypeId,
      currentStatus,
      spinnerConfigData,
      serverError
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.openEditHandler}
          onclick={this.closeEditHandler}
          title="Edit Task Detail"
        >
          {/* {taskTypeId == 1 || taskTypeId == 4 ? (
            <LoyaltySchemeData
              currentRowData={currentRowData}
              currentStatus={currentStatus}
              onclickClose={this.closeEditHandler}
            />
          ) : taskTypeId == 2 || taskTypeId == 5 ? (
            <MarketingSchemeData
              currentRowData={currentRowData}
              currentStatus={currentStatus}
              onclickClose={this.closeEditHandler}
            />
          ) : taskTypeId == 8 ? (
            <ApprovePurchaseTask />
          ) : taskTypeId == 7 ? (
            <ApproveLoyaltyClaimTask />
          ) : taskTypeId == 6 ? (
            <ApproveMarketingRedeemTask
              currentRowData={currentRowData}
              currentStatus={currentStatus}
              onclickClose={this.closeEditHandler}
            />
          ) : null} */}

          <EditTask
            currentRowData={currentRowData}
            currentStatus={currentStatus}
            onclickClose={this.closeEditHandler}
          ></EditTask>
        </Modal>

        <AuthLevelsCard>
          <AuthLevelsHeader>
            <WrapperLeft>{strings.Task}</WrapperLeft>
          </AuthLevelsHeader>

          <AuthLevelsBody toggleFilter={this.state.filterShow}>
            <table className="table bordered odd-even">
              <thead>
                <tr>
                  <th width="10%" align="left">
                    {strings.Task_type_id}
                  </th>
                  {/* <th width="20%" align="left">
                    {strings.Brand_Name}
                  </th> */}
                  <th width="30%" align="left">
                    {strings.Task_name}
                  </th>
                  <th width="15%" align="left">
                    {strings.Current_status}
                  </th>
                  <th width="25%" align="left">
                    {strings.Submitter_id}
                  </th>
                  <th width="12%" align="center">
                    {strings.Submission_date}
                  </th>
                  <th width="80px" style={{ maxWidth: "100px" }}>
                    {strings.Action}
                  </th>
                </tr>
              </thead>
              <tbody>
                {spinnerConfigData == true ? (
                  <tr>
                    <td colSpan="7">
                      <SpinnerConfigData>
                        <Spinner />
                      </SpinnerConfigData>
                    </td>
                  </tr>
                ) : allTaskData.length == 0 ? (
                  <tr>
                    <td colSpan="7">
                      <NoDataFound />
                    </td>
                  </tr>
                ) : (
                  allTaskData.map((item, index) => (
                    <tr>
                      {/* <td>
                        {item.TASK_TYPE_ID}, {item.TASK_ID}
                      </td> */}
                      {/* <td>{item.BRAND_NAME}</td> */}
                      <td>{item.TASK_TYPE_ID}</td>
                      <td>{item.TASK_NAME}</td>
                      <td align="left">{item.CURRENT_STATUS}</td>
                      <td align="left">{item.SUBMITTER_ID}</td>
                      {/* <td align="center">{item.SUBMISSION_DATE}</td> */}
                      <td align="center">
                        {moment(
                          moment(item.SUBMISSION_DATE, "DD-MM-YYYY hh:mm:ss")
                        ).format("DD/MM/YYYY, hh:mm")}
                      </td>

                      <td align="center">
                        <ButtonGroup>
                          <Icon
                            name="pen"
                            onclick={e => this.openEditHandler(e, item)}
                          />
                        </ButtonGroup>
                      </td>
                    </tr>
                  ))
                )}
              </tbody>
            </table>
            {this.state.fetchErrorMsg != "" ? (
              <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
            ) : null}
            {this.state.serverError.statusText == "" ? (
              <ServerError>
                {serverError.status} {serverError.statusText}
              </ServerError>
            ) : null}
          </AuthLevelsBody>
        </AuthLevelsCard>
      </React.Fragment>
    );
  }
}

// ViewDealer.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default ReceivedTask;
