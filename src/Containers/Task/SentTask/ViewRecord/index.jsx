import React, { Component } from "react";
import styled from "styled-components";
import { strings } from "./../../../../Localization/index";

const FormRowDivider = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background: #c5c5c5;
  height: 32px;
  margin-bottom: 1.2rem;
  justify-content: center;
  padding: 0.6rem;
  font-size: 1.5rem;
`;
const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const DetailView = styled.div`
  ul {
    display: flex;
    flex-flow: row wrap;
    align-items: center;
  }
  li {
    font-size: 1.5rem;
    font-family: "Roboto", sans-serif;

    box-sizing: border-box;
    flex-basis: 50%;
    list-style: none;
    padding: 0.6rem;
  }
  li b {
    font-weight: 500;
  }
`;

class ViewRecord extends Component {
  render() {
    const { currentRowData } = this.props;
    const currentRowDataTaskData = JSON.parse(currentRowData.TASK_DATA);
    console.log(currentRowDataTaskData);
    return (
      <React.Fragment>
        <table className="table record-view-table">
          <tbody>
            <tr>
              <td>
                <b>{strings.Task_name} </b>
              </td>
              <td>: {currentRowData.TASK_NAME}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Brand_Name} </b>
              </td>
              <td>: {currentRowData.BRAND_NAME}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Current_status} </b>
              </td>
              <td>: {currentRowData.CURRENT_STATUS}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Status_Type} </b>
              </td>
              <td>: {currentRowData.TASK_STATUS}</td>
            </tr>

            <tr>
              <td>
                <b>{strings.Submission_date} </b>
              </td>
              <td>: {currentRowData.SUBMISSION_DATE}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Submitter_id} </b>
              </td>
              <td>: {currentRowData.SUBMITTER_ID}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Updated_By} </b>
              </td>
              <td>: {currentRowData.UPDATED_BY}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Updated_date} </b>
              </td>
              <td>: {currentRowData.UPDATED_DATE}</td>
            </tr>

            <tr>
              <td>
                <b>{strings.Assigned_Users} </b>
              </td>
              <td>
                :{" "}
                {currentRowData.ASSIGNED_USERS.map(
                  item => item.ASSIGNED_USER_ID + ", "
                )}
              </td>
            </tr>
          </tbody>
        </table>

        <FormRowDivider>
          <WrapperLeft>{strings.More_Details}</WrapperLeft>
        </FormRowDivider>

        <DetailView>
          <ul>
            <li>
              <label>
                <b>{strings.Scheme_Name} </b>
              </label>{" "}
              : {currentRowDataTaskData.SCHEME_NAME}{" "}
            </li>
            <li>
              <label>
                <b>{strings.Scheme_Type} </b>
              </label>{" "}
              : {currentRowDataTaskData.SCHEME_TYPE_ID}
            </li>
            <li>
              <label>
                <b>{strings.Start_Date}</b>{" "}
              </label>{" "}
              : {currentRowDataTaskData.START_DATE}
            </li>
            <li>
              <label>
                <b>{strings.End_Date}</b>{" "}
              </label>{" "}
              : {currentRowDataTaskData.END_DATE}
            </li>
            <li>
              <label>
                <b>{strings.Product_Name}</b>{" "}
              </label>{" "}
              : {currentRowDataTaskData.PRODUCTS}
            </li>
            <li>
              <label>
                <b>{strings.Location_name}</b>{" "}
              </label>{" "}
              : {currentRowDataTaskData.LOCATIONS}
            </li>
            <li>
              <label>
                <b>{strings.Description}</b>{" "}
              </label>{" "}
              : {currentRowDataTaskData.DESCRIPTION}
            </li>
          </ul>
        </DetailView>
      </React.Fragment>
    );
  }
}

export default ViewRecord;
