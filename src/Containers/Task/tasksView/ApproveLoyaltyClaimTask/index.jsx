import React, { Component } from "react";
import Button from "../../../../Components/Button";
import { strings } from "./../../../../Localization/index";
import axios from "axios";
import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ErrorMsg
} from "./style";
import { Auth } from "../../../../Auth";
import { userService, approveLoyaltyRedeemTask } from "../../../Config";

class ApproveLoyaltyClaimTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskTypeId: this.props.currentRowData.TASK_TYPE_ID,
      taskStatus: "",
      taskStatusId: "",
      comment: "",
      responseAddLevelStatusAPI: "",
      validateAllFilds: "",
      serverError: {},
      inputDisabled:
        this.props.currentStatus === "REJECTED" ||
        this.props.currentStatus === "APPROVED"
          ? true
          : false
    };
  }

  changeHandler = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      validateAllFilds: ""
    });
  };

  statusChange = e => {
    // debugger;
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      taskStatusId: id,
      taskStatus: e.target.value,
      responseAddLevelStatusAPI: ""
    });
  };

  submitStatus = () => {
    // debugger;
    const { comment, taskStatus, taskTypeId } = this.state;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    if (taskStatus === "" || comment === "") {
      return this.setState({
        validateAllFilds: strings.All_field_must_be_filled_out
      });
    }

    const approveAPI = approveLoyaltyRedeemTask;

    axios
      .post(
        taskTypeId == 7 ? approveAPI : null,
        {
          BRAND_ID: authBrandId,
          USER_ID: authUserName,
          TENANT_ID: authTenantId,
          TASK_ID: this.props.currentRowData.TASK_ID,
          STATUS: taskStatus,
          COMMENT: comment
        },
        { headers: Auth }
      )
      // .then(res => console.log(res))
      .then(res => res.data)
      .then(res => {
        if (res.ok) {
          return res.data;
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    console.log(this.state.inputDisabled);
    const { currentRowData } = this.props;
    const {
      comment,
      validateAllFilds,
      responseAddLevelStatusAPI,
      serverError
    } = this.state;
    // debugger;
    return (
      <React.Fragment>
        <Form onSubmit={this.addRecord}>
          <FormRow>
            <LabelInputGroup>
              <h5>{strings.Task_name}</h5>
              <input type="text" value={currentRowData.TASK_NAME} disabled />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>{strings.Current_status}</h5>
              <select
                name="taskStatus"
                onChange={e => this.statusChange(e)}
                disabled={this.state.inputDisabled}
              >
                <option>{strings.Change_status}</option>
                <option id="1">NEW</option>
                <option id="2">APPROVED</option>
                <option id="3">REJECTED</option>
                <option id="4">UNDER-REVIEW</option>
              </select>
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <LabelInputGroup>
              <h5>{strings.Submitter_id}</h5>
              <input type="text" value={currentRowData.SUBMITTER_ID} disabled />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>{strings.Submission_date}</h5>
              <input
                type="text"
                value={currentRowData.SUBMISSION_DATE}
                disabled
              />
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <textarea
              name="comment"
              row="4"
              col="20"
              value={comment}
              onChange={this.changeHandler}
              disabled={this.state.inputDisabled}
            ></textarea>
          </FormRow>

          <FormRow>
            <LabelButtonDU>
              <Button onclick={this.submitStatus}>{strings.Submit}</Button>
            </LabelButtonDU>
          </FormRow>
        </Form>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        <ErrorMsg>
          {serverError.STATUS} {serverError.MESSAGE}
        </ErrorMsg>

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.data.STATUS +
              ": " +
              responseAddLevelStatusAPI.data.MESSAGE}
          </h4>
        ) : null}
      </React.Fragment>
    );
  }
}

export default ApproveLoyaltyClaimTask;
