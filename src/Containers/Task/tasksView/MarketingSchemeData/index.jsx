import React, { Component } from "react";
import Button from "../../../../Components/Button";
import { strings } from "./../../../../Localization/index";
import axios from "axios";
import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ErrorMsg
} from "./style";
import { Auth } from "../../../../Auth";
import {
  userService,
  updateMarketingScheme,
  approveMarketingScheme
} from "../../../Config";

class MarketingSchemeData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  changeHandler = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      validateAllFilds: ""
    });
  };

  statusChange = e => {
    // debugger;
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      taskStatusId: id,
      taskStatus: e.target.value,
      responseAddLevelStatusAPI: ""
    });
  };

  submitStatus = () => {
    // debugger;
    const { comment, taskStatus, taskTypeId } = this.state;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    if (taskStatus === "" || comment === "") {
      return this.setState({
        validateAllFilds: strings.All_field_must_be_filled_out
      });
    }

    const approveAPI = approveMarketingScheme;
    const updateAPI = updateMarketingScheme;

    axios
      .post(
        taskTypeId == 2 ? approveAPI : updateAPI,
        {
          BRAND_ID: authBrandId,
          USER_ID: authUserName,
          TENANT_ID: authTenantId,
          TASK_ID: this.props.currentRowData.TASK_ID,
          STATUS: taskStatus,
          COMMENT: comment
        },
        { headers: Auth }
      )
      // .then(res => console.log(res))
      .then(res => res.data)
      .then(res => {
        if (res.ok) {
          return res.data;
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    // debugger;
    const { currentRowData } = this.props;

    // debugger;
    return (
      <React.Fragment>
        <table className="table record-view-table">
          <tbody>
            <tr>
              <td>
                <b>{strings.Task_name} </b>
              </td>
              <td>: {currentRowData.TASK_NAME}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Brand_Name} </b>
              </td>
              <td>: {currentRowData.BRAND_NAME}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Current_status} </b>
              </td>
              <td>: {currentRowData.CURRENT_STATUS}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Submission_date} </b>
              </td>
              <td>: {currentRowData.SUBMISSION_DATE}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Submitter_id} </b>
              </td>
              <td>: {currentRowData.SUBMITTER_ID}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Updated_By} </b>
              </td>
              <td>: {currentRowData.UPDATED_BY}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Updated_date} </b>
              </td>
              <td>: {currentRowData.UPDATED_DATE}</td>
            </tr>
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default MarketingSchemeData;
