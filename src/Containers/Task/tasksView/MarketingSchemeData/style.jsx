import styled from "styled-components";

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0px;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.2rem;
    }
  }
`;

export const LabelButtonDU = styled.div`
  width: auto !important;
  display: flex;
  margin: 20px auto 0px;
  float: none !important;
  cursor: pointer;
  div {
    width: auto;
    margin: 0 auto;
    display: inline-block;
    min-width: 151px;
  }
  div:first-child {
    margin-right: 20px;
  }
`;

export const NameButtonWrap = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ImageRatio = styled.div`
  object-fit: cover;
  width: 80px;
`;
export const ErrorMsg = styled.div`
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;

export const ButtonImageNameWrap = styled.div`
  display: flex;
  align-items: center;

  p {
    margin-left: 10px;
  }

  .grey {
    width: 140px;
    padding: 0.8rem;
    text-transform: capitalize;
  }
`;

export const AddImage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  input {
    border: none;
    box-shadow: none;
    padding: 0px;
    text-align: center;
    display: flex;
    align-self: center;
    margin: 0 auto;
    width: 30rem;
  }
`;

export const Preview = styled.div`
  width: 300px;
  height: 200px;
  border: 1px solid #333;
  margin: 10px auto;
  overflow: hidden;

  .previewText {
    font-size: 13px;
    padding: 10px;
    color: #999;
  }

  img {
    width: 100%;
    display: block;
  }
`;

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

// Custom Tabs
export const TabsMain = styled(Tabs)`
  width: 100%;
`;

export const TabGroup = styled(TabList)`
  list-style-type: none;
  display: flex;
  margin: 0;
`;

// TabGroup.tabsRole = "TabList";
export const TabLists = styled(Tab)`
  border: 1px solid #6f94ac;
  padding: 1.2rem 0.6rem;
  user-select: none;
  cursor: pointer;
  flex-grow: 1;
  width: 100%;
  background-color: #6f94ac;
  text-align: center;
  color: white;

  &.is-selected {
    background-color: #025063;
    border-bottom: none;
  }
`;
// TabLists.tabsRole = "Tab";
export const TabPanels = styled(TabPanel)`
  display: none;
  min-height: 4rem;
  padding: 1rem 0;
  border-top: none;
  background-color: white;
  flex-direction: row;
  align-items: center;
  min-height: 24rem;
  &.is-selected {
    display: block;
  }
`;
