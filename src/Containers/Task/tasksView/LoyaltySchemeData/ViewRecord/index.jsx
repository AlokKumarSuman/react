import React, { Component } from "react";
import { strings } from "./../../../../../Localization/index";

class LoyaltySchemeDataView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // debugger;
    const { currentRowData } = this.props;

    // debugger;
    return (
      <React.Fragment>
        <table className="table record-view-table">
          <tbody>
            <tr>
              <td>
                <b>{strings.Task_name} </b>
              </td>
              <td>: {currentRowData.TASK_NAME}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Brand_Name} </b>
              </td>
              <td>: {currentRowData.BRAND_NAME}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Current_status} </b>
              </td>
              <td>: {currentRowData.CURRENT_STATUS}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Submission_date} </b>
              </td>
              <td>: {currentRowData.SUBMISSION_DATE}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Submitter_id} </b>
              </td>
              <td>: {currentRowData.SUBMITTER_ID}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Updated_By} </b>
              </td>
              <td>: {currentRowData.UPDATED_BY}</td>
            </tr>
            <tr>
              <td>
                <b>{strings.Updated_date} </b>
              </td>
              <td>: {currentRowData.UPDATED_DATE}</td>
            </tr>
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default LoyaltySchemeDataView;
