import React, { Component } from "react";
import { strings } from "./../../../Localization/index";
import Button from "../../../Components/Button";
import Modal from "../../../Components/Modal";
import Icon from "../../../Components/Icons";
import NoDataFound from "../../../Components/NoDataFound";
import Spinner from "../../../Components/Spinner";

import {
  userService,
  listDomainUsers,
  updatePassword,
  deleteUser
} from "../../Config";
import { Auth } from "../../../Auth";

import {
  ProfileWrapper,
  ProfileHeader,
  ProfileBody,
  LabelButton,
  LabelInputGroup,
  LabelButtonDU,
  SpinnerConfigData,
  ErrorMsg,
  SucessMsg,
  ServerError,
  Form,
  FormRow,
  ButtonWrapDelete
} from "./style.jsx";

class ListUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // fetch user's list
      passwordInputModel: true,
      spinnerConfigData: true,
      fetchSubmitted: false,
      dataUsersList: [],
      fetchErrorMsg: {},
      passwordBlankError: "",
      //////
      currentItem: "",
      currAdminPassword: "",
      addNewPassword: "",
      confirmNewPassword: "",
      updatePasswordModal: false,
      passwordConfirmationError: "",
      responseUpdatePassword: {},
      //
      deleteUserModal: false,
      responseDeleteUserAPI: {},
      serverErrorDelete: {}
    };
  }

  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      passwordConfirmationError: "",
      passwordBlankError: ""
    });
  };

  componentDidMount() {
    // this.passwordInputHandler();
  }

  // Conform Password
  passwordInputHandler = e => {
    this.setState({
      passwordInputModel: true,
      fetchErrorMsg: {},
      fetchSubmitted: false
    });
  };
  passwordInputModelClose = e => {
    this.setState({
      passwordInputModel: false,
      currAdminPassword: ""
    });
  };

  getUsersList = () => {
    // debugger;

    const authUserName = userService.authUserName();
    const authEmailaddress = userService.authEmailaddress();
    const authTenantName = userService.authTenantName();
    const authOrganization = userService.authOrganization();
    const authBrandName = userService.authBrandName();

    const { currAdminPassword } = this.state;

    if (currAdminPassword === "") {
      return this.setState({
        passwordBlankError: strings.Password_field_must_be_filled_out,
        fetchSubmitted: false
      });
    }

    // debugger;

    fetch(listDomainUsers, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        PASSWORD: currAdminPassword,
        TENANT_NAME: authTenantName,
        DOMAIN: authBrandName
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            dataUsersList: res.RESULT,
            fetchSubmitted: true,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res,
            fetchSubmitted: true,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          fetchErrorMsg: error,
          fetchSubmitted: true,
          spinnerConfigData: false
        })
      );

    setTimeout(() => {
      this.passwordInputModelClose();
    }, 1000);
  };

  // Update Password
  updatePasswordHandler = (e, currentItems) => {
    this.setState({
      updatePasswordModal: true,
      currentItem: currentItems
    });
  };
  updatePasswordHandlerClose = e => {
    this.setState({
      updatePasswordModal: false,
      currentItem: "",
      currAdminPassword: "",
      responseUpdatePassword: {}
    });
  };

  // Update Paassword
  updatePasswordClick = e => {
    // debugger;
    const authEmailaddress = userService.authEmailaddress();
    const authTenantName = userService.authTenantName();
    const authOrganization = userService.authOrganization();
    const authUserName = userService.authUserName();
    const authBrandName = userService.authBrandName();

    const {
      currAdminPassword,
      addNewPassword,
      confirmNewPassword,
      currentItem
    } = this.state;

    // debugger;

    if (currAdminPassword === "") {
      return this.setState({
        passwordConfirmationError: strings.Admin_password_must_be_filled_out
      });
    }

    // To check a password between 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character
    var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

    if (addNewPassword === "" && confirmNewPassword === "") {
      return this.setState({
        passwordConfirmationError: strings.New_Password_field_must_be_filled_out
      });
    }

    if (!addNewPassword.match(decimal)) {
      return this.setState({
        passwordConfirmationError: strings.Validate_password
      });
    }
    if (!(addNewPassword == confirmNewPassword)) {
      return this.setState({
        passwordConfirmationError: strings.New_password_must_be_same
      });
    }
    // debugger;
    fetch(updatePassword, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        PASSWORD: currAdminPassword,
        USER_TO_UPDATE: currentItem,
        USER_NEW_PASS: addNewPassword,
        TENANT_NAME: authTenantName,
        DOMAIN: authBrandName
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        }
      })
      .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseUpdatePassword: res
          });
          setTimeout(() => {
            this.updatePasswordHandlerClose();
          }, 2000);
        } else {
          this.setState({
            responseUpdatePassword: res
          });
        }
      })
      .catch(err =>
        this.setState({
          responseUpdatePassword: err
        })
      );
  };

  // Delete User
  deleteUserHandler = (e, currentItems) => {
    this.setState({
      deleteUserModal: true,
      currentItem: currentItems
    });
  };
  deleteUserHandlerClose = e => {
    this.setState({
      deleteUserModal: false,
      currentItem: "",
      currAdminPassword: ""
    });
  };

  // Delete User
  deleteCurrentUser = () => {
    // debugger;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantName = userService.authTenantName();
    const authBrandName = userService.authBrandName();

    const deleteLevelKey = this.state.currentItem;

    const { currAdminPassword } = this.state;

    if (currAdminPassword === "") {
      return this.setState({
        passwordConfirmationError: strings.Admin_password_must_be_filled_out
      });
    }

    // debugger;
    fetch(deleteUser, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        PASSWORD: currAdminPassword,
        USER_TO_DELETE: deleteLevelKey,
        TENANT_NAME: authTenantName,
        DOMAIN: authBrandName
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        // console.log(res.response);
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseDeleteUserAPI: res
          });

          setTimeout(() => {
            this.deleteUserHandlerClose();
            location.reload();
          }, 2000);
        } else {
          this.setState({
            responseDeleteUserAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorDelete: err
        })
      );
  };

  render() {
    const {
      spinnerConfigData,
      fetchSubmitted,
      dataUsersList,
      fetchErrorMsg,
      passwordBlankError,
      currAdminPassword,
      //
      passwordConfirmationError,
      responseUpdatePassword,
      responseDeleteUserAPI,
      serverErrorDelete
    } = this.state;
    // debugger;
    return (
      <ProfileWrapper>
        <ProfileHeader>{strings.List_of_users}</ProfileHeader>

        <ProfileBody>
          {fetchSubmitted == true ? (
            Object.keys(fetchErrorMsg).length === 0 ? (
              spinnerConfigData == true ? (
                <table className="table">
                  <tr>
                    <td>
                      <SpinnerConfigData>
                        <Spinner />
                      </SpinnerConfigData>
                    </td>
                  </tr>
                </table>
              ) : dataUsersList.length == 0 ? (
                <table className="table">
                  <tr>
                    <td>
                      <NoDataFound />
                    </td>
                  </tr>
                </table>
              ) : (
                <>
                  <table className="table bordered odd-even">
                    <thead>
                      <tr>
                        <th align="left">{strings.Users}</th>
                        <th>{strings.Change_Password}</th>
                        <th>{strings.Action}</th>
                      </tr>
                    </thead>
                    <tbody>
                      {dataUsersList.map((item, index) => (
                        <tr key={index}>
                          <td>{item}</td>
                          <td align="center">
                            <Icon
                              name="pen"
                              onclick={e => this.updatePasswordHandler(e, item)}
                            />
                          </td>

                          <td align="center">
                            <Icon
                              name="trash"
                              onclick={e => this.deleteUserHandler(e, item)}
                            />
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </>
              )
            ) : (
              <>
                <LabelButton>
                  <Button classname="green" onclick={this.passwordInputHandler}>
                    {strings.Confirm_admin_password_to_see_users}
                  </Button>
                </LabelButton>

                <ErrorMsg>
                  {fetchErrorMsg.status} : {fetchErrorMsg.statusText}
                </ErrorMsg>
              </>
            )
          ) : this.state.passwordBlankError === "" ? (
            <LabelButton>
              <Button classname="green" onclick={this.passwordInputHandler}>
                {strings.Confirm_admin_password_to_see_users}
              </Button>
            </LabelButton>
          ) : (
            <LabelButton>
              <Button classname="green" onclick={this.passwordInputHandler}>
                {strings.Confirm_admin_password_to_see_users}
              </Button>
            </LabelButton>
          )}
        </ProfileBody>

        {/* Conform password  */}
        {this.state.passwordInputModel == true ? (
          <Modal
            modalOpen={this.state.passwordInputModel}
            onclick={this.passwordInputModelClose}
            title={strings.Confirm_password}
            size="sm"
          >
            <LabelInputGroup>
              <h5>{strings.Confirm_password}</h5>
              <input
                type="password"
                name="currAdminPassword"
                onChange={this.handleInputChange}
                autoComplete="off"
              />
            </LabelInputGroup>
            {passwordBlankError != "" ? (
              <ErrorMsg>{passwordBlankError}</ErrorMsg>
            ) : null}
            <LabelButtonDU>
              <Button onclick={this.getUsersList}>{strings.Submit}</Button>
            </LabelButtonDU>
          </Modal>
        ) : null}

        {/* Update password  */}
        {this.state.updatePasswordModal == true ? (
          <Modal
            modalOpen={this.state.updatePasswordModal}
            onclick={this.updatePasswordHandlerClose}
            title={strings.Update_user_password}
            size="md"
          >
            <FormRow>
              <LabelInputGroup>
                <h5>{strings.Enter_new_password}</h5>
                <input
                  type="text"
                  name="addNewPassword"
                  onChange={this.handleInputChange}
                  autoComplete="off"
                />
              </LabelInputGroup>
              <LabelInputGroup>
                <h5>{strings.Confirm_password}</h5>
                <input
                  type="password"
                  name="confirmNewPassword"
                  onChange={this.handleInputChange}
                  autoComplete="off"
                />
              </LabelInputGroup>
            </FormRow>

            <FormRow>
              <LabelInputGroup>
                <h5>{strings.Enter_admin_password}</h5>
                <input
                  type="password"
                  name="currAdminPassword"
                  onChange={this.handleInputChange}
                  autoComplete="off"
                />
              </LabelInputGroup>
            </FormRow>

            <LabelButtonDU>
              <Button onclick={this.updatePasswordClick}>
                {strings.Update_password}
              </Button>
            </LabelButtonDU>

            {!(passwordConfirmationError === "") ? (
              <ErrorMsg>{passwordConfirmationError}</ErrorMsg>
            ) : null}

            {/* SucessMsg */}
            {responseUpdatePassword.STATUS == "" ? (
              responseUpdatePassword.STATUS == "SUCCESS" ? (
                <SucessMsg>
                  {responseUpdatePassword.STATUS} :
                  {responseUpdatePassword.MESSAGE}
                </SucessMsg>
              ) : (
                <ErrorMsg>
                  {responseUpdatePassword.STATUS} :
                  {responseUpdatePassword.MESSAGE}
                </ErrorMsg>
              )
            ) : null}
          </Modal>
        ) : null}

        {/* Modal Popup for Delete Delete User */}
        {this.state.deleteUserModal == true ? (
          <Modal
            modalOpen={this.state.deleteUserModal}
            onclick={this.deleteUserHandlerClose}
            title={strings.Delete_current_user}
            size="sm"
          >
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                textTransform: "none"
              }}
            >
              {strings.Enter_admin_password}
            </h4>

            <LabelInputGroup>
              <input
                type="password"
                name="currAdminPassword"
                onChange={this.handleInputChange}
              />
            </LabelInputGroup>

            <ButtonWrapDelete>
              <Button classname="red" onclick={this.deleteCurrentUser}>
                {strings.Confirm}
              </Button>
            </ButtonWrapDelete>

            {/* Response Msg */}
            {responseDeleteUserAPI.STATUS == "" ? (
              responseDeleteUserAPI.STATUS == "SUCCESS" ? (
                <SucessMsg>
                  {responseDeleteUserAPI.STATUS} :
                  {responseDeleteUserAPI.MESSAGE}
                </SucessMsg>
              ) : (
                <ErrorMsg>
                  {responseDeleteUserAPI.STATUS} :
                  {responseDeleteUserAPI.MESSAGE}
                </ErrorMsg>
              )
            ) : null}

            {!(passwordConfirmationError === "") ? (
              <ErrorMsg>{passwordConfirmationError}</ErrorMsg>
            ) : null}

            <ErrorMsg>
              {serverErrorDelete.status} {serverErrorDelete.statusText}
            </ErrorMsg>
          </Modal>
        ) : null}
      </ProfileWrapper>
    );
  }
}

export default ListUsers;
