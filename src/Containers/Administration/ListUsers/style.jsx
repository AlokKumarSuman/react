import styled from "styled-components";
export const ProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .dropdown-heading {
    font-size: 14px;
  }
`;

export const ProfileHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1.5rem 2rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;

export const ProfileBody = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
`;
export const LabelButton = styled.div`
  width: 100% !important;
  float: none !important;
  padding-bottom: 2rem;
  padding-top: 2rem;
  div {
    margin: 0 auto;
    width: 420px;
  }
`;
export const Form = styled.div`
  width: 100%;
`;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;
export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.8rem;
    }
  }
`;

export const LabelButtonDU = styled.div`
  width: auto !important;
  display: flex;
  margin: 1.6rem auto 0;
  float: none !important;
  div {
    width: auto;
    margin: 0 auto;
    display: inline-block;
    min-width: 151px;
  }
`;
export const ButtonWrapDelete = styled.div`
  width: auto !important;
  display: flex;
  margin: 1.6rem auto 0;
  float: none !important;
  div {
    width: auto;
    margin: 0 auto;
    display: inline-block;
    min-width: 151px;
  }
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
export const SucessMsg = styled.div`
  display: flex;
  justify-content: center;
  color: green;
  font-size: 13px;
`;
