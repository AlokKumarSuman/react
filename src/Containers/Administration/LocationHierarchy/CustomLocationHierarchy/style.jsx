import styled from "styled-components";

export const MainWrapper = styled.div``;

export const AddLevel = styled.div`
  width: 120px;
  border: 1px solid #000;
  margin-top: 2rem;
  cursor: pointer;

  p {
    text-align: center;
    padding: 5px;
  }
`;
export const ButtonWrapDelete = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px auto 0 auto;
  width: 60%;
`;

export const BoxWrap = styled.div`
  display: flex;
  flex-direction: column;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  background: #ccc;
  padding: 5px;
  min-width: 10rem;
  max-width: 10rem;
  margin: 0px 5px;

  p {
    padding: 10px;
    background: #fff;
    color: #000;
    text-align: center;
    font-size: 12px;
    text-transform: uppercase;
  }
`;

export const ButtonWrap = styled.div`
  display: block;
  overflow: hidden;
  padding: 3px;
  border: solid 3px #41c3e3;
  border-top: none !important;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
  span {
    display: block;
    font-size: 18px;
    width: 16px;
    height: 16px;
    text-align: center;
    border-radius: 10px;
    color: #fff;
    line-height: 17px;
    cursor: pointer;
  }

  .add {
    background: #41c3e3;
    float: left;
  }

  .remove {
    background: #ff5e5f;
    float: right;
  }
`;

export const LevelWrap = styled.div`
  margin-top: 10px;
  display: flex;
  align-items: center;
`;

export const InputOuterWrap = styled.div`
  display: flex;
  flex-direction: column;

  input,
  select {
    margin-bottom: 16px;
  }
`;

export const InputInnerWrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  input,
  select {
    width: 48%;
  }
`;
export const AutoCompleteSugession = styled.div`
  display: block;
  width: 48%;
  position: relative;
  input {
    width: 100%;
  }
  ul {
    position: absolute;
    background: #fff;
    width: 100%;
    top: 32px;
    left: 0;
    border: solid 1px #c5c5c5;
  }
  li {
    padding: 3px 6px;
    border-bottom: solid 1px #c5c5c5;
  }
  li:last-child {
    border-bottom: none;
  }
`;

export const HelpBlock = styled.div`
  color: red;
  text-align: center;
  font-size: 13px;
  margin: 5px 0px;
`;

// export const initechNode = styled.div`
//   border: solid 3px red;
//   border-radius: 3px;
//   padding: 5px;
//   width: 150px;
//   display: inline-block;
// `;

// export const initechOrgChart = styled.div`
//   border-right: solid 3px red .nodeLineBorderTop {
//     border-top: solid 3px red;
//   }
// `;

// export const orgNodeChildGroup = styled.div`
//   border-right: solid 3px red;
// `;

// export const nodeGroupLineVerticalMiddle = styled.div`
//   border-right: solid 3px red;
// `;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;
