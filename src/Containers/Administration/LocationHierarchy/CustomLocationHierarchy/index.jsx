import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../Localization/index";
import OrgChart from "react-orgchart";
import "react-orgchart/index.css";
import Button from "../../../../Components/Button";
import Modal from "../../../../Components/Modal";
import Icon from "../../../../Components/Icons/index.jsx";
import { Auth } from "../../../../Auth";
import {
  fetchSiblings,
  updateLocation,
  addLocation,
  deleteLocation,
  customHierarchy,
  fetchLocationUsers,
  fetchunassignedlocations,
  userService
} from "../../../Config";

import {
  MainWrapper,
  ButtonWrap,
  InputOuterWrap,
  InputInnerWrap,
  ButtonWrapDelete,
  AutoCompleteSugession,
  HelpBlock,
  ServerError
} from "./style.jsx";

class CustomLocationHierarchy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listLevels: this.props.listLevels,
      data: [],
      openLevelModal: false,
      openDeleteModal: false,
      editLevelModal: false,
      allParentId: [],
      currentNode: [],
      locationName: "",
      locationId: "",
      customfield1: "",
      customfield2: "",
      customfield3: "",
      customfield4: "",
      editLabel: "",
      editTitle: "",
      editParentId: "",
      lastNodeDisable: true,
      query: "",
      resultsAllLocations: [],
      showAutoComplete: false,
      submitted: false,
      deleteLocationModal: false,
      responseDeleteLocationStatusAPI: "",
      serverError: {}
    };
  }

  openLevelModal = (e, node) => {
    e.stopPropagation();
    e.preventDefault();
    let nodeAssign;
    if (node == undefined) {
      nodeAssign = this.state.currentNode;
    } else {
      nodeAssign = node;
    }

    this.setState(prevState => ({
      openLevelModal: !prevState.openLevelModal,
      currentNode: nodeAssign,
      locationname: "",
      locationId: "",
      showAutoComplete: false
    }));
  };

  //for Edit Work get All parent ID
  getParentID = (e, currentnode) => {
    const authBrandId = userService.authBrandId();

    const currentnodeN = currentnode;
    fetch(fetchSiblings, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        PARENT_ID: currentnodeN.PARENT_ID,
        BRAND_ID: authBrandId
      })
    })
      .then(response => response.json())
      .then(response => {
        this.setState({
          allParentId: response.RESULT
        });
      })
      .catch(err => console.log(err));
  };

  editLevelModal = (e, node) => {
    e.stopPropagation();
    e.preventDefault();
    // eslint-disable-next-line
    node = node;
    this.setState(prevState => ({
      editLevelModal: !prevState.editLevelModal
    }));

    if (!(node == undefined || node == null)) {
      this.getAllNode(node);
      this.getParentID(e, node);
    }
  };
  getAllNode = currentnode => {
    this.setState(prevState => ({
      currentNode: currentnode,
      editLabel: currentnode.LABEL,
      editTitle: currentnode.TITLE,
      editParentId: currentnode.PARENT_ID
    }));
  };
  //// Edit Hierarchy NODE ////
  updateHierachcy = (e, currentnode) => {
    const authUserName = userService.authUserName();

    const currentnodeV = currentnode;
    event.preventDefault();
    fetch(updateLocation, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        locations: [
          {
            NAME: this.state.editTitle,
            CUSTOM_ID: this.state.editTitle,
            CUSTOM1: null,
            CUSTOM2: null,
            CUSTOM3: null,
            CUSTOM4: null,
            LEVEL_ID: currentnode.LEVEL_ID,
            PARENT_ID: this.state.editParentId,
            USERID: authUserName,
            ID: currentnodeV.ID
          }
        ]
      })
    })
      .then(response => response.json())
      .then(this.editLevelModal(e))
      .then(this.refreshRootNode())
      .catch(err => console.log(err));
  };

  closeDeleteModal = e => {
    e.stopPropagation();
    e.preventDefault();
    this.setState(prevState => ({
      deleteLocationModal: false,
      serverError: {},
      responseDeleteLocationStatusAPI: ""
    }));
  };

  deleteLocationModal = (e, node) => {
    e.stopPropagation();
    e.preventDefault();
    // eslint-disable-next-line
    node = node;
    !(node.LEVEL_ID == 0 || node.ID == 0)
      ? this.setState(prevState => ({
          deleteLocationModal: !prevState.deleteLocationModal,
          currentNode: node,
          serverError: {},
          responseDeleteLocationStatusAPI: ""
        }))
      : null;
  };

  onChangeHandle = evt => {
    this.setState({
      [evt.target.name]: evt.target.value
    });
  };

  clearText = () => {
    this.setState({
      locationname: "",
      locationid: "",
      customfield1: "",
      customfield2: "",
      customfield3: "",
      customfield4: ""
    });
  };

  refreshRootNode = () => {
    const data = [];
    this.setState({
      data: data
    });
  };
  //// ADD NODE ////
  addNewHierachcy = (e, currentnode) => {
    e.preventDefault();
    const currentnodeValue = currentnode;

    this.setState({ submitted: true });
    const { locationname, locationId } = this.state;

    // stop here if form is invalid
    if (!(locationname && locationId)) {
      return;
    }
    if (currentnode.location_id === 0) {
      var currlevelID = this.props.listLevels[0].LOCATION_ID;
    } else {
      var currlevelID = this.props.listLevels.find(
        x => x.LOCATION_ID === currentnode.NEXT_LEVEL
      ).LOCATION_ID;
    }

    const authUserName = userService.authUserName();
    // // debugger;
    fetch(addLocation, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        NAME: this.search.value,
        CUSTOM_ID: this.state.locationId,
        CUSTOM1: "",
        CUSTOM2: "",
        CUSTOM3: "",
        CUSTOM4: "",
        LEVEL_ID: currlevelID,
        PARENT_ID: currentnodeValue.ID,
        USERID: authUserName
      })
    })
      .then(function(response) {
        if (response.ok) {
          return response;
        }
        throw Error(response.statusText);
      })
      .then(function(response) {
        return response.json();
      })
      .then(this.openLevelModal(e))
      .then(this.refreshHierarchy(e, currentnodeValue))
      .then(json => {
        if (json.STATUS == "SUCCESS") {
          this.clearText();
        }
      })
      .catch(err => console.log(err));
  };

  //// Delete Node ////
  deleteNodeHierachcy = (e, currentnode) => {
    const currentnodeValue = currentnode;
    const cnParentId = currentnodeValue.PARENT_ID;
    const cnPrevLevel = currentnodeValue.PARENT_LEVEL;
    event.preventDefault();
    fetch(deleteLocation, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        ID: currentnode.ID
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        if (res.RESULT == "SUCCESS") {
          this.setState({
            responseDeleteLocationStatusAPI: res
          });

          this.refreshRootNode();
          setTimeout(() => {
            this.closeDeleteModal(e);
          }, 1500);
        } else {
          this.setState({
            responseDeleteLocationStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
    // .then(response => response.json())
    // .then(this.closeDeleteModal(e))
    // .then(res => this.refreshRootNode())
    // .catch(err => console.log(err));
  };

  objloop = (res, parent, node) => {
    // // debugger;
    const dataItem = res.map(item => {
      return item;
    });
    if (!(res[0] == undefined)) {
      if (res[0].PARENT_ID === 0) {
        this.setState({
          data: dataItem
        });
      } else {
        if (this.state.data.length > 0) {
          node.children = [];
        }

        if ("children" in node) {
          node.children = res;
        }
      }
    }
  };

  refreshHierarchy = (e, node) => {
    e.stopPropagation();
    e.preventDefault();
    const authBrandId = userService.authBrandId();

    const nodeid = node.ID;
    var nodeNextLevel;

    if (node.NEXT_LEVEL == 1) {
      nodeNextLevel = this.state.listLevels[0].LOCATION_ID;
    } else {
      nodeNextLevel = node.NEXT_LEVEL;
    }

    fetch(customHierarchy, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        brandid: authBrandId,
        parent: nodeid,
        level: nodeNextLevel
      })
    })
      .then(res => res.json())
      .then(res => res.RESULT)
      .then(res => this.objloop(res, nodeid, node))
      .then(res => {
        this.setState({
          parent: nodeid,
          level: nodeNextLevel
        });
      })
      .catch(err => console.log(err));
  };

  handleInputChange = (e, currNode) => {
    // // debugger;
    // e.stopPropagation();
    // e.preventDefault();
    const crNode = currNode;
    this.setState(
      {
        query: this.search.value,
        locationname: this.search.value,
        showAutoComplete: true
      },
      () => {
        const setBlank = [];
        if (this.state.query.length == 1) {
          this.setState({
            resultsAllLocations: setBlank
          });
        }
        if (this.state.query && this.state.query.length > 1) {
          this.fetchUsersByLocation(crNode);
        }
      }
    );
  };

  //
  // Fetch Auto locations of Users
  fetchUsersByLocation = crNode => {
    const authBrandId = userService.authBrandId();

    let locationUrlCheck =
      this.state.listLevels.slice(-2)[0].LOCATION_ID == crNode.LEVEL_ID;
    const locationURL = locationUrlCheck
      ? fetchLocationUsers
      : fetchunassignedlocations;
    fetch(locationURL, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        NAME: this.state.query,
        BRAND_ID: authBrandId
      })
    })
      .then(response => response.json())
      .then(response =>
        this.setState({
          resultsAllLocations: response.RESULT
        })
      )
      .catch(err => console.log(err));
  };

  getAutoCompleteValue = (e, currValue) => {
    this.setState({
      locationname: currValue,
      showAutoComplete: false
    });
  };

  handleInputFocus = () => {
    this.setState({ showAutoComplete: false });
  };

  componentWillMount() {}

  render() {
    const {
      submitted,
      locationname,
      locationId,
      serverError,
      responseDeleteLocationStatusAPI
    } = this.state;
    const currentnode = this.state.currentNode;
    const allParentId = this.state.allParentId;

    var rootTitle = [];
    let modalTitle = [];

    this.state.currentNode !== ""
      ? this.state.currentNode.NEXT_LEVEL == 1
        ? (rootTitle = this.state.listLevels[0].LEVEL_NAME)
        : (rootTitle = this.state.listLevels.find(
            z => z.LOCATION_ID === this.state.currentNode.NEXT_LEVEL
          ))
      : null;
    if (typeof rootTitle === "string") {
      modalTitle = rootTitle;
    } else if (typeof rootTitle === "object") {
      modalTitle = rootTitle.LEVEL_NAME;
    }

    const initechOrg = {
      ID: "0",
      LABEL: "Root",
      TITLE: "Root",
      // eslint-disable-next-line
      location_id: 0,
      LEVEL_ID: "0",
      NEXT_LEVEL: "1",
      PARENT_ID: "0",
      children: this.state.data
    };

    const MyNodeComponent = ({ node }) => {
      return (
        <div
          className="initechNode"
          onClick={e => this.refreshHierarchy(e, node)}
          nodeid={node.ID}
        >
          <span>
            <div>{node.LABEL}</div>
            <div>{node.TITLE}</div>

            {!(node.LEVEL_ID == 0) ? (
              <span
                className="edit"
                onClick={e => this.editLevelModal(e, node)}
              >
                ...
              </span>
            ) : null}
            {!(node.NEXT_LEVEL == -2) ? (
              <span className="drop-arrow">
                <Icon name="arrowDown" />
              </span>
            ) : null}
          </span>

          <ButtonWrap>
            {!(node.NEXT_LEVEL == -2) ? (
              <span className="add" onClick={e => this.openLevelModal(e, node)}>
                +
              </span>
            ) : null}
            {!(node.LEVEL_ID == 0) ? (
              <span
                className="remove"
                onClick={e => this.deleteLocationModal(e, node)}
              >
                -
              </span>
            ) : null}
          </ButtonWrap>
        </div>
      );
    };

    return (
      <React.Fragment>
        <MainWrapper>
          <OrgChart tree={initechOrg} NodeComponent={MyNodeComponent} />
        </MainWrapper>

        {/* Add New Hierarchy POPUP */}
        {this.state.openLevelModal === true ? (
          <Modal
            modalOpen={this.openLevelModal}
            onclick={this.openLevelModal}
            title={"ADD " + modalTitle + " TYPE"}
            size="lg"
          >
            <InputOuterWrap onClick={this.handleInputFocus}>
              <form
                name="form"
                onSubmit={e => this.addNewHierachcy(e, currentnode)}
              >
                <InputInnerWrap>
                  <label>{strings.Location_name}</label>
                  {/* <input
                  type="text"
                  name="locationName"
                  value={this.state.label}
                  placeholder="Location Name"
                  onChange={this.onChangeHandle}
                /> */}
                  <AutoCompleteSugession>
                    <input
                      name="locationName"
                      value={this.state.locationname}
                      placeholder={strings.Location_name}
                      ref={input => (this.search = input)}
                      onChange={e => this.handleInputChange(e, currentnode)}
                      autoComplete="off"
                    />
                    {this.state.showAutoComplete ? (
                      <ul>
                        {this.state.resultsAllLocations.map((list, index) => (
                          <li
                            key={list.VALUE}
                            value={list.VALUE}
                            onClick={e =>
                              this.getAutoCompleteValue(e, list.VALUE)
                            }
                          >
                            {list.VALUE}
                          </li>
                        ))}
                      </ul>
                    ) : null}
                  </AutoCompleteSugession>
                </InputInnerWrap>

                <InputInnerWrap>
                  <label>{strings.Location_Id}</label>
                  <input
                    type="text"
                    name="locationId"
                    value={this.state.title}
                    placeholder={strings.Location_Id}
                    onChange={this.onChangeHandle}
                    autoComplete="off"
                  />
                </InputInnerWrap>
                <InputInnerWrap>
                  <label>{strings.Custom_field}</label>
                  <input
                    type="text"
                    name="customfield1"
                    value={this.state.title}
                    placeholder={strings.Custom_field}
                    onChange={this.onChangeHandle}
                    autoComplete="off"
                    disabled
                  />
                </InputInnerWrap>
                <InputInnerWrap>
                  <label>{strings.Custom_field}</label>
                  <input
                    type="text"
                    name="customfield2"
                    value={this.state.title}
                    placeholder={strings.Custom_field}
                    onChange={this.onChangeHandle}
                    autoComplete="off"
                    disabled
                  />
                </InputInnerWrap>
                <InputInnerWrap>
                  <label>{strings.Custom_field}</label>
                  <input
                    type="text"
                    name="customfield3"
                    value={this.state.title}
                    placeholder={strings.Custom_field}
                    onChange={this.onChangeHandle}
                    autoComplete="off"
                    disabled
                  />
                </InputInnerWrap>
                <InputInnerWrap>
                  <label>{strings.Custom_field}</label>
                  <input
                    type="text"
                    name="customfield4"
                    value={this.state.title}
                    placeholder={strings.Custom_field}
                    onChange={this.onChangeHandle}
                    autoComplete="off"
                    disabled
                  />
                </InputInnerWrap>

                <Button
                  size="fullwidth"
                  onclick={e => this.addNewHierachcy(e, currentnode)}
                >
                  {strings.Add}
                </Button>
                {submitted && !locationname && (
                  <HelpBlock>{strings.Location_name_is_required}</HelpBlock>
                )}
                {submitted && !locationId && (
                  <HelpBlock>{strings.Location_id_is_required}</HelpBlock>
                )}
              </form>
            </InputOuterWrap>
          </Modal>
        ) : null}
        {/* Delete Locations */}
        {this.state.deleteLocationModal === true ? (
          <Modal
            modalOpen={this.state.deleteLocationModal}
            onclick={this.closeDeleteModal}
            title={strings.Delete_hierarchy_location}
            size="sm"
          >
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                textTransform: "none"
              }}
            >
              {strings.Are_you_sure_you_want_to_delete_this_locaton}
            </h4>
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
            <ButtonWrapDelete>
              <Button
                classname="red"
                onclick={e => this.deleteNodeHierachcy(e, currentnode)}
              >
                {strings.Yes}
              </Button>
              <Button classname="blue" onclick={this.closeDeleteModal}>
                {strings.No}
              </Button>
            </ButtonWrapDelete>
            {!(
              responseDeleteLocationStatusAPI == "undefined" ||
              responseDeleteLocationStatusAPI == ""
            ) ? (
              <h4
                style={{
                  fontWeight: "500",
                  textAlign: "center",
                  fontSize: "13px",
                  color: "red",
                  paddingTop: "10px"
                }}
              >
                {responseDeleteLocationStatusAPI.STATUS +
                  ": " +
                  responseDeleteLocationStatusAPI.MESSAGE}
              </h4>
            ) : null}
          </Modal>
        ) : null}

        {/* Edit Hierarchy POPUP */}
        {this.state.editLevelModal === true ? (
          <Modal
            modalOpen={this.editLevelModal}
            onclick={this.editLevelModal}
            title={strings.Edit_hierarchy}
            size="lg"
          >
            <InputOuterWrap>
              {allParentId.length !== 0 ? (
                <InputInnerWrap>
                  <label>{strings.Parent_label}</label>
                  <select
                    name="editParentId"
                    value={this.state.editParentId}
                    onChange={this.onChangeHandle}
                  >
                    {allParentId.map((list, index) => (
                      <option key={list.KEY} value={list.KEY}>
                        {list.VALUE}
                      </option>
                    ))}
                  </select>
                </InputInnerWrap>
              ) : null}

              <InputInnerWrap>
                <label>{strings.Label_name}</label>
                <input
                  type="text"
                  name="editLabel"
                  value={this.state.editLabel}
                  placeholder={strings.Custom_field}
                  onChange={this.onChangeHandle}
                  disabled
                />
              </InputInnerWrap>
              <InputInnerWrap>
                <label>{strings.Location_name}</label>
                <input
                  type="text"
                  name="editTitle"
                  value={this.state.editTitle}
                  placeholder="Location ID"
                  onChange={this.onChangeHandle}
                  autoComplete="off"
                />
              </InputInnerWrap>

              <Button
                size="fullwidth"
                onclick={e => this.updateHierachcy(e, currentnode)}
              >
                Update
              </Button>
            </InputOuterWrap>
          </Modal>
        ) : null}
      </React.Fragment>
    );
  }
}

CustomLocationHierarchy.propTypes = {
  listLevels: PropTypes.any
};

export default CustomLocationHierarchy;
