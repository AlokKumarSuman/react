import React, { Component } from "react";
import styled from "styled-components";
import Spinner from "../../../../Components/Spinner/index";
import OrgChart from "react-orgchart";
import "react-orgchart/index.css";

import { fetchDefaultHierarchy } from "../../../Config";
import { Auth } from "../../../../Auth";

const DefaultSalesWraper = styled.div`
  position: relative;
`;
const SpinnerWraper = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;

class DefaultLocationHierarchy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productPost: [],
      spinner: true
    };
  }

  componentDidMount() {
    fetch(fetchDefaultHierarchy, {
      method: "GET",
      headers: Auth
    })
      .then(response => response.json())
      .then(response => {
        this.setState({
          productPost: response.RESULT,
          spinner: false
        });
      })
      .catch(err => console.log(err));
  }

  render() {
    const initechOrg = this.state.productPost;
    const MyNodeComponent = ({ node }) => {
      return (
        <div className="initechNodeDefault">
          <div>
            <span>{node.name}</span>
            <span>{node.title}</span>
          </div>
        </div>
      );
    };
    return (
      <DefaultSalesWraper>
        {this.state.spinner == true ? (
          <SpinnerWraper>
            <Spinner />
          </SpinnerWraper>
        ) : (
          <OrgChart tree={initechOrg} NodeComponent={MyNodeComponent} />
        )}
      </DefaultSalesWraper>
    );
  }
}

export default DefaultLocationHierarchy;
