import styled from "styled-components";

export const MainWrapper = styled.div`
  height: 300px;
`;

export const HierarchyWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
`;

export const HierarchyHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1.5rem 2rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;

export const HierarchyBody = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem 4rem;
  background-color: white;
  position: relative;
  overflow-x: auto;
  flex-wrap: wrap;
`;

export const RadioLabelWrapMain = styled.div`
  display: flex;
  flex-direction: row;
  width: 60rem;
  justify-content: space-between;
  align-items: left;
`;

export const RadioLabelWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const ViewButton = styled.div`
  margin-top: 20px;
  > div {
    width: 180px;
  }
`;
export const InputBoxInner = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 10px;

  label {
    width: 20rem;
  }

  .red {
    margin: 0px 5px;
  }
`;
export const IconGroup = styled.div`
  display: flex;
  align-items: left;
  justify-content: space-around;
  width: 100px;
  position: relative;

  .icon {
    position: static;
  }
`;

export const LevelQuiz = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  margin: 3rem 0;
  input[type="number"] {
    width: 12rem;
    margin: 0px 10px;

    ::-webkit-inner-spin-button {
      -webkit-appearance: none;
    }
  }
`;
export const ButtonWrapDelete = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px auto 0 auto;
  width: 60%;
`;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;
