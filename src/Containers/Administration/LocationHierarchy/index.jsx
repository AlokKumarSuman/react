import React, { Component } from "react";
import { strings } from "./../../../Localization/index";
import CustomLocationHierarchy from "./CustomLocationHierarchy";
import DefaultLocationHierarchy from "./DefaultLocationHierarchy";
import InputRange from "./InputRange";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";
import Modal from "../../../Components/Modal";
import {
  brandID,
  listLevels,
  addLevel,
  deleteCustomerLevel,
  updateLevel,
  userService
} from "../../Config";
import { Auth } from "../../../Auth";

import {
  HierarchyWrapper,
  HierarchyHeader,
  HierarchyBody,
  RadioLabelWrap,
  RadioLabelWrapMain,
  ViewButton,
  InputBoxInner,
  IconGroup,
  LevelQuiz,
  ButtonWrapDelete,
  ServerError
} from "./style.jsx";

class LocationHierarchy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedValue: "DefaultSalesHierarchy",
      listLevels: [],
      openViewLevelModal: false,
      totalNoOfLevelCount: "1",
      userGTInput: false,
      openAddLevelModal: false,
      levelInputItems: [],
      inputItems: [],
      addLevelError: false,
      addLevelErrorSameLevelName: false,
      levelname1: "",
      levelname2: "",
      levelname3: "",
      levelname4: "",
      levelname5: "",
      levelname6: "",
      levelname7: "",
      levelname8: "",
      levelname9: "",
      levelname10: "",
      responseAddStatusAPI: "",
      responseUpdateStatusAPI: "",
      deleteLevelModal: false,
      deleteCrrNode: {},
      serverError: {},
      responseDeleteLevelStatusAPI: ""
    };
  }

  onChangeHandlerAdd = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      addLevelError: false,
      addLevelErrorSameLevelName: false,
      addLevelError: false
    });
    this.updateInputsItems();
  };

  updateInputsItems = () => {
    const crrValue = [];
    const {
      levelname1,
      levelname2,
      levelname3,
      levelname4,
      levelname5,
      levelname6,
      levelname7,
      levelname8,
      levelname9,
      levelname10
    } = this.state;

    crrValue.push(
      levelname1,
      levelname2,
      levelname3,
      levelname4,
      levelname5,
      levelname6,
      levelname7,
      levelname8,
      levelname9,
      levelname10
    );

    this.setState({
      inputItems: crrValue
    });
  };

  onChangeHandler = (evt, indexPosition) => {
    this.state.listLevels[indexPosition].LEVEL_NAME = evt.target.value;
    this.setState({
      addLevelError: false,
      addLevelErrorSameLevelName: false,
      addLevelError: false
    });
    this.forceUpdate();
  };
  customSalesHandler = event => {
    this.setState({
      checkedValue: "CustomSalesHierarchy"
    });
  };
  defaultSalesHandler = event => {
    this.setState({
      checkedValue: "DefaultSalesHierarchy"
    });
  };

  componentDidMount() {
    this.fetchLevels();
  }

  fetchLevels = () => {
    const authBrandId = userService.authBrandId();
    const authCompanyId = userService.authCompanyId();
    const authUserName = userService.authUserName();

    fetch(listLevels, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        COMPANY_ID: authCompanyId,
        USER_ID: authUserName
      })
    })
      .then(response => response.json())
      .then(response =>
        response.RESULT.length == 0
          ? this.setState({
              // listLevels: "",
              checkedValue: "DefaultSalesHierarchy"
            })
          : this.setState({
              listLevels: response.RESULT,
              checkedValue: "CustomSalesHierarchy"
            })
      )
      .catch(err => console.log(err));
  };

  fetchLevelsaaa = () => {
    const authBrandId = userService.authBrandId();
    const authCompanyId = userService.authCompanyId();
    const authUserName = userService.authUserName();

    fetch(listLevels, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        COMPANY_ID: authCompanyId,
        USER_ID: authUserName
      })
    })
      .then(response => response.json())
      // .then(response => console.log(response))
      .then(response =>
        this.setState({
          listLevels: response.RESULT,
          checkedValue: "CustomSalesHierarchy"
        })
      )
      .catch(err => console.log(err));
  };
  //Add levels
  // get total number of levels on change
  totalNoOfLevelCount = e => {
    this.setState({
      totalNoOfLevelCount: e.target.value
    });
  };
  // Add New lable popup
  openAddLevelModal = e => {
    if (
      this.state.totalNoOfLevelCount < 1 ||
      this.state.totalNoOfLevelCount > 10
    ) {
      return this.setState({ userGTInput: true });
    }
    this.setState(prevState => ({
      openAddLevelModal: !prevState.openAddLevelModal
    }));
    this.createInputLevels();
  };

  closeAddLevelModal = () => {
    /* for popup close */
    setTimeout(() => {
      this.setState(prevState => ({
        openAddLevelModal: false,
        responseAddStatusAPI: ""
      }));
    }, 3000);
  };

  // Add New Lables on Popup // create input fields
  createInputLevels = () => {
    const noOflevelCreate = this.state.totalNoOfLevelCount;
    const levelInputItems = [];
    for (var i = 0; i < noOflevelCreate; i++) {
      const levelname = "levelname" + (i + 1);
      levelInputItems.push(
        <InputBoxInner>
          <label>
            {strings.Level_name} {i + 1}
          </label>
          <input
            type="text"
            name={levelname}
            placeholder={strings.Level_name}
            onChange={e => this.onChangeHandlerAdd(e)}
            autoComplete="off"
          />
        </InputBoxInner>
      );
    }
    // var arr = new Array(5);
    // arr.fill("");
    this.setState({
      levelInputItems: levelInputItems
    });
  };

  addBodyObjCreate = (e, inputItems) => {
    const getValue = [];
    const {
      levelname1,
      levelname2,
      levelname3,
      levelname4,
      levelname5,
      levelname6,
      levelname7,
      levelname8,
      levelname9,
      levelname10
    } = this.state;

    getValue.push(
      levelname1,
      levelname2,
      levelname3,
      levelname4,
      levelname5,
      levelname6,
      levelname7,
      levelname8,
      levelname9,
      levelname10
    );

    this.setState({
      inputItems: getValue
    });

    setTimeout(() => {
      this.saveLevels();
    }, 1000);
  };
  // Save New Levels
  saveLevels = () => {
    // debugger;
    const authBrandId = userService.authBrandId();

    const setListLevels = [];
    const namedata = this.state.inputItems;
    const temp = this.state.levelInputItems.length;

    for (var i = 0; i < temp; i++) {
      if (namedata[i] == "") {
        this.setState({
          addLevelError: true,
          inputItems: []
        });
        return;
      } else {
        const order = i + 1;
        setListLevels.push({
          BRAND_ID: authBrandId,
          NAME: namedata[i],
          ORDER: order
        });
      }
    }

    var valueArr = setListLevels.map(function(item) {
      return item.NAME;
    });
    var isDuplicate = valueArr.some(function(item, idx) {
      return valueArr.indexOf(item) != idx;
    });

    if (isDuplicate) {
      this.setState({
        addLevelErrorSameLevelName: true
      });
      return;
    }

    const getAllObj = setListLevels;
    // const postLevelData = [];
    // for (var i = 0; i < getAllObj.length; i++) {
    //   postLevelData.push({
    //     //
    //     // BRAND_ID: getAllObj[i].BRAND_ID,
    //     BRAND_ID: getAllObj[i].BRAND_ID,
    //     NAME: getAllObj[i].NAME,
    //     ORDER: getAllObj[i].ORDER
    //   });
    // }
    // let blankArray = [];
    fetch(addLevel, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        LEVELS: setListLevels
      })
    })
      .then(function(response) {
        // Shorthand to check for an HTTP 2xx response status.
        // See https://fetch.spec.whatwg.org/#dom-response-ok
        if (response.ok) {
          return response;
        }
        // Raise an exception to reject the promise and trigger the outer .catch() handler.
        // By default, an error response status (4xx, 5xx) does NOT cause the promise to reject!
        throw Error(response.statusText);
      })
      .then(function(response) {
        return response.json();
      })
      // .then(response => console.log(response))
      .then(json => {
        // console.log("Request succeeded with JSON response:", json);
        if (json.STATUS == "SUCCESS") {
          this.closeAddLevelModal();

          this.setState({
            responseAddStatusAPI: json,
            listLevels: [],
            inputItems: []
          });
        }
      })
      .catch(err => console.log(err));

    this.state.responseAddStatusAPI == ""
      ? setTimeout(() => {
          this.fetchLevelsaaa();
        }, 3000)
      : null;
  };

  // View/Edit lable popup
  openViewLevelModal = e => {
    e.stopPropagation();
    e.preventDefault();
    this.setState(prevState => ({
      openViewLevelModal: !prevState.openViewLevelModal,
      deleteLevelModal: false,
      responseUpdateStatusAPI: ""
    }));
  };
  // View/Edit lable popup
  closeViewLevelModal = e => {
    e.stopPropagation();
    e.preventDefault();
    setTimeout(() => {
      this.setState({
        openViewLevelModal: false,
        deleteLevelModal: false,
        responseUpdateStatusAPI: "",
        addLevelErrorSameLevelName: false
      });
    }, 3000);
  };

  // Delete Level Modal
  deleteLevelModal = (e, item) => {
    e.stopPropagation();
    e.preventDefault();
    this.setState(prevState => ({
      deleteLevelModal: !prevState.deleteLevelModal,
      deleteCrrNode: item,
      serverError: {},
      responseDeleteLevelStatusAPI: ""
    }));
  };
  deleteLevelModalClose = e => {
    e.stopPropagation();
    e.preventDefault();
    this.setState({
      deleteLevelModal: false,
      serverError: {},
      responseDeleteLevelStatusAPI: ""
    });
  };

  // Delete Level
  deleteCurrentLevel = () => {
    const deleteLevelKey = this.state.deleteCrrNode;

    fetch(deleteCustomerLevel, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        levelsID: deleteLevelKey.LOCATION_ID
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        console.log(res.response);
        if (res.RESULT == "SUCCESS") {
          // remove current item from table
          const data = this.state.listLevels.filter(
            i => i.LOCATION_ID !== deleteLevelKey.LOCATION_ID
          );
          this.setState({
            listLevels: data,
            totalNoOfLevelCount: "1",
            responseDeleteLevelStatusAPI: res
          });

          setTimeout(() => {
            this.setState({
              deleteLevelModal: false
            });
          }, 2000);
        } else {
          this.setState({
            responseDeleteLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );

    // .then(function(response) {
    //   return response.json();
    // })
    // .then(response => {
    //   if (response.STATUS == "SUCCESS") {
    //     // remove current item from table
    //     const data = this.state.listLevels.filter(
    //       i => i.LOCATION_ID !== deleteLevelKey.LOCATION_ID
    //     );
    //     this.setState({
    //       listLevels: data,
    //       totalNoOfLevelCount: "1"
    //     });
    //   }
    // })
    // .catch(err => console.log(err));
  };

  // Update Levels Handler
  updateLevelsHandler = () => {
    const listLevels = this.state.listLevels;
    const updateLevelData = [];
    for (var i = 0; i < listLevels.length; i++) {
      if (listLevels[i].LEVEL_NAME == "") {
        this.setState({
          addLevelError: true
        });
        return;
      } else {
        updateLevelData.push({
          ID: listLevels[i].LOCATION_ID,
          NAME: listLevels[i].LEVEL_NAME
        });
      }
    }

    var valueArr = updateLevelData.map(function(item) {
      return item.NAME.toUpperCase();
    });
    var isDuplicate = valueArr.some(function(item, idx) {
      return valueArr.indexOf(item) != idx;
    });

    if (isDuplicate) {
      this.setState({
        addLevelErrorSameLevelName: true
      });
      return;
    }

    fetch(updateLevel, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        LEVELS: updateLevelData
      })
    })
      .then(function(response) {
        return response.json();
      })
      .then(response => {
        // console.log("Request succeeded with response response:", response);
        if (response.STATUS == "SUCCESS") {
          this.setState({
            responseUpdateStatusAPI: response
          });
          setTimeout(() => {
            this.setState({
              openViewLevelModal: false
            });
          }, 1500);
        }
      })

      .catch(err => console.log(err));
  };

  componentWillUnmount() {
    this.fetchLevels();
  }

  render() {
    const {
      checkedStatus,
      checkedValue,
      listLevels,
      openViewLevelModal,
      userGTInput,
      openAddLevelModal,
      totalNoOfLevelCount,
      levelInputItems,
      inputItems,
      addLevelError,
      responseAddStatusAPI,
      responseUpdateStatusAPI,
      addLevelErrorSameLevelName,
      serverError,
      responseDeleteLevelStatusAPI
    } = this.state;

    const levelCheckedStatus =
      listLevels.length == 0 ? (
        checkedValue == "DefaultSalesHierarchy" ? (
          <>
            <RadioLabelWrapMain>
              <RadioLabelWrap>
                <input
                  type="radio"
                  name="manage"
                  value="DefaultSalesHierarchy"
                  id="dsh"
                  checked
                />
                <label for="dsh">{strings.Default_sales_hierarchy}</label>
              </RadioLabelWrap>
              <RadioLabelWrap>
                <input
                  type="radio"
                  name="manage"
                  value="CustomSalesHierarchy"
                  id="csh"
                  onClick={this.customSalesHandler}
                />
                <label for="csh">{strings.Custom_sales_hierarchy}</label>
              </RadioLabelWrap>
            </RadioLabelWrapMain>
          </>
        ) : (
          <>
            <RadioLabelWrapMain>
              <RadioLabelWrap>
                <input
                  type="radio"
                  name="manage"
                  value="DefaultSalesHierarchy"
                  id="dsh"
                  onClick={this.defaultSalesHandler}
                />
                <label for="dsh">{strings.Default_sales_hierarchy}</label>
              </RadioLabelWrap>
              <RadioLabelWrap>
                <input
                  type="radio"
                  name="manage"
                  value="CustomSalesHierarchy"
                  id="csh"
                  checked
                />
                <label for="csh">{strings.Custom_sales_hierarchy}</label>
              </RadioLabelWrap>
            </RadioLabelWrapMain>
          </>
        )
      ) : (
        <>
          <RadioLabelWrapMain>
            <RadioLabelWrap>
              <input
                type="radio"
                name="manage"
                value="DefaultSalesHierarchy"
                id="dsh"
                disabled
              />
              <label for="dsh">{strings.Default_sales_hierarchy}</label>
            </RadioLabelWrap>
            <RadioLabelWrap>
              <input
                type="radio"
                name="manage"
                value="CustomSalesHierarchy"
                id="csh"
                checked
              />
              <label for="csh">{strings.Custom_sales_hierarchy}</label>
            </RadioLabelWrap>
          </RadioLabelWrapMain>
        </>
      );

    const renderLevels =
      listLevels.length == 0 ? (
        checkedValue == "DefaultSalesHierarchy" ? (
          <DefaultLocationHierarchy />
        ) : (
          <LevelQuiz>
            <h4>{strings.How_many_level_you_have_to_add}</h4>
            <InputRange
              type="number"
              min="1"
              max="10"
              defaultValue="1"
              placeholder="No. of Levels"
              name="totalNoOfLevelCount"
              onchange={this.totalNoOfLevelCount}
              autoComplete="off"
            />
            <Button className="button" onclick={e => this.openAddLevelModal(e)}>
              {strings.Add}
            </Button>
            {userGTInput ? (
              <p
                style={{
                  color: "red",
                  marginLeft: "10px"
                }}
              >
                {strings.You_can_add_1_10_levels_only}
              </p>
            ) : null}
          </LevelQuiz>
        )
      ) : (
        <>
          {listLevels.length == 0 ? null : (
            <>
              <ViewButton>
                <Button className="button" onclick={this.openViewLevelModal}>
                  {strings.View_levels}
                </Button>
              </ViewButton>
              <CustomLocationHierarchy listLevels={listLevels} />
            </>
          )}
        </>
      );

    return (
      <HierarchyWrapper>
        <HierarchyHeader>
          {checkedValue == "DefaultSalesHierarchy"
            ? strings.Default_location_hierarchy_levels
            : strings.Custom_location_hierarchy_levels}
        </HierarchyHeader>

        <HierarchyBody>
          {levelCheckedStatus}

          {renderLevels}
        </HierarchyBody>
        {/* Add New Level Popup */}
        {openAddLevelModal ? (
          <Modal
            modalOpen={this.openAddLevelModal}
            onclick={this.openAddLevelModal}
            title={strings.Add_new_level_name}
            size="lg"
          >
            <div>
              {levelInputItems.map((item, index) => {
                return item;
              })}

              {/* Level filds should not be BLANK */}
              {addLevelError ? (
                <p
                  style={{
                    color: "red"
                  }}
                >
                  {strings.All_field_must_be_filled_out}
                </p>
              ) : null}
              {/* Level filds should not Equal */}
              {addLevelErrorSameLevelName ? (
                <p
                  style={{
                    color: "red"
                  }}
                >
                  {strings.Levels_name_should_not_equal}
                </p>
              ) : null}

              <Button
                classname="save"
                size="fullwidth"
                onclick={e => this.addBodyObjCreate(e, inputItems)}
              >
                {strings.Save_level}
              </Button>
              {!(
                responseAddStatusAPI == "undefined" ||
                responseAddStatusAPI == ""
              ) ? (
                <h4
                  style={{
                    fontWeight: "500",
                    textAlign: "center",
                    fontSize: "13px",
                    color: "green",
                    paddingTop: "10px"
                  }}
                >
                  {responseAddStatusAPI.STATUS +
                    ":" +
                    responseAddStatusAPI.MESSAGE}
                </h4>
              ) : null}
            </div>
          </Modal>
        ) : null}

        {/* View / Edit / Update Level popup */}
        {openViewLevelModal ? (
          <Modal
            modalOpen={this.openViewLevelModal}
            onclick={this.openViewLevelModal}
            title={strings.View_Edit_level_name}
            size="lg"
          >
            <div>
              {listLevels.map((item, index) => (
                <InputBoxInner key={item.LOCATION_ID}>
                  <label>
                    {strings.Level_name} {index + 1}
                  </label>
                  <input
                    type="text"
                    value={item.LEVEL_NAME}
                    name={"levelname" + (index + 1)}
                    onChange={e => this.onChangeHandler(e, index)}
                    autoComplete="off"
                  />
                  <IconGroup>
                    <Icon
                      name="trash"
                      // onclick={e => this.deleteCurrentLevel(e, item)}
                      onclick={e => this.deleteLevelModal(e, item)}
                    />
                    {/* <Icon
                      name="pen"
                      onclick={e => this.editIconHandler(e, item)}
                    /> */}
                  </IconGroup>
                </InputBoxInner>
              ))}
              {/* Level filds should not be BLANK */}
              {addLevelError ? (
                <p
                  style={{
                    color: "red"
                  }}
                >
                  {strings.All_field_must_be_filled_out}
                </p>
              ) : null}
              {/* Level filds should not Equal */}
              {addLevelErrorSameLevelName ? (
                <p
                  style={{
                    color: "red"
                  }}
                >
                  {strings.Levels_name_should_not_equal}
                </p>
              ) : null}
              <Button
                classname="save"
                size="fullwidth"
                onclick={this.updateLevelsHandler}
              >
                {strings.Update_level}
              </Button>
              {!(
                responseUpdateStatusAPI == "undefined" ||
                responseUpdateStatusAPI == ""
              ) ? (
                <h4
                  style={{
                    fontWeight: "500",
                    textAlign: "center",
                    fontSize: "13px",
                    color: "green",
                    paddingTop: "10px"
                  }}
                >
                  {responseUpdateStatusAPI.STATUS +
                    ":" +
                    responseUpdateStatusAPI.MESSAGE}
                </h4>
              ) : null}
            </div>
          </Modal>
        ) : null}

        {/* Delete Levels Modal */}
        {this.state.deleteLevelModal === true ? (
          <Modal
            modalOpen={this.state.deleteLevelModal}
            onclick={this.deleteLevelModalClose}
            title={strings.Delete_hierarchy}
            size="sm"
          >
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                textTransform: "none"
              }}
            >
              {strings.Are_you_sure_you_want_to_delete_this_level}
            </h4>

            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>

            <ButtonWrapDelete>
              <Button classname="red" onclick={this.deleteCurrentLevel}>
                {strings.Yes}
              </Button>
              <Button classname="blue" onclick={this.deleteLevelModalClose}>
                {strings.No}
              </Button>
            </ButtonWrapDelete>
            {!(
              responseDeleteLevelStatusAPI == "undefined" ||
              responseDeleteLevelStatusAPI == ""
            ) ? (
              <h4
                style={{
                  fontWeight: "500",
                  textAlign: "center",
                  fontSize: "13px",
                  color: "red",
                  paddingTop: "10px"
                }}
              >
                {responseDeleteLevelStatusAPI.STATUS +
                  ": " +
                  responseDeleteLevelStatusAPI.MESSAGE}
              </h4>
            ) : null}
          </Modal>
        ) : null}
      </HierarchyWrapper>
    );
  }
}

export default LocationHierarchy;
