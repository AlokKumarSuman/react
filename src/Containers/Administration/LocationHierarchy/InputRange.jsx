import React from "react";
import PropTypes from "prop-types";

const InputRange = props => {
  return (
    <input
      type={props.type}
      min={props.min}
      max={props.max}
      defaultValue={props.defaultValue}
      placeholder={props.placeholder}
      onChange={props.onchange}
    />
  );
};

InputRange.propTypes = {
  onchange: PropTypes.func,
  type: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  defaultValue: PropTypes.number,
  placeholder: PropTypes.string
};

export default InputRange;
