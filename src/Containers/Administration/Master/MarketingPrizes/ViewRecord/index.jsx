import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../../Localization/index";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // debugger;
    const { currentItem } = this.props;
    return (
      <table className="table record-view-table">
        <tbody>
          <tr>
            <td>
              <b>{strings.Prize_image} </b>
            </td>
            <td>: {currentItem.IMAGE_URL}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Brand_Name} </b>
            </td>
            <td>: {currentItem.BRAND_NAME}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Prize_name} </b>
            </td>
            <td>: {currentItem.NAME}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Prize_value}</b>
            </td>
            <td>: {currentItem.PRIZE_VALUE}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Prize_limit}</b>
            </td>
            <td>: {currentItem.PRIZE_LIMIT}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Currency_name}</b>
            </td>
            <td>
              :
              {currentItem.CURRENCY_NAME + " (" + currentItem.CURRENCY_ID + ")"}
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
