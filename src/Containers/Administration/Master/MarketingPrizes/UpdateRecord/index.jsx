import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../../Localization/index";
import Button from "../../../../../Components/Button";
import Modal from "../../../../../Components/Modal";
import { Auth } from "../../../../../Auth";
import UploadImage from "./UploadImage";
import {
  userService,
  currency,
  updateMarketingPrize
} from "../../../../Config";

import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ButtonImageNameWrap,
  NameButtonWrap,
  ErrorMsg
} from "./style";

class UpdateRecord extends Component {
  constructor(props) {
    super(props);
    // debugger;
    this.state = {
      prizeName: this.props.upd.NAME,
      prizeValue: this.props.upd.PRIZE_VALUE,
      prizeLimit: this.props.upd.PRIZE_LIMIT,
      currencyId: this.props.upd.CURRENCY_ID,
      currencyName: this.props.upd.CURRENCY_NAME,
      imageNameByServer: this.props.upd.IMAGE_URL,
      prizeID: this.props.upd.PRIZE_ID,
      imageName: "",
      upd: [],
      currencyList: [],
      validateAllFilds: "",
      prizeValueValidate: "",
      prizeLimitValidate: "",
      responseAddLevelStatusAPI: "",
      serverError: "",
      modalOpen: false
    };
  }

  openHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  closeHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  displayImageNameHandler = (displayImageName, imageNameByServer) => {
    this.setState({
      imageName: displayImageName,
      imageNameByServer
    });
  };

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      validateAllFilds: "",
      prizeValueValidate: "",
      prizeLimitValidate: "",
      responseAddLevelStatusAPI: ""
    });
  };

  currencyChange = e => {
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      currencyId: id,
      currencyName: e.target.value
    });
  };

  componentDidMount() {
    fetch(currency, {
      method: "GET",
      headers: Auth
    })
      .then(res => res.json())
      .then(res => {
        this.setState({
          currencyList: res.response.docs
        });
      })
      .catch(err => console.log(err));
  }

  editRecord = e => {
    e.preventDefault();
    const { upd } = this.props;
    // debugger;
    const {
      prizeName,
      prizeValue,
      prizeLimit,
      currencyId,
      currencyName,
      prizeID,
      imageName,
      imageNameByServer
    } = this.state;

    if (
      prizeName === "" ||
      prizeValue === "" ||
      prizeLimit === "" ||
      currencyId === "" ||
      currencyName === "" ||
      // imageName === "" ||
      imageNameByServer === ""
    ) {
      return this.setState({
        validateAllFilds: strings.All_field_must_be_filled_out
      });
    }

    const numberOnly = /^[0-9\b]+$/;
    if (!prizeValue.match(numberOnly)) {
      return this.setState({
        prizeValueValidate: strings.Prize_value_should_be_the_numeric_value
      });
    }
    if (!prizeLimit.match(numberOnly)) {
      return this.setState({
        prizeLimitValidate: strings.Prize_limit_should_be_the_numeric_value
      });
    }

    var editValue = {
      prizeName,
      prizeValue,
      prizeLimit,
      prizeID,
      currencyId,
      currencyName,
      imageNameByServer
    };

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch(updateMarketingPrize, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        PRIZE_NAME: prizeName,
        PRIZE_VALUE: prizeValue,
        CURRENCY_ID: currencyId,
        PRIZE_LIMIT: prizeLimit,
        ICON: imageNameByServer,
        PRIZE_ID: prizeID
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.updatePrize(editValue);
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    // debugger;
    const {
      prizeName,
      prizeValue,
      prizeLimit,
      currencyName,
      currencyList,
      imageName,
      validateAllFilds,
      prizeValueValidate,
      prizeLimitValidate,
      responseAddLevelStatusAPI
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.modalOpen}
          onclick={this.closeHandler}
          title={strings.Upload_Image}
        >
          <UploadImage
            onclickClose={this.closeHandler}
            displayImageName={this.displayImageNameHandler}
          />
        </Modal>
        <Form onSubmit={this.editRecord}>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Prize_name} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="prizeName"
                value={prizeName}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Prize_value} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="prizeValue"
                value={prizeValue}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Prize_limit} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="prizeLimit"
                value={prizeLimit}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
            <NameButtonWrap>
              <LabelInputGroup>
                <h5>
                  {strings.Currency_name} <span className="error">*</span>
                </h5>
                <select
                  name="currencyName"
                  onChange={e => this.currencyChange(e)}
                >
                  <option value="default">{currencyName}</option>
                  {currencyList.map((list, index) => (
                    //  eslint-disable-next-line
                    <option key={list.CURRENCY_CODE} id={list.CURRENCY_CODE}>
                      {list.CURRENCY_NAME}
                    </option>
                  ))}
                </select>
              </LabelInputGroup>
            </NameButtonWrap>
          </FormRow>

          <FormRow>
            <NameButtonWrap>
              <LabelInputGroup>
                <h5>{strings.Upload_Image}</h5>
                <ButtonImageNameWrap>
                  <Button classname="grey" onclick={this.openHandler}>
                    {strings.Browse}
                  </Button>
                  <p>{imageName}</p>
                </ButtonImageNameWrap>
              </LabelInputGroup>
            </NameButtonWrap>
          </FormRow>

          <FormRow>
            <LabelButtonDU>
              <Button onclick={this.editRecord}>{strings.Update_prize}</Button>
              <Button onclick={this.props.onclickClose}>
                {strings.Cancel}
              </Button>
            </LabelButtonDU>
          </FormRow>
        </Form>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        {!(prizeValueValidate === "") ? (
          <ErrorMsg>{prizeValueValidate}</ErrorMsg>
        ) : null}

        {!(prizeLimitValidate === "") ? (
          <ErrorMsg>{prizeLimitValidate}</ErrorMsg>
        ) : null}

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </h4>
        ) : null}
      </React.Fragment>
    );
  }
}

// AddRecord.propTypes = {
//   addRec: PropTypes.func,
//   onclickClose: PropTypes.func
// };

export default UpdateRecord;
