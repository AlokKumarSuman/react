import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../Localization/index";
import { Redirect } from "react-router-dom";
import Spinner from "../../../../Components/Spinner";
import Button from "../../../../Components/Button";
import Icon from "../../../../Components/Icons";
import Modal from "../../../../Components/Modal";
import NoDataFound from "../../../../Components/NoDataFound";
import UpdateRecord from "./UpdateRecord";
import AddLevels from "./AddRecord";
import { userService, fetchAvailablePrize } from "../../../Config";
import { Auth } from "../../../../Auth";
import ViewRecord from "./ViewRecord";

import {
  AuthLevelsCard,
  AuthLevelsHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  AuthLevelsBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError,
  ButtonWrapDelete
} from "./style";

class MarketingPrizes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      filterShow: false,
      searchByBrandName: "",
      searchByLevelName: "",
      crrLevelLists: [],
      currentItem: [],
      responseStatusAPI: "",
      sortIcon: false,
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      openViewHandler: false,
      addNewLevelsModal: false,
      openEditHandler: false,
      responseDeleteLevelStatusAPI: ""
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch(fetchAvailablePrize, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            crrLevelLists: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  filterHandler = () => {
    /* for filter toggle*/
    this.setState(prevState => ({
      filterShow: !prevState.filterShow
    }));
  };

  searchByBrandNameHandler = e => {
    this.setState({
      searchByBrandName: e.target.value
    });
  };

  searchByLevelNameHandler = e => {
    this.setState({
      searchByLevelName: e.target.value
    });
  };

  addNewLevelsModal = () => {
    this.setState({
      addNewLevelsModal: true
    });
  };

  openEditHandler = (e, currentItems) => {
    // debugger;
    this.setState({
      openEditHandler: true,
      currentItem: currentItems
    });
  };

  openViewHandler = (e, currentItems) => {
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler,
      currentItem: currentItems
    }));
  };

  closeViewHandler = () => {
    this.setState({
      openViewHandler: false,
      addNewLevelsModal: false,
      openEditHandler: false
      // currentItem: []
    });
  };

  // sortBy = () => {
  //   var obj = [...this.state.crrLevelLists];
  //   obj.reverse((a, b) => a.USER_ID - b.USER_ID);
  //   this.setState(prevState => ({
  //     crrLevelLists: obj,
  //     sortIcon: !prevState.sortIcon
  //   }));
  // };

  // add form data in table
  addNewLevelsHandler = formVal => {
    const authBrandId = userService.authBrandId();
    const authBrandName = userService.authBrandName();

    const { crrLevelLists } = this.state;

    var newData = {
      PRIZE_ID: "",
      BRAND_ID: authBrandId,
      BRAND_NAME: authBrandName,
      NAME: formVal.prizeName,
      PRIZE_VALUE: formVal.prizeValue,
      CURRENCY_ID: formVal.currencyId,
      CURRENCY_NAME: formVal.currencyName,
      PRIZE_LIMIT: formVal.prizeLimit,
      IMAGE_URL: formVal.imageNameByServer
    };
    this.setState({ crrLevelLists: crrLevelLists.concat([newData]), UPD: {} });
  };

  // add form data in table
  updatePrize = editValue => {
    // debugger;
    const authBrandId = userService.authBrandId();
    const authBrandName = userService.authBrandName();

    var currPushArray = {
      PRIZE_ID: editValue.prizeID,
      BRAND_ID: authBrandId,
      BRAND_NAME: authBrandName,
      NAME: editValue.prizeName,
      PRIZE_VALUE: editValue.prizeValue,
      CURRENCY_ID: editValue.currencyId,
      CURRENCY_NAME: editValue.currencyName,
      PRIZE_LIMIT: editValue.prizeLimit,
      IMAGE_URL: editValue.imageNameByServer
    };

    var obj = this.state.crrLevelLists;

    var index = obj.findIndex(e => e.PRIZE_ID == editValue.prizeID);
    obj[index] = currPushArray;
    this.setState({ crrLevelLists: obj, UPD: {} });
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }

    const {
      currentItem,
      spinnerConfigData,
      serverError,
      responseDeleteLevelStatusAPI,
      serverErrorDelete
    } = this.state;

    let dataItems = this.state.crrLevelLists;

    const searchByBrandName = this.state.searchByBrandName.trim().toLowerCase();
    const searchByLevelName = this.state.searchByLevelName.trim().toLowerCase();

    if (searchByBrandName.length > 0) {
      dataItems = this.state.crrLevelLists.filter(function(i) {
        return i.BRAND_NAME.toLowerCase().match(searchByBrandName);
      });
    }

    if (searchByLevelName.length > 0) {
      dataItems = this.state.crrLevelLists.filter(function(i) {
        return i.NAME.toLowerCase().match(searchByLevelName);
      });
    }

    return (
      <AuthLevelsCard>
        <AuthLevelsHeader>
          <WrapperLeft>{strings.Seller_Levels}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              <Button classname="green" onclick={this.addNewLevelsModal}>
                <Icon name="addCircle" color="white" /> {strings.Add_new_levels}
              </Button>
              <Button classname="blue" onclick={this.filterHandler}>
                <Icon name="filter" color="white" /> {strings.Filter}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </AuthLevelsHeader>

        <Modal
          modalOpen={this.state.addNewLevelsModal}
          onclick={this.closeViewHandler}
          title={strings.Add_new_prize}
        >
          <AddLevels
            onclickClose={this.closeViewHandler}
            addNewLevels={this.addNewLevelsHandler}
          />
        </Modal>

        {/* Modal Popup for View Levels */}
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Current_prize_details}
          size="md"
        >
          <ViewRecord currentItem={currentItem} />
        </Modal>

        <Modal
          modalOpen={this.state.openEditHandler}
          onclick={this.closeViewHandler}
          title={strings.Edit_prize}
        >
          <UpdateRecord
            onclickClose={this.closeViewHandler}
            upd={currentItem}
            updatePrize={this.updatePrize}
          />
        </Modal>

        <AuthLevelsBody toggleFilter={this.state.filterShow}>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="20%" align="left">
                  {strings.Prize_image}
                </th>
                <th width="20%" align="left">
                  {strings.Prize_name}
                </th>
                <th width="10%" align="center">
                  {strings.Prize_value}
                </th>
                <th width="18%">{strings.Prize_limit}</th>
                <th width="15%">{strings.Currency_name}</th>
                <th width="80px" style={{ minWidth: "80px" }}>
                  {strings.Action}
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input
                    type="text"
                    value={this.state.searchByBrandName}
                    onChange={this.searchByBrandNameHandler}
                    placeholder={strings.Search_by_name}
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchByLevelName}
                    onChange={this.searchByLevelNameHandler}
                    placeholder={strings.Search_by_type}
                    className="textbox"
                  />
                </td>
                <td />
                <td />
                <td />
                <td />
              </tr>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="6">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : dataItems.length == 0 ? (
                <tr>
                  <td colSpan="6">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                dataItems.map((item, index) => (
                  <tr>
                    <td width="10%">
                      <img src={item.IMAGE_URL} alt="image" />
                    </td>
                    <td width="30%">{item.NAME}</td>
                    <td width="8%" align="center">
                      {item.PRIZE_VALUE}
                    </td>
                    <td width="8%" align="center">
                      {item.PRIZE_LIMIT}
                    </td>
                    <td width="15%" align="center">
                      {item.CURRENCY_NAME}
                    </td>
                    <td align="center">
                      <ButtonGroup>
                        <Icon
                          name="pen"
                          onclick={e => this.openEditHandler(e, item)}
                        />
                        <Icon
                          name="eye"
                          onclick={e => this.openViewHandler(e, item)}
                        />
                      </ButtonGroup>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
          {this.state.serverError.statusText == "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
        </AuthLevelsBody>
      </AuthLevelsCard>
    );
  }
}

// ViewDealer.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default MarketingPrizes;
