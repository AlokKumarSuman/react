import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../Localization/index";
import { Redirect } from "react-router-dom";
import Spinner from "../../../../Components/Spinner";
import Button from "../../../../Components/Button";
import Icon from "../../../../Components/Icons";
import Modal from "../../../../Components/Modal";
import NoDataFound from "../../../../Components/NoDataFound";

import AddLevels from "./AddRecord";

import {
  deleteCustomerLevel,
  userService,
  searchCustomerLevel
} from "../../../Config";
import { Auth } from "../../../../Auth";

import ViewRecord from "./ViewRecord";
// import EditRecord from "./EditRecord";

// import clientImg from "../../../../Assets/Images/img_avatar.png";

import {
  AuthLevelsCard,
  AuthLevelsHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  AuthLevelsBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError,
  ButtonWrapDelete
} from "./style";

class SellerLevels extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      filterShow: false,
      searchByBrandName: "",
      searchByLevelName: "",
      crrLevelLists: [],
      openViewHandler: false,
      currentItem: [],
      responseStatusAPI: "",
      sortIcon: false,
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      addNewLevelsModal: false,
      openDeleteHandler: false,
      responseDeleteLevelStatusAPI: ""

      // openEditHandler: false
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(searchCustomerLevel, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            crrLevelLists: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  filterHandler = () => {
    /* for filter toggle*/
    this.setState(prevState => ({
      filterShow: !prevState.filterShow
    }));
  };

  searchByBrandNameHandler = e => {
    this.setState({
      searchByBrandName: e.target.value
    });
  };

  searchByLevelNameHandler = e => {
    this.setState({
      searchByLevelName: e.target.value
    });
  };

  addNewLevelsModal = () => {
    this.setState({
      addNewLevelsModal: true
    });
  };
  openEditHandler = (e, currentItems) => {
    this.setState({
      addNewLevelsModal: true,
      currentItem: currentItems
    });
  };

  openViewHandler = (e, currentItems) => {
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler,
      currentItem: currentItems
    }));
  };

  openDeleteHandler = (e, currentItems) => {
    this.setState(prevState => ({
      openDeleteHandler: !prevState.openDeleteHandler,
      currentItem: currentItems,
      responseDeleteLevelStatusAPI: ""
    }));
  };

  // openEditHandler = (e, currentItems) => {
  //   this.setState(prevState => ({
  //     openEditHandler: !prevState.openEditHandler,
  //     currentItem: currentItems
  //   }));
  // };

  closeViewHandler = () => {
    this.setState({
      openViewHandler: false,
      addNewLevelsModal: false,
      openDeleteHandler: false,
      currentItem: []
      // openEditHandler: false
    });
  };

  // sortBy = () => {
  //   var obj = [...this.state.crrLevelLists];
  //   obj.reverse((a, b) => a.USER_ID - b.USER_ID);
  //   this.setState(prevState => ({
  //     crrLevelLists: obj,
  //     sortIcon: !prevState.sortIcon
  //   }));
  // };

  // Delete current Level
  deleteCurrentLevel = () => {
    // debugger;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    const deleteLevelKey = this.state.currentItem;

    fetch(deleteCustomerLevel, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LEVEL_ID: deleteLevelKey.ID
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      .then(res => {
        console.log(res.response);
        if (res.STATUS == "SUCCESS") {
          // remove current item from table
          const data = this.state.crrLevelLists.filter(
            i => i.ID !== deleteLevelKey.ID
          );
          this.setState({
            crrLevelLists: data,
            responseDeleteLevelStatusAPI: res,
            currentItem: []
          });

          setTimeout(() => {
            this.setState({
              openDeleteHandler: false
            });
          }, 2000);
        } else {
          this.setState({
            responseDeleteLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorDelete: err
        })
      );
  };

  // add form data in table
  addNewLevelsHandler = formVal => {
    const authBrandId = userService.authBrandId();
    const authBrandName = userService.authBrandName();

    const { crrLevelLists } = this.state;

    var ctr = crrLevelLists.length + 1;
    var newData = {
      BRAND_ID: authBrandId,
      BRAND_NAME: authBrandName,
      CURRENCY_ID: formVal.addCurrencyType,
      CURRENCY_NAME: formVal.addCurrencyName,
      ID: ctr,
      NAME: formVal.addLevelName,
      POINTS_REQUIRED: formVal.addPointsReq,
      SALE_AMT_REQUIRED: formVal.addSaleReq,
      SALE_TERM: formVal.saleterm,
      ICON_IMAGE: formVal.imageNameByServer
    };
    this.setState({ crrLevelLists: crrLevelLists.concat([newData]), UPD: {} });
  };

  // add form data in table
  updateLevels = formVal => {
    const authBrandId = userService.authBrandId();
    const authBrandName = userService.authBrandName();

    var currPushArray = {
      BRAND_ID: authBrandId,
      BRAND_NAME: authBrandName,
      CURRENCY_ID: formVal.addCurrencyType,
      CURRENCY_NAME: formVal.addCurrencyName,
      ID: formVal.id,
      NAME: formVal.addLevelName,
      POINTS_REQUIRED: formVal.addPointsReq,
      SALE_AMT_REQUIRED: formVal.addSaleReq,
      SALE_TERM: formVal.saleterm,
      ICON_IMAGE: formVal.imageNameByServer
    };

    var obj = this.state.crrLevelLists;

    var index = obj.findIndex(e => e.ID == formVal.id);
    obj[index] = currPushArray;
    this.setState({ crrLevelLists: obj, UPD: {} });
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }

    const {
      currentItem,
      spinnerConfigData,
      serverError,
      responseDeleteLevelStatusAPI,
      serverErrorDelete
    } = this.state;

    let dataItems = this.state.crrLevelLists;

    const searchByBrandName = this.state.searchByBrandName.trim().toLowerCase();
    const searchByLevelName = this.state.searchByLevelName.trim().toLowerCase();

    if (searchByBrandName.length > 0) {
      dataItems = this.state.crrLevelLists.filter(function(i) {
        return i.BRAND_NAME.toLowerCase().match(searchByBrandName);
      });
    }

    if (searchByLevelName.length > 0) {
      dataItems = this.state.crrLevelLists.filter(function(i) {
        return i.NAME.toLowerCase().match(searchByLevelName);
      });
    }

    return (
      <AuthLevelsCard>
        <AuthLevelsHeader>
          <WrapperLeft>{strings.Seller_Levels}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              {/* <Button classname="blue" onclick={this.sortBy}>
                {this.state.sortIcon ? (
                  <Icon name="arrowUp" color="white" />
                ) : (
                  <Icon name="arrowDown" color="white" />
                )}
                Sort
              </Button> */}
              <Button classname="green" onclick={this.addNewLevelsModal}>
                <Icon name="addCircle" color="white" /> {strings.Add_new_levels}
              </Button>
              <Button classname="blue" onclick={this.filterHandler}>
                <Icon name="filter" color="white" /> {strings.Filter}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </AuthLevelsHeader>

        {/* Modal Popup for View Levels */}
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Current_level_details}
          size="md"
        >
          <ViewRecord currentItem={currentItem} />
        </Modal>

        {/* Modal Popup for Edit Levels */}
        {/* <Modal
          modalOpen={this.state.openEditHandler}
          onclick={this.closeViewHandler}
          title="Edit Current Level Details"
        >
          <EditRecord
            onclickClose={this.closeViewHandler}
            currentItem={currentItem}
          />
        </Modal> */}

        {/* Modal Popup for Add New Levels */}

        <Modal
          modalOpen={this.state.addNewLevelsModal}
          onclick={this.closeViewHandler}
          title={
            currentItem != ""
              ? strings.Update_levels
              : strings.Request_to_add_new_levels
          }
        >
          <AddLevels
            onclickClose={this.closeViewHandler}
            addNewLevels={this.addNewLevelsHandler}
            //
            upd={currentItem}
            updateLevels={this.updateLevels}
            // propUpd={this.propcessUpd}
          />
        </Modal>

        {/* Modal Popup for Delete current Levels */}
        <Modal
          modalOpen={this.state.openDeleteHandler}
          onclick={this.closeViewHandler}
          title={strings.Delete_Current_Level}
          size="sm"
        >
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              textTransform: "none"
            }}
          >
            {strings.Are_you_sure_you_want_to_delete_this_level}
          </h4>

          <ServerError>
            {serverErrorDelete.status} {serverErrorDelete.statusText}
          </ServerError>

          <ButtonWrapDelete>
            <Button classname="red" onclick={this.deleteCurrentLevel}>
              {strings.Yes}
            </Button>
            <Button classname="blue" onclick={this.closeViewHandler}>
              {strings.No}
            </Button>
          </ButtonWrapDelete>
          {!(
            responseDeleteLevelStatusAPI == "undefined" ||
            responseDeleteLevelStatusAPI == ""
          ) ? (
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                fontSize: "13px",
                color: "red",
                paddingTop: "10px"
              }}
            >
              {responseDeleteLevelStatusAPI.STATUS +
                ": " +
                responseDeleteLevelStatusAPI.MESSAGE}
            </h4>
          ) : null}
        </Modal>

        <AuthLevelsBody toggleFilter={this.state.filterShow}>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="20%" align="left">
                  {strings.Brand_Name}
                </th>
                <th width="20%" align="left">
                  {strings.Level_name}
                </th>
                <th width="10%" align="left">
                  {strings.Sale_term}
                </th>
                <th width="10%">{strings.Points_required}</th>
                <th width="15%">{strings.Sale_amount_required}</th>
                <th width="15%">{strings.Currency_type}</th>
                <th width="80px" style={{ minWidth: "80px" }}>
                  {strings.Action}
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input
                    type="text"
                    value={this.state.searchByBrandName}
                    onChange={this.searchByBrandNameHandler}
                    placeholder={strings.Search_by_name}
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchByLevelName}
                    onChange={this.searchByLevelNameHandler}
                    placeholder={strings.Search_by_type}
                    className="textbox"
                  />
                </td>
                <td />
                <td />
                <td />
                <td />
              </tr>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="6">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : dataItems.length == 0 ? (
                <tr>
                  <td colSpan="6">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                dataItems.map((item, index) => (
                  <tr>
                    <td>{item.BRAND_NAME}</td>
                    <td>{item.NAME}</td>
                    <td>{item.SALE_TERM}</td>
                    <td align="center">{item.POINTS_REQUIRED}</td>
                    <td align="center">{item.SALE_AMT_REQUIRED}</td>
                    <td align="center">{item.CURRENCY_NAME}</td>
                    <td align="center">
                      <ButtonGroup>
                        <Icon
                          name="pen"
                          onclick={e => this.openEditHandler(e, item)}
                        />
                        <Icon
                          name="trash"
                          onclick={e => this.openDeleteHandler(e, item)}
                        />
                        <Icon
                          name="eye"
                          onclick={e => this.openViewHandler(e, item)}
                        />
                      </ButtonGroup>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
          {this.state.serverError.statusText == "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
        </AuthLevelsBody>
      </AuthLevelsCard>
    );
  }
}

// ViewDealer.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default SellerLevels;
