import React, { Component } from "react";
import Button from "../../../../../../Components/Button";
import { Auth } from "../../../../../../Auth";
import { uploadfile, userService } from "../../../../../Config";

import { AddImage, Preview, ErrorMsg } from "../style.jsx";

class UploadImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      organisationName: "",
      selectedFileName: "",
      selectedFile: "",
      uploadedImage: "",
      forPreview: "",
      fileSize: ""
    };
  }

  fileChangedHandler = event => {
    const authOrganization = userService.authOrganization();
    const imageName = event.target.files[0].name;
    var file = event.target.files[0];
    let reader = new FileReader();
    // debugger;

    if (file.size > 5242880) {
      // debugger;
      return this.setState({
        fileSize: "Your image size should not be maximum 5MB"
      });
    } else {
      reader.onloadend = () => {
        // debugger;
        let forPreview = reader.result;
        let result = reader.result.split(",");
        let finalResult = result[1];

        this.setState({
          selectedFile: finalResult,
          forPreview: forPreview
        });
      };
      reader.readAsDataURL(file);

      this.setState({
        selectedFileName: imageName,
        fileSize: "",
        organisationName: authOrganization
      });
    }
  };

  uploadHandler = () => {
    const { selectedFileName, organisationName } = this.state;
    // debugger;
    fetch(uploadfile, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        fileName: this.state.selectedFileName,
        fileContent: this.state.selectedFile,
        brandName: organisationName
      })
    })
      .then(res => res.json())
      .then(res => this.props.displayImageName(selectedFileName, res.FileName))
      .then(this.props.onclickClose)
      .catch(error => console.log(error));
  };

  render() {
    let { selectedFile, forPreview, fileSize } = this.state;
    let $imagePreview = null;
    if (selectedFile) {
      $imagePreview = <img src={forPreview} alt="image" />;
    } else {
      $imagePreview = (
        <div className="previewText">Please select an Image for Preview</div>
      );
    }

    return (
      <AddImage>
        <input type="file" onChange={this.fileChangedHandler} accept="image/*" />
        <ErrorMsg>{fileSize != "" ? fileSize : null}</ErrorMsg>
        <Preview>{$imagePreview}</Preview>
        <Button size="fullwidth" onclick={this.uploadHandler}>
          Upload Image
        </Button>
      </AddImage>
    );
  }
}

export default UploadImage;
