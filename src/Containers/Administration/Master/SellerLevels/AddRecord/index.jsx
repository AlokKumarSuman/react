import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../../Localization/index";
import Button from "../../../../../Components/Button";
import Modal from "../../../../../Components/Modal";
import { Auth } from "../../../../../Auth";
import UploadImage from "./UploadImage";
import {
  userService,
  currency,
  updateCustomerLevel,
  addCustomerLevel
} from "../../../../Config";

import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ButtonImageNameWrap,
  NameButtonWrap,
  ErrorMsg
} from "./style";

class AddRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addLevelName: "",
      addPointsReq: "",
      addSaleReq: "",
      addCurrencyType: "",
      saleterm: "Yearly",
      countryListAPI: [],
      responseAddLevelStatusAPI: "",
      validateFilds: "",
      numberValidateTP: "",
      numberValidateTS: "",
      upd: [],
      modalOpen: false,
      imageName: "",
      imageNameByServer: ""
    };
  }

  openHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  closeHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  displayImageNameHandler = (displayImageName, imageNameByServer) => {
    this.setState({
      imageName: displayImageName,
      imageNameByServer
    });
  };

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      responseAddLevelStatusAPI: "",
      passwordConfirmation: "",
      validateFilds: "",
      numberValidateTP: "",
      numberValidateTS: ""
    });
  };

  onSaleTermChange = e => {
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      saleterm: e.target.value
    });
  };

  componentDidMount() {
    // debugger;
    const upd = this.props.upd;
    if (upd != "") {
      this.setState({
        upd: upd,
        addLevelName: upd.NAME,
        addPointsReq: upd.POINTS_REQUIRED,
        addSaleReq: upd.SALE_AMT_REQUIRED,
        saleterm: upd.SALE_TERM,
        addCurrencyType: ""
      });
    }

    fetch(currency, {
      method: "GET",
      headers: Auth
    })
      .then(res => res.json())
      .then(res => {
        this.setState({
          countryListAPI: res.response.docs
        });
      })
      .catch(err => console.log(err));
  }

  addNewRecord = e => {
    e.preventDefault();
    // debugger;
    const {
      addLevelName,
      addPointsReq,
      addSaleReq,
      addCurrencyType,
      saleterm,
      countryListAPI,
      imageNameByServer
    } = this.state;

    if (
      addLevelName === "" ||
      addPointsReq === "" ||
      addSaleReq === "" ||
      addCurrencyType === "" ||
      saleterm === "" ||
      imageNameByServer === ""
    ) {
      return this.setState({
        validateFilds: strings.All_field_must_be_filled_out
      });
    }

    const numberOnly = /^[0-9\b]+$/;
    if (!addPointsReq.match(numberOnly)) {
      return this.setState({
        numberValidateTP: strings.Total_points_should_be_the_numeric_value
      });
    }
    if (!addSaleReq.match(numberOnly)) {
      return this.setState({
        numberValidateTS: strings.Total_sale_should_be_the_numeric_value
      });
    }
    ///// find currency name based on currency code
    const findCountryCode = countryListAPI.filter(
      i => i.CURRENCY_CODE == addCurrencyType
    );
    const addCurrencyName = findCountryCode[0].CURRENCY_NAME;
    var formValue = {
      addLevelName,
      addPointsReq,
      addSaleReq,
      addCurrencyType,
      addCurrencyName,
      saleterm,
      imageNameByServer
    };

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    // debugger;
    fetch(addCustomerLevel, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LEVEL_NAME: addLevelName,
        ICON: imageNameByServer,
        POINTS_REQUIRED: addPointsReq,
        TOTAL_SALE_REQUIRED: addSaleReq,
        CURRENCY: addCurrencyType,
        SALE_TERM: saleterm
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        console.log(res.response);
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res,
            addLevelName: "",
            addPointsReq: "",
            addSaleReq: "",
            addCurrencyType: ""
          });

          setTimeout(() => {
            this.props.addNewLevels(formValue);
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorDelete: err
        })
      );
  };

  // Update Record
  UpdateRecord = e => {
    // debugger;
    const {
      addLevelName,
      addCurrencyType,
      countryListAPI,
      upd,
      saleterm,
      imageNameByServer
    } = this.state;

    var addPointsReq = this.state.addPointsReq.toString();
    var addSaleReq = this.state.addSaleReq.toString();

    if (
      addLevelName === "" ||
      addPointsReq === "" ||
      addSaleReq === "" ||
      addCurrencyType === "" ||
      saleterm === ""
      // imageNameByServer === ""
    ) {
      return this.setState({
        validateFilds: strings.All_field_must_be_filled_out
      });
    }

    const numberOnly = /^[0-9\b]+$/;
    if (!addPointsReq.match(numberOnly)) {
      return this.setState({
        numberValidateTP: strings.Total_points_should_be_the_numeric_value
      });
    }
    if (!addSaleReq.match(numberOnly)) {
      return this.setState({
        numberValidateTS: strings.Total_sale_should_be_the_numeric_value
      });
    }
    ///// find currency name based on currency code
    const findCountryCode = countryListAPI.filter(
      i => i.CURRENCY_CODE == addCurrencyType
    );
    const addCurrencyName = findCountryCode[0].CURRENCY_NAME;
    const id = upd.ID;
    var formValue = {
      addLevelName,
      addPointsReq,
      addSaleReq,
      addCurrencyType,
      addCurrencyName,
      saleterm,
      id,
      imageNameByServer
    };

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    // debugger;
    fetch(updateCustomerLevel, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LEVEL_ID: id,
        LEVEL_NAME: addLevelName,
        ICON: imageNameByServer,
        POINTS_REQUIRED: addPointsReq,
        TOTAL_SALE_REQUIRED: addSaleReq,
        CURRENCY: addCurrencyType,
        SALE_TERM: saleterm
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        console.log(res.response);
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res,
            addLevelName: "",
            addPointsReq: "",
            addSaleReq: "",
            addCurrencyType: ""
          });

          setTimeout(() => {
            this.props.updateLevels(formValue);
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorDelete: err
        })
      );
  };

  render() {
    // debugger;
    const {
      countryListAPI,
      responseAddLevelStatusAPI,
      validateFilds,
      numberValidateTP,
      numberValidateTS,
      upd,
      imageName
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.modalOpen}
          onclick={this.closeHandler}
          title={strings.Upload_Image}
        >
          <UploadImage
            onclickClose={this.closeHandler}
            displayImageName={this.displayImageNameHandler}
          />
        </Modal>
        <Form onSubmit={this.addRecord}>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Level_name} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="addLevelName"
                placeholder={strings.Level_name}
                value={this.state.addLevelName}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Total_points_required} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="addPointsReq"
                placeholder={strings.Total_points_required}
                value={this.state.addPointsReq}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Total_sale_required} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="addSaleReq"
                placeholder={strings.Total_sale_required}
                value={this.state.addSaleReq}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
            <NameButtonWrap>
              <LabelInputGroup>
                <h5>
                  {strings.Currency_type} <span className="error">*</span>
                </h5>
                <select name="addCurrencyType" onChange={this.handleChange}>
                  <option value="default">
                    {strings.Select_currency_type}
                  </option>
                  {countryListAPI.map((list, index) => (
                    //  eslint-disable-next-line
                    <option key={index} value={list.CURRENCY_CODE}>
                      {list.CURRENCY_NAME + " - (" + list.CURRENCY_CODE + ")"}
                    </option>
                  ))}
                </select>
              </LabelInputGroup>
            </NameButtonWrap>
          </FormRow>

          <FormRow>
            <NameButtonWrap>
              <LabelInputGroup>
                <h5>{strings.Upload_Image}</h5>
                <ButtonImageNameWrap>
                  <Button classname="grey" onclick={this.openHandler}>
                    {strings.Browse}
                  </Button>
                  <p>{imageName}</p>
                </ButtonImageNameWrap>
              </LabelInputGroup>
            </NameButtonWrap>
            <LabelInputGroup>
              <h5>{strings.Sale_term}</h5>
              <select name="saleterm" onChange={e => this.onSaleTermChange(e)}>
                <option>Yearly</option>
                <option>Quarterly</option>
                <option>Monthly</option>
              </select>
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <LabelButtonDU>
              {upd != "" ? (
                <Button onclick={this.UpdateRecord}>{strings.Update}</Button>
              ) : (
                <Button onclick={this.addNewRecord}>
                  {strings.Add_levels}
                </Button>
              )}

              <Button onclick={this.props.onclickClose}>
                {strings.Cancel}
              </Button>
            </LabelButtonDU>
          </FormRow>
          {/* ///// */}
          {!(validateFilds === "") ? (
            <ErrorMsg>{validateFilds}</ErrorMsg>
          ) : null}

          {!(numberValidateTP === "") ? (
            <ErrorMsg>{numberValidateTP}</ErrorMsg>
          ) : null}

          {!(numberValidateTS === "") ? (
            <ErrorMsg>{numberValidateTS}</ErrorMsg>
          ) : null}

          {/* ////// */}
          {!(
            responseAddLevelStatusAPI == "undefined" ||
            responseAddLevelStatusAPI == ""
          ) ? (
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                fontSize: "13px",
                color: "red",
                paddingTop: "10px"
              }}
            >
              {responseAddLevelStatusAPI.STATUS +
                ": " +
                responseAddLevelStatusAPI.MESSAGE}
            </h4>
          ) : null}
        </Form>
      </React.Fragment>
    );
  }
}

// AddRecord.propTypes = {
//   addRec: PropTypes.func,
//   onclickClose: PropTypes.func
// };

export default AddRecord;
