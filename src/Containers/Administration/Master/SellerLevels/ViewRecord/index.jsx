import React, { Component } from "react";
import PropTypes from "prop-types";
import { TabsMain, TabGroup, TabLists, TabPanels } from "./style";
import clientImg from "../../../../../Assets/Images/img_avatar.png";
import { strings } from "./../../../../../Localization/index";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // debugger;
    const { currentItem } = this.props;
    return (
      <table className="table record-view-table">
        <tbody>
          <tr>
            <td>
              <b>{strings.Image_name} </b>
            </td>
            <td>: {currentItem.ICON_IMAGE}</td>
          </tr>
          <tr>
            <td>
              <b>{strings.Level_id} </b>
            </td>
            <td>: {currentItem.ID}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Level_name}</b>
            </td>
            <td>: {currentItem.NAME}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Brand_Name}</b>
            </td>
            <td>: {currentItem.BRAND_NAME}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Total_points_required}</b>
            </td>
            <td>: {currentItem.POINTS_REQUIRED}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Total_sale_required}</b>
            </td>
            <td>: {currentItem.SALE_AMT_REQUIRED}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Currency_name}</b>
            </td>
            <td>
              :
              {currentItem.CURRENCY_NAME + " (" + currentItem.CURRENCY_ID + ")"}
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
