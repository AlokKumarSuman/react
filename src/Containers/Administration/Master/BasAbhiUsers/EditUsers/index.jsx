import React, { Component } from "react";
import Button from "../../../../../Components/Button";
import {
  userService,
  fetchMemberTypesList,
  updateLoyaltyUserMemberType
} from "../../../../Config";
import { Auth } from "../../../../../Auth";
import { Form, FormRow, LabelInputGroup, LabelButtonDU } from "../style.jsx";

class EditUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      memberTypeId: this.props.upd.MEMBER_TYPE_ID,
      typeName: this.props.upd.MEMBER_TYPE,
      fetchMemberTypes: [],
      responseAddLevelStatusAPI: "",
      serverError: ""
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch(fetchMemberTypesList, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        BRAND_ID: authBrandId,
        LANGUAGE_ID: "-1"
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res))
      .then(res =>
        this.setState({
          fetchMemberTypes: res.RESULT
        })
      )
      .catch(err => console.log(err));
  }

  memberChange = e => {
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      memberTypeId: id,
      typeName: e.target.value,
      responseAddLevelStatusAPI: ""
    });
  };

  updateRecord = e => {
    // debugger;
    e.preventDefault();
    const { memberTypeId, typeName } = this.state;
    const { upd } = this.props;
    const authBrandId = userService.authBrandId();
    const authTenantId = userService.authTenantId();
    // debugger;

    var editValue = {
      userid: upd.USER_ID,
      membertypeid: memberTypeId,
      membertype: typeName,
      businessname: upd.BUSINESS_NAME
    };

    fetch(updateLoyaltyUserMemberType, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: upd.USER_ID,
        BRAND_ID: authBrandId,
        TENANT_ID: authTenantId,
        MEMBER_TYPE: memberTypeId
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.editLevel(editValue);
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    const { upd } = this.props;
    const { fetchMemberTypes, responseAddLevelStatusAPI } = this.state;

    return (
      <Form>
        <FormRow>
          <LabelInputGroup>
            <h5>User ID</h5>
            <input type="text" value={upd.USER_ID} disabled />
          </LabelInputGroup>
          <LabelInputGroup>
            <h5>Business Name</h5>
            <input type="text" value={upd.BUSINESS_NAME} disabled />
          </LabelInputGroup>
        </FormRow>
        <FormRow>
          <LabelInputGroup>
            <h5>Member Type</h5>
            <select name="memberType" onChange={e => this.memberChange(e)}>
              <option value="default">{upd.MEMBER_TYPE}</option>
              {fetchMemberTypes.map((list, index) => (
                <option key={list.TYPE_ID} id={list.TYPE_ID}>
                  {list.TYPE_NAME}
                </option>
              ))}
            </select>
          </LabelInputGroup>
          <LabelInputGroup>
            <h5>Member ID</h5>
            <input type="text" value={upd.MEMBER_TYPE_ID} disabled />
          </LabelInputGroup>
        </FormRow>
        <FormRow>
          <LabelButtonDU>
            <Button onclick={this.updateRecord}>Update Member Type</Button>
          </LabelButtonDU>
        </FormRow>
        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </h4>
        ) : null}
      </Form>
    );
  }
}

export default EditUsers;
