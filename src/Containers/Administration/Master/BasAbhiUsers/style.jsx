import styled from "styled-components";

export const AuthLevelsCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .product-image {
    height: 4rem;
    width: 4rem;
  }

  .client-image {
    height: 4rem;
    width: 4rem;
    border-radius: 50%;
  }
  .client-image-view {
    height: 6rem;
    width: 6rem;
    border-radius: 50%;
  }
`;

export const AuthLevelsHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
  height: 40px;
`;

export const AuthLevelsBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;

  .table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};
  }
  .table tbody tr td {
    padding: 0.8rem 0.2rem;
  }

  @media (max-width: 768px) {
    td:nth-of-type(1):before {
      content: "User ID";
    }
    td:nth-of-type(2):before {
      content: "Consumer Name";
    }
    td:nth-of-type(3):before {
      content: "Member Type";
    }
    td:nth-of-type(4):before {
      content: "Member ID.";
    }
    td:nth-of-type(5):before {
      content: "Action";
    }

    td:before {
      position: absolute;
      top: 50%;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      transform: translateY(-50%);
      font-weight: 600;
      text-transform: capitalize;
    }
  }
`;

export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 768px) {
    display: none;
  }
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  .green,
  .red,
  .blue {
    width: 230px;
  }
`;
export const ButtonWrapDelete = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px auto 0 auto;
  width: 60%;
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
export const SpinnerApproveReq = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0px;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.2rem;
    }
  }
`;

export const LabelButtonDU = styled.div`
  width: auto !important;
  display: flex;
  margin: 20px auto 0px;
  float: none !important;
  div {
    width: auto;
    margin: 0 auto;
    display: inline-block;
    min-width: 151px;
  }
  div:first-child {
    margin-right: 20px;
  }
`;
