import React, { Component } from "react";
import Spinner from "../../../../Components/Spinner";
import NoDataFound from "../../../../Components/NoDataFound";
import { userService, FetchSaudaUserForBrands } from "../../../Config";
import { Auth } from "../../../../Auth";
import { strings } from "./../../../../Localization/index";
import Modal from "../../../../Components/Modal";
import Icon from "../../../../Components/Icons";
import Button from "../../../../Components/Button";
import EditUsers from "./EditUsers";
import MemberType from "./MemberType";
import {
  AuthLevelsCard,
  AuthLevelsHeader,
  WrapperLeft,
  WrapperRight,
  AuthLevelsBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError
} from "./style";

class SaudaUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spinnerConfigData: true,
      allMarketingData: [],
      responseStatusAPI: "",
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      responseDeleteLevelStatusAPI: "",
      openEditModal: false,
      openMemberModal: false,
      currentRowData: []
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch(FetchSaudaUserForBrands, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        LIMIT: "1000",
        OFFSET: "0"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            allMarketingData: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  openEditHandler = (e, currentItems) => {
    this.setState({
      openEditModal: true,
      currentRowData: currentItems
    });
  };

  openMemberHandler = () => {
    this.setState({
      openMemberModal: true
    });
  };

  closeEditHandler = () => {
    this.setState({
      openEditModal: false,
      openMemberModal: false
    });
  };

  editHandler = editValue => {
    // debugger;

    var currPushArray = {
      BUSINESS_NAME: editValue.businessname,
      MEMBER_TYPE: editValue.membertype,
      MEMBER_TYPE_ID: editValue.membertypeid,
      USER_ID: editValue.userid
    };

    var obj = this.state.allMarketingData;

    var index = obj.findIndex(e => e.USER_ID == editValue.userid);
    obj[index] = currPushArray;
    this.setState({ allMarketingData: obj, UPD: {} });
  };

  render() {
    // if (sessionStorage.length == 0) {
    //   return <Redirect to="/login" />;
    // }

    const {
      allMarketingData,
      spinnerConfigData,
      serverError,
      fetchMemberTypes,
      currentRowData
    } = this.state;

    console.log(currentRowData);

    return (
      <React.Fragment>
        <AuthLevelsCard>
          <AuthLevelsHeader>
            <WrapperLeft>Sauda Users List</WrapperLeft>
            <WrapperRight>
              <Button classname="blue" onclick={this.openMemberHandler}>
                Member Type
              </Button>
            </WrapperRight>
          </AuthLevelsHeader>

          <Modal
            modalOpen={this.state.openMemberModal}
            onclick={this.closeEditHandler}
            title="Sauda Member Type"
            size="md"
          >
            <MemberType onclickClose={this.closeEditHandler} />
          </Modal>

          <Modal
            modalOpen={this.state.openEditModal}
            onclick={this.closeEditHandler}
            title="Current Prize Details"
            size="md"
          >
            <EditUsers
              upd={currentRowData}
              editLevel={this.editHandler}
              onclickClose={this.closeEditHandler}
            />
          </Modal>

          <AuthLevelsBody toggleFilter={this.state.filterShow}>
            <table className="table bordered odd-even">
              <thead>
                <tr>
                  <th width="20%" align="left">
                    User Id
                  </th>
                  <th width="20%" align="left">
                    Business Name
                  </th>
                  <th width="20%" align="left">
                    Member Type
                  </th>
                  <th width="20%" align="left">
                    Member ID
                  </th>
                  <th width="10%" align="left">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {spinnerConfigData == true ? (
                  <tr>
                    <td colSpan="6">
                      <SpinnerConfigData>
                        <Spinner />
                      </SpinnerConfigData>
                    </td>
                  </tr>
                ) : allMarketingData.length == 0 ? (
                  <tr>
                    <td colSpan="6">
                      <NoDataFound />
                    </td>
                  </tr>
                ) : (
                  allMarketingData.map((item, index) => (
                    <tr>
                      <td>{item.USER_ID}</td>
                      <td>{item.BUSINESS_NAME}</td>
                      <td>{item.MEMBER_TYPE}</td>
                      <td>{item.MEMBER_TYPE_ID}</td>
                      <td>
                        <Icon
                          name="pen"
                          onclick={e => this.openEditHandler(e, item)}
                        />
                      </td>
                    </tr>
                  ))
                )}
              </tbody>
            </table>
            {this.state.fetchErrorMsg != "" ? (
              <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
            ) : null}
            {this.state.serverError.statusText == "" ? (
              <ServerError>
                {serverError.status} {serverError.statusText}
              </ServerError>
            ) : null}
          </AuthLevelsBody>
        </AuthLevelsCard>
      </React.Fragment>
    );
  }
}

// ViewDealer.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default SaudaUsers;
