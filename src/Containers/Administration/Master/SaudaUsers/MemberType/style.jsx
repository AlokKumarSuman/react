import styled from "styled-components";

export const AuthLevelsCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .product-image {
    height: 4rem;
    width: 4rem;
  }

  .client-image {
    height: 4rem;
    width: 4rem;
    border-radius: 50%;
  }
  .client-image-view {
    height: 6rem;
    width: 6rem;
    border-radius: 50%;
  }
`;

export const AuthLevelsHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
  height: 40px;
`;

export const AuthLevelsBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;

  .table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};
  }
  .table tbody tr td {
    padding: 0.8rem 0.2rem;
  }
`;

export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const ButtonWrapDelete = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px auto 0 auto;
  width: 60%;
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
export const SpinnerApproveReq = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0px;
  justify-content: space-between;
  flex-direction: column;

  @media (max-width: 768px) {
    table.table.bordered.odd-even tbody tr td {
      padding: initial;
    }
    table.table.bordered.odd-even tbody tr {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
    }
  }
`;

export const InnerFormRow = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin-top: 2rem;
`;

export const LabelInputGroup = styled.div`
  width: 60%;
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.2rem;
    }
  }
`;

export const LabelButtonDU = styled.div`
  width: auto !important;
  display: flex;
  margin: 0px auto;
  float: none !important;
  height: 33px;
  margin-top: 0px;
  div {
    width: auto;
    margin: 0 auto;
    display: inline-block;
    min-width: 151px;
  }
  div:first-child {
    margin-right: 20px;
  }
`;

export const TrashIconStyled = styled.div`
  .icon {
    position: static;
  }
`;
