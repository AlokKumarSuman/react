import React, { Component } from "react";
import Button from "../../../../../Components/Button";
import Icon from "../../../../../Components/Icons";
import {
  userService,
  addSaudaMemberType,
  deleteSaudaMemberType,
  fetchMarketingMemberType
} from "../../../../Config";
import { Auth } from "../../../../../Auth";
import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  TrashIconStyled,
  InnerFormRow,
  ErrorMsg
} from "./style.jsx";

class MemberType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      memberType: "",
      responseAddLevelStatusAPI: "",
      fetchMemberTypes: [],
      validateAllFilds: ""
    };
  }

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      validateAllFilds: ""
    });
  };

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(fetchMarketingMemberType, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        BRAND_ID: authBrandId,
        LANGUAGE_ID: "-1"
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res))
      .then(res =>
        this.setState({
          fetchMemberTypes: res.RESULT
        })
      )
      .catch(err => console.log(err));
  }

  addMemberType = () => {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();
    const { memberType } = this.state;
    // debugger;

    if (memberType === "") {
      return this.setState({
        validateAllFilds: "You need to type a member "
      });
    }

    fetch(addSaudaMemberType, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        BRAND_ID: authBrandId,
        TENANT_ID: authTenantId,
        NAME: this.state.memberType,
        LANGUAGE_ID: "-1"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  deleteMemberType = (e, list) => {
    e.preventDefault();
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();
    // debugger;
    fetch(deleteSaudaMemberType, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        BRAND_ID: authBrandId,
        TENANT_ID: authTenantId,
        ID: list.TYPE_ID,
        LANGUAGE_ID: "-1"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    const {
      responseAddLevelStatusAPI,
      memberType,
      fetchMemberTypes,
      validateAllFilds
    } = this.state;
    return (
      <Form>
        <FormRow>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="20%" align="left">
                  Member Id
                </th>
                <th width="20%" align="left">
                  Member Type
                </th>
                <th width="20%" align="right">
                  Action
                </th>
              </tr>
            </thead>

            <tbody>
              {fetchMemberTypes.map((list, index) => (
                <tr>
                  <td>{list.TYPE_ID}</td>
                  <td>{list.TYPE_NAME}</td>
                  <td align="right">
                    <TrashIconStyled>
                      <Icon
                        name="trash"
                        datakey={list.TYPE_ID}
                        onclick={e => this.deleteMemberType(e, list)}
                      />
                    </TrashIconStyled>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <InnerFormRow>
            <LabelInputGroup>
              <input
                type="text"
                name="memberType"
                placeholder="Add New Member Type"
                value={memberType}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
            <LabelButtonDU>
              <Button onclick={this.addMemberType}>Add Member Type</Button>
            </LabelButtonDU>
          </InnerFormRow>
        </FormRow>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </h4>
        ) : null}
      </Form>
    );
  }
}

export default MemberType;
