import styled from "styled-components";

export const LoyaltyWrapper = styled.div``;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;

export const IssueHeader = styled.div`
  display: flex;
  width: 100%;
  padding: 10px 10px;
  background: #2dc3e8;
`;

export const HeaderLeft = styled.div`
  font-family: "Gotham";
  font-size: 16px;
  color: #fff;
  letter-spacing: 1px;
  font-weight: 500;
`;

export const IssueBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
