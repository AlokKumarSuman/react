import React, { Component } from "react";
import { userService, fetchMemberTypesList } from "../../../Config";
import { strings } from "./../../../../Localization/index";
import { Auth } from "../../../../Auth";
import Spinner from "../../../../Components/Spinner";
import NoDataFound from "../../../../Components/NoDataFound";
import moment from "moment";
import {
  LoyaltyWrapper,
  SpinnerConfigData,
  IssueHeader,
  HeaderLeft,
  IssueBody,
  ErrorMsg
} from "./style.jsx";

class LoyaltyMemberType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loyaltymembertype: [],
      fetchErrorMsg: "",
      spinnerConfigData: true
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(fetchMemberTypesList, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LANGUAGE_ID: "-1"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      // .then(res => console.log(res))

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            loyaltymembertype: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err,
          spinnerConfigData: false
        })
      );
  }

  render() {
    // console.log(this.state.loyaltymembertype);
    const { loyaltymembertype, spinnerConfigData } = this.state;
    return (
      <LoyaltyWrapper>
        <IssueHeader>
          <HeaderLeft>{strings.Raised_issue_list}</HeaderLeft>
        </IssueHeader>
        <IssueBody>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th align="left" width="5%">
                  {strings.Type_id}
                </th>
                <th align="left" width="5%">
                  {strings.Brand_Name}
                </th>
                <th align="left" width="15%">
                  {strings.Created_By}
                </th>
                <th align="left" width="15%">
                  {strings.Created_Date}
                </th>
                <th align="left" width="10%">
                  {strings.Type_name}
                </th>
              </tr>
            </thead>
            <tbody>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="7">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : loyaltymembertype.length == 0 ? (
                <tr>
                  <td colSpan="7">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                loyaltymembertype.map((item, index) => (
                  <tr key={index}>
                    <td>{item.TYPE_ID}</td>
                    <td>{item.BRAND_NAME}</td>
                    <td>{item.CREATED_BY}</td>
                    <td>
                      {moment(moment(item.CREATED_DATE)).format(
                        "DD-MM-YYYY hh:mm:ss"
                      )}
                    </td>

                    <td>{item.TYPE_NAME}</td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
        </IssueBody>
      </LoyaltyWrapper>
    );
  }
}

export default LoyaltyMemberType;
