import React, { Component } from "react";
import { strings } from "./../../../../../Localization/index";
import moment from "moment";
import Button from "../../../../../Components/Button";
import { Auth } from "../../../../../Auth";
import { addRewardChooseType, updateReward } from "../../../../Config";
import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButton,
  CheckboxLabelWrap,
  ErrorMsg,
  LabelButtonDU
} from "./style";

class EditReward extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rewardTypeAll: [],
      rewardname: this.props.currRowData.NAME,
      rewardtype: this.props.currRowData.REWARD_TYPE_NAME,
      rewardtypeId: this.props.currRowData.REWARD_TYPE_ID,
      expdate: this.props.currRowData.EXPIRY_DATE,
      rewardvalue: this.props.currRowData.REWARD_VALUE,
      description: this.props.currRowData.DESCRIPTION,
      validateAllFilds: "",
      rewardValueValidate: "",
      responseAddLevelStatusAPI: ""
    };
  }

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      validateAllFilds: "",
      rewardValueValidate: "",
      responseAddLevelStatusAPI: ""
    });
  };

  onRewardTypeChange = e => {
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      rewardtype: e.target.value,
      rewardtypeId: id
    });
  };

  componentDidMount() {
    fetch(addRewardChooseType, {
      method: "GET",
      headers: Auth
    })
      .then(res => res.json())
      .then(res => this.setState({ rewardTypeAll: res.response.docs }))
      .catch(err => console.log(err));
  }

  updateReward = e => {
    e.preventDefault();
    const { currRowData, editDataKey } = this.props;

    const {
      rewardname,
      rewardtype,
      rewardtypeId,
      expdate,
      rewardvalue,
      description
    } = this.state;

    if (
      rewardname == "" ||
      rewardtype == "" ||
      expdate == "" ||
      rewardvalue == "" ||
      description == ""
    ) {
      return this.setState({
        inputError: strings.All_field_must_be_filled_out
      });
    }

    const numberOnly = /^[0-9\b]+$/;
    if (!rewardvalue.match(numberOnly)) {
      return this.setState({
        validation: strings.Except_only_numeric_value_only
      });
    }

    const expdateFormat = moment(new Date(expdate)).format("DD-MM-YYYY");

    const editValue = {
      rewardname: rewardname,
      rewardtype: rewardtype,
      rewardtypeId: rewardtypeId,
      expdate: expdate,
      rewardvalue: rewardvalue,
      description: description
    };

    // debugger;

    fetch(updateReward, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: currRowData.BRAND_ID,
        ID: editDataKey,
        STATUS: currRowData.STATUS,
        USER_ID: currRowData.CREATED_BY,
        LANGUAGE_ID: currRowData.LANGUAGE_ID,
        REWARD_NAME: rewardname,
        DESCRIPTION: description,
        EXPIRY_DATE: expdateFormat,
        REWARD_TYPE: rewardtypeId,
        IMAGE_URL: currRowData.IMAGE_URL,
        VALUE: rewardvalue,
        AVAILABLE_LIMIT: "100"
      })
    })
      .then(res => res.json())
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.sendEditValue(editValue);
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    const {
      rewardTypeAll,
      inputError,
      validation,
      serverError,
      validateAllFilds,
      rewardValueValidate,
      responseAddLevelStatusAPI
    } = this.state;
    const { currRowData } = this.props;
    const expdate = moment(new Date(currRowData.EXPIRY_DATE)).format(
      "YYYY-MM-DD"
    );

    return (
      <React.Fragment>
        <Form>
          <FormRow>
            <LabelInputGroup>
              <h5>{strings.User_id}</h5>
              <input
                type="text"
                name="userid"
                value={currRowData.CREATED_BY}
                disabled
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>{strings.Reward_name}</h5>
              <input
                type="text"
                name="rewardname"
                defaultValue={currRowData.NAME}
                onChange={this.onChange}
              />
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup>
              <h5>{strings.Reward_type}</h5>
              <select name="rewardtype" onChange={this.onRewardTypeChange}>
                <option>{currRowData.REWARD_TYPE_NAME}</option>
                {rewardTypeAll.map(type => (
                  <option key={type.REWARD_TYPE_ID} id={type.REWARD_TYPE_ID}>
                    {type.REWARD_TYPE_NAME}
                  </option>
                ))}
              </select>
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>{strings.Expiry_date}</h5>
              <input
                type="date"
                name="expdate"
                defaultValue={expdate}
                onChange={this.onChange}
              />
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup>
              <h5>{strings.Reward_value}</h5>
              <input
                type="text"
                name="rewardvalue"
                defaultValue={currRowData.REWARD_VALUE}
                onChange={this.onChange}
              />
              <ErrorMsg>{validation}</ErrorMsg>
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>{strings.Description}</h5>
              <textarea
                type="text"
                name="description"
                defaultValue={currRowData.DESCRIPTION}
                onChange={this.onChange}
              />
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            <LabelButtonDU>
              <Button onclick={this.updateReward}>{strings.Submit}</Button>
            </LabelButtonDU>
          </FormRow>
          <ErrorMsg>
            {inputError} {serverError}
          </ErrorMsg>
        </Form>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        {!(rewardValueValidate === "") ? (
          <ErrorMsg>{rewardValueValidate}</ErrorMsg>
        ) : null}

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </h4>
        ) : null}
      </React.Fragment>
    );
  }
}

export default EditReward;
