import styled from "styled-components";

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;
    align-items: flex-start;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;

    sup {
      color: red;
    }
  }

  .grey {
    width: 140px;
    padding: 0.8rem;
    text-transform: capitalize;
}

    icon {
      position: static;
    }
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.2rem;
    }
  }
`;
export const LabelButton = styled.div`
  padding-top: 2rem;
  .button {
    color: #fff;
  }
`;

export const CheckboxLabelWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  input {
    width: 20px;
    height: 20px;
    margin-right: 10px;
  }
`;

export const ErrorMsg = styled.div`
  color: red;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 10px auto;
  text-align: left;
`;

export const ServerError = styled.div`
  color: red;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 10px auto;
  text-align: left;
`;

export const AddImage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  input {
    border: none;
    box-shadow: none;
    padding: 0px;
    text-align: center;
    display: flex;
    align-self: center;
    margin: 0 auto;
    width: 30rem;
  }
`;

export const Preview = styled.div`
  width: 300px;
  height: 200px;
  border: 1px solid #333;
  margin: 10px auto;
  overflow: hidden;

  .previewText {
    font-size: 13px;
    padding: 10px;
    color: #999;
  }

  img {
    width: 100%;
    display: block;
  }
`;

export const ButtonImageNameWrap = styled.div`
  display: flex;
  align-items: center;

  p {
    margin-left: 10px;
  }
`;
