import React, { Component } from "react";
import moment from "moment";
import PropTypes from "prop-types";
import { strings } from "./../../../../../Localization/index";
import Button from "../../../../../Components/Button";
import { Auth } from "../../../../../Auth";
import UploadImage from "./UploadImage";
import Icon from "../../../../../Components/Icons";
import Modal from "../../../../../Components/Modal";
import {
  userService,
  addRewardChooseType,
  addRewards
} from "../../../../Config";
import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButton,
  ErrorMsg,
  ServerError,
  AddImage,
  ButtonImageNameWrap
} from "./style";

class AddReward extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rewardname: "",
      rewardtype: "",
      rewardtypeId: "",
      expdate: "",
      rewardvalue: "",
      description: "",
      inputError: "",
      serverError: {},
      rewardTypeAll: [],
      validation: "",
      modalOpen: false,
      imageName: "",
      imageNameByServer: "",

      validateAllFilds: "",
      rewardValueValidate: "",
      responseAddLevelStatusAPI: ""
    };
  }

  componentDidMount() {
    // debugger;
    fetch(addRewardChooseType, {
      method: "GET",
      headers: Auth
    })
      .then(res => res.json())
      .then(res => this.setState({ rewardTypeAll: res.response.docs }))
      .catch(err => console.log(err));
  }

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      validateAllFilds: "",
      rewardValueValidate: "",
      responseAddLevelStatusAPI: ""
    });
  };

  onRewardTypeChange = e => {
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      rewardtype: e.target.value,
      rewardtypeId: id
    });
  };

  openHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  closeHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  displayImageNameHandler = (displayImageName, imageNameByServer) => {
    // debugger;
    this.setState({
      imageName: displayImageName,
      imageNameByServer
    });
  };

  addReward = e => {
    // debugger;
    e.preventDefault();
    const alldata = userService.authUserDataAll();
    const {
      rewardname,
      rewardtype,
      rewardtypeId,
      expdate,
      rewardvalue,
      description,
      imageNameByServer,
      imageName,
      inputError,
      validation
    } = this.state;

    const expdateFormat = moment(new Date(expdate)).format("DD-MM-YYYY");

    const addValue = {
      rewardname: rewardname,
      rewardtype: rewardtype,
      rewardtypeId: rewardtypeId,
      expdate: expdate,
      rewardvalue: rewardvalue,
      imageNameByServer: imageNameByServer,
      description: description
    };

    if (
      rewardname == "" ||
      rewardtype == "" ||
      expdate == "" ||
      rewardvalue == "" ||
      description == "" ||
      imageName == ""
    ) {
      return this.setState({
        inputError: strings.All_field_must_be_filled_out
      });
    }

    const numberOnly = /^[0-9\b]+$/;
    if (!rewardvalue.match(numberOnly)) {
      return this.setState({
        validation: strings.Reward_value_should_be_numeric_value
      });
    }

    // debugger;
    fetch(addRewards, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: alldata.BRAND_ID,
        USER_ID: alldata.username,
        LANGUAGE_ID: "-1",
        REWARD_NAME: rewardname,
        DESCRIPTION: description,
        EXPIRY_DATE: expdateFormat,
        REWARD_TYPE: rewardtypeId,
        IMAGE_URL: imageNameByServer,
        VALUE: rewardvalue,
        AVAILABLE_LIMIT: "100"
      })
    })
      .then(res => res.json())
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.onclickClose();
            this.props.sendValue(addValue);
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    const {
      rewardTypeAll,
      serverError,
      imageName,
      validateAllFilds,
      rewardValueValidate,
      responseAddLevelStatusAPI
    } = this.state;
    const alldata = userService.authUserDataAll();
    const {
      rewardname,
      rewardtype,
      expdate,
      rewardvalue,
      description
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.modalOpen}
          onclick={this.closeHandler}
          title={strings.Upload_Image}
        >
          <UploadImage
            onclickClose={this.closeHandler}
            displayImageName={this.displayImageNameHandler}
          />
        </Modal>
        <Form>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.User_id}
                <sup>*</sup>
              </h5>
              <input
                type="text"
                name="userid"
                value={alldata.username}
                disabled
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Reward_name} <sup>*</sup>
              </h5>
              <input
                type="text"
                name="rewardname"
                value={rewardname}
                onChange={this.onChange}
              />
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Reward_type} <sup>*</sup>
              </h5>
              <select
                name="rewardtype"
                onChange={e => this.onRewardTypeChange(e)}
              >
                <option>{strings.Choose_type}</option>
                {rewardTypeAll.map(type => (
                  <option key={type.REWARD_TYPE_ID} id={type.REWARD_TYPE_ID}>
                    {type.REWARD_TYPE_NAME}
                  </option>
                ))}
              </select>
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Expiry_date} <sup>*</sup>
              </h5>
              <input
                type="date"
                name="expdate"
                value={expdate}
                onChange={this.onChange}
              />
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Reward_value} <sup>*</sup>
              </h5>
              <input
                type="text"
                name="rewardvalue"
                value={rewardvalue}
                onChange={this.onChange}
              />
              <ErrorMsg>{this.state.validation}</ErrorMsg>
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.Upload_Image} <sup>*</sup>
              </h5>
              <ButtonImageNameWrap>
                <Button classname="grey" onclick={this.openHandler}>
                  {/* <Icon name="systemUpdate" color="white" /> */}
                  {strings.Browse}
                </Button>
                <p>{imageName}</p>
              </ButtonImageNameWrap>
            </LabelInputGroup>
          </FormRow>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Description} <sup>*</sup>
              </h5>
              <textarea
                type="text"
                name="description"
                value={description}
                onChange={this.onChange}
              />
            </LabelInputGroup>

            <LabelButton>
              <Button onclick={this.addReward}>{strings.Submit} </Button>
            </LabelButton>
          </FormRow>
          <ErrorMsg>{this.state.inputError}</ErrorMsg>
          <ServerError>
            {serverError.status} {serverError.statusText}
          </ServerError>
        </Form>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        {!(rewardValueValidate === "") ? (
          <ErrorMsg>{rewardValueValidate}</ErrorMsg>
        ) : null}

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </h4>
        ) : null}
      </React.Fragment>
    );
  }
}

AddReward.propTypes = {
  addRec: PropTypes.func,
  onclickClose: PropTypes.func
};

export default AddReward;
