import styled from "styled-components";

export const RewardCampaignCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: "Roboto", sans-serif;
  font-size: initial;
`;

export const RewardCampaignHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;
export const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  div {
    margin-right: 1rem;
  }
  div:last-child {
    margin-right: 0;
  }
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    width:auto;
    padding: 0.6rem 0.8rem;
  }
  div svg.icon {
    margin-right:0.6rem;
    margin-top: -3px;
  }
  div svg.icon-fa {
    margin-right:0.6rem;
  }
  }
  svg.icon:hover {
    fill: #cb0202;
    cursor: pointer;
}
`;
export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  text-transform: uppercase;
  font-weight: 600;

  @media (max-width: 568px) {
    display: none;
  }
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  @media (max-width: 568px) {
    .blue {
      display: none;
    }
  }
`;
export const RewardCampaignBody = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;
  font-size: 1.4rem;
  text-transform: capitalize;

  table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};
  }

  th.action {
    text-align: center !important;
  }

  @media (max-width: 768px) {
    td:nth-of-type(1):before {
      content: "Reward Name";
    }
    td:nth-of-type(2):before {
      content: "Reward Type";
    }
    td:nth-of-type(3):before {
      content: "Reward Status";
    }
    td:nth-of-type(4):before {
      content: "Start Date";
    }
    td:nth-of-type(5):before {
      content: "End Date";
    }
    td:nth-of-type(6):before {
      content: "Action";
    }

    td:before {
      position: absolute;
      top: 50%;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      transform: translateY(-50%);
      font-weight: 600;
      text-transform: capitalize;
    }

    table tbody tr:first-child {
      display: none !important;
    }
  }
`;
export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
