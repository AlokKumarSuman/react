import styled from "styled-components";

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;
    align-items: flex-start;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  p {
    font-size: 14px;
    margin: 10px;
  }
`;
export const LabelButton = styled.div`
  padding-top: 2rem;
  .button {
    color: #fff;
  }
`;

export const CheckboxLabelWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  input {
    width: 20px;
    height: 20px;
    margin-right: 10px;
  }
`;

export const ErrorMsg = styled.div`
  color: red;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 0 auto;
  text-align: center;
`;
