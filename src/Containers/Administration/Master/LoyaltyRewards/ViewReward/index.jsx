import React, { Component } from "react";
import moment from "moment";
import { Form, FormRow, LabelInputGroup, LabelButton } from "./style";
import { strings } from "./../../../../../Localization/index";

class ViewReward extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    // debugger;
    const { currRowData } = this.props;
    const createDateFormat = moment(new Date(currRowData.CREATED_DATE)).format(
      "DD-MM-YYYY"
    );
    const expiryDateFormat = moment(new Date(currRowData.EXPIRY_DATE)).format(
      "DD-MM-YYYY"
    );

    return (
      <Form>
        <FormRow>
          <LabelInputGroup>
            <h5>{strings.Reward_name}</h5>
            <p>{currRowData.NAME}</p>
          </LabelInputGroup>
          <LabelInputGroup>
            <h5>{strings.Reward_type}</h5>
            <p>{currRowData.REWARD_TYPE_NAME}</p>
          </LabelInputGroup>
        </FormRow>
        <FormRow>
          <LabelInputGroup>
            <h5>{strings.Reward_value}</h5>
            <p>{currRowData.REWARD_VALUE}</p>
          </LabelInputGroup>

          <LabelInputGroup>
            <h5>{strings.Created_By}</h5>
            <p>{currRowData.CREATED_BY}</p>
          </LabelInputGroup>
        </FormRow>

        <FormRow>
          <LabelInputGroup>
            <h5>{strings.Created_Date}</h5>
            <p>{createDateFormat}</p>
          </LabelInputGroup>

          <LabelInputGroup>
            <h5>{strings.Expiry_date}</h5>
            <p>{expiryDateFormat}</p>
          </LabelInputGroup>
        </FormRow>

        <FormRow>
          <LabelInputGroup>
            <h5>{strings.Brand_Name}</h5>
            <p>{currRowData.BRAND_NAME}</p>
          </LabelInputGroup>

          <LabelInputGroup>
            <h5>{strings.Status}</h5>
            <p>{currRowData.STATUS}</p>
          </LabelInputGroup>
        </FormRow>

        <FormRow>
          <LabelInputGroup>
            <h5>{strings.Description}</h5>
            <p>{currRowData.DESCRIPTION}</p>
          </LabelInputGroup>
          <LabelInputGroup>
            <h5>{strings.Image}</h5>
            <img src={currRowData.IMAGE_URL} alt="image" />
            {/* <p>{currRowData.IMAGE_URL}</p> */}
          </LabelInputGroup>
        </FormRow>
      </Form>
    );
  }
}

export default ViewReward;
