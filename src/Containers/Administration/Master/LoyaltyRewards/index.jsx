import React, { Component } from "react";
import moment from "moment";
import { Redirect } from "react-router-dom";
import { strings } from "./../../../../Localization/index";
import Button from "../../../../Components/Button";
import Spinner from "../../../../Components/Spinner";
import Icon from "../../../../Components/Icons";
import Modal from "../../../../Components/Modal";
import AddReward from "./AddReward";
import EditReward from "./EditReward";
import ViewReward from "./ViewReward";
import { baseUrl, userService, fetchRewradsList } from "../../../Config";
import { Auth } from "../../../../Auth";

import {
  RewardCampaignCard,
  RewardCampaignHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  RewardCampaignBody,
  SpinnerConfigData
} from "./style";

class LoyaltyRewards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterShow: false,
      modalOpen: false,
      openEditModal: false,
      openViewModal: false,
      searchName: "",
      searchType: "",
      fetchRewards: [],
      editDataKey: "",
      viewDataKey: "",
      currRowData: [],
      spinnerConfigData: true,
      serverError: {}
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(fetchRewradsList, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LANGUAGE_ID: "-1"
      })
    })
      .then(res => res.json())
      // .then(res => {
      //   if (res.STATUS == "SUCCESS") {
      //     this.setState({
      //       spinnerConfigData: false
      //     });
      //     return res.json();
      //   } else if (res.status == 401) {
      //     this.setState({
      //       spinnerConfigData: false
      //     });
      //     sessionStorage.clear();
      //   } else {
      //     return Promise.reject(
      //       Object.assign({}, res, {
      //         status: res.status,
      //         statusText: res.statusText
      //       })
      //     );
      //   }
      // })
      .then(fetchRewards =>
        this.setState({ fetchRewards: fetchRewards.RESULT })
      )
      .catch(err => console.log(err));
  }

  filterHandler = () => {
    /* for filter toggle*/
    this.setState(prevState => ({
      filterShow: !prevState.filterShow
    }));
  };

  handleName = e => {
    /* for name search */
    this.setState({
      searchName: e.target.value
    });
  };

  handleType = e => {
    /*  for CampaignType Search */
    this.setState({
      searchType: e.target.value
    });
  };

  openHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  closeHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  openEditModal = (e, currRowData) => {
    // debugger;
    const EditId = e.target.getAttribute("datakey");
    this.setState(prevState => ({
      openEditModal: !prevState.openEditModal,
      editDataKey: EditId,
      currRowData
    }));
  };

  closeEditModal = () => {
    this.setState(prevState => ({
      openEditModal: false
    }));
  };

  openViewModal = (e, currRowData) => {
    const ViewId = e.target.getAttribute("datakey");
    this.setState(prevState => ({
      openViewModal: !prevState.openViewModal,
      viewDataKey: ViewId,
      currRowData
    }));
  };

  closeViewModal = () => {
    this.setState(prevState => ({
      openViewModal: !prevState.openViewModal
    }));
  };

  getEditValue = editValue => {
    var editData = {
      REWARD_ID: this.state.editDataKey,
      STATUS: this.state.currRowData.STATUS,
      BRAND_ID: this.state.currRowData.BRAND_ID,
      BRAND_NAME: this.state.currRowData.BRAND_NAME,
      NAME: editValue.rewardname,
      DESCRIPTION: editValue.description,
      LANGUAGE_ID: this.state.currRowData.LANGUAGE_ID,
      EXPIRY_DATE: editValue.expdate,
      REWARD_VALUE: editValue.rewardvalue,
      AVAILABLE_LIMIT: -1,
      CREATED_DATE: this.state.currRowData.CREATED_DATE,
      CREATED_BY: this.state.currRowData.CREATED_BY,
      IMAGE_URL: this.state.currRowData.IMAGE_URL,
      REWARD_TYPE_ID: editValue.rewardtypeId,
      REWARD_TYPE_NAME: editValue.rewardtype
    };

    const index = this.state.fetchRewards.findIndex(
      item => item.REWARD_ID == this.state.editDataKey
    );
    this.state.fetchRewards[index] = editData;

    this.setState({
      fetchRewards: this.state.fetchRewards
    });
  };

  getAddValue = addValue => {
    // debugger;
    const alldata = userService.authUserDataAll();
    const currentDate = new Date();
    const currentDateFormat = moment(new Date(currentDate)).format(
      "DD-MM-YYYY"
    );

    // debugger;
    var addData = {
      REWARD_ID: "",
      STATUS: "SAVED",
      BRAND_ID: alldata.BRAND_ID,
      BRAND_NAME: alldata.BRAND_NAME,
      NAME: addValue.rewardname,
      DESCRIPTION: addValue.description,
      LANGUAGE_ID: -1,
      EXPIRY_DATE: addValue.expdate,
      REWARD_VALUE: addValue.rewardvalue,
      AVAILABLE_LIMIT: -1,
      CREATED_DATE: currentDate,
      CREATED_BY: alldata.username,
      IMAGE_URL: addValue.uploadedImage,
      REWARD_TYPE_ID: addValue.rewardtypeId,
      REWARD_TYPE_NAME: addValue.rewardtype
    };
    this.setState({
      fetchRewards: this.state.fetchRewards.concat([addData])
    });
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }
    const {
      fetchRewards,
      currRowData,
      editDataKey,
      spinnerConfigData
    } = this.state;
    return (
      <RewardCampaignCard>
        <RewardCampaignHeader>
          <WrapperLeft>{strings.Rewards}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              <Button classname="blue" onclick={this.filterHandler}>
                <Icon name="filter" color="white" /> {strings.Filter}
              </Button>

              <Button classname="green" onclick={this.openHandler}>
                <Icon name="addCircle" color="white" /> {strings.Add_new_reward}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </RewardCampaignHeader>
        {/* Add New Reward Campaign */}
        <Modal
          modalOpen={this.state.modalOpen}
          onclick={this.closeHandler}
          title={strings.Add_new_reward}
        >
          <AddReward
            onclickClose={this.closeHandler}
            sendValue={this.getAddValue}
          />
        </Modal>

        {/* Edit Rewards */}
        <Modal
          modalOpen={this.state.openEditModal}
          onclick={this.closeEditModal}
          title={strings.Edit_reward}
        >
          <EditReward
            onclickClose={this.closeEditModal}
            currRowData={currRowData}
            editDataKey={editDataKey}
            sendEditValue={this.getEditValue}
          />
        </Modal>

        {/* View Rewards */}
        <Modal
          modalOpen={this.state.openViewModal}
          onclick={this.closeViewModal}
          title={strings.View_reward}
        >
          <ViewReward
            onclickClose={this.openViewModal}
            currRowData={currRowData}
          />
        </Modal>

        <RewardCampaignBody toggleFilter={this.state.filterShow}>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="30%" align="left">
                  {strings.Reward_name}
                </th>
                <th width="20%" align="left">
                  {strings.Reward_type}
                </th>
                <th width="120px" align="left">
                  {strings.Reward_status}
                </th>
                {/* <th width="156px" style={{ minWidth: "156px" }}>
                   Subscribed Customers
                 </th> */}
                <th width="110px" align="left">
                  {strings.Start_Date}
                </th>
                <th width="110px" align="left">
                  {strings.End_Date}
                </th>
                <th className="action" width="40px">
                  {strings.Action}
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input
                    type="text"
                    value={this.state.searchName}
                    onChange={this.handleName}
                    placeholder={strings.Search_By_Name}
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchType}
                    onChange={this.handleType}
                    placeholder={strings.Search_By_Type}
                    className="textbox"
                  />
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>

              {/* spinnerConfigData ? (
                <tr>
                  <td colSpan="6">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : (  */}
              {fetchRewards.map((item, index) => (
                <tr key={index}>
                  <td key={item.NAME}>{item.NAME}</td>
                  <td key={item.REWARD_TYPE_NAME}>{item.REWARD_TYPE_NAME}</td>
                  <td key={item.STATUS}>{item.STATUS}</td>
                  <td key={item.CREATED_DATE}>
                    {moment(new Date(item.CREATED_DATE)).format("DD-MM-YYYY")}
                  </td>
                  <td key={item.EXPIRY_DATE}>
                    {moment(new Date(item.EXPIRY_DATE)).format("DD-MM-YYYY")}
                  </td>
                  <td>
                    <ButtonGroup>
                      <Icon
                        name="pen"
                        onclick={e => this.openEditModal(e, item)}
                        datakey={item.REWARD_ID}
                      />
                      <Icon
                        name="eye"
                        onclick={e => this.openViewModal(e, item)}
                        datakey={item.REWARD_ID}
                      />
                    </ButtonGroup>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </RewardCampaignBody>
      </RewardCampaignCard>
    );
  }
}

export default LoyaltyRewards;
