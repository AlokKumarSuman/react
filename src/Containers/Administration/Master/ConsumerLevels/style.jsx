import styled from "styled-components";

export const AuthLevelsCard = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .product-image {
    height: 4rem;
    width: 4rem;
  }

  .client-image {
    height: 4rem;
    width: 4rem;
    border-radius: 50%;
  }
  .client-image-view {
    height: 6rem;
    width: 6rem;
    border-radius: 50%;
  }
`;

export const AuthLevelsHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0.6rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;

export const AuthLevelsBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;

  .table tbody tr:first-child {
    width: 100%;
    display: ${props => (props.toggleFilter === false ? "none" : "table-row")};
  }
  .table tbody tr td {
    padding: 0.8rem 0.2rem;
  }

  @media (max-width: 768px) {
    td:nth-of-type(1):before {
      content: "Brand Name";
    }
    td:nth-of-type(2):before {
      content: "Level Name";
    }
    td:nth-of-type(3):before {
      content: "Sale Term";
    }
    td:nth-of-type(4):before {
      content: "Points Req.";
    }
    td:nth-of-type(5):before {
      content: Sale Amt. Req.;
    }
    td:nth-of-type(6):before {
      content: "Currency";
    }
    td:nth-of-type(7):before {
      content: "Action";
    }

    td:before {
      position: absolute;
      top: 50%;
      left: 6px;
      width: 45%;
      padding-right: 10px;
      white-space: nowrap;
      transform: translateY(-50%);
      font-weight: 600;
      text-transform: capitalize;
    }

    table tbody tr:first-child {
      display: none !important;
    }
  }
`;
export const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  div {
    margin-right: 1rem;
  }
  div:last-child {
    margin-right: 0;
  }
  div {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: auto;
    padding: 0.6rem 0.8rem;
  }
  div svg.icon {
    margin-right: 0.6rem;
    margin-top: 0px;
    width: 14px;
    height: 14px;
    transform: scale(1.5);
  }
  div svg.icon-fa {
    margin-right: 0.6rem;
  }
`;
export const WrapperLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 568px) {
    display: none;
  }
`;
export const WrapperRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  @media (max-width: 568px) {
    .blue {
      display: none;
    }
  }
`;
export const ButtonWrapDelete = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px auto 0 auto;
  width: 60%;
`;

export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
export const SpinnerApproveReq = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
`;

export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;

export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
