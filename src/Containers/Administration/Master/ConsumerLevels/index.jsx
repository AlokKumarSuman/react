import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../Localization/index";
import { Redirect } from "react-router-dom";
import Spinner from "../../../../Components/Spinner";
import Button from "../../../../Components/Button";
import Icon from "../../../../Components/Icons";
import Modal from "../../../../Components/Modal";
import NoDataFound from "../../../../Components/NoDataFound";
import UpdateRecord from "./UpdateRecord";
import AddLevels from "./AddRecord";
import { userService, fetchAvailableConsumerLevels } from "../../../Config";
import { Auth } from "../../../../Auth";
import ViewRecord from "./ViewRecord";

import {
  AuthLevelsCard,
  AuthLevelsHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  AuthLevelsBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError,
  ButtonWrapDelete
} from "./style";

class MarketingPrizes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageName: "",
      imageNameByServer: "",
      spinnerConfigData: true,
      filterShow: false,
      searchByBrandName: "",
      searchByLevelName: "",
      crrLevelLists: [],
      currentItem: [],
      responseStatusAPI: "",
      sortIcon: false,
      fetchErrorMsg: "",
      serverError: {},
      serverErrorDelete: {},
      openViewHandler: false,
      addNewLevelsModal: false,
      openEditHandler: false,
      responseDeleteLevelStatusAPI: ""
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(fetchAvailableConsumerLevels, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName
      })
    })
      // .then(res => console.log(res.status))
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            crrLevelLists: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  filterHandler = () => {
    /* for filter toggle*/
    this.setState(prevState => ({
      filterShow: !prevState.filterShow
    }));
  };

  searchByBrandNameHandler = e => {
    this.setState({
      searchByBrandName: e.target.value
    });
  };

  searchByLevelNameHandler = e => {
    this.setState({
      searchByLevelName: e.target.value
    });
  };

  addNewLevelsModal = () => {
    this.setState({
      addNewLevelsModal: true
    });
  };

  openEditHandler = (e, currentItems) => {
    // debugger;
    this.setState({
      openEditHandler: true,
      currentItem: currentItems
    });
  };

  openViewHandler = (e, currentItems) => {
    this.setState(prevState => ({
      openViewHandler: !prevState.openViewHandler,
      currentItem: currentItems
    }));
  };

  closeViewHandler = () => {
    this.setState({
      openViewHandler: false,
      addNewLevelsModal: false,
      openEditHandler: false
      // currentItem: []
    });
  };

  // sortBy = () => {
  //   var obj = [...this.state.crrLevelLists];
  //   obj.reverse((a, b) => a.USER_ID - b.USER_ID);
  //   this.setState(prevState => ({
  //     crrLevelLists: obj,
  //     sortIcon: !prevState.sortIcon
  //   }));
  // };

  // add form data in table
  addNewLevelsHandler = formVal => {
    // debugger;
    const authBrandId = userService.authBrandId();
    const authBrandName = userService.authBrandName();
    const { crrLevelLists } = this.state;

    var newData = {
      ID: "",
      NAME: formVal.levelName,
      BRAND_ID: authBrandId,
      BRAND_NAME: authBrandName,
      POINTS_REQUIRED: formVal.pointsRequired,
      PURCHASE_AMT_REQUIRED: formVal.purchaseAmtRequired,
      CURRENCY_ID: formVal.currencyId,
      CURRENCY_NAME: formVal.currencyName,
      PURCHASE_TERM: formVal.purchaseTerm,
      ORDER: formVal.order,
      ICON_IMAGE: formVal.imageNameByServer
    };
    this.setState({ crrLevelLists: crrLevelLists.concat([newData]), UPD: {} });
  };

  // add form data in table
  editHandler = editValue => {
    // debugger;
    const authBrandId = userService.authBrandId();
    const authBrandName = userService.authBrandName();

    var currPushArray = {
      ID: editValue.levelID,
      NAME: editValue.levelName,
      BRAND_ID: authBrandId,
      BRAND_NAME: authBrandName,
      POINTS_REQUIRED: editValue.pointsRequired,
      PURCHASE_AMT_REQUIRED: editValue.purchaseAmtRequired,
      CURRENCY_ID: editValue.currencyId,
      CURRENCY_NAME: editValue.currencyName,
      PURCHASE_TERM: editValue.purchaseTerm,
      ORDER: editValue.order,
      ICON_IMAGE: editValue.imageNameByServer
    };

    var obj = this.state.crrLevelLists;

    var index = obj.findIndex(e => e.levelID == editValue.levelID);
    obj[index] = currPushArray;
    this.setState({ crrLevelLists: obj, UPD: {} });
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }

    const { currentItem, spinnerConfigData, serverError } = this.state;

    let dataItems = this.state.crrLevelLists;

    const searchByBrandName = this.state.searchByBrandName.trim().toLowerCase();
    const searchByLevelName = this.state.searchByLevelName.trim().toLowerCase();

    if (searchByBrandName.length > 0) {
      dataItems = this.state.crrLevelLists.filter(function(i) {
        return i.BRAND_NAME.toLowerCase().match(searchByBrandName);
      });
    }

    if (searchByLevelName.length > 0) {
      dataItems = this.state.crrLevelLists.filter(function(i) {
        return i.NAME.toLowerCase().match(searchByLevelName);
      });
    }

    return (
      <AuthLevelsCard>
        <AuthLevelsHeader>
          <WrapperLeft>{strings.Consumer_Levels}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              <Button classname="green" onclick={this.addNewLevelsModal}>
                <Icon name="addCircle" color="white" /> {strings.Add_new_levels}
              </Button>
              <Button classname="blue" onclick={this.filterHandler}>
                <Icon name="filter" color="white" /> {strings.Filter}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </AuthLevelsHeader>

        <Modal
          modalOpen={this.state.addNewLevelsModal}
          onclick={this.closeViewHandler}
          title={strings.Add_consumer_level}
        >
          <AddLevels
            onclickClose={this.closeViewHandler}
            addNewLevels={this.addNewLevelsHandler}
          />
        </Modal>

        {/* Modal Popup for View Levels */}
        <Modal
          modalOpen={this.state.openViewHandler}
          onclick={this.closeViewHandler}
          title={strings.Current_prize_details}
          size="md"
        >
          <ViewRecord currentItem={currentItem} />
        </Modal>

        <Modal
          modalOpen={this.state.openEditHandler}
          onclick={this.closeViewHandler}
          title={strings.Edit_prize}
        >
          <UpdateRecord
            onclickClose={this.closeViewHandler}
            upd={currentItem}
            editLevel={this.editHandler}
          />
        </Modal>

        <AuthLevelsBody toggleFilter={this.state.filterShow}>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th width="10%" align="left">
                  {strings.Image}
                </th>
                <th width="10%" align="left">
                  {strings.Brand_Name}
                </th>
                <th width="20%" align="left">
                  {strings.Name}
                </th>
                <th width="12%" align="center">
                  {strings.Points_required}
                </th>
                {/* <th width="12%">{strings.Purchase_amount_required}</th> */}
                <th width="15%">{strings.Currency_name}</th>
                <th width="15%">{strings.Purchase_term}</th>
                <th width="5%">{strings.Action}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input
                    type="text"
                    value={this.state.searchByBrandName}
                    onChange={this.searchByBrandNameHandler}
                    placeholder={strings.Search_by_name}
                    className="textbox"
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={this.state.searchByLevelName}
                    onChange={this.searchByLevelNameHandler}
                    placeholder={strings.Search_by_type}
                    className="textbox"
                  />
                </td>
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="6">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : dataItems.length == 0 ? (
                <tr>
                  <td colSpan="6">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                dataItems.map((item, index) => (
                  <tr>
                    <td>
                      <img src={item.ICON_IMAGE} alt="imagename" />
                    </td>
                    <td>{item.BRAND_NAME}</td>
                    <td>{item.NAME}</td>
                    <td align="center">{item.POINTS_REQUIRED}</td>
                    {/* <td align="center">{item.PURCHASE_AMT_REQUIRED}</td> */}
                    <td align="center">{item.CURRENCY_NAME}</td>
                    <td align="center">{item.PURCHASE_TERM}</td>
                    <td align="center">
                      <ButtonGroup>
                        <Icon
                          name="pen"
                          onclick={e => this.openEditHandler(e, item)}
                        />
                        <Icon
                          name="eye"
                          onclick={e => this.openViewHandler(e, item)}
                        />
                      </ButtonGroup>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
          {this.state.serverError.statusText == "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
        </AuthLevelsBody>
      </AuthLevelsCard>
    );
  }
}

// ViewDealer.propTypes = {
//   currentItem: PropTypes.array,
//   CUSTOMER_NAME: PropTypes.string,
//   BUSINESS_NAME: PropTypes.string,
//   BUSINESS_TYPE: PropTypes.string,
//   BRAND_NAME: PropTypes.string,
//   REQUEST_DATE: PropTypes.string,
//   COMMENTS: PropTypes.string
// };

export default MarketingPrizes;
