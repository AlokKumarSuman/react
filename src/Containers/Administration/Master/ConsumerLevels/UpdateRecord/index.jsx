import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../../Localization/index";
import Button from "../../../../../Components/Button";
import Modal from "../../../../../Components/Modal";
import { Auth } from "../../../../../Auth";
import UploadImage from "./UploadImage";
import {
  userService,
  currency,
  fetchAvailableConsumerLevels,
  updateConsumerLevels
} from "../../../../Config";

import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ButtonImageNameWrap,
  NameButtonWrap,
  ErrorMsg
} from "./style";

class UpdateRecord extends Component {
  constructor(props) {
    super(props);
    // debugger;
    this.state = {
      levelID: this.props.upd.ID,
      levelName: this.props.upd.NAME,
      pointsRequired: this.props.upd.POINTS_REQUIRED,
      purchaseAmtRequired: this.props.upd.PURCHASE_AMT_REQUIRED,
      currencyName: this.props.upd.CURRENCY_NAME,
      currencyId: this.props.upd.CURRENCY_ID,
      purchaseTerm: this.props.upd.PURCHASE_TERM,
      order: this.props.upd.ORDER,
      imageNameByServer: this.props.upd.ICON_IMAGE,
      imageName: "",
      currencyList: [],
      validateAllFilds: "",
      pointsValueValidate: "",
      purchaseAmtValidate: "",
      responseAddLevelStatusAPI: "",
      modalOpen: false
    };
  }

  openHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  closeHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  displayImageNameHandler = (displayImageName, imageNameByServer) => {
    this.setState({
      imageName: displayImageName,
      imageNameByServer
    });
  };

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      validateAllFilds: "",
      pointsValueValidate: "",
      purchaseAmtValidate: "",
      responseAddLevelStatusAPI: ""
    });
  };

  currencyChange = e => {
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      currencyId: id,
      currencyName: e.target.value
    });
  };

  onPurchaseTermChange = e => {
    let options = e.target.options;
    // const id = options[options.selectedIndex].id;
    this.setState({
      purchaseTerm: e.target.value
    });
  };

  componentDidMount() {
    // debugger;
    fetch(currency, {
      method: "GET",
      headers: Auth
    })
      .then(res => res.json())
      .then(res => {
        this.setState({
          currencyList: res.response.docs
        });
      })
      .catch(err => console.log(err));
  }

  updateRecord = e => {
    e.preventDefault();
    const { upd } = this.props;
    // debugger;
    const {
      levelID,
      levelName,
      pointsRequired,
      purchaseAmtRequired,
      currencyName,
      currencyId,
      purchaseTerm,
      order,
      imageName,
      imageNameByServer
    } = this.state;

    if (
      levelName === "" ||
      pointsRequired === "" ||
      purchaseAmtRequired === "" ||
      currencyName === "" ||
      purchaseTerm === "" ||
      // imageName === "" ||
      order === "" ||
      imageNameByServer === ""
    ) {
      return this.setState({
        validateAllFilds: strings.All_field_must_be_filled_out
      });
    }

    // const numberOnly = /^[0-9\b]+$/;
    // if (!pointsRequired.match(numberOnly)) {
    //   return this.setState({
    //     pointsValueValidate: "Points should be the numeric value"
    //   });
    // }
    // if (!purchaseAmtRequired.match(numberOnly)) {
    //   return this.setState({
    //     purchaseAmtValidate: "Purchase Amount should be the numeric value"
    //   });
    // }

    var editValue = {
      levelID,
      levelName,
      pointsRequired,
      purchaseAmtRequired,
      currencyName,
      currencyId,
      purchaseTerm,
      order,
      imageNameByServer
    };

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(updateConsumerLevels, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LEVEL_ID: levelID,
        LEVEL_NAME: levelName,
        ICON: imageNameByServer,
        POINTS_REQUIRED: pointsRequired,
        TOTAL_PURCHASE_REQUIRED: purchaseAmtRequired,
        CURRENCY: currencyId,
        PURCHASE_TERM: purchaseTerm
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.editLevel(editValue);
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    // debugger;
    const {
      levelName,
      pointsRequired,
      purchaseAmtRequired,
      currencyList,
      imageName,
      purchaseTerm,
      currencyName,
      validateAllFilds,
      pointsValueValidate,
      purchaseAmtValidate,
      responseAddLevelStatusAPI
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.modalOpen}
          onclick={this.closeHandler}
          title={strings.Upload_Image}
        >
          <UploadImage
            onclickClose={this.closeHandler}
            displayImageName={this.displayImageNameHandler}
          />
        </Modal>
        <Form onSubmit={this.addRecord}>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Level_name} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="levelName"
                placeholder="Add Level name"
                value={levelName}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Points_required} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="pointsRequired"
                placeholder={strings.Points_required}
                value={pointsRequired}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            {/* <LabelInputGroup>
              <h5>
                {strings.Purchase_amount_required}{" "}
                <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="purchaseAmtRequired"
                placeholder={strings.Purchase_amount_required}
                value={purchaseAmtRequired}
                onChange={this.handleChange}
              />
            </LabelInputGroup> */}

            <LabelInputGroup>
              <h5>
                {strings.Purchase_term} <span className="error">*</span>
              </h5>
              <select
                name="purchaseTerm"
                onChange={e => this.onPurchaseTermChange(e)}
              >
                <option>{purchaseTerm}</option>
                <option>Yearly</option>
                <option>Quarterly</option>
                <option>Monthly</option>
              </select>
            </LabelInputGroup>
            <NameButtonWrap>
              <LabelInputGroup>
                <h5>
                  {strings.Currency_name} <span className="error">*</span>
                </h5>
                <select
                  name="currencyName"
                  onChange={e => this.currencyChange(e)}
                >
                  <option value="default">{currencyName}</option>
                  {currencyList.map((list, index) => (
                    //  eslint-disable-next-line
                    <option key={list.CURRENCY_CODE} id={list.CURRENCY_CODE}>
                      {list.CURRENCY_NAME}
                    </option>
                  ))}
                </select>
              </LabelInputGroup>
            </NameButtonWrap>
          </FormRow>

          <FormRow>
            <NameButtonWrap>
              <LabelInputGroup>
                <h5>{strings.Upload_Image}</h5>
                <ButtonImageNameWrap>
                  <Button classname="grey" onclick={this.openHandler}>
                    {strings.Browse}
                  </Button>
                  <p>{imageName}</p>
                </ButtonImageNameWrap>
              </LabelInputGroup>
            </NameButtonWrap>
          </FormRow>

          <FormRow>
            <LabelButtonDU>
              <Button onclick={this.updateRecord}>
                {strings.Update_levels}
              </Button>
              <Button onclick={this.props.onclickClose}>
                {strings.Cancel}
              </Button>
            </LabelButtonDU>
          </FormRow>
        </Form>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        {!(pointsValueValidate === "") ? (
          <ErrorMsg>{pointsValueValidate}</ErrorMsg>
        ) : null}

        {!(purchaseAmtValidate === "") ? (
          <ErrorMsg>{purchaseAmtValidate}</ErrorMsg>
        ) : null}

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </h4>
        ) : null}
      </React.Fragment>
    );
  }
}

// AddRecord.propTypes = {
//   addRec: PropTypes.func,
//   onclickClose: PropTypes.func
// };

export default UpdateRecord;
