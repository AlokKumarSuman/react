import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../../Localization/index";

class ViewRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // debugger;
    const { currentItem } = this.props;
    return (
      <table className="table record-view-table">
        <tbody>
          <tr>
            <td>
              <b>{strings.Order} </b>
            </td>
            <td>: {currentItem.ORDER}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Brand_Name} </b>
            </td>
            <td>: {currentItem.BRAND_NAME}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Level_name} </b>
            </td>
            <td>: {currentItem.NAME}</td>
          </tr>

          <tr>
            <td>
              <b>{strings.Points_required}</b>
            </td>
            <td>: {currentItem.POINTS_REQUIRED}</td>
          </tr>

          {/* <tr>
            <td>
              <b>{strings.Purchase_amount_required}</b>
            </td>
            <td>: {currentItem.PURCHASE_AMT_REQUIRED}</td>
          </tr> */}

          <tr>
            <td>
              <b>{strings.Currency_name}</b>
            </td>
            <td>
              :
              {currentItem.CURRENCY_NAME + " (" + currentItem.CURRENCY_ID + ")"}
            </td>
          </tr>
          <tr>
            <td>
              <b>{strings.Purchase_term}</b>
            </td>
            <td>: {currentItem.PURCHASE_TERM}</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

ViewRecord.propTypes = {
  currentItem: PropTypes.any
};

export default ViewRecord;
