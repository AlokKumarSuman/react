import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../../Localization/index";
import Button from "../../../../../Components/Button";
import Modal from "../../../../../Components/Modal";
import { Auth } from "../../../../../Auth";
import UploadImage from "./UploadImage";
import {
  userService,
  currency,
  fetchAvailableConsumerLevels,
  addNewConsumerLevel
} from "../../../../Config";

import {
  Form,
  FormRow,
  LabelInputGroup,
  LabelButtonDU,
  ButtonImageNameWrap,
  NameButtonWrap,
  ErrorMsg
} from "./style";

class AddRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      levelName: "",
      pointsRequired: "",
      purchaseAmtRequired: "0",
      currencyName: "",
      currencyId: "",
      purchaseTerm: "",
      order: "",
      currencyList: [],
      validateAllFilds: "",
      pointsValueValidate: "",
      purchaseAmtValidate: "",
      responseAddLevelStatusAPI: "",
      modalOpen: false,
      imageName: "",
      imageNameByServer: ""
    };
  }

  openHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  closeHandler = () => {
    /* for popup Open */
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen
    }));
  };

  displayImageNameHandler = (displayImageName, imageNameByServer) => {
    this.setState({
      imageName: displayImageName,
      imageNameByServer
    });
  };

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      validateAllFilds: "",
      pointsValueValidate: "",
      purchaseAmtValidate: "",
      responseAddLevelStatusAPI: ""
    });
  };

  currencyChange = e => {
    // debugger;
    let options = e.target.options;
    const id = options[options.selectedIndex].id;
    this.setState({
      currencyId: id,
      currencyName: e.target.value
    });
  };

  onPurchaseTermChange = e => {
    let options = e.target.options;
    // const id = options[options.selectedIndex].id;
    this.setState({
      purchaseTerm: e.target.value
    });
  };

  componentDidMount() {
    // debugger;
    fetch(currency, {
      method: "GET",
      headers: Auth
    })
      .then(res => res.json())
      .then(res => {
        this.setState({
          currencyList: res.response.docs
        });
      })
      .catch(err => console.log(err));

    // Below Service integration for ORDER INPUT in add
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    fetch(fetchAvailableConsumerLevels, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res))
      .then(res =>
        this.setState({
          order: res.RESULT.length + 1
        })
      )
      .catch(err => console.log(err));
  }

  addNewRecord = e => {
    e.preventDefault();
    // debugger;
    const {
      levelName,
      pointsRequired,
      purchaseAmtRequired,
      currencyName,
      currencyId,
      purchaseTerm,
      order,
      imageName,
      imageNameByServer
    } = this.state;

    if (
      levelName === "" ||
      pointsRequired === "" ||
      purchaseAmtRequired === "" ||
      currencyName === "" ||
      purchaseTerm === "" ||
      imageName === "" ||
      order === "" ||
      imageNameByServer === ""
    ) {
      return this.setState({
        validateAllFilds: strings.All_field_must_be_filled_out
      });
    }

    const numberOnly = /^[0-9\b]+$/;
    if (!pointsRequired.match(numberOnly)) {
      return this.setState({
        pointsValueValidate: strings.Points_should_be_the_numeric_value
      });
    }
    if (!purchaseAmtRequired.match(numberOnly)) {
      return this.setState({
        purchaseAmtValidate: strings.Purchase_amount_should_be_the_numeric_value
      });
    }

    var formValue = {
      levelName,
      pointsRequired,
      purchaseAmtRequired,
      currencyName,
      currencyId,
      purchaseTerm,
      order,
      imageName,
      imageNameByServer
    };

    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    fetch(addNewConsumerLevel, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        LEVEL_NAME: levelName,
        ICON: imageNameByServer,
        POINTS_REQUIRED: pointsRequired,
        TOTAL_PURCHASE_REQUIRED: purchaseAmtRequired,
        CURRENCY: currencyId,
        PURCHASE_TERM: purchaseTerm,
        ORDER: order
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddLevelStatusAPI: res
          });
          setTimeout(() => {
            this.props.addNewLevels(formValue);
            this.props.onclickClose();
          }, 2000);
        } else {
          this.setState({
            responseAddLevelStatusAPI: res
          });
        }
      })
      .catch(err =>
        this.setState({
          serverError: err
        })
      );
  };

  render() {
    // debugger;
    const {
      levelName,
      pointsRequired,
      purchaseAmtRequired,
      currencyList,
      imageName,
      validateAllFilds,
      pointsValueValidate,
      purchaseAmtValidate,
      responseAddLevelStatusAPI
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          modalOpen={this.state.modalOpen}
          onclick={this.closeHandler}
          title={strings.Upload_Image}
        >
          <UploadImage
            onclickClose={this.closeHandler}
            displayImageName={this.displayImageNameHandler}
          />
        </Modal>
        <Form onSubmit={this.addRecord}>
          <FormRow>
            <LabelInputGroup>
              <h5>
                {strings.Level_name} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="levelName"
                placeholder={strings.Level_name}
                value={levelName}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Points_required} <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="pointsRequired"
                placeholder={strings.Points_required}
                value={pointsRequired}
                onChange={this.handleChange}
              />
            </LabelInputGroup>
          </FormRow>

          <FormRow>
            {/* <LabelInputGroup>
              <h5>
                {strings.Purchase_amount_required}{" "}
                <span className="error">*</span>
              </h5>
              <input
                type="text"
                name="purchaseAmtRequired"
                placeholder={strings.Purchase_amount_required}
                value={purchaseAmtRequired}
                onChange={this.handleChange}
              />
            </LabelInputGroup> */}
            <LabelInputGroup>
              <h5>
                {strings.Purchase_term} <span className="error">*</span>
              </h5>
              <select
                name="purchaseTerm"
                onChange={e => this.onPurchaseTermChange(e)}
              >
                <option>Choose One...</option>
                <option>Yearly</option>
                <option>Quarterly</option>
                <option>Monthly</option>
              </select>
            </LabelInputGroup>

            <NameButtonWrap>
              <LabelInputGroup>
                <h5>
                  {strings.Currency_name} <span className="error">*</span>
                </h5>
                <select
                  name="currencyName"
                  onChange={e => this.currencyChange(e)}
                >
                  <option value="default">
                    {strings.Select_currency_type}
                  </option>
                  {currencyList.map((list, index) => (
                    //  eslint-disable-next-line
                    <option key={list.CURRENCY_CODE} id={list.CURRENCY_CODE}>
                      {list.CURRENCY_NAME}
                    </option>
                  ))}
                </select>
              </LabelInputGroup>
            </NameButtonWrap>
          </FormRow>

          <FormRow>
            <NameButtonWrap>
              <LabelInputGroup>
                <h5>{strings.Upload_Image}</h5>
                <ButtonImageNameWrap>
                  <Button classname="grey" onclick={this.openHandler}>
                    {strings.Browse}
                  </Button>
                  <p>{imageName}</p>
                </ButtonImageNameWrap>
              </LabelInputGroup>
            </NameButtonWrap>
          </FormRow>

          <FormRow>
            <LabelButtonDU>
              <Button onclick={this.addNewRecord}>{strings.Add_levels}</Button>
              <Button onclick={this.props.onclickClose}>
                {strings.Cancel}
              </Button>
            </LabelButtonDU>
          </FormRow>
        </Form>

        {!(validateAllFilds === "") ? (
          <ErrorMsg>{validateAllFilds}</ErrorMsg>
        ) : null}

        {!(pointsValueValidate === "") ? (
          <ErrorMsg>{pointsValueValidate}</ErrorMsg>
        ) : null}

        {!(purchaseAmtValidate === "") ? (
          <ErrorMsg>{purchaseAmtValidate}</ErrorMsg>
        ) : null}

        {!(
          responseAddLevelStatusAPI == "undefined" ||
          responseAddLevelStatusAPI == ""
        ) ? (
          <h4
            style={{
              fontWeight: "500",
              textAlign: "center",
              fontSize: "13px",
              color: "red",
              paddingTop: "10px"
            }}
          >
            {responseAddLevelStatusAPI.STATUS +
              ": " +
              responseAddLevelStatusAPI.MESSAGE}
          </h4>
        ) : null}
      </React.Fragment>
    );
  }
}

// AddRecord.propTypes = {
//   addRec: PropTypes.func,
//   onclickClose: PropTypes.func
// };

export default AddRecord;
