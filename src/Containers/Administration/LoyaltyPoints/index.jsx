import React, { Component } from "react";
import moment from "moment";
import { strings } from "./../../../Localization/index";
import { Redirect } from "react-router-dom";
import Spinner from "../../../Components/Spinner";
import Button from "../../../Components/Button";
import Icon from "../../../Components/Icons";
import Modal from "../../../Components/Modal";
import NoDataFound from "../../../Components/NoDataFound";
import schemesImg from "../../../Assets/Images/schemes.png";
import AddLevels from "./AddRecord";

import {
  listLoyaltyPointsValuesByBrand,
  deleteLoyaltyScheme,
  userService
} from "../../Config";
import { Auth } from "../../../Auth";

// import ViewRecord from "./ViewRecord";

import {
  SchemesCard,
  SchemesHeader,
  ButtonGroup,
  WrapperLeft,
  WrapperRight,
  SchemesBody,
  SpinnerConfigData,
  ErrorMsg,
  ServerError,
  ButtonWrapDelete
} from "./style";

class LoyaltyPoints extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //fetch list
      spinnerConfigData: true,
      crrLevelLists: [],
      fetchErrorMsg: "",
      serverError: {},
      //
      currentItem: [],
      //
      addNewLevelsModal: false
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();

    fetch(listLoyaltyPointsValuesByBrand, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          // sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res.RESULT))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            crrLevelLists: res.RESULT,
            spinnerConfigData: false
          });
        } else {
          this.setState({
            fetchErrorMsg: res.MESSAGE,
            spinnerConfigData: false
          });
        }
      })
      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  }

  addNewLevelsModal = () => {
    this.setState({
      addNewLevelsModal: true,
      currentItem: []
    });
  };
  openEditHandler = (e, currentItems) => {
    this.setState({
      addNewLevelsModal: true,
      currentItem: currentItems
    });
  };

  closeViewHandler = () => {
    this.setState({
      addNewLevelsModal: false,
      currentItem: []
    });
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }

    const {
      crrLevelLists,
      currentItem,
      spinnerConfigData,
      serverError
    } = this.state;
    // debugger;

    return (
      <SchemesCard>
        <SchemesHeader>
          <WrapperLeft>{strings.Loyalty_points_value}</WrapperLeft>
          <WrapperRight>
            <ButtonGroup>
              <Button classname="green" onclick={this.addNewLevelsModal}>
                <Icon name="addCircle" color="white" />{" "}
                {strings.Add_loyalty_points}
              </Button>
            </ButtonGroup>
          </WrapperRight>
        </SchemesHeader>

        {/* Modal Popup for Add New Levels */}
        <Modal
          modalOpen={this.state.addNewLevelsModal}
          onclick={this.closeViewHandler}
          title={
            currentItem != ""
              ? strings.Update_loyality_point_and_details
              : strings.Add_new_loyality_points
          }
        >
          <AddLevels
            onclickClose={this.closeViewHandler}
            addNewLevels={this.addNewLevelsHandler}
            upd={currentItem}
            // updateSchemes={this.updateSchemes}
          />
        </Modal>

        <SchemesBody>
          <table className="table bordered odd-even">
            <thead>
              <tr>
                <th align="left">{strings.Points_required}</th>
                <th>{strings.Points_value}</th>

                <th>{strings.Currency_type}</th>
                <th>{strings.Expire_After_day}</th>

                <th>{strings.Status}</th>
                <th width="120px" style={{ maxWidth: "120px" }}>
                  {strings.Action}
                </th>
              </tr>
            </thead>
            <tbody>
              {spinnerConfigData == true ? (
                <tr>
                  <td colSpan="6">
                    <SpinnerConfigData>
                      <Spinner />
                    </SpinnerConfigData>
                  </td>
                </tr>
              ) : crrLevelLists.length == 0 ? (
                <tr>
                  <td colSpan="6">
                    <NoDataFound />
                  </td>
                </tr>
              ) : (
                crrLevelLists.map((item, index) => (
                  <tr>
                    <td>{item.POINTS}</td>
                    <td align="center">{item.VALUE}</td>

                    <td align="center">{item.CURRENT_ID}</td>
                    <td align="center">
                      {item.EXPIRY_DATE}
                      {/* {moment(new Date(item.EXPIRY_DATE)).format("DD-MM-YYYY")} */}
                    </td>

                    <td align="center">
                      <Button type="primary" size="fullwidth" classname="green">
                        {item.STATUS}
                      </Button>
                    </td>
                    <td align="center">
                      <Icon
                        name="pen"
                        onclick={e => this.openEditHandler(e, item)}
                      />
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
          {this.state.fetchErrorMsg != "" ? (
            <ErrorMsg>{this.state.fetchErrorMsg}</ErrorMsg>
          ) : null}
          {this.state.serverError.statusText == "" ? (
            <ServerError>
              {serverError.status} {serverError.statusText}
            </ServerError>
          ) : null}
        </SchemesBody>
      </SchemesCard>
    );
  }
}

export default LoyaltyPoints;
