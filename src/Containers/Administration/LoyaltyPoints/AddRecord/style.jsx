import styled from "styled-components";

export const Form = styled.div``;
export const FormRow = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;

    & > div {
      width: 49%;
      float: left;
    }
  }
`;
export const FormRowFull = styled.div`
  display: flex;
  margin-bottom: 0;
  justify-content: space-between;
  flex-direction: column;

  @media (min-width: 769px) {
    margin-bottom: 1.2rem;
    flex-direction: row;
  }
  & > div {
    width: 100%;
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
  }

  h4 {
    border: 1px solid #ccc;
    padding: 7px 10px;
    border-radius: 4px;
    width: 364px;
    color: #939393;
  }

  ul.levelDefine {
    display: ${props => (props.consumerLevelDD === false ? "none" : "block")};

    li {
      display: inline-block;
      height: 18px;
      margin: 10px 0px;
      width: 30%;

      input[type="checkbox"] {
        width: 16px;
        height: 16px;
        display: table-cell;
        vertical-align: middle;
        margin: 3px 10px;
      }

      label {
        display: table-cell;
        vertical-align: middle;
      }
    }
  }

  @media (max-width: 768px) {
    input {
      margin-bottom: 1.2rem;
    }
  }
`;

export const LabelButtonDU = styled.div`
  width: auto !important;
  display: flex;
  margin: 0 auto;
  float: none !important;
  div {
    width: auto;
    margin: 0 auto;
    display: inline-block;
    min-width: 151px;
  }
  div:first-child {
    margin-right: 20px;
  }
`;

export const ImageRatio = styled.div`
  object-fit: cover;
  width: 80px;
`;
export const ErrorMsg = styled.div`
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
export const SucessMsg = styled.div`
  display: flex;
  justify-content: center;
  color: green;
  font-size: 13px;
`;
export const ServerError = styled.div`
  color: red;
  font-size: 13px;
  text-align: center;
  margin: 5px 0px;
`;
