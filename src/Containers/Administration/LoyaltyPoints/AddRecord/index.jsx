import React, { Component } from "react";
import PropTypes from "prop-types";
import { strings } from "./../../../../Localization/index";
import moment from "moment";
import Button from "../../../../Components/Button";

import { Auth } from "../../../../Auth";

import { userService, saveLoyaltyPointValue, currency } from "../../../Config";

import {
  Form,
  FormRow,
  FormRowFull,
  LabelInputGroup,
  LabelButtonDU,
  ErrorMsg,
  ServerError,
  SucessMsg
} from "./style";

class AddRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //default fetch
      currencyList: [],
      // setState editable content

      addPointsRequired: "",
      addPointValue: "",
      addStatus: "",
      addCurrencyType: "",
      addEndDate: "",

      // add Server responses
      responseAddSucess: "",
      responseError: "",
      serverErrorStatus: {},
      validateFilds: "",
      //
      disabledEdit: true,
      // On update values
      upd: []
    };
  }

  handleChange = evt => {
    // //   // debugger;
    this.setState({
      [evt.target.name]: evt.target.value,
      responseAddSucess: "",
      responseError: "",
      validateFilds: "",
      serverErrorStatus: {}
    });
  };

  // Component Did Mount
  componentDidMount() {
    const upd = this.props.upd;
    // debugger;
    if (upd != "") {
      // const addEndDateFormated = moment(
      //   moment(upd.EXPIRY_DATE, "DD-MM-YYYY")
      // ).format("YYYY-MM-DD");
      ///////////////////
      // debugger;

      if (upd.STATUS == "Expired") {
        this.setState({
          upd: upd,
          //
          addPointsRequired: upd.POINTS,
          addPointValue: upd.VALUE,
          addStatus: upd.STATUS,
          addCurrencyType: upd.CURRENT_ID,
          addEndDate: upd.EXPIRY_DATE,
          //
          disabledEdit: true
        });
      } else {
        this.setState({
          upd: upd,
          //
          addPointsRequired: upd.POINTS,
          addPointValue: upd.VALUE,
          addStatus: upd.STATUS,
          addCurrencyType: upd.CURRENT_ID,
          addEndDate: upd.EXPIRY_DATE,
          //
          disabledEdit: false
        });
      }
    }

    fetch(currency, {
      method: "GET",
      headers: Auth
    })
      .then(res => res.json())
      .then(res => {
        this.setState({
          currencyList: res.response.docs
        });
      })
      .catch(err => console.log(err));
  }

  // Add New record
  addNewRecord = e => {
    e.preventDefault();
    const authBrandId = userService.authBrandId();

    const {
      addPointsRequired,
      addPointValue,
      addStatus,
      addCurrencyType,
      addEndDate
    } = this.state;

    if (
      addPointsRequired === "" ||
      addPointValue === "" ||
      addCurrencyType === "" ||
      addEndDate === ""
    ) {
      return this.setState({
        validateFilds: strings.All_field_must_be_filled_out
      });
    }

    // const addEndDateFormated = moment(new Date(addEndDate)).format(
    //   "DD-MM-YYYY"
    // );

    fetch(saveLoyaltyPointValue, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        POINTS: addPointsRequired,
        VALUE: addPointValue,
        CURRENCY_ID: addCurrencyType,

        EXPIRY_DATE: addEndDate,
        ID: "",
        STATUS: ""
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddSucess: res.MESSAGE,

            addPointsRequired: "",
            addPointValue: "",
            addStatus: "",
            addCurrencyType: "",
            addEndDate: ""
          });

          setTimeout(() => {
            this.props.onclickClose();
          }, 2000);
          location.reload();
        } else {
          this.setState({
            responseError: res.MESSAGE
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorStatus: err
        })
      );
  };
  /////////////////
  // Update Record
  updateRecord = e => {
    e.preventDefault();
    const authBrandId = userService.authBrandId();

    const {
      addPointsRequired,
      addPointValue,
      addStatus,
      addCurrencyType,
      addEndDate,
      //
      upd
    } = this.state;

    if (
      addPointsRequired === "" ||
      addPointValue === "" ||
      addCurrencyType === "" ||
      addEndDate === ""
    ) {
      return this.setState({
        validateFilds: strings.All_field_must_be_filled_out
      });
    }

    // const addEndDateFormated = moment(new Date(addEndDate)).format(
    //   "DD-MM-YYYY"
    // );

    const updID = upd.ID;
    // debugger;
    fetch(saveLoyaltyPointValue, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        POINTS: addPointsRequired,
        VALUE: addPointValue,
        CURRENCY_ID: addCurrencyType,

        EXPIRY_DATE: addEndDate,
        ID: updID,
        STATUS: addStatus
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseAddSucess: res.MESSAGE,

            addPointsRequired: "",
            addPointValue: "",
            addStatus: "",
            addCurrencyType: "",
            addEndDate: ""
          });

          setTimeout(() => {
            this.props.onclickClose();
          }, 2000);
          location.reload();
        } else {
          this.setState({
            responseError: res.MESSAGE
          });
        }
      })
      .catch(err =>
        this.setState({
          serverErrorStatus: err
        })
      );
  };

  render() {
    const {
      currencyList,
      //
      addPointsRequired,
      addPointValue,
      addStatus,
      addCurrencyType,
      addEndDate,
      // addReward,
      responseAddSucess,
      responseError,
      serverErrorStatus,
      validateFilds,

      upd,
      disabledEdit
    } = this.state;

    return (
      <Form onSubmit={this.addRecord}>
        <FormRow>
          <LabelInputGroup>
            <h5>
              {strings.Points_required} <span className="error">*</span>
            </h5>
            <input
              type="text"
              name="addPointsRequired"
              placeholder={strings.Total_points}
              value={addPointsRequired}
              onChange={this.handleChange}
            />
          </LabelInputGroup>

          <LabelInputGroup>
            <h5>
              {strings.Points_value_in_currency}{" "}
              <span className="error">*</span>
            </h5>

            <input
              type="text"
              name="addPointValue"
              placeholder={strings.Total_points}
              value={addPointValue}
              onChange={this.handleChange}
            />
          </LabelInputGroup>
        </FormRow>

        <FormRow>
          <LabelInputGroup>
            <h5>
              {strings.Current_status} <span className="error">*</span>
            </h5>

            <select
              name="addStatus"
              onChange={this.handleChange}
              value={addStatus}
              disabled={disabledEdit}
            >
              <option value="Active" Selected>
                {strings.Active}
              </option>
              <option value="Expired">{strings.Expired}</option>
            </select>
          </LabelInputGroup>

          <LabelInputGroup>
            <h5>
              {strings.Currency_type} <span className="error">*</span>
            </h5>
            <select
              name="addCurrencyType"
              onChange={this.handleChange}
              value={addCurrencyType}
            >
              <option value="default">{strings.Select_currency_type}</option>
              {currencyList.map((list, index) => (
                //  eslint-disable-next-line
                <option key={index} value={list.CURRENCY_CODE}>
                  {list.CURRENCY_NAME + " - (" + list.CURRENCY_CODE + ")"}
                </option>
              ))}
            </select>
          </LabelInputGroup>
        </FormRow>

        <FormRow>
          <LabelInputGroup>
            <h5>
              Days until points expire <span className="error">*</span>
            </h5>
            <input
              type="text"
              name="addEndDate"
              placeholder={strings.End_Date}
              value={addEndDate}
              onChange={this.handleChange}
            />
          </LabelInputGroup>
        </FormRow>

        <FormRow>
          <LabelButtonDU>
            {upd != "" ? (
              <Button onclick={this.updateRecord}>
                {strings.Edit_Update_loyality_points}
              </Button>
            ) : (
              <Button onclick={this.addNewRecord}>
                {strings.Add_loyality_points}
              </Button>
            )}

            <Button onclick={this.props.onclickClose}>{strings.Cancel}</Button>
          </LabelButtonDU>
        </FormRow>
        {/* ///// */}

        {responseAddSucess != "" ? (
          <SucessMsg>{responseAddSucess}</SucessMsg>
        ) : null}

        {responseError != "" ? <ErrorMsg>{responseError}</ErrorMsg> : null}

        {serverErrorStatus.status == "" ? (
          <ServerError>
            {serverErrorStatus.status} {serverErrorStatus.statusText}
          </ServerError>
        ) : null}

        {!(validateFilds === "") ? <ErrorMsg>{validateFilds}</ErrorMsg> : null}

        {/* ////////////// */}
      </Form>
    );
  }
}

// AddRecord.propTypes = {
//   addRec: PropTypes.func,
//   onclickClose: PropTypes.func
// };

export default AddRecord;
