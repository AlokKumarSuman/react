import styled from "styled-components";

export const HierarchyWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
`;

export const HierarchyHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1.5rem 2rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;

export const HierarchyBody = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem 4rem;
  background-color: white;
  position: relative;
  overflow-x: auto;
`;

export const RadioLabelWrapMain = styled.div`
  display: flex;
  flex-direction: row;
  width: 60rem;
  justify-content: space-between;
  align-items: left;
  margin-bottom: 5rem;
`;

export const RadioLabelWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const ContentRender = styled.div`
  display: flex;
  align-self: center;
`;
