import styled from "styled-components";

export const CountryWrap = styled.div`
  display: flex;
  font-family: "Roboto", sans-serif;
  font-size: initial;
`;

export const BoxWrap = styled.div`
  display: flex;
  flex-direction: column;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  background: #ccc;
  padding: 5px;
  min-width: 10rem;
  margin: 0px 5px;

  p {
    padding: 10px;
    background: #fff;
    color: #000;
    text-align: center;
    text-transform: uppercase;
  }
`;

export const ButtonWrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 1rem;

  span {
    display: block;
    font-size: 20px;
    width: 18px;
    height: 18px;
    text-align: center;
    border-radius: 10px;
    color: #fff;
    line-height: 19px;
  }

  .add {
    background: blue;
  }

  .remove {
    background: red;
  }
`;

export const CountryListSearch = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  right: 0;
  top: 0;
  max-width: 30rem;
  min-width: 30rem;
  overflow-x: auto;
  height: calc(100vh - 22rem);
  background: #fff;

  ul li {
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    padding: 10px;
    border: 1px solid #f0f0f0;

    label {
      width: 100%;
      display: flex;
      flex-direction: row;
      align-items: center;

      input {
        width: 18px;
        height: 18px;
        margin-right: 10px;
      }
    }
  }
`;

export const InputIconWrap = styled.div`
  position: relative;

  input {
    height: 40px;
  }

  .icon {
    position: absolute;
    top: 10px;
    right: 10px;
  }
`;
