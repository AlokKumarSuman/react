import styled from "styled-components";

export const CustomSalesBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0 0.2rem 2rem;
  background-color: white;
  width: 100%;
`;

export const MainWrapper = styled.div``;

export const ButtonWrap = styled.div`
  display: block;
  overflow: hidden;
  padding: 3px;
  border: solid 3px #41c3e3;
  border-top: none !important;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
  height: 25px;
  span {
    display: block;
    font-size: 11px;
    text-align: center;
    color: #fff;
    line-height: 17px;
    cursor: pointer;
    padding: 0 4px;
  }

  .add {
    background: #008e00;
    float: left;
  }

  .remove {
    background: #ff5e5f;
    float: right;
  }

  .view {
    background: #41c3e3;
    float: right;
  }
`;
export const ErrorMsg = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: red;
  font-size: 13px;
`;
export const ResponceSucess = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  color: green;
  font-size: 13px;
`;
export const UnAssignedUser = styled.span`
  color: #ffffff;
  padding: 2px 4px;
  background: red;
  font-size: 12px;
`;
export const ButtonConfirmation = styled.div`
  display: flex;
  align-items: left;
  justify-content: space-around;
  width: 20rem;
  margin: 3rem auto 0;
`;
export const SpinnerConfigData = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;
