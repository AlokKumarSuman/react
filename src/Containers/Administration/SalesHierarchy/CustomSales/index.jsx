import React, { Component } from "react";
import { Redirect } from "react-router-dom";
// import Country from "./Country";

import PropTypes from "prop-types";

import OrgChart from "react-orgchart";
import "react-orgchart/index.css";
import Button from "../../../../Components/Button";
import Modal from "../../../../Components/Modal";
import Icon from "../../../../Components/Icons/index.jsx";

import NoDataFound from "../../../../Components/NoDataFound";
import Spinner from "../../../../Components/Spinner";

import { Auth } from "../../../../Auth";

import {
  userService,
  getBrandUsers,
  unAssignUserFromLocation,
  customHierarchy,
  fetchAssignedUsersForLocation,
  assignUserLocation
} from "../../../Config";

import {
  CustomSalesBody,
  MainWrapper,
  ButtonWrap,
  ErrorMsg,
  ResponceSucess,
  UnAssignedUser,
  ButtonConfirmation,
  SpinnerConfigData
} from "./style.jsx";

class CustomSales extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // data: [],
      errorMsg: {},

      brandUserList: [],
      searchbrandUserName: "",

      assignUserForLocation: false,
      viewUserForLocation: false,
      assignForLocation: "",

      responseStatusAPI: "",
      ErrorMsgAssign: {},
      currentItem: [],
      listOfAssignedUser: [],
      unAssignUserByLocation: false,
      unAssignCurrUser: [],
      spinnerConfigData: true,
      serverError: {},
      ErrorMsgUnAssign: {},
      responseUnAssignedAPI: "",
      //
      initechOrg: {
        ID: "0",
        LABEL: "Root",
        TITLE: "Root",
        location_id: 0,
        LEVEL_ID: "0",
        NEXT_LEVEL: "1",
        PARENT_ID: "0",
        children: []
      }
    };
  }

  handleChange = evt => {
    this.setState({
      [evt.target.name]: evt.target.value,
      responseStatusAPI: "",
      ErrorMsgAssign: {}
    });
  };
  // Add New Assigned User Popup
  assignUserForLocation = (e, currentItems) => {
    // debugger;
    e.stopPropagation();
    this.setState(prevState => ({
      assignUserForLocation: true,
      currentItem: currentItems
    }));
  };
  assignUserForLocationClose = e => {
    // e.stopPropagation();
    this.setState({
      assignUserForLocation: false,
      errorMsg: {},
      responseStatusAPI: "",
      ErrorMsgAssign: {},
      currUserForAssign: "",
      currentItem: ""
    });
  };

  componentDidMount() {
    const authUserName = userService.authUserName();
    const authBrandName = userService.authBrandName();

    fetch(getBrandUsers, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_NAME: authBrandName,
        USER_ID: authUserName
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res))
      .then(res => {
        this.setState({ brandUserList: res.RESULT });
      })
      .catch(err => console.log(err));
  }

  objloop = (res, node) => {
    if (res.length >= 1) {
      node.children = res;
      this.forceUpdate();
    }
  };

  refreshHierarchy = (e, node) => {
    e.stopPropagation();
    e.preventDefault();
    const authBrandId = userService.authBrandId();

    fetch(customHierarchy, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        brandid: authBrandId,
        parent: node.ID,
        level: node.NEXT_LEVEL
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.objloop(res.RESULT, node);
        } else {
          this.setState({
            errorMsg: res
          });
        }
      })
      .catch(error =>
        this.setState({
          errorMsg: error
        })
      );
  };

  //  Assign User For Locations
  assignUserLocation = (e, approveReq) => {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const currUserForAssign = this.state.assignForLocation;
    const currSelectdItem = this.state.currentItem;

    if (currUserForAssign === "" || currUserForAssign === "default") {
      return this.setState({
        ErrorMsgAssign: {
          MESSAGE: "Please Select a user to assign in this location..."
        }
      });
    }

    fetch(assignUserLocation, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        BRAND_ID: authBrandId,

        ASSIGNED_USER_ID: currUserForAssign,
        LEVEL_ID: currSelectdItem.LEVEL_ID,
        LOCATION_ID: currSelectdItem.ID,
        TYPE: "CUSTOM"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            responseStatusAPI: res.MESSAGE
          });
          setTimeout(() => {
            this.assignUserForLocationClose();
          }, 2000);
        } else {
          this.setState({
            ErrorMsgAssign: res
          });
        }
      })
      .catch(error => console.log("Request failed:", error));
  };

  // View Assigned user popup
  viewUserForLocation = (e, currentItems) => {
    // debugger;
    e.stopPropagation();
    this.setState({
      viewUserForLocation: true
    });
    this.viewUserLocation(currentItems);
  };
  viewUserForLocationClose = e => {
    e.stopPropagation();
    this.setState({
      viewUserForLocation: false
    });
  };
  //  Assign User For Locations
  viewUserLocation = currNode => {
    // debugger;
    const authBrandId = userService.authBrandId();
    const currLevelId = currNode.LEVEL_ID;
    const currLocationId = currNode.ID;

    fetch(fetchAssignedUsersForLocation, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,

        LOCATION_ID: currLocationId,
        LEVEL_ID: currLevelId,
        TYPE: "CUSTOM"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          this.setState({
            spinnerConfigData: false
          });
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })

      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            listOfAssignedUser: res.RESULT,
            spinnerConfigData: false
          });
        }
      })

      .catch(error =>
        this.setState({
          serverError: error,
          spinnerConfigData: false
        })
      );
  };

  // Un-Assign User By location
  unAssignUserByLocation = (e, unAssignCurrUsers) => {
    // debugger;
    e.stopPropagation();
    this.setState({
      unAssignUserByLocation: true,
      unAssignCurrUser: unAssignCurrUsers
    });
  };
  unAssignUserByLocationClose = e => {
    // debugger;
    this.setState({
      unAssignUserByLocation: false,
      ErrorMsgUnAssign: {},
      responseUnAssignedAPI: ""
    });
  };

  // unAssignConformd
  // unAssignCurrUser
  unAssignConformd = e => {
    // debugger;
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();

    const deleteCurrUser = this.state.unAssignCurrUser;
    // const aa = this.state.listOfAssignedUser;

    fetch(unAssignUserFromLocation, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        USER_ID: authUserName,
        BRAND_ID: authBrandId,

        LEVEL_ID: deleteCurrUser.LEVEL_ID,
        ASSIGNED_USER_ID: deleteCurrUser.USER_ID,
        LOCATION_ID: deleteCurrUser.LOCATION_ID,
        TYPE: "CUSTOM"
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          const data = this.state.listOfAssignedUser.filter(
            i => i.ID !== deleteCurrUser.ID
          );
          this.setState({
            responseUnAssignedAPI: res.MESSAGE,
            listOfAssignedUser: data
          });
          setTimeout(() => {
            this.unAssignUserByLocationClose();
          }, 2000);
        } else {
          this.setState({
            ErrorMsgUnAssign: res
          });
        }
      })
      .catch(error => console.log("Request failed:", error));
  };

  render() {
    if (sessionStorage.length == 0) {
      return <Redirect to="/login" />;
    }

    const {
      initechOrg,
      errorMsg,
      assignUserForLocation,
      brandUserList,
      assignForLocation,
      responseStatusAPI,
      ErrorMsgAssign,
      viewUserForLocation,
      listOfAssignedUser,
      unAssignUserByLocation,
      SpinnerConfigData,
      serverError,
      responseUnAssignedAPI
    } = this.state;

    // debugger;

    const MyNodeComponent = ({ node }) => {
      return (
        <div
          className="initechNode"
          onClick={e => this.refreshHierarchy(e, node)}
          nodeid={node.ID}
        >
          <span>
            <div>{node.LABEL}</div>
            <div>{node.TITLE}</div>
            {!(node.NEXT_LEVEL == -2) ? (
              <span className="drop-arrow">
                <Icon name="arrowDown" />
              </span>
            ) : null}
          </span>

          <ButtonWrap>
            {!(node.LEVEL_ID == 0) ? (
              <>
                <span
                  className="add"
                  onClick={e => this.assignUserForLocation(e, node)}
                >
                  Add
                </span>
                <span
                  className="view"
                  onClick={e => this.viewUserForLocation(e, node)}
                >
                  View
                </span>
              </>
            ) : null}
          </ButtonWrap>
        </div>
      );
    };

    return (
      <CustomSalesBody>
        <MainWrapper>
          <OrgChart tree={initechOrg} NodeComponent={MyNodeComponent} />
        </MainWrapper>

        {/* Add New Assigned User */}
        {assignUserForLocation ? (
          <Modal
            modalOpen={this.assignUserForLocation}
            onclick={this.assignUserForLocationClose}
            title="Assign User For Location"
            size="sm"
          >
            <select name="assignForLocation" onChange={this.handleChange}>
              <option value="default">Assign User ...</option>
              {brandUserList.map((list, index) => (
                <option value={list.USER_ID}>{list.USER_NAME}</option>
              ))}
            </select>
            {ErrorMsgAssign.MESSAGE != "" ? (
              <ErrorMsg>{ErrorMsgAssign.MESSAGE}</ErrorMsg>
            ) : null}
            {responseStatusAPI != "" ? (
              <ResponceSucess>{responseStatusAPI}</ResponceSucess>
            ) : null}
            <br />
            <Button size="fullwidth" onclick={this.assignUserLocation}>
              Assign
            </Button>
          </Modal>
        ) : null}

        {/* View assigned user */}
        {viewUserForLocation ? (
          <Modal
            modalOpen={this.viewUserForLocation}
            onclick={this.viewUserForLocationClose}
            title="View Assign User For Location"
            size="sm"
          >
            <table className="table">
              <tbody>
                {SpinnerConfigData == true ? (
                  <tr>
                    <td colSpan="2">
                      <SpinnerConfigData>
                        <Spinner />
                      </SpinnerConfigData>
                    </td>
                  </tr>
                ) : listOfAssignedUser.length == 0 ? (
                  <tr>
                    <td colSpan="2">
                      <NoDataFound />
                    </td>
                  </tr>
                ) : (
                  listOfAssignedUser.map((list, index) => (
                    <tr key={index}>
                      <td>{list.FULL_NAME}</td>
                      <td align="right">
                        <UnAssignedUser>
                          <span
                            onClick={e => this.unAssignUserByLocation(e, list)}
                          >
                            Un-assign user
                          </span>
                        </UnAssignedUser>
                      </td>
                    </tr>
                  ))
                )}
              </tbody>
            </table>
            {serverError.status != "" ? (
              <ErrorMsg>
                {serverError.status} {serverError.statusText}
              </ErrorMsg>
            ) : null}
          </Modal>
        ) : null}

        {/* Delete assigned user from Location */}
        {unAssignUserByLocation ? (
          <Modal
            modalOpen={this.unAssignUserByLocation}
            onclick={this.unAssignUserByLocationClose}
            title="Un-assign current User"
            size="sm"
          >
            <h4
              style={{
                fontWeight: "500",
                textAlign: "center",
                textTransform: "none"
              }}
            >
              Are you sure you want to Un-Assign this User?
            </h4>

            <ButtonConfirmation>
              <Button classname="red" onclick={this.unAssignConformd}>
                Yes
              </Button>
              <Button
                classname="blue"
                onclick={this.unAssignUserByLocationClose}
              >
                No
              </Button>
            </ButtonConfirmation>
            {ErrorMsgAssign.MESSAGE != "" ? (
              <ErrorMsg>{ErrorMsgAssign.MESSAGE}</ErrorMsg>
            ) : null}
            {responseUnAssignedAPI != "" ? (
              <ErrorMsg>{responseUnAssignedAPI}</ErrorMsg>
            ) : null}
          </Modal>
        ) : null}

        {errorMsg.status != "" ? (
          <ErrorMsg>
            {errorMsg.status} {errorMsg.statusText}
          </ErrorMsg>
        ) : null}
        {ErrorMsg.MESSAGE != "" ? (
          <ErrorMsg>{errorMsg.MESSAGE}</ErrorMsg>
        ) : null}
      </CustomSalesBody>
    );
  }
}

export default CustomSales;
