import React, { Component } from "react";
import { Auth } from "../../../../../Auth";
import { baseUrl, getBrandUsers } from "../../../../Config";
import Icon from "../../../../../Components/Icons";
import {
  CountryWrap,
  BoxWrap,
  ButtonWrap,
  CountryListSearch,
  InputIconWrap
} from "./style.jsx";

class Country extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchName: "",
      countryName: [],
      selectCountryName: []
    };
  }

  handleSearch = e => {
    /*  for country name Search */
    this.setState({
      searchName: e.target.value
    });
  };

  componentDidMount() {
    const authOrganization = userService.authOrganization();
    const authUserName = userService.authUserName();

    fetch(getBrandUsers, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_NAME: authOrganization,
        USER_ID: authUserName
      })
    })
      .then(res => res.json())
      .then(res => this.setState({ countryName: res.RESULT }))
      .catch(err => console.log(err));
  }

  selectCountryName = e => {
    if (e.currentTarget.checked) {
      const addName = this.state.selectCountryName.slice();
      addName.push(e.currentTarget.name);
      this.setState({
        selectCountryName: addName
      });
    } else {
      this.setState({
        selectCountryName: this.state.selectCountryName.filter(function(item) {
          return item !== e.currentTarget.name;
        })
      });
    }
  };

  render() {
    let countryList = this.state.countryName;
    const searchName = this.state.searchName.trim().toLowerCase();

    if (searchName.length > 0) {
      countryList = this.state.countryName.filter(function(i) {
        return i.name.toLowerCase().match(searchName);
      });
    }

    const countryName = countryList.map((list, index) => (
      <li key={list.code}>
        <label for={list.name}>
          <input
            type="checkbox"
            name={list.name}
            id={list.name}
            onClick={this.selectCountryName}
          />
          {list.name}
        </label>
      </li>
    ));

    const BoxItem = this.state.selectCountryName.map((item, index) => (
      <BoxWrap>
        <p>{item}</p>
        <ButtonWrap>
          <span className="add">+</span>
          <span className="remove">-</span>
        </ButtonWrap>
      </BoxWrap>
    ));

    return (
      <CountryWrap>
        {BoxItem}
        <CountryListSearch>
          <InputIconWrap>
            <input
              type="text"
              placeholder="Search Country Name..."
              value={this.state.searchName}
              onChange={this.handleSearch}
            />
            <Icon name="search" />
          </InputIconWrap>

          <ul>{countryName}</ul>
        </CountryListSearch>
      </CountryWrap>
    );
  }
}

export default Country;
