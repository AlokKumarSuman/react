import React, { Component } from "react";
import styled from "styled-components";
import Spinner from "../../../../Components/Spinner/index";
import OrgChart from "react-orgchart";
import "react-orgchart/index.css";

import { fetchDefaultHierarchy, userService } from "../../../Config";
import { Auth } from "../../../../Auth";

const DefaultSalesWraper = styled.div`
  position: relative;
`;
const SpinnerWraper = styled.div`
  display: flex;
  min-height: 200px;
  align-items: center;
  justify-content: center;
`;

class DefaultSales extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initechOrg: {
        ID: "0",
        LABEL: "Root",
        TITLE: "Root",
        location_id: 0,
        LEVEL_ID: "0",
        NEXT_LEVEL: "1",
        PARENT_ID: "0",
        children: []
      },
      //
      errorMsg: {}
    };
  }

  objloop = (res, node) => {
    // debugger;
    if (res.length >= 1) {
      node.children = res;
      this.forceUpdate();
    }
  };

  refreshHierarchy = (e, node) => {
    // debugger;
    e.stopPropagation();
    e.preventDefault();
    const authBrandId = userService.authBrandId();

    fetch(fetchDefaultHierarchy, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        LEVEL: node.NEXT_LEVEL,
        PARENT: node.ID
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else if (res.status == 401) {
          sessionStorage.clear();
        } else {
          return Promise.reject(
            Object.assign({}, res, {
              status: res.status,
              statusText: res.statusText
            })
          );
        }
      })
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.objloop(res.RESULT, node);
        } else {
          this.setState({
            errorMsg: res
          });
        }
      })
      .catch(error =>
        this.setState({
          errorMsg: error
        })
      );
  };

  render() {
    const { initechOrg } = this.state;
    const MyNodeComponent = ({ node }) => {
      return (
        <div
          className="initechNodeDefault"
          onClick={e => this.refreshHierarchy(e, node)}
        >
          <div>
            <span>{node.LABEL}</span>
            <span>{node.TITLE}</span>
          </div>
        </div>
      );
    };
    return (
      <DefaultSalesWraper>
        {this.state.spinner == true ? (
          <SpinnerWraper>
            <Spinner />
          </SpinnerWraper>
        ) : (
          <OrgChart tree={initechOrg} NodeComponent={MyNodeComponent} />
        )}
      </DefaultSalesWraper>
    );
  }
}

export default DefaultSales;
