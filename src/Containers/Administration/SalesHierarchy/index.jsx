import React, { Component } from "react";
import CustomSales from "./CustomSales";
import DefaultSales from "./DefaultSales";
import { listLevels, userService } from "../../Config";
import { Auth } from "../../../Auth";

import {
  HierarchyWrapper,
  HierarchyHeader,
  HierarchyBody,
  RadioLabelWrap,
  RadioLabelWrapMain,
  ContentRender
} from "./style.jsx";

class SalesHierarchy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedStatus: "defaultSalesHierarchy"
    };
  }

  clickHandler = event => {
    if (event.target.checked) {
      this.setState({
        checkedStatus: event.target.value
      });
    }
  };

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authCompanyId = userService.authCompanyId();
    const authUserName = userService.authUserName();

    fetch(listLevels, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        COMPANY_ID: authCompanyId,
        USER_ID: authUserName
      })
    })
      .then(response => response.json())
      .then(response =>
        response.RESULT.length == 0
          ? this.setState({
              checkedStatus: "defaultSalesHierarchy"
            })
          : this.setState({
              checkedStatus: "customSalesHierarchy"
            })
      )
      .catch(err => console.log(err));
  }
  render() {
    const { checkedStatus } = this.state;

    return (
      <HierarchyWrapper>
        <HierarchyHeader>
          {checkedStatus == "defaultSalesHierarchy"
            ? "Region Default Sales Hierarchy"
            : "Region Custom Sales Hierarchy"}
        </HierarchyHeader>
        <HierarchyBody>
          <RadioLabelWrapMain>
            <RadioLabelWrap>
              <input
                type="radio"
                name="manage"
                value="defaultSalesHierarchy"
                id="dsh"
                onChange={this.clickHandler}
                checked={checkedStatus === "defaultSalesHierarchy"}
                disabled={
                  checkedStatus === "defaultSalesHierarchy" ? false : true
                }
              />
              <label for="dsh">Default Sales Hierarchy</label>
            </RadioLabelWrap>
            <RadioLabelWrap>
              <input
                type="radio"
                name="manage"
                value="customSalesHierarchy"
                id="csh"
                onChange={this.clickHandler}
                checked={checkedStatus === "customSalesHierarchy"}
                disabled={
                  checkedStatus === "customSalesHierarchy" ? false : true
                }
              />
              <label for="csh">Custom Sales Hierarchy</label>
            </RadioLabelWrap>
          </RadioLabelWrapMain>

          <ContentRender>
            {checkedStatus == "defaultSalesHierarchy" ? (
              <DefaultSales />
            ) : (
              <CustomSales />
            )}
          </ContentRender>
        </HierarchyBody>
      </HierarchyWrapper>
    );
  }
}

export default SalesHierarchy;
