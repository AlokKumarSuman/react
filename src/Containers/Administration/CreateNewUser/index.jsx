import React, { Component } from "react";
import MultiSelect from "@khanacademy/react-multi-select";
import Modal from "../../../Components/Modal";
import { strings } from "./../../../Localization/index";

import UploadImage from "./UploadImage";

import {
  userService,
  registerUser,
  baseUrlApi,
  baseUrlApiPort,
  countryMaster,
  fetchBrandRoles,
  fetchBrandUserGroups
} from "../../Config";
import { Auth, LoginAuth } from "../../../Auth";
import {
  ProfileWrapper,
  ProfileHeader,
  ProfileBody,
  Form,
  LabelInputGroup,
  LabelButton,
  ErrorMsg,
  SuccessMsg
} from "./style.jsx";
import Button from "../../../Components/Button";

class CreateNewUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countryList: [],
      fetchBrandRoles: [],
      fetchBrandUserGroups: [],
      inputError: "",
      loginMessage: "",
      successMessage: "",
      adminpassword: "",
      firstname: "",
      lastname: "",
      mobile: "",
      email: "",
      organisation: "",
      country: "",
      userid: "",
      password: "",
      address: "",
      currOrganisation: "",
      ///////
      selectedBrandRoles: [],
      selectedBrandUser: [],
      uploadImageModel: false,
      imageName: "",
      imageNameByServer: ""
    };
  }

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
      inputError: "",
      loginMessage: "",
      successMessage: ""
    });
  };

  componentDidMount() {
    const authOrganization = userService.authOrganization();
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();
    // // debugger;
    Promise.all([
      fetch(countryMaster, {
        method: "POST",
        headers: Auth
      }),
      fetch(fetchBrandRoles, {
        method: "POST",
        headers: Auth,
        body: JSON.stringify({
          TENANT_ID: authTenantId,
          BRAND_ID: authBrandId,
          USER_ID: authUserName
        })
      }),
      fetch(fetchBrandUserGroups, {
        method: "POST",
        headers: Auth,
        body: JSON.stringify({
          TENANT_ID: authTenantId,
          BRAND_ID: authBrandId,
          USER_ID: authUserName
        })
      })
    ])
      .then(([res1, res2, res3]) =>
        Promise.all([res1.json(), res2.json(), res3.json()])
      )
      // .then(([res1, res2, res3]) => console.log(res1.COUNTRIES.COUNTRY))
      .then(([countryList, fetchBrandRoles, fetchBrandUserGroups]) =>
        this.setState({
          countryList: countryList.COUNTRIES.COUNTRY,
          fetchBrandRoles: fetchBrandRoles.RESULT,
          fetchBrandUserGroups: fetchBrandUserGroups.RESULT
        })
      )
      .catch(err => console.log(err));
    this.setState({
      currOrganisation: authOrganization
    });
  }

  createProfile = () => {
    // // debugger;

    const {
      adminpassword,
      firstname,
      lastname,
      mobile,
      email,
      country,
      userid,
      password,
      address,
      currOrganisation,
      selectedBrandRoles,
      selectedBrandUser,
      imageNameByServer
    } = this.state;

    const brandRoles = selectedBrandRoles.toString();
    const brandUserGroup = selectedBrandUser.toString();

    if (
      adminpassword === "" ||
      firstname === "" ||
      lastname === "" ||
      mobile === "" ||
      email === "" ||
      country === "" ||
      userid === "" ||
      password === "" ||
      address === "" ||
      brandRoles === "" ||
      brandUserGroup === ""
      // imageNameByServer === ""
    ) {
      return this.setState({
        inputError: strings.All_field_must_be_filled_out
      });
    }

    // To check a password between 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character
    var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (!password.match(decimal)) {
      return this.setState({
        inputError: strings.Validate_password
      });
    }

    const authBrandName = userService.authBrandName();
    const authTenantId = userService.authTenantId();
    const authBrandId = userService.authBrandId();
    const authTenantName = userService.authTenantName();
    const authUserName = userService.authUserName();

    // // debugger;
    fetch(registerUser, {
      method: "POST",
      headers: LoginAuth,
      body: JSON.stringify({
        ADMIN_USER_ID: authUserName,
        ADMIN_PASSWORD: adminpassword,
        TENANT_NAME: authTenantName,
        TENANT_ID: authTenantId,
        BRAND_ID: authBrandId,
        FIRST_NAME: firstname,
        LAST_NAME: lastname,
        PASSWORD: password,
        MOBILE: mobile,
        EMAIL: email,
        ORGANISATION: currOrganisation,
        COUNTRY: country,
        USER_ID: userid,
        ADDRESS: address,
        PHOTO: imageNameByServer,
        ROLES: brandRoles,
        GROUPS: brandUserGroup
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            successMessage: res.MESSAGE
          });
        } else {
          this.setState({
            loginMessage: res.MESSAGE
          });
        }
      })
      .catch(err => console.log(err));
  };

  uploadImageModelHandler = e => {
    /* for popup Open */
    this.setState({
      uploadImageModel: true,
      inputError: ""
    });
  };
  uploadImageModelHandlerClose = e => {
    /* for popup Open */
    this.setState({
      uploadImageModel: false
    });
  };

  displayImageNameHandler = (displayImageName, imageNameByServers) => {
    // // debugger;
    this.setState({
      imageName: displayImageName,
      imageNameByServer: imageNameByServers
    });
  };

  render() {
    const {
      inputError,
      loginMessage,
      successMessage,
      countryList,
      fetchBrandRoles,
      fetchBrandUserGroups,
      ////
      selectedBrandRoles,
      selectedBrandUser,
      imageNameByServer
    } = this.state;

    const listBrandRoles = fetchBrandRoles.map((list, index) => ({
      label: list.ROLE_NAME,
      value: list.ROLE_ID
    }));

    const listBrandUser = fetchBrandUserGroups.map((list, index) => ({
      label: list.GROUP_NAME,
      value: list.GROUP_ID
    }));

    const authUserName = userService.authUserName();

    // const options = [
    //   { label: "One", value: 1 },
    //   { label: "Two", value: 2 },
    //   { label: "Three", value: 3 }
    // ];

    return (
      <ProfileWrapper>
        <ProfileHeader>{strings.Create_New_User}</ProfileHeader>
        <ProfileBody>
          <Form>
            <LabelInputGroup>
              <h5>
                {strings.Admin_User_Id}
                <sup>*</sup>
              </h5>
              <input
                type="text"
                name="adminuserid"
                disabled
                value={authUserName}
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Admin_Password}
                <sup>*</sup>
              </h5>
              <input
                type="text"
                name="adminpassword"
                placeholder={strings.Type_your_password}
                onChange={this.onChange}
                autoComplete="off"
              />
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.First_Name}
                <sup>*</sup>
              </h5>
              <input
                type="text"
                name="firstname"
                placeholder={strings.Add_first_name}
                onChange={this.onChange}
                autoComplete="off"
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Last_Name}
                <sup>*</sup>
              </h5>
              <input
                type="text"
                name="lastname"
                placeholder={strings.Add_last_name}
                onChange={this.onChange}
                autoComplete="off"
              />
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.Mobile}
                <sup>*</sup>
              </h5>
              <input
                type="tel"
                name="mobile"
                pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                placeholder={strings.Mobile_number}
                onChange={this.onChange}
                autoComplete="off"
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Email}
                <sup>*</sup>
              </h5>
              <input
                type="text"
                name="email"
                placeholder={strings.Email}
                onChange={this.onChange}
                autoComplete="off"
              />
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.Organisation}
                <sup>*</sup>
              </h5>
              <input
                type="text"
                name="organisation"
                placeholder={strings.Organisation_name}
                onChange={this.onChange}
                value={this.state.currOrganisation}
                autoComplete="off"
                disabled
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.Country}
                <sup>*</sup>
              </h5>
              <select
                name="country"
                onChange={this.onChange}
                autoComplete="off"
                placeholder={strings.Country}
              >
                <option value="default">{strings.Choose_Country}</option>
                {countryList.map((list, index) => (
                  <option key={list.COUNTRY_CD} value={list.COUNTRY_NM}>
                    {list.COUNTRY_NM}
                  </option>
                ))}
              </select>
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.User_id}
                <sup>*</sup>
              </h5>
              <div className="input-group-addon-left">
                {/* <span class="input-group">
                  {this.state.currOrganisation + "/"}
                </span> */}
                <input
                  type="text"
                  name="userid"
                  placeholder={strings.User_id}
                  onChange={this.onChange}
                  autoComplete="off"
                />
              </div>
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.Password}
                <sup>*</sup>
              </h5>
              <input
                type="password"
                name="password"
                placeholder={strings.Password}
                onChange={this.onChange}
                autoComplete="off"
              />
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.Upload_Image}
                <sup>*</sup>
              </h5>
              <div className="input-group-addon-right">
                <input
                  type="text"
                  name="imageName"
                  placeholder={strings.Upload_Image}
                  value={this.state.imageName}
                  onChange={this.onChange}
                  autoComplete="off"
                  disabled
                />

                <span
                  class="input-group blue-button"
                  onClick={this.uploadImageModelHandler}
                >
                  {strings.Browse}
                </span>
              </div>
            </LabelInputGroup>

            {/* <LabelInputGroup>
              <h5>Upload Image</h5>
              <ButtonImageNameWrap>
                <Button classname="grey" onclick={this.openHandler}>
                  Browse Image
                </Button>
                <p>{imageName}</p>
              </ButtonImageNameWrap>
            </LabelInputGroup> */}

            <LabelInputGroup>
              <h5>
                {strings.User_Role}
                <sup>*</sup>
              </h5>
              <MultiSelect
                options={listBrandRoles}
                selected={selectedBrandRoles}
                onSelectedChanged={selectedBrandRoles =>
                  this.setState({ selectedBrandRoles })
                }
              />
            </LabelInputGroup>
            <LabelInputGroup>
              <h5>
                {strings.User_Group}
                <sup>*</sup>
              </h5>
              <MultiSelect
                options={listBrandUser}
                selected={selectedBrandUser}
                onSelectedChanged={selectedBrandUser =>
                  this.setState({ selectedBrandUser })
                }
              />
            </LabelInputGroup>

            <LabelInputGroup>
              <h5>
                {strings.Full_Address}
                <sup>*</sup>
              </h5>
              <textarea
                type="text"
                rows="4"
                cols="50"
                name="address"
                placeholder={strings.Full_Address}
                onChange={this.onChange}
                autoComplete="off"
              />
            </LabelInputGroup>
            <ErrorMsg>
              {inputError}
              {loginMessage}
            </ErrorMsg>
            <SuccessMsg>{successMessage}</SuccessMsg>
            <LabelButton>
              <Button size="fullwidth" onclick={this.createProfile}>
                {strings.Create_New_User}
              </Button>
            </LabelButton>
          </Form>
        </ProfileBody>
        {this.state.uploadImageModel == true ? (
          <Modal
            modalOpen={this.state.uploadImageModel}
            onclick={this.uploadImageModelHandlerClose}
            title={strings.Upload_Image}
            size="sm"
          >
            <UploadImage
              onclickClose={this.uploadImageModelHandlerClose}
              displayImageName={this.displayImageNameHandler}
            />
          </Modal>
        ) : null}
      </ProfileWrapper>
    );
  }
}

export default CreateNewUser;
