import styled from "styled-components";

export const ProfileWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  // padding: 2rem 5rem;
  font-family: "Roboto", sans-serif;
  font-size: initial;
  .dropdown-heading {
    font-size: 14px;
  }
  .dropdown-content {
    font-size: 12px;

    ::-webkit-scrollbar {
      width: 5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555;
    }
  }
  input[name="userid"] {
    width: auto;
    flex-grow: 9;
  }
`;

export const ProfileHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1.5rem 2rem;
  width: 100%;
  background: #2dc3e8;
  font-family: "Gotham";
  font-weight: 500;
  font-size: 14px;
  color: #fff;
`;

export const ProfileBody = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 3rem 4rem;
  background-color: white;

  @media (max-width: 568px) {
    padding: 2rem 1rem;
  }
`;

export const Form = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  align-self: center;
  margin: 0 auto;
  padding: 30px 20px 10px 20px;
  background: #ccc;
}

@media (max-width: 768px) {
  width: 80%;
}

@media (max-width: 480px) {
  width: 100%;
}

`;

export const LabelInputGroup = styled.div`
  margin-bottom: 20px;
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 8px;
    font-weight: 600;
    color: #333;
    font-size: 14px;
    font-family: sans-serif;
    letter-spacing: 0.5px;
    sup {
      color: red;
    }
  }
`;
export const LabelButton = styled.div`
  padding-top: 2rem;
  .button {
    color: #fff;
  }
`;

export const ErrorMsg = styled.div`
  color: red;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 0 auto;
  text-align: center;
`;

export const SuccessMsg = styled.div`
  color: green;
  margin-top: 10px;
  display: -ms-flexbox;
  font-size: 12px;
  margin: 0 auto;
  text-align: center;
`;
export const AutoCompleteSugession = styled.div`
  display: block;
  width: 48%;
  position: relative;
  input {
    width: 100%;
  }
  ul {
    position: absolute;
    background: #fff;
    width: 100%;
    top: 32px;
    left: 0;
    border: solid 1px #c5c5c5;
  }
  li {
    padding: 3px 6px;
    border-bottom: solid 1px #c5c5c5;
  }
  li:last-child {
    border-bottom: none;
  }
`;
