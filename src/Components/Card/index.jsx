import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const CardComponent = styled.div`
  height: auto;
  width: auto;
  background-color: #ffffff;
  font-size: initial;
  padding: 1rem;
  margin: 0;
  margin-bottom: 1rem;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  &.card {
    color: blue;
  }
`;

class Card extends React.Component {
  render() {
    return (
      <CardComponent className={this.props.classname}>
        {this.props.children}
      </CardComponent>
    );
  }
}
Card.propTypes = {
  children: PropTypes.any,
  classname: PropTypes.string
};
export default Card;
