import React from "react";
import styled from "styled-components";

const BadgeComponent = styled.div`
  height: 1.8rem;
  min-width: 20px;
  background: #e82d2d;
  border-radius: 50%;
  color: white;
  font-size: 1.2rem;
  padding: 0.3rem;
  text-align: center;
  position: absolute;
  line-height: 1.2rem;
  top: 0;
  right: 0;
`;
export default class Badge extends React.Component {
  render() {
    return <BadgeComponent>{this.props.children}</BadgeComponent>;
  }
}
