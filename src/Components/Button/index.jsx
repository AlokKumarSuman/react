import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const StyledButton = styled.div`
  transition: 0.5s all;
  font-family: "Roboto", sans-serif;
  font-weight: 300;
  letter-spacing: 0.2rem;
  cursor: pointer;
  width: ${props => (props.size === "fullwidth" ? "100%" : "90px")};
  font-size: 1.4rem;
  text-align: center;
  padding: 1rem 0.8rem;
  text-transform: uppercase;
  color: ${props => (props.type === "primary" ? "#ffffff" : "#000000")};
  background: ${props =>
    props.type === "primary" ? "#63b9e1" : "transparent"};
  border: 0.1rem solid
    ${props => (props.type === "secondary" ? "#63b9e1" : "transparent")};

  background: ${props =>
    props.disabled === true ? "gray !important" : "#63b9e1"};
  pointer-events: ${props => (props.disabled === true ? "none" : "initial")};
  cursor: ${props =>
    props.disabled === true ? "no-drop !important" : "pointer"};

  &:hover {
    background: ${props => (props.type === "primary" ? "#55a9d0" : "#63b9e1")};
  }
  &.green {
    background: #53a93f;
    :hover {
      background: #65b951;
    }
  }
  &.red {
    background: #cb0202;
  }
  &.grey {
    background: #999;
  }
  &.blue {
    background: #427fed;
    :hover {
      background: #2962c8;
    }
  }
`;

const Button = props => {
  const { classname, type, children, onclick, disabled, size } = props;
  return (
    <StyledButton
      className={classname}
      type={type}
      onClick={onclick}
      disabled={disabled}
      size={size}
    >
      {children}
    </StyledButton>
  );
};

Button.defaultProps = {
  type: "primary"
};

Button.propTypes = {
  classname: PropTypes.string,
  type: PropTypes.string.isRequired,
  fullwidth: PropTypes.string,
  size: PropTypes.string,
  children: PropTypes.node,
  onclick: PropTypes.func,
  disabled: PropTypes.any
};

export default Button;
