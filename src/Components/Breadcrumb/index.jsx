import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Route, Link } from "react-router-dom";

const BreadCrumbComponent = styled.div`
  height: auto;
  width: auto;
  background-color: #ffffff;
  font-size: initial;
  padding: 2rem 5rem;
  margin: 0;

  ul li {
    display: inline-block;
    margin: 0 2px;
    text-transform: capitalize;
    :first-child {
      display: none;
    }
    a {
      color: #333;
      text-decoration: none;
      cursor-pointer: none;
      font-family: "Gotham";
      font-weight: 500;
      font-size: 13px;
    }

    small {
      line-height: 10px;
      font-size: 12px;
      margin: 0px 5px;
    }
  }

  ul li.active a {
    color: #cb0202;
    pointer-events: none;
  }

  @media (max-width: 568px) {
    padding: 2rem 1rem;
  }
`;

const pathBasename = urlPath =>
  urlPath
    .split("/")
    .filter(p => p)
    .pop();
// eslint-disable-next-line
const BreadcrumbsItem = ({ match }) => (
  <React.Fragment>
    <li>
      <small>/</small>
    </li>
    <li className={match.isExact && "active"}>
      <Link to={match.url || ""}>{pathBasename(match.url)}</Link>
    </li>

    <Route path={`${match.url}/:path`} component={BreadcrumbsItem} />
  </React.Fragment>
);
const Breadcrumb = props => (
  <BreadCrumbComponent className={props.className}>
    <ul>
      <Route path="/:path" component={BreadcrumbsItem} />
    </ul>
  </BreadCrumbComponent>
);

Breadcrumb.propTypes = {
  match: PropTypes.object,
  className: PropTypes.string
};

export default Breadcrumb;
