import styled from "styled-components";

export const CustomSakMultiselect = styled.div`
  & > div:first-child {
    flex-direction: row;
    margin-bottom: 1.2rem;
    // width: 49%;
    width: 100%;
  }
  .multiSelectWrap > div {
    width: 49%;
    // width: 100%;
    margin-bottom: 1.2rem;
    &:nth-child(odd) {
      float: left;
    }
    &:nth-child(even) {
      float: right;
    }
  }
`;

export const LabelInputGroup = styled.div`
  h5 {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 600;
    color: #333;
    font-size: 1.4rem;
    font-family: "Gotham";
    font-weight: 500;
    letter-spacing: 1px;
  }

  h4 {
    border: 1px solid #ccc;
    padding: 7px 10px;
    border-radius: 4px;
    width: 364px;
    color: #939393;
  }

  ul.productsDefine {
    display: ${props => (props.productsDD === false ? "none" : "block")};
    max-height: 20rem;
    overflow-y: auto;
    position: absolute;
    background: #eaeaea;
    width: 100%;
    border: solid 1px #c5c5c5;
    padding: 4px;
    border-top-right-radius: 2px;
    border-top-left-radius: 2px;
    z-index: 9;

    li {
      display: block;
      padding: 5px 0px;

      input[type="text"] {
        width: 100%;
        height: 30px;
      }

      input[type="checkbox"] {
        margin-right: 5px;
        width: 14px;
        height: 14px;
      }

      p {
        display: inline-block;
      }
    }
  }
`;
