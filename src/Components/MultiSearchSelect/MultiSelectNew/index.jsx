import React, { Component, PureComponent } from "react";
import PropTypes from "prop-types";
import "./styles.css";
import Spinner from "./../../Spinner/index";

export class MultiSearchSelectLevel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: [],
      disabled: "",
      spinner: "",
      checkedLocationId: [],
      checkedSelectedAll: false,
      dropDownValue: []
    };
  }

  componentWillMount() {
    // this.setState({
    //   dropDownValue: this.props.options,
    //   disabled: this.props.disabled,
    //   spinner: this.props.spinner
    // });
  }

  componentWillReceiveProps(props, state) {
    this.setState({
      dropDownValue: props.options,
      disabled: props.disabled,
      spinner: props.spinner
    });
  }

  removeChip = value => {
    this.checkBox(value, false);
  };

  checkBox = (value, data, condition) => {
    let checkedValue = this.state.checked;
    let checkedLocationIdValue = this.state.checkedLocationId;
    let checkedOrder = data.ORDER;

    let checkedSelectAll = false;
    let checkedLength = this.state.checked.length + 1;
    if (condition) {
      checkedValue.push(value);
      checkedLocationIdValue.push(data.LOCATION_ID);

      checkedLength == this.state.dropDownValue.length
        ? (checkedSelectAll = true)
        : "";
    } else {
      let indexValue = checkedValue.indexOf(value);
      checkedValue.splice(indexValue, 1);
      //
      let indexID = checkedLocationIdValue.indexOf(data.LOCATION_ID);
      checkedLocationIdValue.splice(indexID, 1);
      //
      checkedSelectAll = false;
    }

    this.setState(
      {
        checked: checkedValue,
        checkedLocationId: checkedLocationIdValue,
        checkedSelectedAll: checkedSelectAll
      },
      () => {
        if (checkedValue.length != 0) {
          this.props.onSelectOptions(
            this.state.checked,
            this.state.checkedLocationId,
            checkedOrder
          );
        }
      }
    );
  };

  selectAll = condition => {
    // // debugger;
    let checkedValue = [];
    let checkedLocationIdValue = [];
    let dropDownValueAll = this.state.dropDownValue;
    if (dropDownValueAll.length == 0) {
      return;
    }
    let checkedOrder = dropDownValueAll[0].ORDER;
    let checkedSelectAll = false;

    for (var i = 0; i < dropDownValueAll.length; i++) {
      checkedValue.push(dropDownValueAll[i].LOCATION_NAME);
      checkedLocationIdValue.push(dropDownValueAll[i].LOCATION_ID);
    }
    if (condition) {
      //
      checkedSelectAll = true;
    } else {
      checkedValue = [];
      checkedLocationIdValue = [];

      checkedSelectAll = false;
    }

    this.setState(
      {
        checked: checkedValue,
        checkedLocationId: checkedLocationIdValue,
        checkedSelectedAll: checkedSelectAll
      },
      () => {
        this.props.onSelectOptions(
          this.state.checked,
          this.state.checkedLocationId,
          checkedOrder
        );
      }
    );
  };

  searchFun = e => {
    if (e.target.value.length !== 0) {
      let enteredValue = e.target.value.toLowerCase();
      let presentValue = this.props.options.filter(function(data) {
        return data.LOCATION_NAME.toLowerCase().indexOf(enteredValue) > -1;
      });
      this.setState({ dropDownValue: presentValue });
    } else {
      this.setState({ dropDownValue: this.props.options });
    }
  };

  returnChip = () => {
    // // debugger;
    const chip = this.state.checked
      ? this.state.checked.length > 2
        ? this.state.checked.length + " items selected"
        : this.state.checked.map((data, index) => (
            // eslint-disable-next-line
            <div className="chip-body" key={index}>
              <p className="chip-text">{data}</p>
              {/* <button
                className="chip-close"
                onClick={e => this.removeChip(data)}
              >
                &times;
              </button> */}
            </div>
          ))
      : [];
    return chip;
  };

  returnList = () => {
    // // debugger;
    return (
      <>
        <label className="container all">
          Select All
          <input
            type="checkbox"
            value="allSelected"
            onChange={e => this.selectAll(e.target.checked)}
            checked={this.state.checkedSelectedAll ? true : false}
          />
          <span className="checkmark"></span>
        </label>
        {this.state.dropDownValue
          ? this.state.dropDownValue.map((
              data,
              index // eslint-disable-next-line
            ) => (
              <label className="container" key={index}>
                {data.LOCATION_NAME}
                <input
                  type="checkbox"
                  value={data.LOCATION_NAME}
                  id={data.ORDER}
                  onChange={e =>
                    this.checkBox(
                      e.target.value,
                      // e.target.id,
                      data,
                      e.target.checked
                    )
                  }
                  checked={
                    this.state.checked.includes(data.LOCATION_NAME)
                      ? true
                      : false
                  }
                />
                <span className="checkmark"></span>
              </label>
            ))
          : null}
      </>
    );
  };

  render() {
    // // debugger;
    return (
      <div className="multiSelect">
        <div
          className="chip"
          onClick={this.props.onclick}
          disabled={this.state.disabled == "true" ? true : false}
        >
          {this.state.spinner == "true" ? <Spinner /> : null}

          {this.returnChip()}
        </div>

        <div className="search-result">
          <input
            type="text"
            name="Search"
            placeholder="Search Data"
            className="input-box"
            onChange={e => this.searchFun(e)}
          />
          <div className="list-result">{this.returnList()}</div>
        </div>
      </div>
    );
  }
}

MultiSearchSelectLevel.defaultProps = {
  options: []
};

/** define proptypes including fields which is required */
MultiSearchSelectLevel.prototypes = {
  options: PropTypes.array.isRequired
  // onSelectOptions: PropTypes.func
};

export default MultiSearchSelectLevel;
