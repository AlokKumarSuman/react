import React, { Component, PureComponent } from "react";
import { Auth } from "../../Auth";
import {
  userService,
  fetchLevelsForDropDown,
  fetchLocationsForDropDown
} from "../../Containers/Config";
import MultiSearchSelectLevelNew from "./MultiSelectNew";

import { CustomSakMultiselect, LabelInputGroup } from "./styles";

export class MultiSearchSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      levelValues: [],
      //
      locationNameCreated: [],
      locationNameCreatedDisabled: false
    };
  }

  componentDidMount() {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    fetch(fetchLevelsForDropDown, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        ROLE: "BRAND-ADMIN",
        LOC_TYPE: "CUSTOM"
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res))
      .then(res =>
        this.setState({
          levelValues: res.RESULT
        })
      )
      .catch(err => console.log(err));
  }

  // componentWillMount() {
  //   this.setState({
  //     dropDownValue: this.props.options
  //   });
  // }

  hide = e => {
    for (
      var i = 0;
      i < document.getElementsByClassName("multiSelectWrap").length;
      i++
    ) {
      for (
        var j = 0;
        j <
        document.getElementsByClassName("multiSelectWrap")[i].children.length;
        j++
      ) {
        if (
          document
            .getElementsByClassName("multiSelectWrap")
            [i].children[j].lastChild.classList.contains("open")
        ) {
          document
            .getElementsByClassName("multiSelectWrap")
            [i].children[j].lastChild.classList.remove("open");
        }
      }
    }
    e.target.parentElement.classList.add("open");
  };

  openCloseDrop = e => {
    e.preventDefault();
    if (e.target.parentElement.classList.contains("open")) {
      e.target.parentElement.classList.remove("open");
    } else {
      this.hide(e);
    }
  };

  levelChangeHandler = event => {
    const optionValue = event.target.value;

    let options = event.target.options;
    const optionId = options[options.selectedIndex].id;

    let assignValue = [];
    assignValue = this.state.levelValues.slice(optionId - 1);

    let locationNameCreatedPush = [];

    for (var i = 0; i < assignValue.length; i++) {
      if (i === 0) {
        locationNameCreatedPush.push({
          levelName: assignValue[i].LEVEL_NAME,
          order: assignValue[i].ORDER,
          locationName: assignValue[i].LOCATION_NAME,
          disabled: "true",
          spinner: "true"
        });
      } else {
        locationNameCreatedPush.push({
          levelName: assignValue[i].LEVEL_NAME,
          order: assignValue[i].ORDER,
          locationName: assignValue[i].LOCATION_NAME,
          disabled: "true",
          spinner: "true"
        });
      }
    }
    this.setState(
      {
        locationNameCreated: locationNameCreatedPush,
        locationNameCreatedDisabled: true
      },
      () => {
        this.fetchLocations(optionId, optionValue);
      }
    );
  };
  //////////
  fetchLocations = (orderId, optionName) => {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    const { locationNameCreated, levelValues } = this.state;
    var nextLevelShow = locationNameCreated;

    var index = nextLevelShow.findIndex(e => e.locationName == optionName);
    nextLevelShow[index].disabled = "false";
    nextLevelShow[index].spinner = "false";

    const fetchLocationName = optionName + "Values";
    fetch(fetchLocationsForDropDown, {
      method: "POST",
      headers: Auth,
      body: JSON.stringify({
        BRAND_ID: authBrandId,
        USER_ID: authUserName,
        TENANT_ID: authTenantId,
        ORDER: orderId,
        LOC_TYPE: "CUSTOM",
        LOC_ID: [],
        ROLES: "BRAND-ADMIN"
      })
    })
      .then(res => res.json())
      // .then(res => console.log(res))
      .then(res => {
        if (res.STATUS == "SUCCESS") {
          this.setState({
            [fetchLocationName]: res.RESULT,
            locationNameCreated: nextLevelShow
          });
        }
      })
      .catch(err => console.log(err));
  };

  resultSelected = (values, id, order) => {
    const authBrandId = userService.authBrandId();
    const authUserName = userService.authUserName();
    const authTenantId = userService.authTenantId();

    let nextOrder = order + 1;
    const { locationNameCreated, levelValues } = this.state;

    // if(values.length)
    var nextLevelShow = locationNameCreated;
    var nextLevelShowTrue = locationNameCreated;
    let optionName = nextLevelShow.find(element => {
      return element.order === nextOrder;
    });

    if (optionName === undefined) {
      const name = nextLevelShow[nextLevelShow.length - 1].locationName;
      let _state = name + "Values";
      const postLocations = this.state[_state];
      const sendlocationsId = id;
      // console.log(postLocations);
      // return;
      // return this.props.recievedLocations(postLocations);
      return this.props.recievedLocationsId(sendlocationsId);
    }
    // if selected SUCESS to fetch new location data then disabled & spiner set to false on next textbox
    var index = nextLevelShow.findIndex(e => e.order == nextOrder);
    nextLevelShow[index].disabled = "false";
    nextLevelShow[index].spinner = "false";
    const fetchLocationName = nextLevelShow[index].levelName + "Values";

    if (id.length != 0) {
      fetch(fetchLocationsForDropDown, {
        method: "POST",
        headers: Auth,
        body: JSON.stringify({
          BRAND_ID: authBrandId,
          USER_ID: authUserName,
          TENANT_ID: authTenantId,
          ORDER: order,
          LOC_TYPE: "CUSTOM",
          LOC_ID: id,
          ROLES: "BRAND-ADMIN"
        })
      })
        .then(res => res.json())
        // .then(res => console.log(res))
        .then(res => {
          if (res.STATUS == "SUCCESS") {
            this.setState({
              [fetchLocationName]: res.RESULT,
              locationNameCreated: nextLevelShow
            });
          }
        })
        .catch(err => console.log(err));
    } else {
      this.setState({
        [fetchLocationName]: []
      });
    }
  };

  getDataValues(name) {
    let _state = name + "Values";
    return this.state[_state];
  }

  multiSelectLevels = () => {
    return this.state.locationNameCreated.map((data, index) => (
      <div key={index}>
        <h5>
          Select {data.levelName} <span className="error">*</span>
        </h5>
        <MultiSearchSelectLevelNew
          options={this.getDataValues(data.levelName)}
          disabled={data.disabled}
          spinner={data.spinner}
          onSelectOptions={this.resultSelected}
          onclick={this.openCloseDrop}
        />
      </div>
    ));
  };

  render() {
    const { locationNameCreated, locationNameCreatedDisabled } = this.state;

    return (
      <CustomSakMultiselect>
        <LabelInputGroup>
          <h5>
            Select Levels <span className="error">*</span>
          </h5>
          {locationNameCreatedDisabled ? (
            <select
              onChange={this.levelChangeHandler}
              selectedIndex="1"
              disabled
            >
              <option value="default">Choose levels...</option>
              {this.state.levelValues.map((data, index) => (
                <option key={index} value={data.LEVEL_NAME} id={data.ORDER}>
                  {data.LOCATION_NAME}
                </option>
              ))}
            </select>
          ) : (
            <select onChange={this.levelChangeHandler} selectedIndex="1">
              <option value="default">Choose levels...</option>
              {this.state.levelValues.map((data, index) => (
                <option key={index} value={data.LEVEL_NAME} id={data.ORDER}>
                  {data.LOCATION_NAME}
                </option>
              ))}
            </select>
          )}
        </LabelInputGroup>
        <div className="multiSelectWrap">{this.multiSelectLevels()}</div>
      </CustomSakMultiselect>
    );
  }
}

export default MultiSearchSelect;
