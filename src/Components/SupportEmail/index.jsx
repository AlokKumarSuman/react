import React, { Component } from "react";
import { strings } from "../../Localization";
import styled from "styled-components";
import Icon from "../Icons";

const SupportStyled = styled.div`
  font-size: 1.3rem;
  font-family: "Roboto", sans-serif;

  h5 {
    margin-bottom: 0.5rem;
  }
`;

const MailIconWrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  .icon {
    margin-right: 1rem;
  }

  a {
    color: #333;
    cursor: pointer;
    text-decoration: none;
    outline: none;
  }
`;

class SupportEmail extends Component {
  state = {};
  render() {
    return (
      <SupportStyled>
        <h5>{strings.For_Support}</h5>
        <MailIconWrap>
          <Icon name="sms" />
          <a href="mailto: support@kpfactors.com">support@kpfactors.com</a>
        </MailIconWrap>
      </SupportStyled>
    );
  }
}

export default SupportEmail;
