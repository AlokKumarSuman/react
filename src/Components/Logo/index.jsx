import React from "react";
import styled from "styled-components";
import logo from "../../Assets/Images/logo.png";
import { BrowserRouter as Router, Link } from "react-router-dom";

const StyledLogo = styled.div`
  a img {
    display: block;
    width: 96px;
    box-shadow: 0px 0px 2px 2px rgba(0, 0, 0, 0.1);
  }
`;

const Logo = () => {
  return (
    <StyledLogo>
      <Link to="/">
        <img src={logo} alt="logo" />
      </Link>
    </StyledLogo>
  );
};

export default Logo;
