import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import Icon from "../Icons";

export const ModalWrapperMain = styled.div`
  display: ${props => (props.openModal === false ? "none" : "block")};
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  z-index: 991;
`;
// export const ModalBGWrapper = styled.div`
// display: ${props => (props.openModal === false ? "none" : "block")};
// position: fixed;
// top: 0;
// left: 0;
// width: 100%;
// height: 100%;
// overflow: auto;
// z-index: 991;
// background: rgba(0, 0, 0, 0.5);
// `;
export const ModalBGWrapper = styled.div`
  display: block;
  position: relative;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
`;

const ModalWrapper = styled.div`
  display: ${props => (props.openModal === false ? "none" : "block")};
  max-width: 800px;
  width: 90%;
  height: auto;
  background: #fff;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  box-shadow: 0 20px 30px rgba(0, 0, 0, 0.2);
  border-radius: 0.8rem;
  z-index: 992;
  .icon {
    position: absolute;
    right: 10px;
    top: 10px;
    cursor: pointer;
  }
  &.sm {
    max-width: 420px;
  }
  &.md {
    max-width: 540px;
  }
  &.lg {
    max-width: 660px;
  }
`;

const ModalHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 1.4rem;
  font-size: 16px;
  background-color: #2dc3e8;
  width: 100%;
  color: #fff;
  position: relative;
  border-top-left-radius: 0.8rem;
  border-top-right-radius: 0.8rem;
`;
const ModalBody = styled.div`
  padding: 20px;
  border-bottom-left-radius: 0.8rem;
  border-bottom-right-radius: 0.8rem;
  max-height: 400px;
  overflow-y: auto;
`;

class Modal extends React.Component {
  render() {
    return (
      <React.Fragment>
        {this.props.modalOpen ? (
          <React.Fragment>
            <ModalWrapperMain className="modalWrapperMain">
              {this.props.onclick ? (
                <ModalBGWrapper onClick={this.props.onclick} />
              ) : this.props.modelClose ? (
                <ModalBGWrapper onClick={this.props.modelClose} />
              ) : (
                ""
              )}

              <ModalWrapper className={this.props.size}>
                <ModalHeader>
                  {this.props.title}

                  {this.props.onclick ? (
                    <Icon
                      name="cancel"
                      color="white"
                      onclick={this.props.onclick}
                    />
                  ) : this.props.modelClose ? (
                    <Icon
                      name="cancel"
                      color="white"
                      onclick={this.props.modelClose}
                    />
                  ) : (
                    ""
                  )}
                </ModalHeader>
                <ModalBody>{this.props.children}</ModalBody>
              </ModalWrapper>
            </ModalWrapperMain>
          </React.Fragment>
        ) : null}
      </React.Fragment>
    );
  }
}

Modal.propTypes = {
  modalOpen: PropTypes.any,
  title: PropTypes.string,
  onclick: PropTypes.func,
  children: PropTypes.object,
  size: PropTypes.any
};

export default Modal;
