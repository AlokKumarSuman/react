import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import AvatarThumb from "../../Assets/Images/img_avatar.png";

const StyledDiv = styled.div`
  width: 50px;
  height: 50px;
  cursor: pointer;
  position: relative;
  z-index: 1;

  img {
    width: 100%;
    display: block;
    border-radius: 50%;
  }
`;

const Avatar = props => {
  return (
    <StyledDiv onMouseEnter={props.onmouseenter}>
      <img src={AvatarThumb} alt="avatar" />
    </StyledDiv>
  );
};

Avatar.propTypes = {
  onmouseenter: PropTypes.func
};

export default Avatar;
