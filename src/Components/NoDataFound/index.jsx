import React from "react";
import styled from "styled-components";

const NotFound = styled.div`
  color: red;
  text-align: center;
`;

const NoDataFound = () => {
  return <NotFound>No Data Found</NotFound>;
};

export default NoDataFound;
