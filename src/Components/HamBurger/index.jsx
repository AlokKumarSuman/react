import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const OuterDiv = styled.div`
  width: 100%;
  height: 56px;
  background: #ddd;
  position: sticky;
  top: 0;
  left: 0;
  z-index: 99;
`;

const StyledMenu = styled.div`
  display: inline-block;
  cursor: pointer;
  position: absolute;
  top: 15px;
  left: 15px;

  span {
    width: 30px;
    height: 2px;
    background-color: #191818;
    transition: 0.4s;
    display: block;
  }

  span:nth-child(1) {
    transform: ${props =>
      props.lineChange === true
        ? "rotate(-45deg) translate(-9px, 6px)"
        : "initial"};

    @media (max-width: 1023px) {
      transform: ${props =>
        props.lineChange === false
          ? "rotate(-45deg) translate(-9px, 6px)"
          : "initial"};
    }
  }
  span:nth-child(2) {
    margin: ${props => (props.lineChange === true ? "8.7px 0" : "6px 0")};
    opacity: ${props => (props.lineChange === true ? "0" : "1")};

    @media (max-width: 1023px) {
      margin: ${props => (props.lineChange === false ? "8.7px 0" : "6px 0")};
      opacity: ${props => (props.lineChange === false ? "0" : "1")};
    }
  }
  span:nth-child(3) {
    transform: ${props =>
      props.lineChange === true
        ? "rotate(45deg) translate(-8px, -8px)"
        : "initial"};

    @media (max-width: 1023px) {
      transform: ${props =>
        props.lineChange === false
          ? "rotate(45deg) translate(-8px, -8px)"
          : "initial"};
    }
  }
`;

class HamBurger extends Component {
  render() {
    return (
      <OuterDiv>
        <StyledMenu lineChange={this.props.change} onClick={this.props.onclick}>
          <span />
          <span />
          <span />
        </StyledMenu>
      </OuterDiv>
    );
  }
}
HamBurger.propTypes = {
  onclick: PropTypes.func,
  change: PropTypes.node
};
export default HamBurger;
