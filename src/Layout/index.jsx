import React, { Component } from "react";
import PropTypes from "prop-types";
import { Header } from "../Containers/Login/Header";
import Footer from "../Containers/Footer";
import Sidebar from "../Containers/Sidebar";
import Breadcrumb from "../Components/Breadcrumb";
import { BrowserRouter as Router, NavLink, Redirect } from "react-router-dom";
import { LayoutWrapper, MainArea, Wrapper, LogoutWarn, ChildrenArea } from "./style.jsx";

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = { setSidebarInnerHeight: 0 };
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }

  updateDimensions() {
    const getInnerHeight = window.innerHeight;
    const setInnerHeight = getInnerHeight - 152;
    if (window.innerHeight >= setInnerHeight) {
      this.setState({
        setSidebarInnerHeight: setInnerHeight
      });
    }
  }

  setLanguage = event => {
    this.props.languageProp(event);
  };

  render() {
    return (
      <LayoutWrapper>
        <Header languageProp={this.setLanguage} />
        <MainArea>
          <Sidebar sidebarHeight={this.state.setSidebarInnerHeight} />
          <Wrapper
            style={{
              height: `${this.state.setSidebarInnerHeight}px`,
              overflow: "auto"
            }}
          >
            <LogoutWarn>{this.props.logoutWarn}</LogoutWarn>
            <Breadcrumb />

            <ChildrenArea>{this.props.children}</ChildrenArea>
          </Wrapper>
        </MainArea>
        <Footer />
      </LayoutWrapper>
    );
  }
}
Layout.propTypes = {
  children: PropTypes.node,
  logoutWarn: PropTypes.any
};
export default Layout;
