import styled from "styled-components";

export const LayoutWrapper = styled.div`
  position: relative;
`;

export const SidebarArea = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  font-size: 1.4rem;
  transition: 0.5s all;

  ul {
    width: 100%;
  }

  li {
    border-top: 1px solid #d9d9d9;
  }

  a {
    padding: 0.8rem;
    display: flex;
    flex-direction: row;
    align-items: center;
    text-decoration: none;
    cursor: pointer;
    color: #333;
    font-family: "Roboto", sans-serif;
  }

  li span {
    width: 20rem;
    margin-left: 2rem;
    transition: 0.5s all;
  }
`;

export const MainArea = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  background: #e4e5e6;
  position: absolute;
  margin-top: 70.6px;
  width: 100%;

  @media (max-width: 480px) {
    margin-top: 114.6px;
  }
`;

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const LogoutWarn = styled.div`
  display: flex;
  align-self: center;
  color: red;
  font-size: 13px;
`;

export const ChildrenArea = styled.div`
  padding: 2rem;

  @media (max-width: 568px) {
    padding: 1rem;
  }
`;
